<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateHfTagsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hf_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100)->default('')->comment('标签名称');
            $table->unsignedTinyInteger('time_range_status')->default('0')->comment('时间区间0无时间区间限制,1固定时间区间');
            $table->timestamp('start_time')->comment('开始时间');
            $table->timestamp('end_time')->comment('结束时间');
            $table->unsignedInteger('deal_ok')->default(0)->comment('累计成功交易');
            $table->decimal('total_amount',15,2)->default(0)->comment('累计购买金额');
            $table->unsignedInteger('no_consume')->default(0)->comment('累计几天未消费');
            $table->decimal('unit_price',15,2)->default(0)->comment('客单价');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hf_tags');
    }
}
