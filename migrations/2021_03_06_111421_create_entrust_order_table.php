<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateEntrustOrderTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entrust_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id')->index();
            $table->char('phone',11)->default('')->index()->comment('店员登录手机号');
            $table->unsignedDecimal('amount',10,2)->default(0)->comment('合计');
            $table->unsignedDecimal('dis_amount',10,2)->default(0)->comment('优惠后合计');
            $table->unsignedDecimal('discount',10,2)->default(0)->comment('优惠');
            $table->unsignedTinyInteger('status')->default(0)->comment('0未支付，1已支付，支付后不展示');
            $table->text('goods')->comment('商品列表');
            $table->unsignedTinyInteger('order_source')->default(0)->comment('0及时达，1次日达');
            $table->text('share')->comment('分享信息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entrust_order');
    }
}
