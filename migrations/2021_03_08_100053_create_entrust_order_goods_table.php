<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateEntrustOrderGoodsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entrust_order_goods', function (Blueprint $table) {
            $table->unsignedBigInteger('entrust_order_id')->index();
            $table->unsignedBigInteger('goods_id')->index();
            $table->string('title')->default('');
            $table->string('logo')->default('');
            $table->string('spec')->default('');
            $table->unsignedSmallInteger('number')->default(0);
            $table->unsignedDecimal('unit_price')->default(0)->comment('原单价');
            $table->unsignedDecimal('dis_unit_price')->default(0)->comment('优惠后单价');
            $table->unsignedDecimal('subtotal')->default(0)->comment('合计');
            $table->unsignedDecimal('dis_subtotal')->default(0)->comment('优惠后合计');
            $table->string('kz_type_id')->default('')->comment('客至品类ID');
            $table->string('kz_goods_id')->default('')->comment('客至商品编码');
            $table->string('kz_self_num')->default('')->comment('客至唯一货架码');
            $table->unsignedTinyInteger('scant_id')->default(0)->comment('0标品,1非标品');
            $table->unsignedInteger('ratio')->default(0)->comment('商品转换系数');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entrust_order_goods');
    }
}
