<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateHfGroupOrderTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hf_group_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no',160)->default('')->comment('订单号');
            $table->string('group_num',100)->default('')->comment('团号');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hf_group_order');
    }
}
