<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateHfPayLogTable extends Migration
{
    protected $connection = 'default3';
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hf_pay_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event',100)->default('')->comment('事件');
            $table->text('params')->comment('参数');
            $table->text('response')->comment('响应');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hf_pay_log');
    }
}
