<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET'], '/v1/heartbeat', 'App\Common\Controller\PingController@heartbeat');
//Router::addRoute(['GET'], '/v1/userIndex', 'App\User\Controller\UserController@index');
