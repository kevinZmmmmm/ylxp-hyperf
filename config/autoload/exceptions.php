<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'handler' => [
        'http' => [
            Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler::class,
            App\Common\Exception\Handler\AppExceptionHandler::class,
            App\Common\Exception\Handler\JwtExceptionHandler::class,
	        \Hyperf\ExceptionHandler\Handler\WhoopsExceptionHandler::class,
            //\Hyperf\Validation\ValidationExceptionHandler::class,
            App\Common\Exception\Handler\RefundExceptionHandler::class,
        ],
    ],
];
