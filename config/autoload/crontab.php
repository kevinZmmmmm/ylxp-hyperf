<?php
return [
    // 是否开启定时任务
    'enable' => env('ENABLE_CRONTAB', true),
    // 自动成团时间 秒
    'auto' => env('AUTO_CRONTAB', 6*3600)
];