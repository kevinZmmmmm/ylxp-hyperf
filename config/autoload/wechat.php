<?php
return [
    'app_id'    => env('APP_ID', 'wx43a215392b89d41f'),
    'secret'    => env('SECRET', '29237f5540ee41145a5fc15b70bf12c1'),
    'token'     => 'easywechat',
    'log' => [
        'level' => 'debug',
        'file'  => '/tmp/easywechat.log',
    ],
    'miniprogram' => [
        'state' => env('MINI_PROGRAM_STATE', 'formal'),
    ],
    'official_account' => [
        'app_id' => env('OFFICIAL_ACCOUNT_OPENID', 'wx9cd65c0a99acd179'),
        'secret' => env('OFFICIAL_ACCOUNT_SECRET', 'e7a0b38bef70f826efaea1e579bb2562'),
    ],
];
