<?php

declare(strict_types=1);

return [
    'default' => [
        'driver' => Hyperf\AsyncQueue\Driver\RedisDriver::class,
        'redis' => [
            'pool' => 'default'
        ],
        'channel' => 'queue',
        'timeout' => 2,
        'retry_seconds' => 5,
        'handle_timeout' => 10,
        'processes' => 1,
        'concurrent' => [
            'limit' => 5,
        ],
    ],
    'order' => [
        'driver' => Hyperf\AsyncQueue\Driver\RedisDriver::class,
        'redis' => [
            'pool' => 'resource'
        ],
        'channel' => 'order.queue',
        'timeout'        => 5,
        'retry_seconds'  => [5, 10, 20, 60],
        'handle_timeout' => 120,
        'processes'      => 1,
        'concurrent'     => [
            'limit' => 5,
        ],
    ],
    'log' => [
        'driver' => Hyperf\AsyncQueue\Driver\RedisDriver::class,
        'redis' => [
            'pool' => 'resource'
        ],
        'channel' => 'log.queue',
        'timeout'        => 5,
        'retry_seconds'  => [5, 10, 20, 60],
        'handle_timeout' => 120,
        'processes'      => 1,
        'concurrent'     => [
            'limit' => 5,
        ],
    ],
    'paid' => [
        'driver' => Hyperf\AsyncQueue\Driver\RedisDriver::class,
        'redis' => [
            'pool' => 'resource'
        ],
        'channel' => 'paid.queue',
        'timeout' => 5,
        'retry_seconds' => [5, 10, 20, 60],
        'handle_timeout' => 120,
        'processes' => 3,
        'concurrent' => [
            'limit' => 5,
        ],
    ],
    'subscribe' => [
        'driver'         => Hyperf\AsyncQueue\Driver\RedisDriver::class,
        'redis'          => [
            'pool' => 'resource'
        ],
        'channel'        => '{subscribe.queue}',
        'timeout'        => 5,
        'retry_seconds'  => [5, 10, 20, 60],
        'handle_timeout' => 120,
        'processes'      => 1,
        'concurrent'     => [
            'limit' => 5,
        ],
    ],
    'free' => [
        'driver'         => Hyperf\AsyncQueue\Driver\RedisDriver::class,
        'redis'          => [
            'pool' => 'resource'
        ],
        'channel'        => '{free.queue}',
        'timeout'        => 5,
        'retry_seconds'  => [5, 10, 20, 60],
        'handle_timeout' => 120,
        'processes'      => 1,
        'concurrent'     => [
            'limit' => 5,
        ],
    ],
];
