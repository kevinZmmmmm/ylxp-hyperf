<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */


return [
    'InnerHttp' => Hyperf\HttpServer\Server::class,
    Hyperf\DbConnection\Frequency::class => Hyperf\Pool\ConstantFrequency::class,
    \Hyperf\Crontab\Strategy\StrategyInterface::class => \Hyperf\Crontab\Strategy\CoroutineStrategy::class,
    Hyperf\Framework\Bootstrap\ServerStartCallback::class => App\Common\Bootstrap\BeforeStartCallback::class,
];
