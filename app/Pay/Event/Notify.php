<?php

declare(strict_types=1);

namespace App\Pay\Event;


class Notify
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
}
