<?php

declare(strict_types=1);

namespace App\Pay\Event;


class KzPayPush
{
    public $orderNo;

    public function __construct($orderNo)
    {
        $this->orderNo = $orderNo;
    }
}
