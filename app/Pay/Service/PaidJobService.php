<?php

declare(strict_types=1);

namespace App\Pay\Service;

use App\Pay\Job\PaidJob;
use Hyperf\AsyncQueue\Driver\DriverFactory;
use Hyperf\AsyncQueue\Driver\DriverInterface;

class PaidJobService
{
    /**
     * @var DriverInterface
     */
    private $driver;

    public function __construct(DriverFactory $driverFactory)
    {
        $this->driver = $driverFactory->get('paid');
    }

    public function pushChangeSaleAndStock(string $orderNo, int $delay = 0): bool
    {
        return $this->driver->push(new PaidJob($orderNo), $delay);
    }
}
