<?php

declare(strict_types=1);

namespace App\Pay\Process;

use Hyperf\AsyncQueue\Process\ConsumerProcess;
use Hyperf\Process\Annotation\Process;

/**
 * @Process(name="PaidConsumerProcess")
 */
class PaidConsumerProcess extends ConsumerProcess
{
    /**
     * @var string
     */
    protected $queue = 'paid';
}
