<?php

declare(strict_types=1);

namespace App\Pay\Listener;

use App\Pay\Event\Paid;
use App\Third\Service\Wx\WxDeliverService;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class PaidListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            Paid::class
        ];
    }

    public function process(object $event)
    {
        $orderNo = $event->orderNo;
        $this->container->get(WxDeliverService::class)->payPush($orderNo);
    }
}
