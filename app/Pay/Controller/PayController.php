<?php

declare(strict_types=1);

namespace App\Pay\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Constants\Stakeholder;
use App\Pay\Service\PayService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use App\Common\Controller\AbstractController;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 * Class PayController
 * @package App\Pay\Controller
 */
class PayController extends AbstractController
{

    /**
     * @Inject()
     * @var PayService
     */
    private $payService;

    /**
     * @RequestMapping(path="/v1/api/payment/{method:\d+}", methods="post")
     * @param int $method
     * @return array
     */
    public function pay(int $method){
        $params = $this->request->all();
        if (empty($params['order_no'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        switch ($method) {
            case Stakeholder::PAYMENT_METHOD_BALANCE:
                $res = $this->payService->balancePayment($params);
                break;
            case Stakeholder::PAYMENT_METHOD_WECHAT:
                $res = $this->payService->weChatPayment($params);
                break;
            default:
        }
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }

    /**
     * @RequestMapping(path="/v1/api/payment/notify", methods="post")
     * @return array|false|string
     */
    public function notify(){
        $params = $this->request->all();
        // 验签
        return $this->payService->checkSignature($params);
    }


}
