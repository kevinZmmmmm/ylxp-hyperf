<?php

declare(strict_types=1);

namespace App\User\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\User\Request\TagRequest;
use App\User\Service\TagService;
use Exception;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 * @package App\User\Controller
 */
class TagController extends AbstractController
{

    /**
     * @Inject()
     * @var TagService
     */
    private $tagService;

    /**
     * 标签列表
     * @RequestMapping(path="/v1/tags", methods="get")
     * @param TagRequest $tagRequest
     * @return array
     */
    public function index(TagRequest $tagRequest)
    {
        $params = $tagRequest->all();
        $field = [
            'id',
            'name',
            'time_range_status',
            'start_time',
            'end_time',
            'deal_ok',
            'total_amount',
            'no_consume',
            'unit_price'
        ];
        $list = $this->tagService->get($params, $field, (int)$tagRequest->input('perpage', 5));
        return $this->success($list->items(), '获取成功', $list->total());

    }

    /**
     * 新增标签
     * @PostMapping(path="/v1/tags", methods="post")
     * @param TagRequest $tagRequest
     * @return array
     */
    public function store(TagRequest $tagRequest)
    {
        $params = $tagRequest->all();

        $res = $this->tagService->store($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑标签
     * @RequestMapping(path="/v1/tags/{id:\d+}/edit", methods="get")
     * @param int $id
     * @return array
     */
    public function edit(int $id)
    {
        $field = [
            'id',
            'name',
            'time_range_status',
            'start_time',
            'end_time',
            'deal_ok',
            'total_amount',
            'no_consume',
            'unit_price'
        ];
        $tag = $this->tagService->find((int)$id, $field);
        return $this->success($tag, '标签获取成功');
    }

    /**
     * 修改标签
     * @PutMapping(path="/v1/tags/{id:\d+}", methods="put")
     * @param TagRequest $tagRequest
     * @param int $id
     * @return array
     */
    public function update(int $id, TagRequest $tagRequest)
    {
        $params = $tagRequest->all();

        $res = $this->tagService->update((int)$id, $params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除标签
     * @DeleteMapping(path="/v1/tags/{id:\d+}", methods="delete")
     * @param int $id
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->tagService->destroy((int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }


}
