<?php

declare(strict_types=1);

namespace App\User\Controller;

use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\User\Request\PermissionRequest;
use App\User\Service\PermissionService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class PermissionController extends AbstractController
{
    /**
     * @Inject()
     * @var PermissionService
     */
    private $permissionService;

    const PERMISSION = 2;

    /**
     * 权限节点列表
     * @RequestMapping(path="/v1/permission", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 200;
        $field = ['id', 'parent_id', 'name', 'display_name', 'url', 'sort', 'component', 'redirect', 'meta', 'hidden'];
        $res = $this->permissionService->getList($params, self::PERMISSION, $field, $perPage);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取指定节点
     * @RequestMapping(path="/v1/permission/{id:\d+}", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $res = $this->permissionService->getInfoById((int)$id, self::PERMISSION);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加节点
     * @PostMapping(path="/v1/permission", methods="post")
     * @param PermissionRequest $request
     *
     * @return array
     */
    public function store(PermissionRequest $request)
    {
        $params = $request->validated();
        $params['guard_name'] = self::GUARD_NAME;
        $params['sort'] = isset($params['sort']) ? $params['sort'] : 0;
        $res = $this->permissionService->add($params, self::PERMISSION);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑节点
     * @PutMapping(path="/v1/permission/{id:\d+}", methods="put")
     * @param int $id
     * @param PermissionRequest $request
     *
     * @return array
     */
    public function update(int $id, PermissionRequest $request)
    {
        $params = $request->all();
        $params['id'] = $id;
        $params['guard_name'] = self::GUARD_NAME;
        $res = $this->permissionService->update($params, self::PERMISSION, '');
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除节点
     * @DeleteMapping (path="/v1/permission/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->permissionService->delete((int)$id, self::PERMISSION);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

}
