<?php

declare(strict_types=1);

namespace App\User\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\User\Model\UserModel;
use App\User\Request\ShopAccountRequest;
use App\User\Service\UserService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class ShopAccountController extends AbstractController
{
    /**
     * @Inject()
     * @var UserService
     */
    private $userService;
    const SHOPACCOUNT = 3;


    /**
     * 获取店铺账号列表
     * @RequestMapping(path="/v1/account/shop", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 15;
        $res = $this->userService->getList($params, $perPage, $field = ['*'], 2);

        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取店铺账号详情
     * @RequestMapping(path="/v1/account/{id:\d+}/shop", methods="get")
     *
     * @param int $id
     * @return array
     */
    public function info(int $id)
    {
        $res = $this->userService->getInfoById((int)$id, self::SHOPACCOUNT);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加门店账号
     * @PostMapping(path="/v1/account/shop", methods="post")
     * @param ShopAccountRequest $request
     *
     * @return array
     */
    public function store(ShopAccountRequest $request)
    {
        $params = $request->validated();
        $res = $this->userService->add($params, self::SHOPACCOUNT);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑门店账号信息
     * @PutMapping(path="/v1/account/{id:\d+}/shop", methods="put")
     * @param int $id
     * @param ShopAccountRequest $request
     *
     * @return array
     */
    public function update(int $id, ShopAccountRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->userService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 用户启用/禁用
     *
     * @param RequestInterface $request
     * @PutMapping(path="/v1/account/{id:\d+}/shop/status", methods="put")
     *
     * @return array
     */
    public function editStatus(int $id, RequestInterface $request)
    {
        $res = $this->userService->editStatus('user', (int)$id, '3');
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }


    /**
     * 店铺账号绑定店铺
     *
     * @param RequestInterface $request
     * @PostMapping(path="/v1/account/shop/bind", methods="post")
     *
     * @return array
     */
    public function shopAccountBindShopId(RequestInterface $request)
    {
        $params = $request->all();
        if (!$params['phone'] || !$params['shop_id']) return $this->failed(ErrorCode::PARAMS_INVALID);
        //门店账号 phone
        $uid = UserModel::where(['phone' => $params['phone'], 'role' => 3])->value('id');
        //门店ID shop_id
        $shop_id = $params['shop_id'];
        $res = $this->userService->bindUidWithShop((int)$uid, (int)$shop_id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }

    }

}
