<?php

declare(strict_types=1);

namespace App\User\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Constants\Stakeholder;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\User\Request\AddressRequest;
use App\User\Service\AddressService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 * @package App\User\Controller
 */
class AddressController extends AbstractController
{
    /**
     * @Inject()
     * @var AddressService
     */
    private $addressService;


    /**
     * 小程序会员地址列表
     * @RequestMapping(path="/v1/api/address", methods="get")
     * @return array
     * @author liule
     */
    public function index(){
        $mid = $this->request->input('mid');
        if (!is_numeric($mid)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, "参数错误");
        }
        $where = ['mid' => $mid, 'is_deleted' => Stakeholder::MEMBER_ADDRESS_NOT_DELETED];
        $field = ['id as address_id','mid','username','gender','phone','area','address','longitude','latitude','is_default'];
        $res = $this->addressService->all($where, $field);
        return $this->success($res, '获取列表成功', $res->count());
    }

    /**
     * 小程序新增会员收货地址
     * @PostMapping(path="/v1/api/address", methods="post")
     * @param AddressRequest $addressRequest
     * @return array
     * @author liule
     */
    public function store(AddressRequest $addressRequest)
    {
        $params = $addressRequest->all();
        try {
            $this->addressService->store($params);
        } catch (\Exception $e) {
            return $this->failed(ErrorCode::NOT_IN_FORCE, $e->getMessage());
        }
        return $this->success([], '提交成功');
    }

    /**
     * 小程序编辑地址回显信息
     * @RequestMapping(path="/v1/api/address/{address_id:\d+}/edit", methods="get")
     * @param int $address_id
     * @return array
     * @author liule
     */
    public function edit(int $address_id)
    {
        $field = [
            'id as address_id',
            'mid',
            'username',
            'phone',
            'province',
            'city',
            'area',
            'address',
            'longitude',
            'latitude',
            'status',
            'is_default',
            'gender'
        ];
        $address = $this->addressService->find((int)$address_id, $field);
        return $this->success($address, '地址获取成功');
    }

    /**
     * 小程序更新地址信息
     * @PutMapping(path="/v1/api/address/{address_id:\d+}", methods="put")
     * @param int $address_id
     * @param AddressRequest $addressRequest
     * @return array
     * @author liule
     */
    public function update(int $address_id, AddressRequest $addressRequest)
    {
        $params = $addressRequest->all();
        unset($params['platformConfidential']);
        try {
            if (isset($params['shop_id'])) unset($params['shop_id']);
            $this->addressService->update((int)$address_id, $params);
        } catch (\Exception $e) {
            return $this->failed(ErrorCode::NOT_IN_FORCE, $e->getMessage());
        }
        return $this->success([], '提交成功');
    }


    /**
     * 小程序删除地址
     * @DeleteMapping(path="/v1/api/address/{address_id:\d+}", methods="delete")
     * @param int $address_id
     * @return array
     * @author liule
     */
    public function delete(int $address_id)
    {
        try {
            $this->addressService->destroy((int)$address_id);
        } catch (\Exception $e) {
            return $this->failed(ErrorCode::NOT_IN_FORCE, $e->getMessage());
        }
        return $this->success([], '删除成功');
    }


}
