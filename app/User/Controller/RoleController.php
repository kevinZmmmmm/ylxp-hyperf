<?php

declare(strict_types=1);

namespace App\User\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\User\Request\RoleRequest;
use App\User\Service\PermissionService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class RoleController extends AbstractController
{
    /**
     * @Inject()
     * @var PermissionService
     */
    private $permissionService;

    const ROLE = 1;

    /**
     * 角色列表
     * @RequestMapping(path="/v1/role", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 200;
        $field = ['*'];
        $res = $this->permissionService->getList($params, self::ROLE, $field, $perPage);

        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }

    }

    /**
     * 获取详情
     * @RequestMapping(path="/v1/role/{id:\d+}", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        if (!$id) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->permissionService->getInfoById((int)$id, self::ROLE);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加角色
     * @PostMapping(path="/v1/role", methods="post")
     * @param RoleRequest $request
     *
     * @return array
     */
    public function store(RoleRequest $request)
    {
        $params = $request->validated();
        $params['guard_name'] = self::GUARD_NAME;
        $res = $this->permissionService->add($params, self::ROLE);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑角色
     * @PutMapping(path="/v1/role/{id:\d+}", methods="put")
     * @param int $id
     * @param RoleRequest $request
     *
     * @return array
     */
    public function update(int $id, RoleRequest $request)
    {
        $params = $request->all();
        $params['id'] = $id;
        $params['guard_name'] = self::GUARD_NAME;
        $permissionId = $params['permission_id'] ?? '';
        unset($params['permission_id']);
        $res = $this->permissionService->update($params, self::ROLE, $permissionId);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除角色
     * @DeleteMapping(path="/v1/role/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->permissionService->delete($id, self::ROLE);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 角色添加/修改权限节点
     * @PostMapping(path="/v1/role/permission", methods="post")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function addPermission(RequestInterface $request)
    {
        $roleId = $request->input('role_id');
        $permissionId = $request->input('permission_id');
        if (!$roleId || !$permissionId) return $this->failed(ErrorCode::PARAMS_INVALID);
        $res = $this->permissionService->handRoleOrPermission((int)$roleId, $permissionId, self::ROLE);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

}
