<?php

declare(strict_types=1);

namespace App\User\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'username' => ['required', 'alpha_dash', 'between:3,25', 'regex:/^[A-Za-z0-9\-\_]+$/', Rule::unique('admin_users')->where(function ($query) {
                        $query->where('deleted_at', null);
                    })],
                    'password' => 'required|alpha_dash|min:6',
                    'phone' => 'required|regex:/^1[3456789][0-9]{9}$/',
//                    'shop_id'   => 'required|integer',
                    'role_id' => 'sometimes',
                ];

            case 'PUT':
                return [
                    'password' => 'sometimes|alpha_dash|min:6',
                    'phone' => ['sometimes', 'regex:/^1[3456789][0-9]{9}$/'],
//                    'shop_id'   => 'required|integer',
                    'role_id' => 'sometimes',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'username' => '用户名',
            'password' => '密码',
            'shop_id' => '所属门店',
            'phone' => '手机号',
            'role_id' => '角色 ID',
        ];
    }

    public function messages(): array
    {
        return [
            'username.unique' => '用户名已被占用，请重新填写',
            'username.regex' => '用户名只支持英文、数字、横杆和下划线。',
            'username.between' => '用户名必须介于 3 - 25 个字符之间。',
            'username.required' => '用户名不能为空。',
        ];
    }
}
