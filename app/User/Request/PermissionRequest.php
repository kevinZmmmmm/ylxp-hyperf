<?php

declare(strict_types=1);

namespace App\User\Request;


use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class PermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'name' => 'required|between:1,20|unique:permissions,name',
                    'display_name' => 'required|alpha_dash|between:2,10|unique:permissions,display_name',
                    'url' => 'required',
                    'parent_id' => 'required|numeric',
                    'component' => 'sometimes',
                    'redirect' => 'sometimes',
                    'meta' => 'sometimes',
                    'hidden' => 'sometimes',
                    'sort' => 'sometimes',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'name' => [
                        'required',
                        'between:1,20',
                        Rule::unique('permissions')->ignore($id),
                    ],
                    'display_name' => [
                        'required',
                        'alpha_dash',
                        'between:2,10',
                        Rule::unique('permissions')->ignore($id),
                    ],
                    'url' => 'required',
                    'parent_id' => 'required|numeric',
                    'component' => 'sometimes',
                    'redirect' => 'sometimes',
                    'meta' => 'sometimes',
                    'hidden' => 'sometimes',
                    'sort' => 'sometimes',
                ];
        }
    }

    public function attributes(): array
    {
        return [
            'name' => '唯一标识',
            'display_name' => '权限名称',
            'url' => '权限 URL',
            'parent_id' => '父级 ID',
            'sort' => '排序权重',
        ];
    }
}
