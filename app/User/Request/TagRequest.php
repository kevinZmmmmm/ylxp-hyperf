<?php

declare(strict_types=1);

namespace App\User\Request;

use Hyperf\Validation\Request\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'name' => 'required',
                    'time_range_status' => 'required|in:0,1',
                    'start_time' => 'required_if:time_range_status,1',
                    'end_time' => 'required_if:time_range_status,1',
                    'deal_ok' => 'sometimes|integer',
                    'total_amount' => 'sometimes|numeric',
                    'no_consume' => 'sometimes|integer',
                    'unit_price' => 'sometimes|numeric'
                ];
            case 'GET':
                return [
                    'name' => 'sometimes',
                    'page' => 'required|integer',
                    'perpage' => 'required|integer'
                ];
            case 'PUT':
                return [

                ];
        }
    }

    public function messages(): array
    {
        return [
            'name.required' => '标签名称不能为空',
            'time_range_status.required' => '时间区间不能为空',
            'time_range_status.in' => '时间区间参数错误',
            'start_time.required_if' => '开始日期是必须的当 时间区间 参数是 1',
            'end_time.required_if' => '结束日期是必须的当 时间区间 参数是 1',
            'deal_ok.required' => '累计成功交易不能为空',
            'total_amount.required' => '累计购买金额不能为空',
            'no_consume.required' => '累计几天未消费不能为空',
            'unit_price.required' => '客单价不能为空',
            'page.required' => '页码不能为空',
            'perpage.required' => '每页条目数不能为空'
        ];
    }

}
