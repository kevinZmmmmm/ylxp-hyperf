<?php

declare(strict_types=1);

namespace App\User\Request;

use Hyperf\Validation\Request\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'phone' => 'sometimes|regex:/^1[345789][0-9]{9}$/',
            'min_without_order_day' => 'sometimes|required_with:max_without_order_day',
            'max_without_order_day' => 'sometimes|required_with:min_without_order_day',
            'min_buy_num' => 'sometimes|required_with:max_buy_num',
            'max_buy_num' => 'sometimes|required_with:min_buy_num',
            'min_total_amount' => 'sometimes|required_with:max_total_amount',
            'max_total_amount' => 'sometimes|required_with:min_total_amount',
            'min_unit_price' => 'sometimes|required_with:max_unit_price',
            'max_unit_price' => 'sometimes|required_with:min_unit_price',
            'min_age' => 'sometimes|required_with:max_age|integer',
            'max_age' => 'sometimes|required_with:min_age|integer',
            'gender' => 'sometimes|in:0,1,2',
            'perpage' => 'sometimes|integer',
            'page' => 'sometimes|integer',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'phone.regex' => '手机号格式错误',
            'max_age.required_with' => '最大年龄不能为空',
            'min_age.required_with' => '最小年龄不能为空',
            'min_without_order_day.required_with' => '最小未下单天数不能为空',
            'max_without_order_day.required_with' => '最大未下单天数不能为空',
            'min_buy_num.required_with' => '最小消费频次不能为空',
            'max_buy_num.required_with' => '最大消费频次不能为空',
            'min_total_amount.required_with' => '最小累计消费金额不能为空',
            'max_total_amount.required_with' => '最大累计消费金额不能为空',
            'min_unit_price.required_with' => '最小客单价不能为空',
            'max_unit_price.required_with' => '最大客单价不能为空',
            'gender.in' => '性别参数错误',
            'perpage.integer' => '每页显示条目数必须为整数',
            'page.integer' => '页码必须为整数'
        ];
    }
}
