<?php

declare(strict_types=1);

namespace App\User\Request;

use Hyperf\Validation\Request\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'mid' => 'required|integer',
            'phone' => 'required|regex:/^1[3456789][0-9]{9}$/',
            'username' => 'required',
            'area' => 'required',
            'gender' => 'required|integer',
            'address' => 'required',
            'is_default' => 'required|integer',
            'latitude' => 'required',
            'longitude' => 'required'
        ];
    }


    public function messages(): array
    {
        return [
            'mid.required' => '缺少会员ID',
            'phone.required' => '请填写手机号',
            'username.required' => '请填写收货人姓名',
            'address.required' => '地址不能为空',
            'latitude.required' => '经纬度错误',
            'longitude.required' => '经纬度错误'
        ];
    }

}
