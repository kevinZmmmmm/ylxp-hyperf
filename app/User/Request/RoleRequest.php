<?php

declare(strict_types=1);

namespace App\User\Request;


use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'name' => 'required|alpha_dash|between:2,10|unique:roles,name',
                    'description' => 'sometimes',
                    'permission_id' => 'sometimes',
                    'tree_id' => 'sometimes',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'name' => [
                        'required',
                        'alpha_dash',
                        'between:2,10',
                        Rule::unique('roles')->ignore($id),
                    ],
                    'description' => 'sometimes',
                    'permission_id' => 'sometimes',
                    'tree_id' => 'sometimes',
                ];
        }
    }

    public function attributes(): array
    {
        return [
            'name' => '角色名称',
            'description' => '描述',
        ];
    }
}
