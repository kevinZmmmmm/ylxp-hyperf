<?php

declare(strict_types=1);

namespace App\User\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ShopAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'username' => 'required|unique:admin_users,username',
                    'phone' => 'required|regex:/^1[3456789][0-9]{9}$/',
                    'password' => 'required|min:6',
                    'shop_id' => 'required|integer',
                ];

            case 'PUT':
                return [
                    'password' => 'sometimes|min:6',
                    'phone' => ['required', 'regex:/^1[3456789][0-9]{9}$/'],
                    'shop_id' => ['required', 'integer']
                ];
        }
    }

    public function attributes(): array
    {
        return [
            'username' => '门店账号',
            'password' => '密码',
            'shop_id' => '店铺编号',
            'phone' => '手机号'
        ];
    }

    public function messages(): array
    {
        return [
            'phone.regex' => '手机号格式不正确',
            'shop_id.unique' => '所选店铺已经被绑定',
        ];
    }
}
