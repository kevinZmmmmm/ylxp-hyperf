<?php

declare (strict_types=1);

namespace App\User\Model;

use App\Resource\Model\TeamLeaderModel;
use Hyperf\DbConnection\Model\Model;

/**
 */
class MemberModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_member';

    protected $primaryKey = 'mid';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = [];

    /**
     * 不能批量更新的属性
     * @var string[]
     */
    protected $guarded = ['mid'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function leader()
    {
        return $this->hasOne(TeamLeaderModel::class, 'mid', 'mid');
    }

    /**
     * 用户收货地址
     *
     * @return \Hyperf\Database\Model\Relations\HasMany
     */
    public function address()
    {
        return $this->hasMany(AddressModel::class, 'mid', 'mid');
    }
}
