<?php

declare (strict_types=1);

namespace App\User\Model;

use Hyperf\DbConnection\Model\Model;

class ModelHasRolesModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'model_has_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = [];

    /**
     * 不能批量更新的属性
     * @var string[]
     */
//    protected  $guarded = ['mid'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
