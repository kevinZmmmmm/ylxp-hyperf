<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\User\Model;

use Donjan\Permission\Traits\HasRoles;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\DbConnection\Model\Model;

//use Hyperf\ModelCache\Cacheable;
//use Hyperf\ModelCache\CacheableInterface;

class UserModel extends Model
{
    //use Cacheable;
    use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'admin';

    const UPDATED_AT = null;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admin_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'username', 'password', 'phone', 'status', 'role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden
        = [
            'password',
        ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer'];

    protected $attributes = [
        'role' => 2,
    ];

}
