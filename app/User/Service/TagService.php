<?php

declare(strict_types=1);

namespace App\User\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\User\Model\TagModel;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;
use Hyperf\Utils\Collection;

class TagService extends BaseService
{

    /**
     * @param array $params
     *
     * @return array
     */
    public function store(array $params)
    {
        $tag = new TagModel();
        foreach ($params as $k => $v) {
            $tag->$k = $v;
        }
        $res = $tag->save();
        if($res){
            return ['code' => ErrorCode::SUCCESS, 'data' => $res, 'info' => ['target_id' => $tag -> id]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * 标签列表
     * @param array $params
     * @param array|string[] $field
     *
     * @param int $perPage
     * @return LengthAwarePaginatorInterface
     */
    public function get(array $params = [], array $field = ['*'], int $perPage = 5)
    {
        $tagList = TagModel::query()
            ->when($params['name'] ?? 0, function ($query, $name) {
                return $query->whereRaw('INSTR(name, ?) > 0', [$name]);
            })
            ->latest('created_at')
            ->paginate($perPage, $field);
        foreach ($tagList as $k => $v) {
            $tagList[$k]['member_num'] = $this->getNumByTag($v->id);
        }
        return $tagList;
    }

    /**
     * 删除
     * @param int $id
     *
     * @return int|int[]
     */
    public function destroy(int $id)
    {
        // 标签下有会员不能删除
        $count = $this->getNumByTag($id);
        if ($count) {
            return false;
        }
        $res = TagModel::destroy($id);
        if($res){
            return ['code' => ErrorCode::SUCCESS, 'data' => $res, 'info' => ['target_id' => $id]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * 单条数据
     * @param int $id
     * @param array|string[] $field
     *
     * @return Builder|Builder[]|\Hyperf\Database\Model\Collection|Model|null
     */
    public function find(int $id, array $field = ['*'])
    {
        return TagModel::query()->find($id, $field);
    }

    /**
     * @param int $id
     * @param array $params
     *
     * @return array
     */
    public function update(int $id, array $params)
    {
        $tag = TagModel::query()->find($id);
        if(!$tag){
            return ['code' => ErrorCode::NOT_IN_FORCE];
        }
        foreach ($params as $k => $v) {
            $tag->$k = $v;
        }
        $res = $tag->save();
        if($res){
            return ['code' => ErrorCode::SUCCESS, 'data' => $res, 'info' => ['target_id' => $tag -> id]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * 获取所有标签名字
     * @return array|Collection
     */
    public function getAllTagName()
    {
        return TagModel::query()->pluck('name', 'id')->toArray() ?? [];
    }

    /**
     * 会员数
     * @param int $tag_id
     *
     * @return int
     */
    public function getNumByTag(int $tag_id)
    {
        return $this->container->get(MemberService::class)->getMemberNumByTag($tag_id);
    }


}
