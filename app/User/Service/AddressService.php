<?php

declare(strict_types=1);

namespace App\User\Service;

use App\Common\Service\BaseService;
use App\User\Model\AddressModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Collection;
use Hyperf\Database\Model\Model;
use Hyperf\DbConnection\Db;

class AddressService extends BaseService
{

    /**
     * 地址列表
     * @param array $where
     * @param array|string[] $field
     * @return Builder[]|Collection
     * @author liule
     */
    public function all(array $where = [], array $field = ['*'])
    {
        return AddressModel::query()
            ->select($field)
            ->where($where)
            ->latest('is_default')
            ->latest('create_at')
            ->get();
    }

    /**
     * 新增会员收货地址
     * @param array $params
     * @return bool|Builder|Model
     * @author liule
     */
    public function store(array $params)
    {
        // 去掉默认地址
        AddressModel::query()->where('mid', $params['mid'])->update(['is_default' => 0]);
        $data = [
            'mid' => $params['mid'],
            'username' => $params['username'],
            'phone' => $params['phone'],
            'gender' => $params['gender'],
            'area' => $params['area'],
            'address' => $params['address'],
            'is_default' => $params['is_default'],
            'longitude' => $params['longitude'],
            'latitude' => $params['latitude'],
            'province' => $params['province'] ?? '',
            'city' => $params['city'] ?? '',
            'is_deleted' => 0
        ];
        return AddressModel::query()->create($data);
    }

    /**
     * 查询地址
     * @param int $id
     * @param array|string[] $field
     * @return Builder|Builder[]|Collection|Model|null
     * @author liule
     */
    public function find(int $id, array $field = ['*'])
    {
        return AddressModel::query()->find($id, $field);
    }


    /**
     * 更新地址信息
     * @param int $id
     * @param array $params
     * @return bool
     * @author liule
     */
    public function update(int $id, array $params)
    {
        Db::transaction(function () use ($id, $params) {
            // 去掉默认地址
            if (array_key_exists('is_default', $params) && $params['is_default'] == 1) {
                AddressModel::query()->where('mid', $params['mid'])->update(['is_default' => 0]);
            }
            $address = AddressModel::query()->find($id);
            foreach ($params as $k => $v) {
                $address->$k = $v;
            }
            $address->save();
        });
    }

    /**
     * 删除地址
     * @param int $id
     * @return bool
     * @author liule
     */
    public function destroy(int $id)
    {
        return AddressModel::query()->where('id', $id)->update(['is_deleted' => 1]);
    }

    /**
     * 收货人信息 by ids
     * @param array $ids
     * @param array $field
     *
     * @return array
     */
    public function getAddressInfoByIds(array $ids = [], array $field = ['*'])
    {
        return AddressModel::query()
            ->whereIn('id', $ids)
            ->get($field)
            ->toArray();
    }

    /**
     * 主键获取地址信息
     * @param array $where
     * @param array|string[] $field
     * @return array
     */
    public function addressInfo(array $where, array $field = ['*']){
        return AddressModel::query()
            ->where($where)
            ->first($field)
            ->toArray();
    }
}
