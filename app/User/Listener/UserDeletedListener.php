<?php


namespace App\User\Listener;

use App\User\Event\UserDeleted;
use App\User\Model\UserShopModel;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener()
 */
class UserDeletedListener implements ListenerInterface
{
    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            UserDeleted::class,
        ];
    }

    /**
     * @param object $event
     */
    public function process(object $event)
    {
       $uid = $event -> userId;
       // 删除和该用户关联的表数据
        UserShopModel::query()->where('uid', $uid) -> delete();
    }
}
