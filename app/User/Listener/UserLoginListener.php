<?php


namespace App\User\Listener;

use App\User\Event\UserLogin;
use App\Common\Service\BaseService;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class UserLoginListener implements ListenerInterface
{


    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            UserLogin::class,
        ];
    }

    /**
     * @param object $event
     */
    public function process(object $event)
    {
        // 事件触发后该监听器要执行的代码写在这里，比如该示例下的发送用户注册成功短信等
        // 直接访问 $event 的 user 属性获得事件触发时传递的参数值
        // $event->user;
        $event->user->login_num += 1;
        $event->user->last_login_at = date('Y-m-d H:i:s', time());
        $event->user->save();

        $baseService = new BaseService();
        $baseService->userRedis->set($event->user->username, $event->data['token'], $event->data['exp']);
    }
}