<?php

declare(strict_types=1);

namespace App\User\Event;


class UserDeleted
{
    // 建议这里定义成 public 属性，以便监听器对该属性的直接使用，或者你提供该属性的 Getter
    public $userId;

    public function __construct($id)
    {
        $this->userId = $id;
    }
}
