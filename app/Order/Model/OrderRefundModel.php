<?php

declare (strict_types=1);

namespace App\Order\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class OrderRefundModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_order_refund';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function goods()
    {
        return $this->hasMany(RefundGoodsModel::class, 'refund_no', 'refund_no');
    }

    public function refundGoods()
    {
        return $this->goods();
    }

    public function getRefundAtAttribute($value)
    {
        if (is_null($value)) return '';
        return substr($value, 0, 4) == '1970' ? '' : $value;
    }
}
