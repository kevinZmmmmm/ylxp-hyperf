<?php

declare (strict_types=1);
namespace App\Order\Model;

use Hyperf\DbConnection\Model\Model;
/**
 */
class EntrustOrderGoodsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'entrust_order_goods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function entrustOrder(){
        return $this->belongsTo(EntrustOrderModel::class, 'entrust_order_id', 'id');
    }
}
