<?php


namespace App\Order\Model;

use Hyperf\DbConnection\Model\Model;

class ShopSolitaireOrderInfoModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_solitaire_order_info';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'statistics';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

}