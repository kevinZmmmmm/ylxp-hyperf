<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Order\Model;

use App\Common\Constants\Stakeholder;
use App\Resource\Model\ShopModel;
use App\Resource\Model\TeamLeaderModel;
use App\User\Model\AddressModel;
use App\User\Model\MemberModel;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $mobile
 * @property string $realname
 */
class OrderModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_order';

    protected $primaryKey = 'order_no';

    protected $keyType = 'string';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer'];

    /**
     * POS 状态
     *
     * @var array
     */
    public static array $posTypesZh = [
        Stakeholder::POS_STATUS_NORMAL  => '正常',
        Stakeholder::POS_STATUS_PERVERT => '反常',
    ];

    /**
     * 送货方式
     *
     * @var array
     */
    public static array $deliveryMethodZh = [
        1 => '配送',
        2 => '自提',
    ];

    /**
     * 处理状态
     *
     * @var array
     */
    public static array $handleStatusZh = [
        Stakeholder::REFUND_HANDLE_STATUS_1 => '待处理',
        Stakeholder::REFUND_HANDLE_STATUS_2 => '已处理',
    ];

    /**
     * 配送订单状态列表
     *
     */
    public static array $deliveryOrderStatusNamesZh = [
        '订单已取消',
        '待付款',
        '待发货',
        '待收货',
        '已完成',
        '已退款',
        '已驳回',
        '退款处理中',
    ];

    /**
     * 自提单订单状态
     *
     * @var array|string[]
     */
    public static array $selfTakeOrderStatusNameZh = [
        '订单已取消',
        '待付款',
        '待发货',
        '待自提',
        '已完成',
        '已退款',
        '已驳回',
        '退款处理中',
    ];

    /**
     * 订单支付方式
     *
     * @var array
     */
    public static array $payTypesZh = [
        Stakeholder::ORDER_PAY_TYPE_WECHAT  => '微信支付',
        Stakeholder::ORDER_PAY_TYPE_BALANCE => '余额支付',
    ];


    public function goods()
    {
        return $this->hasMany(OrderGoodsModel::class, 'order_no', 'order_no');
    }

    public function address()
    {
        return $this->hasOne(AddressModel::class, 'id', 'address_id');
    }

    public function member()
    {
        return $this->hasOne(MemberModel::class, 'mid', 'mid');
    }

    public function teamLeader()
    {
        return $this->hasOne(TeamLeaderModel::class, 'id', 'leader_id');
    }

    public function shop()
    {
        return $this->hasOne(ShopModel::class, 'shop_id', 'shop_id');
    }

    public function refund()
    {
        return $this->hasOne(OrderRefundModel::class, 'order_no', 'order_no')->orderBy('create_at','desc');
    }

    public function refundGoods()
    {
        return $this->hasMany(RefundGoodsModel::class, 'order_no', 'order_no');
    }

    public function friendOrder()
    {
        return $this->hasMany(self::class, 'free_parent_order_no', 'order_no');
    }

    public function getPayAtAttribute($value)
    {
        if (is_null($value)) return '';
        return substr($value, 0, 4) == '1970' ? '' : $value;
    }

    public function getCompleteDateAttribute($value)
    {
        if (is_null($value)) return '';
        return substr($value, 0, 4) == '1970' ? '' : $value;
    }
}
