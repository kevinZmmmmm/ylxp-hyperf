<?php

declare (strict_types=1);

namespace App\Order\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class WxRefundLogModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_order_wx_refund_log';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = ['id'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}