<?php

declare (strict_types=1);

namespace App\Order\Model;

use App\Resource\Model\CrdCategoryModel;
use App\Resource\Model\GoodsModel;
use Hyperf\DbConnection\Model\Model;

/**
 */
class OrderGoodsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_order_goods';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];


    public function goods()
    {
        return $this->hasOne(GoodsModel::class, 'id', 'goods_id');
    }

    /**
     * 次日达商品分类
     *
     * @return \Hyperf\Database\Model\Relations\BelongsTo
     */
    public function crd_cate()
    {
        return $this->belongsTo(CrdCategoryModel::class, 'crd_cate_id', 'id');
    }

    /**
     * 次日达商品分类
     *
     * @return \Hyperf\Database\Model\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(OrderModel::class, 'order_no', 'order_no');
    }
}
