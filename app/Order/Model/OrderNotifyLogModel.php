<?php

declare (strict_types=1);

namespace App\Order\Model;

use Hyperf\DbConnection\Model\Model;

class OrderNotifyLogModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_notify_log';

    protected $connection = 'log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}