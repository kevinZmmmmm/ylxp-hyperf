<?php


namespace App\Order\Model;


use App\Resource\Model\ShopModel;
use Hyperf\DbConnection\Model\Model;

class ShopOrderModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_order_info';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'statistics';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function shop()
    {
        return $this->hasOne(ShopModel::class, 'shop_id', 'shop_id');
    }

    public static function getOrderSource()
    {
        return [
            '及时达',
            '次日达',
            '拼团',
            '接龙',
        ] ?? '全部来源';
    }
}
