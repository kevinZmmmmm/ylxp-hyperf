<?php


namespace App\Order\Model;

use Hyperf\DbConnection\Model\Model;

class ShopGainOrderInfoModel extends Model
{
    /**
     * 及时达订单财务分析表
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_gain_order_info';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'statistics';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

}