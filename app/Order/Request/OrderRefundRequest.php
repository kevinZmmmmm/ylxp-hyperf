<?php

declare(strict_types=1);

namespace App\Order\Request;

use Hyperf\Validation\Request\FormRequest;

class OrderRefundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'page' => 'required|integer',
            'perpage' => 'required|integer',
            'order_source' => 'sometimes|integer'
        ];
    }

    public function messages(): array
    {
        return [
            'page.required' => '页码不能为空',
            'perpage.required' => '每页条目数不能为空'
        ];
    }

}
