<?php

declare(strict_types=1);

namespace App\Order\Request;

use Hyperf\Validation\Request\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'phone' => 'sometimes|regex:/^1[3456789][0-9]{9}$/',
            'order_type' => 'sometimes|in:0,2,6,7,8,9,10',
            'pay_type' => 'sometimes|in:1,2',
            'mid' => 'sometimes|integer|not_in:0',
            'page' => 'sometimes|integer',
            'perpage' => 'sometimes|integer',
            'order_source' => 'sometimes|integer'
        ];
    }

    public function messages(): array
    {
        return [
            'phone.regex' => '手机号错误',
            'pay_type.in' => '支付方式参数错误',
            'order_type.in' => '订单类型参数错误',
            'mid.not_in' => '会员ID不能为0'
        ];
    }

}
