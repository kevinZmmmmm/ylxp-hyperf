<?php


namespace App\Order\Job;
use App\Order\Model\OrderModel;
use App\Order\Service\Order\OrderBaseService;
use Hyperf\AsyncQueue\Job;

class CancelOrderJob extends Job
{
    public $orderNo;

    /**
     * 任务执行失败后的重试次数，即最大执行次数为 $maxAttempts+1 次
     *
     * @var int
     */
    protected $maxAttempts = 2;

    public function __construct($param)
    {
        $this->orderNo = $param['order_no'];
    }

    public function handle()
    {
        $order = OrderModel::where(['order_no'=>$this->orderNo])->select(['status','order_type'])->first();
        if($order['status'] == OrderBaseService::ORDER_STATUS_1){
            $data = ['status'=>0];
            if($order['order_type']==OrderBaseService::ORDER_TYPE_10){
                $data['is_freed'] =3;
            }
            OrderModel::where(['order_no'=>$this->orderNo])->update($data);
        }

    }
}