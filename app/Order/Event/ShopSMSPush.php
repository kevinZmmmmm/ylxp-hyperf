<?php

declare(strict_types=1);

namespace App\Order\Event;


class ShopSMSPush
{

    public $shopId;
    public $orderNo;

    public function __construct($shopId, $orderNo)
    {
        $this->shopId = $shopId;
        $this->orderNo = $orderNo;
    }
}
