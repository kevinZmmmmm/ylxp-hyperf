<?php

declare(strict_types=1);

namespace App\Order\Event;


class CreateShopOrder
{
    public $orderNo;
    public $orderSerialNum;
    public $writeOffCode;
    public $order;
    public $orderGoods;
    public $userInfo;
    public $params;

    /**
     * CreateShopOrder constructor.
     * @param array $order
     * @param array $orderGoods
     * @param array $userInfo
     * @param array $params
     */
    public function __construct(string $orderNo,string $orderSerialNum,string $writeOffCode,array $order,array $orderGoods,array $userInfo,array $params)
    {
        $this->orderNo= $orderNo;
        $this->orderSerialNum= $orderSerialNum;
        $this->writeOffCode= $writeOffCode;
        $this->order = $order;
        $this->orderGoods = $orderGoods;
        $this->userInfo = $userInfo;
        $this->params = $params;
    }

}