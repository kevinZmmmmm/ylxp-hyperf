<?php

declare(strict_types=1);

namespace App\Order\Event;


class CrdRefund
{

    public $refundNo;

    public function __construct($refundNo)
    {
        $this->refundNo = $refundNo;
    }
}
