<?php

declare(strict_types=1);

namespace App\Order\Event;


class CreateActivityOrder
{
    public $order;
    public $orderGoods;
    public $userInfo;
    public $params;

    /**
     * CreateActivityOrder constructor.
     * @param array $order
     * @param array $orderGoods
     * @param array $userInfo
     * @param array $params
     */
    public function __construct(array $order,array $orderGoods,array $userInfo,array $params)
    {
        $this->order = $order;
        $this->orderGoods = $orderGoods;
        $this->userInfo = $userInfo;
        $this->params = $params;
    }

}
