<?php

declare(strict_types=1);

namespace App\Order\Aspect;

use App\Activity\Model\ActivityModel;
use App\Common\Constants\Stakeholder;
use App\Common\Service\BaseService;
use App\Order\Model\OrderGoodsModel;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Psr\Container\ContainerInterface;
use Hyperf\Di\Aop\ProceedingJoinPoint;

/**
 * @Aspect
 */
class FreeStatusAspect extends AbstractAspect
{
    public $classes = [
        'App\Order\Service\Order\OrderMiniService::getInviteOrder',
        'App\Order\Service\Order\OrderMiniService::getFreeCenter'
    ];

    public $annotations = [
    ];

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
//        $arg = $proceedingJoinPoint->getArguments();
        // 好友支付后回调检测 是否免单成功 is_freed=2 free_step=4
        // 1邀请好友 ，2 重新激活， 3 再试一次 ， 4查看详情
        // 2次失败 且 活动未结束 再试一次
        // 2次失败 且 活动已结束或下架 查看详情
        // 成功 查看详情
        $list = $proceedingJoinPoint->process();

        if($list->total()) {
            $act_id = array_unique(array_column($list->items(), 'activity_id'));
//            $act_goods_id = array_unique(array_column($list->items(), 'activity_goods_id'));
//            $act_success_num = OrderGoodsModel::query()
//                ->from('store_order_goods as og')
//                ->leftJoin('store_order as order', 'og.order_no', '=', 'order.order_no')
//                ->select(['order.order_no','og.activity_goods_id'])
//                ->whereIn('og.activity_goods_id', $act_goods_id)
//                ->whereIn('order.activity_id', $act_id)
//                ->whereIn('order.is_freed', [2,4])
//                ->get()->toArray();
//            $act_success_num = array_column($act_success_num, null, 'activity_goods_id');
//            var_dump($act_success_num);
            $act = ActivityModel::query()
                ->select(['activityID','begin_date','end_date','shop_ids','activity_prams','status'])
                ->whereIn('activityID', $act_id)
                ->get()
                ->keyBy('activityID')
                ->toArray();
            $takeDate = $this->container->get(BaseService::class)->getOverNightTakeDate();
            $now = time();
            $countdown = 0;
            $num = 0;
            foreach ($list->items() as &$v){
//                $status_arr = array_column(collect($v)->get('friend_order'), 'status');
//                $v['free_status'] = count(array_filter($status_arr, fn($v) => in_array($v, [2,3,4,6]))) >= $v['need_invited_num'] ? 3:2;
                $act_info_arr = json_decode($act[$v['activity_id']]['activity_prams'],true);
                $pay_time = strtotime($v['pay_at']);
                $act_status = $act[$v['activity_id']]['status'];
                $act_start = $act[$v['activity_id']]['begin_date'];
                $act_end = $act[$v['activity_id']]['end_date'];
                if ($v['free_num'] == 1 and in_array($v['free_step'], [1,6])) {
                    // 邀请中 邀请好友
                    $countdown = $pay_time + $act_info_arr['first_invite_time']*3600 - $now;
                }
                if ( $act_info_arr['is_second'] ) {
                    if ($v['free_num'] == 1 and $v['free_step'] == 2) {
                        // 重新激活 激活倒计时
                        $countdown = $pay_time + ($act_info_arr['second_activation'] + $act_info_arr['first_invite_time']) * 3600 - $now;
                    }
                    if ($v['free_num'] == 2 and in_array($v['free_step'], [1,6])) {
                        // 二次邀请中 邀请好友
                        $countdown = strtotime($v['free_activation_time']) + $act_info_arr['second_invite_time'] * 3600 - $now;
                    }
                }
                $v['countdown'] = $countdown > 0 ? $countdown:0;
                $v['share_image'] = $act_info_arr['share_image'];
                $v['share_text'] = $act_info_arr['share_text'];
                $v['goods_pick_time'] = $takeDate . ' ' . substr($v['goods_pick_time'], 0, 5);
//                if (array_key_exists($v['activity_goods_id'], $act_success_num)) $num = count($act_success_num[$v['activity_goods_id']]);
                $v['free_success_number'] = $v['free_success_num'] + $num; //已免单人数
            }
        }
        return $list;
    }
}
