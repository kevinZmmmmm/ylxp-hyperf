<?php


namespace App\Order\Listener;


use App\Order\Event\CreateShopOrder;
use App\Order\Model\OrderGoodsModel;
use App\Order\Model\OrderModel;
use Hyperf\DbConnection\Db;
use Hyperf\Event\Contract\ListenerInterface;
use App\Order\Service\Order\OrderBaseService;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;

/**
 * @Listener
 */
class CreateShopOrderListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            CreateShopOrder::class
        ];
    }
    public function process(object $event)
    {
        $container = $this->container;
        $orderBaseService = $container->get(OrderBaseService::class);
        $orderCollectionArray = [
            'mid' => $event->userInfo['mid'],
            'openid' => $event->userInfo['openid'],
            'order_phone' => $event->userInfo['phone'],
            'order_no' => $event->orderNo,
            'activity_id' => $event->orderGoods['activityID']??0,
            'activity_type' => $event->order['activity_type']??0,
            'shop_id' => $event->params['shop_id'],
            'deal_type' => $event->order['deal_type'],
            'goods_price' => $event->order['orderGoodsMoney'],                                     //商品总金额(不含快递费)-优惠金额
            'total_price' => $event->order['orderGoodsMoney']+$event->order['orderFreightPrice'],  //订单总金额(商品金额+快递金额) =goods_price+freight_price
            'pay_type' => $event->params['pay_type'],
            'memo' => $event->params['memo']??'',
            'order_goods_num' => $event->order['orderGoodsNumber'],
            'order_type' => $event->order['orderType'],
            'write_off_code' => $event->writeOffCode,
            'appointment_time' => $event->params['appointment_time'],
            'appointment_date' => $event->order['appointment_date'],
            'order_source' => $event->order['order_source'],
            'order_type_status' => $event->order['order_type_status'],
            'serial_num' => $event->orderSerialNum,
            'create_at' => date('Y-m-d H:i:s'),
            'coupon_info' => $event->order['couponInfo'],                        // 普通优惠劵信息
            'coupon_cut' => $event->order['orderDeductMoney'],                   // 普通优惠抵扣金额
            'coupon_status' => $event->order['orderCouponStatus'],               //是否使用优惠券,0未使用,1使用
            'coupon_freight' => $event->order['couponFreightInfo'],              //运费优惠券的抵扣金额
            'coupon_freight_cut' => $event->order['orderFreightDeductMoney'],    //运费优惠券的抵扣金额
            'order_commission' => $event->order['commission'],
            'leader_id' => $event->params['leader_id']?? 0,
            'platform_type' => $event->params['platform_type'],
        ];
        foreach ($event->orderGoods as $goods) {
            $orderGoodsCollectionArray[] = [
                'mid' => $event->userInfo['mid'],
                'shop_id' => $event->params['shop_id'],
                'order_no' => $event->orderNo,
                'goods_id' => $goods['id'],
                'cate_id' => $goods['cate_id'],
                'activity_id' => $goods['activityID']??0,
                'activity_goods_id' => $goods['activity_goods_id'] ?? 0,
                'itemCode' => $goods['wx_crm_code'],
                'kz_goods_id' => $goods['kz_goods_id'],
                'kz_type_id' => $goods['kz_type_id'] ?? 0,
                'group_id' => $goods['group_id'],
                'goods_title' => $goods['title'],
                'goods_spec' => $goods['goods_spec'],
                'goods_logo' => $goods['logo'],
                'scant_id' => $goods['scant_id'],
                'ratio' => $goods['ratio'],
                'selling_price' => $goods['price_selling'],
                'market_price' => $goods['price_market'],
                'cost_price' => $goods['current_cost_price'] ?? 0.00,
                'number' => $goods['cart_goods_number'],
                'commission' => $goods['commission'],
                'crd_cate_id' => $goods['crd_cate_id'],
                'lsy_goods_name' => $goods['lsy_goods_name'],
                'lsy_unit_name' => $goods['lsy_unit_name'],
                'lsy_class_name' => $goods['lsy_class_name'],
                'create_at' => date('Y-m-d H:i:s'),
                'kz_self_num' => $goods['kz_self_num']
            ];
        }
        $cartIds = collect($event->orderGoods)->pluck('cart_id')->toArray();
        Db::beginTransaction();
        try {
            OrderModel::query()->insert($orderCollectionArray);
            OrderGoodsModel::query()->insert($orderGoodsCollectionArray);
            Db::commit();
            $orderBaseService->cancelCommonOrder($event->orderNo);
            $orderBaseService->deleteCart($cartIds,$event->userInfo['mid']);
            $orderBaseService->createCommonOrderLog('info',$event->params, $event->order['orderType'], $event->orderNo);
        }catch (\Exception $e){
            Db::rollback();
            $orderBaseService->createCommonOrderLog('error', $event->params, $event->order['orderType'], $e->getMessage());
        }
    }


}