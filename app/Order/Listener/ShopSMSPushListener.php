<?php

declare(strict_types=1);

namespace App\Order\Listener;

use App\Order\Event\ShopSMSPush;
use App\Resource\Service\ShopService;
use App\Third\Service\Sms\SmsService;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class ShopSMSPushListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            ShopSMSPush::class
        ];
    }

    public function process(object $event)
    {
        $shopId = $event->shopId;
        $orderNo = $event->orderNo;
        $phone = $this->container->get(ShopService::class)->shopInfoByWhere(['shop_id' => $shopId] , ['shop_tel'])['shop_tel'];
        $this->container->get(SmsService::class)->send($phone , $orderNo, env('REFUND_SMS_SHOP_PUSH_TEMPID','XC10263-0005'));
    }
}
