<?php

declare(strict_types=1);

namespace App\Order\Listener;

use App\Order\Event\CrdRefund;
use App\Order\Service\OrderRefundService;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class CrdRefundListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            CrdRefund::class
        ];
    }

    public function process(object $event)
    {
        $refundNo = $event->refundNo;
        $this->container->get(OrderRefundService::class)->agree($refundNo, 'mini');
    }
}
