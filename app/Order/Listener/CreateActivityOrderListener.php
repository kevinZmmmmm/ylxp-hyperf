<?php

declare(strict_types=1);

namespace App\Order\Listener;
use App\Common\Service\Random;
use App\Order\Event\CreateActivityOrder;
use App\Order\Model\OrderGoodsModel;
use App\Order\Model\OrderModel;
use App\Order\Service\Order\OrderBaseService;
use Hyperf\DbConnection\Db;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class CreateActivityOrderListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            CreateActivityOrder::class
        ];
    }

    /**
     * @param object $event
     * @return array
     * @author ran
     * @date 2021-03-26 9:51
     * mailbox 466180170@qq.com
     */
    public function process(object $event)
    {
        $container = $this->container;
        $orderBaseService = $container->get(OrderBaseService::class);
        $orderCollectionArray = [
            'mid' => $event->userInfo['mid'],
            'openid' => $event->userInfo['openid'],
            'order_phone' => $event->userInfo['phone'],
            'order_no' => $event->order['order_no'],
            'activity_id' => $event->orderGoods['activityID'],
            'activity_type' => $event->order['activity_type'],
            'shop_id' => $event->params['shop_id'],
            'deal_type' => $event->order['deal_type'],
            'goods_price' => $event->order['goods_price'],
            'total_price' => $event->order['total_price'],
            'pay_type' => $event->params['pay_type'],
            'memo' => $event->params['memo']??'',
            'order_goods_num' => $event->order['order_goods_num'],
            'order_type' => $event->order['order_type'],
            'write_off_code' => Random::character(8),
            'appointment_time' => $event->params['appointment_time'],
            'appointment_date' => $event->order['appointment_date'],
            'order_source' => $event->order['order_source'],
            'order_type_status' => $event->order['order_type_status'],
            'serial_num' => $orderBaseService->getToDayOrderSerialNumByShopId((int)$event->params['shop_id']),
            'create_at' => date('Y-m-d H:i:s'),
            'coupon_info' => '',    // 优惠劵信息
            'coupon_cut' => 0,      // 优惠金额
            'coupon_status' => 0,   //是否使用优惠券,0未使用,1使用
            'coupon_freight' => '',
            'coupon_freight_cut' => 0,
            'order_commission' => $event->order['order_commission'] ?? 0,
            'leader_id' => $event->params['leader_id']?? 0,
            'platform_type' => $event->order['platform_type'],
            'free_parent_order_no'=>$event->params['free_parent_order_no']??'',
            'need_invited_num'=>$event->orderGoods['need_invited_num']??0,
            'free_num'=>$event->order['free_num']??0,
            'free_step'=>$event->order['free_step']??0
        ];
        $orderGoodsCollectionArray = [
            'mid' => $event->userInfo['mid'],
            'shop_id' => $event->params['shop_id'],
            'order_no' => $event->order['order_no'],
            'goods_id' => $event->orderGoods['id'],
            'cate_id' => $event->orderGoods['cate_id'] ?? 0,
            'activity_id' => $event->orderGoods['activityID'],
            'activity_goods_id' => $event->orderGoods['activity_goods_id'] ?? 0,
            'itemCode' => $event->orderGoods['wx_crm_code'] ?? 0,
            'kz_goods_id' => $event->orderGoods['kz_goods_id'],
            'kz_type_id' => $event->orderGoods['kz_type_id'] ?? 0,
            'group_id' => $event->orderGoods['group_id'],
            'goods_title' => $event->orderGoods['title'],
            'goods_spec' => $event->orderGoods['goods_spec'],
            'goods_logo' => $event->orderGoods['logo'],
            'scant_id' => $event->orderGoods['scant_id'],
            'ratio' => $event->orderGoods['ratio'],
            'selling_price' => $event->orderGoods['activity_price_selling'],
            'market_price' => $event->orderGoods['activity_price_market'],
            'cost_price' => $event->orderGoods['current_cost_price'] ?? 0.00,
            'number' => $event->order['order_goods_num'],
            'commission' => $event->orderGoods['commission'] ?? 0,
            'crd_cate_id'=>$event->orderGoods['crd_cate_id'],
            'lsy_goods_name'=>$event->orderGoods['lsy_goods_name'],
            'lsy_unit_name'=>$event->orderGoods['lsy_unit_name'],
            'lsy_class_name'=>$event->orderGoods['lsy_class_name'],
            'create_at' => date('Y-m-d H:i:s'),
            'kz_self_num'=>'xp'.Random::character(8)
        ];
        Db::beginTransaction();
        try {
            OrderModel::query()->insert($orderCollectionArray);
            OrderGoodsModel::query()->insert($orderGoodsCollectionArray);
            Db::commit();
            $orderBaseService->cancelCommonOrder($event->order['order_no']);
            $orderBaseService->createCommonOrderLog('info',$event->params, $event->order['order_type'], $event->order['order_no']);
        }catch (\Exception $e){
            Db::rollback();
            $orderBaseService->createCommonOrderLog('error', $event->params, $event->order['order_type'], $e->getMessage());
        }
    }

}