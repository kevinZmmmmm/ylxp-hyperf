<?php


namespace App\Order\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Common\Service\Bc;
use App\Order\Model\KzRefundLogModel;
use App\Order\Model\OrderGoodsModel;
use App\Order\Model\OrderLogModel;
use App\Order\Model\OrderNotifyLogModel;
use App\Order\Model\ShopOrderModel;
use App\Pay\Model\PayLog;
use App\Resource\Model\CategoryModel;
use App\Resource\Model\ShopGroupModel;
use App\Order\Model\WxRefundLogModel;
use App\Resource\Model\ShopModel;
use App\Third\Model\WxDedoLogModel;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Carbon\Carbon;

class FinancialLogService extends BaseService
{

    /**
     * @Inject
     * @var Bc
     */
    protected $bc;

    /**
     * @param array $where
     * @param array|string[] $field
     * @param int $perPage
     * @param int $type     wx:吾享订单退款日志，kz:客至订单退款日志
     *
     * @return array
     */
    public function RefundList(array $where = [], array $field = ['*'], int $perPage = 15, $type = null)
    {
        if($type == 'wx'){
            $query = WxRefundLogModel::query();
            $prefix = 'store_order_wx_refund_log';
        }elseif ($type == 'kz'){
            $query = KzRefundLogModel::query();
            $prefix = 'store_order_kz_refund_log';
        }else{
            return ['code' => ErrorCode::SUCCESS, 'data' => []];
        }

        $list = $query
            ->leftJoin('store_shop', "{$prefix}.shop_id", '=', 'store_shop.shop_id')
            ->when($where['shop_id'] ?? 0, function ($query, $shop_id) use($prefix) {
                return $query->where("{$prefix}.shop_id", $shop_id);
            })
            ->when($where['order_no'] ?? 0, function ($query, $order_no) {
                return $query->where('order_no', $order_no);
            })
            ->when($where['start_time'] ?? 0, function ($query, $startTime) {
                return $query->whereDate('create_time', '>=', $startTime);
            })
            ->when($where['end_time'] ?? 0, function ($query, $endTime) {
                return $query->whereDate('create_time', '<=', $endTime);
            })
            ->when($where['shop_group_id'] ?? 0, function ($query, $shopGroupId) use($prefix) {
                $shopIds = ShopModel::query()->where('shop_group_id',$shopGroupId)->where('is_deleted',0)->pluck('shop_id');
                return $query->whereIn("{$prefix}.shop_id", $shopIds->toArray());
            })
            ->selectRaw("{$prefix}.*,store_shop.shop_name")
            ->orderBy('create_time','desc')
            ->paginate($perPage, $field);

        if (!$list) {
            return ['code' => ErrorCode::SUCCESS, 'data' => []];
        }

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }


    /**
     * @param array $where
     * @param array|string[] $field
     * @param int $perPage
     *
     * @return array
     */
//    public function kzRefundList(array $where = [], array $field = ['*'], int $perPage = 15)
//    {
//        $list = KzRefundLogModel::query()
//            ->leftJoin('store_shop', 'store_order_kz_refund_log.shop_id', '=', 'store_shop.shop_id')
//            ->when($where['shop_id'] ?? 0, function ($query, $shop_id) {
//                return $query->where('store_order_kz_refund_log.shop_id', $shop_id);
//            })
//            ->when($where['order_no'] ?? 0, function ($query, $order_no) {
//                return $query->where('order_no', $order_no);
//            })
//            ->when($where['start_time'] ?? 0, function ($query, $startTime) {
//                return $query->whereDate('create_time', '>=', $startTime);
//            })
//            ->when($where['end_time'] ?? 0, function ($query, $endTime) {
//                return $query->whereDate('create_time', '<=', $endTime);
//            })
//            ->when($where['shop_group_id'] ?? 0, function ($query, $shopGroupId) {
//                $shopIds = ShopModel::query()->where('shop_group_id',$shopGroupId)->where('is_deleted',0)->pluck('shop_id');
//                return $query->whereIn('store_order_kz_refund_log.shop_id', $shopIds->toArray());
//            })
//            ->selectRaw('store_order_kz_refund_log.*,store_shop.shop_name')
//            ->orderBy('create_time','desc')
//            ->paginate($perPage, $field);
//
//        if (!$list) {
//            return ['code' => ErrorCode::SUCCESS, 'data' => []];
//        }
//
//        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
//    }

    /**
     * @param array $where
     * @param array|string[] $field
     * @param int $perPage
     *
     * @return array
     */
    public function dePushList(array $where = [], array $field = ['*'], int $perPage = 15)
    {
        $list = WxDedoLogModel::query()
            ->where('qb_wx_dedo_log.is_deleted',0)
            ->leftJoin('shopylxp.store_shop', 'qb_wx_dedo_log.shop_id', '=', 'store_shop.shop_id')
            ->when($where['shop_group_id'] ?? 0, function ($query, $shopGroupId) {
                $shopIds = ShopModel::query()->where('shop_group_id',$shopGroupId)->where('is_deleted',0)->pluck('shop_id');
                return $query->whereIn('qb_wx_dedo_log.shop_id', $shopIds->toArray());
            })
            ->when($where['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('qb_wx_dedo_log.shop_id', $shop_id);
            })
            ->when($where['order_no'] ?? 0, function ($query, $order_no) {
                return $query->where('order_no', $order_no);
            })
            ->when($where['deId'] ?? 0, function ($query, $deId) {
                return $query->where('deId', $deId);
            })
            ->when($where['deNo'] ?? 0, function ($query, $deNo) {
                return $query->where('deNo', $deNo);
            })
            ->when($where['pos_type'] ?? 0, function ($query, $pos_type) {
                return $query->where('pos_type', $pos_type);
            })
            ->when($where['type'] ?? 0, function ($query, $type) {
                return $query->where('type', $type);
            })
            ->when($where['start_time'] ?? 0, function ($query, $startTime) {
                return $query->whereDate('created_at', '>=', $startTime);
            })
            ->when($where['end_time'] ?? 0, function ($query, $endTime) {
                return $query->whereDate('created_at', '<=', $endTime);
            })
            ->selectRaw('qb_wx_dedo_log.*,store_shop.shop_name')
            ->orderBy('id','desc')
            ->paginate($perPage, $field);

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }

    /**
     * @param array $where
     * @param array|string[] $field
     * @param int $perPage
     * @param string $type  wxCallback：小跑吾享回调日志，payment：小跑支付日志，placeOrder：小跑下单日志
     *
     * @return array
     */
    public function xiaoPaoLog(array $where = [], array $field = ['*'], int $perPage = 15, string $type = 'wxCallback')
    {
        if($type == 'wxCallback'){
            $query = OrderNotifyLogModel::query();
        }elseif ($type == 'payment'){
            $query = PayLog::query();
        }elseif ($type == 'placeOrder'){
            $query = OrderLogModel::query();
        }else{
            return ['code' => ErrorCode::SUCCESS, 'data' => []];
        }

        $list = $query
            ->when($where['params'] ?? 0, function ($query, $params){
                return $query->where('params', 'regexp', $params);
            })
            ->latest()
            ->paginate($perPage, $field);

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }
}
