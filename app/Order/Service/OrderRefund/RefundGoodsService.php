<?php

declare(strict_types=1);

namespace App\Order\Service\OrderRefund;

use App\Common\Service\BaseService;
use App\Order\Model\OrderRefundModel;
use App\Order\Model\RefundGoodsModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Collection;

class RefundGoodsService extends BaseService
{
    /**
     * 退款商品列表
     * @param array $where
     * @param array|string[] $field
     * @return Builder[]|Collection
     */
    public function getRefundGoodsList(array $where = [], array $field = ['*'])
    {
        return RefundGoodsModel::query()
            ->select($field)
            ->where($where)
            ->get();
    }

    /**
     * 退款商品名称,格式化字符串
     * @param array $refund_no
     * @return array
     */
    public function refundGoodsName(array $refund_no = [])
    {
        return RefundGoodsModel::query()
            ->selectRaw('GROUP_CONCAT(goods_title) as goods_title,refund_no')
            ->whereIn('refund_no', $refund_no)
            ->groupBy(['refund_no'])
            ->get()
            ->toArray();
    }

    /**
     * 商品分析---退款商品数量
     * @param array $params
     * @return array
     */
    public function refundGoodsNum(array $params = [])
    {
        $paid = OrderRefundModel::query()
            ->from('store_order_refund as refund')
            ->leftJoin('store_order_refund_goods as rg', 'refund.refund_no', '=', 'rg.refund_no')
            ->selectRaw('SUM(rg.refund_number) as refund_number')
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('refund.order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('refund.order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('refund.shop_id', $shop_id);
            })
            ->where('rg.status', 1)
            ->whereBetween('rg.create_at', $params['date'])
            ->first()
            ->toArray();

        $contrast = OrderRefundModel::query()
            ->from('store_order_refund as refund')
            ->leftJoin('store_order_refund_goods as rg', 'refund.refund_no', '=', 'rg.refund_no')
            ->selectRaw('SUM(rg.refund_number) as refund_number')
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('refund.order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('refund.order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('refund.shop_id', $shop_id);
            })
            ->where('rg.status', 1)
            ->whereBetween('rg.create_at', $params['contrasttime'])
            ->first()
            ->toArray();

        $hrend = $this->coefficientsAndTrends((string)$paid['refund_number'], (string)$contrast['refund_number']);

        return ['amt' => $paid['refund_number'] ?? 0, 'changeFlag' => $hrend['changeFlag'], 'ratio' => $hrend['ratio']];
    }

    /**
     * 商品分析---退款商品数量
     * @param array $params
     * @return array
     */
    public function refundGoodsNumByDateRange(array $params)
    {
        if ($params['flag'] == 1) {
            $selectRaw = "DATE_FORMAT(rg.create_at, '%Y-%m-%d') as date,SUM(rg.refund_number) as refund_number";
        } else {
            $selectRaw = "DATE_FORMAT(rg.create_at, '%Y-%m') as date,SUM(rg.refund_number) as refund_number";
        }
        $paidr = OrderRefundModel::query()
            ->from('store_order_refund as refund')
            ->leftJoin('store_order_refund_goods as rg', 'refund.refund_no', '=', 'rg.refund_no')
            ->selectRaw($selectRaw)
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('refund.order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('refund.order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('refund.shop_id', $shop_id);
            })
            ->where('rg.status', 1)
            ->whereBetween('rg.create_at', $params['date'])
            ->groupBy(['date'])
            ->get()
            ->toArray();
        $contrastr = OrderRefundModel::query()
            ->from('store_order_refund as refund')
            ->leftJoin('store_order_refund_goods as rg', 'refund.refund_no', '=', 'rg.refund_no')
            ->selectRaw($selectRaw)
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('refund.order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('refund.order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('refund.shop_id', $shop_id);
            })
            ->where('rg.status', 1)
            ->whereBetween('rg.create_at', $params['contrasttime'])
            ->groupBy(['date'])
            ->get()
            ->toArray();
        return ['basic' => $paidr, 'contrast' => $contrastr];
    }

    /**
     * 更新数据
     * @param array $where
     * @param array $data
     * @return int
     */
    public function update(array $where, array $data)
    {
        if (in_array('in', $where)) {
            return RefundGoodsModel::query()->whereIn($where[0], $where[2])->update($data);
        }
        return RefundGoodsModel::query()->where($where)->update($data);
    }

    // 批量插入
    public function saveAll(array $data){
        return RefundGoodsModel::query()->insert($data);
    }

}
