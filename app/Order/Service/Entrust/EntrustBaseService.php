<?php


namespace App\Order\Service\Entrust;


use App\Common\Service\BaseService;
use App\Order\Repository\RepositoryFactory;

class EntrustBaseService extends BaseService
{

    /**
     * 公共字段集合
     */
    const GOODSFIELD=['id','scant_id','ratio','wx_crm_code', 'kz_goods_id', 'kz_type_id', 'cate_id', 'crd_cate_id', 'lsy_goods_name', 'lsy_unit_name', 'lsy_class_name'];
    const GOODSLISTFIELD=['price_market','number_stock','goods_pick_time','commission','limite_num_per_day'];
    const ENTRUSTFIELD=['entrust_order_id','phone','amount','dis_amount','discount','share','order_source'];
    const ENTRUSTGOODSFIELD=['entrust_order_id','number as cart_goods_number','unit_price as price_selling','dis_unit_price','subtotal'];

    /**
     * 订单来源   0及时达，1次日达
     */
    const ENTRUST_ORDER_SOURCE_0 = 1;
    const ENTRUST_ORDER_SOURCE_1 = 2;

    protected $entrustOrder;

    protected $entrustOrderGoods;

    /**
     * EntrustBaseService constructor.
     * @param RepositoryFactory $repoFactory
     */
    public function __construct(RepositoryFactory $repoFactory)
    {
        parent::__construct();
        $this->entrustOrder = $repoFactory->getRepository("entrustOrder");
        $this->entrustOrderGoods = $repoFactory->getRepository("entrustOrderGoods");
    }
    /**
     * 次日达代客单商品信息公共服务
     * @param array $needExceptKey
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-02-24 13:46
     * mailbox 466180170@qq.com
     */
    protected  function getResourceCommonEntrustGoodsList(array $needExceptKey=[],int $entrust_order_id,int $page=1,int $limit=20,array $extraCondition=[],bool $isPage=true,array $orderBy=[]): array
    {
        $where =['entrust_order_goods.entrust_order_id'=>$entrust_order_id];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereRaw'=>$extraCondition['whereRaw']??[],'whereNotIn'=>$extraCondition['whereNotIn']??[],'whereIn'=>$extraCondition['whereIn']??[]];
        $goodsArray= $this->entrustOrderGoods->selectMoreUnionList(self::ENTRUSTGOODSFIELD,self::GOODSFIELD,self::GOODSLISTFIELD,$condition,$page,$limit,$orderBy,$isPage);
        $goodsArray['list']=$this->arr->exceptMoreArrKey($goodsArray['list'],$needExceptKey);
        return $goodsArray['list'];
    }

}