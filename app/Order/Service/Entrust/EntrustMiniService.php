<?php


namespace App\Order\Service\Entrust;


class EntrustMiniService extends EntrustBaseService implements EntrustServiceInterface
{

    /**
     * 获取小程序代客下单商品信息服务
     * @param int $shop_id
     * @param int $mid
     * @param array $exceptKey
     * @return array
     * @author ran
     * @date 2021-04-05 11:17
     * mailbox 466180170@qq.com
     */
    public function getResourceMiniEntrustGoodsList($entrust_order_id,$exceptKey=[]){
        $needExceptKey=$extraCondition=[];
        return $this->getResourceCommonEntrustGoodsList(array_merge($needExceptKey,$exceptKey),$entrust_order_id,1,20,$extraCondition,false);
    }

}