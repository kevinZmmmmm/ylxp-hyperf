<?php


namespace App\Order\Process;


use Hyperf\AsyncQueue\Process\ConsumerProcess;
use Hyperf\Process\Annotation\Process;

/**
 * @Process()
 */
class CancelOrderProcess extends ConsumerProcess
{
    protected $queue = 'order';
}