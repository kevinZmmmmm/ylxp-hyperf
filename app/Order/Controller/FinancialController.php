<?php

declare(strict_types=1);

namespace App\Order\Controller;

use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Common\Service\Arr;
use App\Order\Service\FinancialService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * 财务报表
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 * Class AnalysisController
 * @package App\Order\Controller
 */
class FinancialController extends AbstractController
{
    /**
     * @Inject()
     * @var FinancialService
     */
    private $financialService;

    /**
     * @Inject()
     * @var Arr
     */
    private $arr;

    /**
     * 订单数据分析
     *
     * @param RequestInterface $request
     * @RequestMapping(path="/v1/financial/order", methods="get")
     *
     * @return array
     */
    public function financialOrder(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 999999;
        $field = ['*'];
        $res = $this->financialService->financialOrder($params, (int)$perPage, $field, $params['order_by'] ?? 'time', $params['order'] ?? 'desc');
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 订单数据总计
     * @RequestMapping(path="/v1/financial/order/total", methods="get")
     * @return array
     */
//    public function financialOrderTotal()
//    {
//        $res = $this->financialService->financialOrderTotal('1');
//        if (!$res['code']) {
//            return $this->success($res['data'], '获取成功');
//        } else {
//            return $this->failed($res['code']);
//        }
//    }

    /**
     * 门店经营对比
     * @RequestMapping(path="/v1/financial/orderData/total", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function financialOrderDateTotal(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->financialService->financialOrderDateTotal($params, $params['order_by'] ?? 'sort', $params['order'] ?? 'asc', 1);
        if (!$res['code']) {
            // 特殊字段数组排序
            if(in_array($params['order_by'],['receivables_price', 'real_recharge_price', 'gross_profit_receivable', 'gross_profit_net_receipts','original_amount_of_goods'])){
                $listArr = $res['data'] -> toArray();
                $res['data'] = array_values($this -> arr -> arraySort($listArr, $params['order_by'] ?? 'time', $params['order'] ?? 'desc'));
            }
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 门店经营环比
     * @RequestMapping(path="/v1/financial/storeOperationMom", methods="get")
     *
     * @param RequestInterface $request
     * @return array
     */
    public function storeOperationMom(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->financialService->storeOperationMom($params, $params['order_by'] ?? 'sort', $params['order'] ?? 'desc');
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 门店经营对账
     * @RequestMapping(path="/v1/financial/storeOperationReconcilia", methods="get")
     *
     * @param RequestInterface $request
     * @return array
     */
    public function storeOperationReconcilia(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->financialService->financialOrderDateTotal($params, $params['order_by'] ?? 'sort', $params['order'] ?? 'asc', null);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 商品数据分析
     * @RequestMapping(path="/v1/financial/goods", methods="get")
     * @return array
     * @author liule
     */
    public function financialGoods()
    {
        $params = $this->request->all();
        $res = $this->financialService->financialGoods($params);

        if (is_numeric($params['perpage'])) {
            return $this->success($res->items(), '获取数据成功', $res->total());
        } else {
            return $this->success($res, '获取数据成功', $res->count());
        }
    }

    /**
     * 商品数据总计
     * @param RequestInterface $request
     * @RequestMapping(path="/v1/financial/goods/total", methods="get")
     * @return array
     */
    public function financialGoodsTotal(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->financialService->financialGoodsTotal($params);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 商品销售 TOP 20
     * @param RequestInterface $request
     * @RequestMapping(path="/v1/financial/goods/rank", methods="get")
     * @return array
     */
    public function financialGoodsRank(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->financialService->financialGoodsRank($params);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 免单订单数据分析
     *
     * @param RequestInterface $request
     * @RequestMapping(path="/v1/financial/freeOrder", methods="get")
     *
     * @return array
     */
    public function financialFreeOrder(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 999999;
        $field = ['*'];
        $params['type'] = 'free';
        $res = $this->financialService->financialOrder($params, (int)$perPage, $field, $params['order_by'] ?? 'time', $params['order'] ?? 'desc');
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }


}
