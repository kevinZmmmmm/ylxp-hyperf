<?php

declare(strict_types=1);

namespace App\Order\Controller\Order;

use App\Common\Constants\ErrorCode;
use App\Common\Constants\Stakeholder;
use App\Common\Controller\AbstractController;
use App\Order\Service\OrderRefundService;
use App\Order\Service\OrderService;
use App\Order\Service\Order\OrderMiniService;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * 相关订单模块服务(小程序、商家端、web后台)
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 * Class OrderController
 * @package App\Order\Controller
 */
class OrderMiniController extends AbstractController
{

    /**
     * @Inject()
     * @var OrderMiniService
     */
    private $orderMiniService;

    /**
     * @Inject()
     * @var OrderService
     */
    private $orderService;
    /**
     * @Inject()
     * @var OrderRefundService
     */
    private $orderRefundService;


    /**
     * 获取及时达代客下单确认订单数据
     * @RequestMapping(path="/v1/api/entrust/order/jsd/info", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getJsdEntrustOrderInfo(RequestInterface $request): array
    {
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        $param['entrust_id'] = $request->input('entrust_id');
        $param['deal_type'] = $request->input('deal_type');
        if (empty($param['deal_type']) || empty($param['mid']) || empty($param['entrust_id'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if (!in_array($param['deal_type'], [$this->orderService::ORDER_DEAL_TYPE_1, $this->orderService::ORDER_DEAL_TYPE_2])) {
            return $this->failed(ErrorCode::ORDER_DEAL_ERROR);
        }
        $res = $this->orderMiniService->getJsdEntrustOrderInfo($param);
        if ($res['code']) {
            return $this->failed($res['code']);
        }
        return $this->success($res['data']);
    }

    /**
     * 获取次日达代客下单确认订单数据
     * @RequestMapping(path="/v1/api/entrust/order/crd/info", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getCrdEntrustOrderInfo(RequestInterface $request): array
    {
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
//        $param['mid'] = 34;
        $param['entrust_id'] = $request->input('entrust_id');
        if (empty($param['tag']) || empty($param['mid']) || empty($param['entrust_id'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderMiniService->getCrdEntrustOrderInfo($param);
        if ($res['code']) {
            return $this->failed($res['code']);
        }
        return $this->success($res['data']);
    }

    /**
     * 及时达生成代客下单订单
     * @RequestMapping(path="/v1/api/entrust/order/jsd/confirm", methods="post")
     * @param RequestInterface $request
     * @return array
     */
    public function confirmEntrustJsdOrder(RequestInterface $request): array
    {
        if (!$this->orderService->getSystemStatus()) {
            return $this->failed(ErrorCode::SHOP_STATUS_CLOSE);
        }
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        if (empty($param['entrust_id']) || empty($param['shop_id']) || empty($param['deal_type']) || empty($param['total_price']) || empty($param['pay_type']) || empty($param['mid']) || empty($param['appointment_time'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if (!in_array($param['deal_type'], [$this->orderService::ORDER_DEAL_TYPE_1, $this->orderService::ORDER_DEAL_TYPE_2])) {
            return $this->failed(ErrorCode::ORDER_DEAL_ERROR);
        }
        if ($param['deal_type'] == $this->orderService::ORDER_DEAL_TYPE_1) {
            if (!$param['address_id']) {
                return $this->failed(ErrorCode::ORDER_ADDRESS_ERROR);
            }
        }
        if (empty($param['order_type']) && $param['order_type'] != 0) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if ($param['total_price'] < 0) {
            return $this->failed(ErrorCode::ORDER_PRICE_ERROR);
        }

        $res = $this->orderMiniService->confirmEntrustJsdOrder($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg'] ?? '');
        } else {
            return $this->success(['order_no' => $res['order_no']]);
        }

    }

    /**
     * 次日达生成代客下单订单
     * @RequestMapping(path="/v1/api/entrust/order/crd/confirm", methods="post")
     * @param RequestInterface $request
     * @return array
     */
    public function confirmEntrustCrdOrder(RequestInterface $request): array
    {
        if (!$this->orderService->getSystemStatus()) {
            return $this->failed(ErrorCode::SHOP_STATUS_CLOSE);
        }
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        if (empty($param['entrust_id']) || empty($param['shop_id']) || empty($param['total_price']) || empty($param['pay_type']) || empty($param['mid']) || empty($param['appointment_time'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if ($param['total_price'] < 0) {
            return $this->failed(ErrorCode::ORDER_PRICE_ERROR);
        }
        $res = $this->orderMiniService->confirmEntrustCrdOrder($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg'] ?? '');
        } else {
            return $this->success(['order_no' => $res['order_no']]);
        }

    }
    /**
     * 及时达订单列表
     * @RequestMapping(path="/v1/api/order/timely", methods="get")
     * @return array
     */
    public function timelyOrderList()
    {
        $param = $this->request->all();
        $param['mid'] = $this->request->getAttribute('mid');
        if (!isset($param['flag']) || empty($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if (!is_numeric($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID, '登陆信息失效,请重新登陆噢');
        }
        $field = [
            'order.order_no',
            'order.mid',
            'shop.shop_name',
            'order.shop_id',
            'order.is_pay',
            'order.pay_type',
            'order.order_type',
            'order.order_goods_num',
            'order.total_price',
            'order.appointment_time',
            'order.status',
            'order.create_at',
            'order.activity_id',
            'order.activity_type'
        ];
        $res = $this->orderMiniService->getTimelyOrderList($param, $field);
        if (isset($param['perpage']) && !empty($param['perpage'])) {
            return $this->success($res->items(), '获取列表成功', $res->total());
        } else {
            return $this->success($res, '获取列表成功', $res->count());
        }
    }

    /**
     * 次日达订单列表
     * @RequestMapping(path="/v1/api/order/tomorrow", methods="get")
     * @return array
     */
    public function tomorrowOrderList()
    {
        $param = $this->request->all();
        $param['mid'] = $this->request->getAttribute('mid');
        if (!isset($param['flag']) || empty($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $param['mid'] = $this->request->getAttribute('mid');

        if (!is_numeric($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID, '登陆信息失效,请重新登陆噢');
        }
        $field = [
            'order.order_no',
            'order.mid',
            'shop.shop_name',
            'order.shop_id',
            'order.is_pay',
            'order.pay_type',
            'order.order_type',
            'order.order_goods_num',
            'order.total_price',
            'order.appointment_time',
            'order.status',
            'order.create_at',
            'order.activity_id',
            'order.activity_type'
        ];
        $res = $this->orderMiniService->getNextDayOrderList($param, $field);
        if (isset($param['perpage']) && !empty($param['perpage'])) {
            return $this->success($res->items(), '获取列表成功', $res->total());
        } else {
            return $this->success($res, '获取列表成功', $res->count());
        }
    }


    /**
     * 小程序订单详情
     * @RequestMapping(path="/v1/api/order", methods="get")
     * @return array
     */
    public function miniOrder(){
        $param = $this->request->all();
        if (empty($param['type'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID, '订单详情类型错误');
        }
        if (in_array($param['type'], ['basic', 'group'])){
            if (empty($param['order_no'])) {
                return $this->failed(ErrorCode::SYSTEM_INVALID, '订单编号错误');
            }
            $where = [
                ['order.order_no', '=', $param['order_no']]
            ];
            $field = ['order.order_no', 'order.shop_id', 'order.mid', 'order.is_pay', 'order.status', 'order.create_at','order.address_id',
                'order.pay_type','order.pay_at','order.refund_at','order.complete_date','order.goods_price','order.freight_price','order.order_type_status',
                'order.total_price','order.order_type','order.deal_type','order.coupon_cut', 'order.appointment_time', 'order.memo','order.need_invited_num',
                'order.write_off_code','order.complete_date','order.entrust_discount','(order.total_price + order.coupon_cut) as no_coupon_price','order.order_goods_num',
                'order.is_freed','order.free_num','order.free_step','order.activity_id','order.free_activation_time','shop.shop_name', 'shop.shop_tel',
                'shop.address as shop_address', 'shop.shop_desc', 'shop.longitude', 'shop.latitude'];
        }
        $res=[];
        switch ($param['type']) {
            case 'basic':
                // 通用订单详情
                $res = $this->orderMiniService->orderDetail($where, $field);
                break;
            case 'group':
                // 团购订单详情
                $res = $this->orderMiniService->getGroupOrderDetail($where, $field, $param);
                break;
            case 'refund':
                if (empty($param['refund_no'])) {
                    return $this->failed(ErrorCode::SYSTEM_INVALID, '退款编号错误');
                }
                $where = ['refund_no' => $param['refund_no']];
                $field = ['refund_no','order_no','refund_memo','refund_money','refund_at','create_at','is_whole',
                    'handle_type','status','deal_type','order_refund_status'];
                // 售后订单详情
                $res = $this->orderRefundService->getOrderRefundDetail($where, $field);
                break;
            default:
                break;
        }
        return $this->success($res, '获取订单详情成功');
    }

    /**
     * 小程序取消订单、确认收货
     * @PutMapping(path="/v1/api/order/{order_no}", methods="put")
     * @param string $order_no
     * @return array
     */
    public function changeOrderStatus(string $order_no){
        $event = $this->request->input('event');
        if (!in_array($event, ['cancel', 'confirm'])){
            return $this->failed(ErrorCode::SYSTEM_INVALID, '参数错误');
        }
        $where =[];
        $data = [];
        switch ($event) {
            case 'cancel':
                $where = ['order_no' => $order_no];
                $data = ['status' => Stakeholder::ORDER_CANCEL];
                break;
            case 'confirm':
                $where = ['order_no' => $order_no];
                $data = ['status' => Stakeholder::ORDER_RECEIVED, 'complete_date' => date('Y-m-d H:i:s')];
                break;
            default:
                break;
        }
        $res =  $this->orderMiniService->update($event,$where, $data);
        if($res){
            return $this->success([], '操作成功');
        }
        return $this->failed(ErrorCode::NOT_IN_FORCE,'操作未生效，请刷新后重试');
    }
    /**
     * 及时达生成订单
     * @RequestMapping(path="/v1/api/order/confirm", methods="post")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function confirmOrder(RequestInterface $request)
    {
        if(!$this->orderService->getSystemStatus()){
            return $this->failed(ErrorCode::SHOP_STATUS_CLOSE);
        }
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        if (empty($param['shop_id']) || empty($param['deal_type']) || empty($param['total_price']) || empty($param['pay_type']) || empty($param['mid'])||empty($param['appointment_time'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if (!in_array($param['deal_type'], [$this->orderService::ORDER_DEAL_TYPE_1, $this->orderService::ORDER_DEAL_TYPE_2])) {
            return $this->failed(ErrorCode::ORDER_DEAL_ERROR);
        }
        if ($param['deal_type'] == $this->orderService::ORDER_DEAL_TYPE_1) {
            if (!$param['address_id']) {
                return $this->failed(ErrorCode::ORDER_ADDRESS_ERROR);
            }
        }
        if (empty($param['order_type']) && $param['order_type'] != 0) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if ($param['total_price'] < 0) {
            return $this->failed(ErrorCode::ORDER_PRICE_ERROR);
        }

        $res = $this->orderMiniService->confirmOrder($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg'] ?? '');
        } else {
            return $this->success(['order_no' => $res['order_no']]);
        }

    }

    /**
     * 次日达生成订单接口
     * @RequestMapping(path="/v1/api/order/confirm/tomorrow", methods="post")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function confirmOrderMorrow(RequestInterface $request)
    {
        if(!$this->orderService->getSystemStatus()){
            return $this->failed(ErrorCode::SHOP_STATUS_CLOSE);
        }
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        if (empty($param['shop_id']) || empty($param['total_price']) || empty($param['pay_type']) || empty($param['mid'])||empty($param['appointment_time'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if ($param['total_price'] <= 0) {
            return $this->failed(ErrorCode::ORDER_PRICE_ERROR);
        }
        $res = $this->orderMiniService->confirmOrderTomorrow($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg'] ?? '');
        } else {
            return $this->success(['order_no' => $res['order_no']]);
        }

    }

    /**
     * 获取及时达确认订单数据
     * @RequestMapping(path="/v1/api/order/confirm/info", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function getConfirmOrderInfo(RequestInterface $request)
    {
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        if (empty($param['shop_id']) || empty($param['deal_type']) || empty($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        if (!in_array($param['deal_type'], [$this->orderService::ORDER_DEAL_TYPE_1, $this->orderService::ORDER_DEAL_TYPE_2])) {
            return $this->failed(ErrorCode::ORDER_DEAL_ERROR);
        }
        $res = $this->orderMiniService->getConfirmOrderInfo($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg']);
        }
        return $this->success($res['data']);
    }

    /**
     * tag  1店铺2团长
     * 获取次日达确认订单数据
     * @RequestMapping(path="/v1/api/order/confirm/tomorrow/info", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function getConfirmOrderInfoTomorrow(RequestInterface $request)
    {
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');

        if (empty($param['shop_id']) || empty($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderMiniService->getConfirmOrderInfoTomorrow($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg']);
        }
        return $this->success($res['data']);
    }

    /**
     * 业务场景（次日达小程序创建订单订单信息）
     * @RequestMapping(path="/v1/api/order/createOverNightShopOrderInfo", methods="post")
     * @author ran
     * @date 2021-04-12 14:27
     * mailbox 466180170@qq.com
     */
    public function createMiniOverNightShopOrderInfo(RequestInterface $request):array
    {
        $params = $request->all();
        $mid = $request->getAttribute('mid');
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if(empty($mid)) return $this->failed(ErrorCode::ERROR_MINI_USER);
        if(empty($params['shop_id']) || empty($params['appointment_time']) || empty($params['pay_type']) || empty($params['platform_type'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->orderMiniService->createOverNightShopOrderInfo((int)$mid,(array)$params);
        if ($res['code']) return $this->failed($res['code'], $res['msg']);
        return $this->success($res['data'],'创建资源成功');
    }

    /**
     * 业务场景（次日达小程序获取确认订单信息）
     * @RequestMapping(path="/v1/api/order/getOverNightShopConfirmOrderInfo", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-04-02 9:41
     * mailbox 466180170@qq.com
     */
    public function getMiniOverNightShopConfirmOrderInfo(RequestInterface $request)
    {
        $params = $request->all();
        $mid = $request->getAttribute('mid');
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if(empty($mid)) return $this->failed(ErrorCode::ERROR_MINI_USER);
        if(empty($params['shop_id']) || empty($params['platform_type'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        switch ($params['platform_type']){
            case Stakeholder::ORDER_PLATFORM_TYPE_1:
                break;
            case Stakeholder::ORDER_PLATFORM_TYPE_2:
                break;
            case Stakeholder::ORDER_PLATFORM_TYPE_3:
                if(!isset($params['entrust_order_id']) && empty($params['entrust_order_id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
                break;
        }
        $res = $this->orderMiniService->getOverNightShopConfirmOrderInfo((array)$params,(int)$mid);
        if ($res['code']) return $this->failed($res['code'], $res['msg']);
        return $this->success($res['data'],'获取资源成功');
    }

    /**
     * 免单活动确认订单信息
     * @RequestMapping(path="/v1/api/order/getFreeActivityConfirmOrderInfo", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-23 21:45
     * mailbox 466180170@qq.com
     */
    public function getMiniFreeActivityConfirmInfo(RequestInterface $request)
    {
        $param = $request->all();
        if(empty($param)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if(empty($param['shop_id']) || empty($param['activity_goods_id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->orderMiniService->getFreeActivityConfirmInfo((int)$param['shop_id'],(int)$param['activity_goods_id']);
        if ($res['code']) return $this->failed($res['code'], $res['msg']);
        return $this->success($res['data'],'获取资源成功');
    }
    /**
     * 免单活动生成订单信息
     * @RequestMapping(path="/v1/api/order/createFreeActivityOrderInfo", methods="post")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-23 21:45
     * mailbox 466180170@qq.com
     */
    public function createMiniFreeActivityOrderInfo(RequestInterface $request)
    {
        $params = $request->all();
        $mid = $request->getAttribute('mid');
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if(empty($mid)) return $this->failed(ErrorCode::ERROR_MINI_USER);
        if(empty($params['shop_id']) || empty($params['activity_goods_id']) || empty($params['appointment_time']) || empty($params['pay_type'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->orderMiniService->createFreeActivityOrderInfo((int)$mid,(array)$params);
        if ($res['code']) return $this->failed($res['code'], $res['msg']??'');
        return $this->success($res['data'],'创建资源成功');
    }
    /**
     * 待废弃
     * 立即购买获取活动商品信息
     * @RequestMapping(path="/v1/api/order/confirm/activity/info", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function getActivityConfirmInfo(RequestInterface $request)
    {
        $param = $request->all();
        if (empty($param['shop_id']) || empty($param['activityID'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderMiniService->getActivityConfirmInfo($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg']);
        }
        return $this->success($res['data']);
    }

    /**
     * 待废弃
     * 活动立即购买生成订单
     * @RequestMapping(path="/v1/api/order/confirm/activity", methods="post")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function activityConfirmOrder(RequestInterface $request)
    {
        if(!$this->orderService->getSystemStatus()){
            return $this->failed(ErrorCode::SHOP_STATUS_CLOSE);
        }
        $param = $request->all();
        $param['mid'] = $request->getAttribute('mid');
        if (empty($param['shop_id']) || empty($param['pay_type']) || empty($param['mid']) || empty($param['activityID'])||empty($param['appointment_time'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }

        if (empty($param['order_type'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }

        $res = $this->orderMiniService->activityConfirmOrder($param);
        if ($res['code']) {
            return $this->failed($res['code'], $res['msg'] ?? '');
        } else {
            return $this->success(['order_no' => $res['order_no']]);
        }
    }
    /**
     * 分享订单  mid  下单用户
     * @RequestMapping(path="/v1/api/order/share", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function orderShare(RequestInterface $request)
    {
        $params = $request->all();
        $params['mid'] = $request->getAttribute('mid');
        $mid = (int)$params['mid'];
        $orderNo = $params['order_no'];
        if(empty($mid)||empty($orderNo)){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $order = $this->orderMiniService->shareOrderDetail($mid,$orderNo);
        $share = [
            'url'=>'pages/follow-goods-share',
            'title'=>'好友叫你来买菜啦~',
            'img'=>'https://yiluxiaopao.oss-cn-beijing.aliyuncs.com/ylxp-admin/202012161050281791608087028.png'
        ];
        //分享title 图片  地址
        return $this->success(['order'=>$order,'share'=>$share]);
    }

    /**
     *
     * 分享订单商品列表   mid  通过分享进入用户
     * @RequestMapping(path="/v1/api/order/share/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function orderShareGoods(RequestInterface $request)
    {

        $params = $request->all();
        $params['mid'] = $request->getAttribute('mid');
        $mid = (int)$params['mid'];
        $orderNo = $params['order_no'];
        if(empty($mid)||empty($orderNo)){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $goods = $this->orderMiniService->shareGoods($mid,$orderNo);
        $goods['totalMoney'] =ceil($goods['totalMoney']);
        $goods['discountMoney'] =ceil($goods['discountMoney']);
        return $this->success($goods);
    }
    /**
     * 年度账单
     * @RequestMapping(path="/v1/api/order/bill", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author lulongfei
     */
    public function orderBill(RequestInterface $request)
    {
        $params = $request->all();
        $params['mid'] = $request->getAttribute('mid');
        $mid = (int)$params['mid'];
        if (empty($mid)) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderMiniService->userOrderBill($mid);
        return $this->success($res, '成功');
    }


    /**
     * 我的免单订单列表
     * @RequestMapping(path="/v1/api/free/order", methods="get")
     * @author 1iu
     * @date 2021-03-20 15:43
     */
    public function inviteOrder(){
        $request = $this->request;
        $mid = (int) $request->getAttribute('mid');
        $perpage = (int) $request->input('perpage', 10);
        $res = $this->orderMiniService->getInviteOrder($mid, $perpage);
        return $this->success($res->items(), '获取列表成功', $res->total());
    }

    /**
     * 重新激活
     * @RequestMapping(path="/v1/api/free/restart", methods="put")
     * @return array
     * @author 1iu
     * @date 2021-03-23 9:45
     */
    public function reactivate(){
        $request = $this->request;
        $mid = (int) $request->getAttribute('mid');
        $order_no = $request->input('order_no','');
        if (empty($order_no)) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->orderMiniService->restartInvite($mid, $order_no);
        if ($res['code']) return $this->success($res['data'], $res['msg']);
        return $this->failed($res['errorCode'], $res['msg']);
    }

    /**
     * 免单商品详情
     * @RequestMapping(path="/v1/api/free/goods/detail", methods="get")
     * @return array
     * @author 1iu
     * @date 2021-03-23 21:10
     */
    public function inviteGoodsDetail(){
        $request = $this->request;
        $mid = (int) $request->getAttribute('mid');
        $free_parent_order_no = $request->input('free_parent_order_no','');
        $act_goods_id = (int) $request->input('activity_goods_id',0);
        if (!$act_goods_id && empty($free_parent_order_no)) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->orderMiniService->inviteGoodsDetail($mid, $act_goods_id, $free_parent_order_no);
        if ($res['code']) return $this->success($res['data'], $res['msg']);
        return $this->failed($res['errorCode'], $res['msg']);
    }

    /**
     * 个人中心我的免单
     *
     * @RequestMapping(path="/v1/api/free/center", methods="get")
     * @return array
     */
    public function freeCenter()
    {
        $request = $this->request;
        $mid = $request->getAttribute('mid');
        if (!$mid) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderMiniService->getFreeCenter($mid);
        return $this->success($res->items(), '获取成功', $res->total());
    }

}
