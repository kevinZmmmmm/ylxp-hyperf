<?php

declare(strict_types=1);

namespace App\Order\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Order\Service\OperateService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * 运营分析
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 * Class AnalysisController
 * @package App\Order\Controller
 */
class OperateController extends AbstractController
{
    /**
     * @Inject()
     * @var OperateService
     */
    private $operateService;

    /**
     * 门店分析
     * @RequestMapping(path="/v1/operate/shop", methods="get")
     * @return array
     */
    public function operateShop()
    {
        $params = $this->request->all();
        $res = $this->operateService->operateShop($params);

        if (isset($params['perpage']) && !empty($params['perpage'])) {
            return $this->success($res->items(), '获取数据成功', $res->total());
        } else {
            return $this->success($res, '获取数据成功', $res->count());
        }

    }

    /**
     * 门店图表分析
     * @RequestMapping(path="/v1/operate/shop/{shop_id}/chart", methods="get")
     * @param string $shop_id
     * @return array
     */
    public function operateShopChart(string $shop_id)
    {
        $res = $this->operateService->operateShopChart($shop_id);
        return $this->success($res, '获取数据成功');
    }

    /**
     * 商品销售分析
     * @RequestMapping(path="/v1/operate/goods", methods="get")
     * @return array
     */
    public function operateGoodsSale()
    {
        $params = $this->request->all();
        if (empty($params['date']) || empty($params['contrasttime']) || empty($params['flag'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $params['order_source'] = $params['order_source'] ?? '';
        $res = $this->operateService->operateGoodsSale($params);

        if (isset($params['perpage']) && !empty($params['perpage'])) {
            return $this->success($res->items(), '获取数据成功', $res->total());
        } else {
            return $this->success($res, '获取数据成功', $res->count());
        }
    }

    /**
     * 分类销售明细
     * @RequestMapping(path="/v1/operate/cate/chart", methods="get")
     * @return array
     */
    public function operateCateChart()
    {
        $params = $this->request->all();
        if (empty($params['date']) || empty($params['contrasttime']) || empty($params['flag'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->operateService->operateCateChart($params);
        return $this->success($res, '获取数据成功');
    }

    /**
     * 商品图表分析
     * @RequestMapping(path="/v1/operate/goods/chart", methods="get")
     * @return array
     */
    public function operateGoodsChart()
    {
        $params = $this->request->all();
        if (empty($params['date']) || empty($params['contrasttime']) || empty($params['flag'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->operateService->operateGoodsChart($params);
        return $this->success($res, '获取数据成功');
    }

    /**
     * 会员概览
     * @RequestMapping(path="/v1/operate/member/overview", methods="get")
     * @return array
     */
    public function operateMemberChart()
    {
        $params = $this->request->all();
        if (empty($params['date']) || empty($params['contrasttime']) || empty($params['flag'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->operateService->operateMemberChart($params);
        return $this->success($res, '获取数据成功');
    }

    /**
     * 会员来源渠道
     * @RequestMapping(path="/v1/operate/member/source", methods="get")
     * @return array
     */
    public function operateMemberSourceChart()
    {
        $params = $this->request->all();
        if (empty($params['date']) || empty($params['contrasttime']) || empty($params['flag'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->operateService->operateMemberSourceChart($params);
        return $this->success($res, '获取数据成功');
    }

    /**
     * 会员来源门店
     * @RequestMapping(path="/v1/operate/member/shop", methods="get")
     * @return array
     */
    public function operateMemberShop()
    {
        $params = $this->request->all();
        if (empty($params['date']) || empty($params['contrasttime']) || empty($params['flag']) || empty($params['page']) || empty($params['perpage'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->operateService->operateMemberSource($params);
        return $this->success($res->items(), '获取数据成功', $res->total());
    }

    /**
     * 拼团活动数据分析
     *
     * @return array
     * @RequestMapping(path="/v1/activity/group/analysis", methods="get")
     *
     */
    public function activityGroupAnalysis()
    {
        $params = $this->request->all();
        $field = ['activityID', 'title', 'begin_date', 'end_date'];

        $res = $this->operateService->activityAnalysis($params, $field);

        if (isset($params['perpage']) && !empty($params['perpage'])) {
            return $this->success($res->items(), '获取数据成功', $res->total());
        } else {
            return $this->success($res, '获取数据成功', $res->count());
        }
    }


}
