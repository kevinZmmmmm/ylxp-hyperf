<?php

declare(strict_types=1);

namespace App\Order\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Order\Service\FinancialLogService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Activity\Request\QuotaActivityRequest;
use App\Activity\Service\ActivityService;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PutMapping;

/**
 * @Controller()
 *  * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class FinancialLogController extends AbstractController
{
    /**
     * @Inject()
     * @var FinancialLogService
     */
    private $financialLogService;

    /**
     * 吾享订单退款日志
     * @RequestMapping(path="/v1/financialLog/wx", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function wxFinancialLog(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['*'];
        $res = $this->financialLogService->RefundList($params, $field, (int)$perPage, 'wx');
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 客至订单退款日志
     * @RequestMapping(path="/v1/financialLog/kz", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function kzFinancialLog(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['*'];
        $res = $this->financialLogService->RefundList($params, $field, (int)$perPage, 'kz');
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 外卖日志
     * @RequestMapping(path="/v1/dePush", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function DePushList(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['*'];
        $res = $this->financialLogService->dePushList($params, $field, (int)$perPage);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小跑吾享回调日志
     * @RequestMapping(path="/v1/financialLog/wxCallback", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function wxCallbackLog(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['*'];
        $res = $this->financialLogService->xiaoPaoLog($params, $field, (int)$perPage, 'wxCallback');
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小跑支付日志
     * @RequestMapping(path="/v1/financialLog/payment", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function paymentLog(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['*'];
        $res = $this->financialLogService->xiaoPaoLog($params, $field, (int)$perPage, 'payment');
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小跑下单日志
     * @RequestMapping(path="/v1/financialLog/placeOrder", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function placeOrderLog(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['*'];
        $res = $this->financialLogService->xiaoPaoLog($params, $field, (int)$perPage, 'placeOrder');
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }


}
