<?php
declare(strict_types=1);

namespace App\Order\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Common\Middleware\AesMiddleware;
use App\Order\Request\OrderRequest;
use App\Order\Service\Order\OrderBusinessService;
use App\Order\Service\OrderRefundService;
use App\Order\Service\OrderService;
use App\Order\Service\ShopOrderService;
use App\Resource\Service\GoodsService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * 相关订单模块服务(小程序、商家端、web后台)
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 * Class OrderController
 * @package App\Order\Controller
 */
class OrderController extends AbstractController
{
    /**
     * @Inject()
     * @var OrderService
     */
    private $orderService;
    /**
     * @Inject()
     * @var ShopOrderService
     */
    private $shopOrderService;

    /**
     * @Inject()
     * @var OrderRefundService
     */
    private $orderRefundService;

    /**
     * @Inject()
     * @var GoodsService
     */
    private $goodsService;

    /**
     * @Inject()
     * @var OrderBusinessService
     */
    private $orderBusinessService;

    /**
     * 订单列表
     *  20210101114200 返回意向提货时间
     * @RequestMapping(path="/v1/order", methods="get")
     * @param OrderRequest $orderRequest
     * @return array
     * @author liule
     */
    public function index(OrderRequest $orderRequest)
    {
        $where = $orderRequest->all();
        $orderListField = [
            'order.order_no',
            'order.free_parent_order_no',
            'order.shop_id',
            'order.mid',
            'order.freight_price',
            'order.total_price',
            'order.is_pay',
            'order.status',
            'order.create_at',
            'order.deal_type',
            'order.pay_type',
            'order.order_phone',
            'order.is_whole',
            'order.order_type',
            'order.pay_at',
            'order.refund_at',
            'order.address_id',
            'order.complete_date',
            'order.coupon_freight_cut',
            'order.coupon_cut',
            'order.order_source',
            'order.order_commission',
            'order.write_off_code',
            'order.appointment_date',
            'order.is_freed',
            'order.free_num',
            'goods.order_goods_name',
            'shop.shop_name',
            'member.nickname',
            'member.phone as mem_phone',
            'addr.username as addr_username',
            'addr.phone as addr_phone',
        ];
        $res = $this->orderService->list($where, $orderListField);
        if (isset($where['perpage']) && !empty($where['perpage'])) {
            return $this->success($res->items(), '获取订单列表成功', $res->total());
        } else {
            return $this->success($res, '获取订单列表成功', $res->count());
        }

    }

    /**
     * 财务订单导出
     * 20210101114200 增加意向提货时间筛选
     * @RequestMapping(path="/v1/order/export", methods="get")
     * @return array
     * @author liule
     */
    public function export()
    {
        $param = $this->request->all();
        $flag = $param['flag'] ?? 1;
        if (($flag == 1) && empty($param['pay_at']) && empty($param['create_at']))
            return $this->failed(ErrorCode::SYSTEM_INVALID, '请选择时间段(支付时间或下单时间)!!!');
        if (in_array($flag, [2,3]) && empty($param['pay_at']) && empty($param['create_at']) && empty($param['appointment_date']))
            return $this->failed(ErrorCode::SYSTEM_INVALID, '请选择时间段(支付时间、下单时间或意向提货时间)!!!');
        switch ($flag) {
            case 1:
                if (!empty($param['appointment_date'])) $param['appointment_date'] = '';
                // 财务订单导出
                $orderListField = [
                    'order.order_no',
                    'shop.shop_name',
                    'order.pay_no',
                    'order.pay_type',
                    'order.freight_price',
                    'order.goods_price',
                    'order.total_price',
                    'order.order_phone',
                    'order.pay_at',
                    'order.refund_at',
                    'order.create_at',
                    'order.deNo',
                    'order.write_off_code',
                    'order.coupon_cut',
                    'order.coupon_freight_cut',
                    'order.order_type',
                    'order.order_commission',
                    'goods.order_goods_name'
                ];
                break;
            case 2:
                // 配货单导出
                $orderListField = [
                    'order.shop_id',
                    'og.goods_id',
                    'og.goods_title',
                    'sum(og.number) as num'
                ];
                break;
            case 3:
                // 疏东坡模板导出
                $orderListField = [
                    'order.shop_id',
                    'shop.shop_sdp_user_code',
                    '(og.itemCode - 2000) as goods_code',
                    'g.lsy_goods_name as goods_title',
                    "g.lsy_unit_name as unit",
                    "if(g.scant_id=0, sum(og.number), (sum(og.number) * g.ratio) / 1000) as num",
                    "DATE_FORMAT(DATE_SUB(curdate(),INTERVAL -1 DAY), '%Y/%m/%d') as ship_date"
                ];
                break;
            default:
        }

        $res = $this->orderService->export($param, $orderListField, (int)$flag);

        return $this->success($res, '获取订单列表成功', count($res));

    }

    /**
     * 订单详情基本信息
     *
     * @RequestMapping(path="/v1/order/{order_no}", methods="get")
     * @param string $order_no
     * @return array
     */
    public function orderInfo(string $order_no)
    {
        $where = ['order_no' => $order_no];
        $field = ['order_no', 'shop_id', 'mid', 'is_pay', 'status', 'create_at', 'deal_type', 'pay_type', 'order_phone',
            'address_id', 'order_type', 'pay_at', 'refund_at', 'complete_date', 'appointment_time', 'is_whole','order_source','order_commission','is_freed','free_num'];
        // 获取退款单号
        $refund = $this->orderRefundService->find($where, ['refund_no']);
        $res = $this->orderService->find($where, $field, $refund['refund_no'] ?? null);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }

    /**
     * 订单详情商品列表
     *
     * @RequestMapping(path="/v1/order/{order_no}/goods", methods="get")
     * @param string $order_no
     * @return array
     */
    public function orderGoods(string $order_no)
    {
        $where = ['order_no' => $order_no];
        $field = [
            'goods_id',
            'goods_logo',
            'goods_title',
            'selling_price',
            'goods_spec',
            'deductMoney',
            'number',
            'refund_number',
            'shop_refund_number',
            'refund_price',
            'dis_price',
            'goods_dis_price',
            'commission',
            'kz_self_num'
        ];
        $res = $this->orderService->findGoods($where, $field);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        $return = $this->success($res['data']['list'], $res['msg']);
        $return['total_amount'] = $res['data']['total_amount'];
        $return['total_commission'] = $res['data']['total_commission'];
        return $return;
    }

    /**
     * 订单详情商品信息
     *
     * @RequestMapping(path="/v1/order/{order_no}/cost", methods="get")
     * @param string $order_no
     * @return array
     */
    public function orderCost(string $order_no)
    {
        $where = ['order_no' => $order_no];
        $field = [
            'goods_price',
            'freight_price',
            'total_price',
            'coupon_cut'
        ];
        $res = $this->orderService->findCost($where, $field);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        $ress[] = $res['data']->toArray();
        return $this->success($ress, $res['msg']);
    }

    /**
     * @RequestMapping(path="/v1/order/{order_no}/free", methods="get")
     * @param string $order_no
     * @return array
     */
    public function orderFreeDetail(string $order_no)
    {
        $where = ['o.free_parent_order_no' => $order_no];
        $field = [
        'o.order_phone',
        'o.mid',
        'o.status',
        'o.order_no',
        'o.refund_at',
        'o.create_at',
        'm.nickname',
        ];
        $res = $this->orderService->getOrderSub($where, $field);
        return $this->success($res,'获取列表成功');
    }


    /**
     * 部分退-确认退款
     * @RequestMapping(path="/v1/order/{order_no}/part", methods="post")
     * @param string $order_no
     * @return array
     */
    public function partRefund(string $order_no)
    {
        $refundParam = $this->request->input('goods_list', '');

        if (is_string($refundParam)) {
            $refundParam = json_decode($refundParam, true);
        }
        if (empty($refundParam)) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderRefundService->shopRefund($order_no, 1, $refundParam);

        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success([], $res['msg']);
    }

    /**
     * 全部退款
     * @RequestMapping(path="/v1/order/{order_no}/whole", methods="post")
     * @param string $order_no
     * @return array
     */
    public function wholeRefund(string $order_no)
    {

        $res = $this->orderRefundService->shopRefund($order_no, 0);

        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success([], $res['msg']);
    }


    /**
     * 代客下单 提交
     * @RequestMapping(path="/v1/app/entrust", methods="post")
     * @author 1iu
     * @date 2021-03-08 11:18
     */
    public function entrustOrder(){
        $request = $this->request;
        $shop_id = $request->getAttribute('shop_id');
        $phone = $request->getAttribute('phone');
        $params = [
            'goods_list' => $request->input('goods_list', ''),
            'discount' => $request->input('discount', 0),
            'dis_amount' => $request->input('dis_amount'),
            'order_source' => $request->input('order_source'),//0及时达，1次日达
        ];
        if (is_string($params['goods_list'])) $goods = json_decode($params['goods_list'], true);
        if (!is_numeric($params['discount']) || !is_numeric($params['dis_amount']) || !in_array($params['order_source'],[0,1]) || empty($goods))
            return $this->failed(ErrorCode::SYSTEM_INVALID);

        $res = $this->orderBusinessService->createEntrustOrder($shop_id, $phone, $goods, $params);

        if($res) return $this->success($res['data'], $res['msg']);
        return $this->failed($res['errorCode'], $res['msg']);
    }

    /**
     * 代客下单 列表
     * @RequestMapping(path="/v1/app/entrust", methods="get")
     * @author 1iu
     * @date 2021-03-08 11:18
     */
    public function entrustOrderList(){
        $request = $this->request;
        $shop_id = $request->getAttribute('shop_id');
        $phone = $request->getAttribute('phone');
        $click = $request->input('click','all');
        $perpage = $request->input('row');
        $keyword = $request->input('keyword');

        if (!is_numeric($perpage)) return $this->failed(ErrorCode::SYSTEM_INVALID, '缺少每页数量');
        switch ($click){
            case 'unpaid':
                $res = $this->orderBusinessService->getTheUnpaidEntrustOrder($shop_id,$phone,$perpage);
                break;
            case 'completed':
                $res = $this->orderBusinessService->getTheCompletedEntrustOrder($shop_id,$phone,$perpage,$keyword);
                break;
            case 'refund':
                $res = $this->orderBusinessService->getTheRefundEntrustOrder($shop_id,$phone,$perpage,$keyword);
                break;
            default:
                $res = $this->orderBusinessService->getAllEntrustOrder($shop_id,$phone,$perpage);
                return $this->success($res, '获取列表成功');
        }
        return $this->success($res->items(), '获取列表成功', $res->total());
    }


    /**
     * 代客待付款 详情
     * @RequestMapping(path="/v1/app/entrust/{id:\d+}", methods="get")
     * @param int $id
     * @return array
     * @author 1iu
     * @date 2021-03-10 15:07
     */
    public function entrustOrderDetail(int $id){
        $type = $this->request->input('type');
        if (!$type) return $this->failed(ErrorCode::SYSTEM_INVALID,'缺少type参数');
        switch ($type){
            case 'detail':
                $where = ['id' => $id];
                $field = ['id','order_source','amount','dis_amount','discount','status','created_at'];
                $res = $this->orderBusinessService->getEntrustOrderDetail($where, $field);
                break;
            case 'share':
                $where = ['id' => $id];
                $field = ['id','order_source','share','dis_amount'];
                $res = $this->orderBusinessService->restartPayment($where, $field);
                break;
            default:
        }
        return $this->success($res, '获取数据成功');
    }



    /**
     * 获取分享确认订单数据
     * @RequestMapping(path="/v1/api/order/confirm/share/info", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getConfirmOrderShareInfo(RequestInterface $request)
    {
        $param = $request->all();
        if (empty($param['shop_id']) || empty($param['mid'])) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }

        $cart = $this->orderService->checkCart((int)$param['mid'], $this->orderService::ORDER_SOURCE_1);
        if ($cart['code']) {
            return $this->failed($cart['code']);
        }
        $cartList = $cart['cartList'];
        $_goodsIdsArr = array_column($cartList, 'goods_id');
        $goodsIdsArr = $this->orderService->getGoodsIds($_goodsIdsArr,(int)$param['shop_id'])??[];
        $goodsInfo = $this->goodsService::getCrdGoodsInfoData($goodsIdsArr);
        $goodsById = array_column($goodsInfo, null, 'goods_id');
        $GoodsTotalMoney = $goodMarketTotalMoney= $GoodsNum = 0;
        foreach ($cartList as $k => $v) {
            if(!in_array($cartList[$k]['goods_id'],$goodsIdsArr)){
                unset($cartList[$k]);
                continue;
            }
            $cartList[$k] = array_merge($cartList[$k], $goodsById[$v['goods_id']]);
            $goodMarketTotalMoney += bcmul((string)$cartList[$k]['goods_num'], (string)$cartList[$k]['price_market'], 2);
            $GoodsTotalMoney += bcmul((string)$cartList[$k]['goods_num'], (string)$cartList[$k]['price_selling'], 2);
            $GoodsNum += $cartList[$k]['goods_num'];
        }
        sort($cartList);

        // 拼团人数团信息
        $activity_type = 0;
        $groupNum = 0;
        $shopInfo = $this->orderService::getInfoByShopId($param['shop_id'], ['shop_id', 'shop_name', 'address', 'shop_tel', 'shop_desc','end_time','start_time']);
        $goods_pick_time = max(array_column($goodsInfo,'goods_pick_time'));
        $peckTime = $this->orderService->timePeriodSettings($shopInfo,(string)$goods_pick_time);
        $data = [
            'activity_type' => $activity_type,
            'groupNum' => $groupNum,
            'shop_info' => $shopInfo,
            'goods_list' => $cartList,
            'goods_num' => $GoodsNum,
            'TotalMoney' => number_format($GoodsTotalMoney,2),
            'peckTime' => $peckTime,
            'discountMoney' => bcsub((string)$goodMarketTotalMoney,(string)$GoodsTotalMoney,2),
        ];

        return $this->success($data);
    }

    /**
     * 获取订单数量角标
     *
     * @RequestMapping(path="/v1/app/order/subscript", methods="get")
     *
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:26
     * Updated_at: 2020/12/29 0029 14:32
     */
    public function getOrderCount()
    {
        $shop_id = $this->request->input('shop_id');
        if (!$shop_id) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $ret = $this->shopOrderService->getOrderCount($shop_id);
        return $this->success($ret['data']);
    }

    /**
     * 订单列表
     *
     * @RequestMapping(path="/v1/app/order/shop2", methods="get")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:40
     * Updated_at: 2020/12/29 0029 14:42
     */
    public function getOrderListByShop(RequestInterface $request)
    {
        $row = $request->input('row', 10);
        $status = $request->input('status');
        $shop_id = $request->getAttribute('shop_id');
        if (!$shop_id || !$row || !$status) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $startDate = $request->input('start_time');
        !$startDate && $startDate = '1970-01-01';
        $endDate = $request->input('end_time', '2080-01-01');
        !$endDate && $endDate = '2080-01-01';
        $where = function ($q) use ($status, $shop_id, $startDate, $endDate) {
            if ($status == 1) {
                $q->where('store_order.status', 2);
                $q->where('store_order.order_type_status', '<>', 2);
            } elseif ($status == 2) {
                $q->where('store_order.status', 3);
                $q->where('store_order.order_type_status', '<>', 2);
                $q->where('store_order.deal_type', 1);
            } elseif ($status == 3) {
                $q->where('store_order.status', 3);
                $q->where('store_order.order_type_status', '<>', 1);
                $q->where('store_order.deal_type', 2);
            } elseif ($status == 4) {
                $q->where('store_order.status', 4);
            } elseif ($status == 5) {
//                $refundNos = $this->shopOrderService->refundOrderNos([
//                    'status' => 0,
//                    'handle_type' => 1,
//                    'shop_id'     => $shop_id,
//                ]);
//                $q->whereIn('store_order.order_no', $refundNos);
            } elseif ($status == 6) {
//                $refundNos = $this->shopOrderService->refundOrderNos([
//                    'handle_type' => 2,
//                    'status'      => 2,
//                    'shop_id'     => $shop_id,
//                ]);
//                $q->whereIn('store_order.order_no', $refundNos)->orWhere('store_order.status', 5);
                $q->where('store_order.status', 5);
            } else {
                $q->whereIn('store_order.order_no', []);
            }
            $q->where('store_order.shop_id', $shop_id);
            $q->whereBetween('store_order.create_at', ["$startDate 00:00:00", "$endDate 23:59:59"]);
        };
        $ret = $this->shopOrderService->getList2(intval($row), $where);
        return $this->resultFormat($ret);
    }

    /**
     * 订单列表
     *
     * @RequestMapping(path="/v1/app/order/shop", methods="get")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:40
     * Updated_at: 2020/12/29 0029 14:42
     */
    public function getOrderListByShop2(RequestInterface $request)
    {
        $row = intval($request->input('row', 10));
        $status = $request->input('status');
        $shop_id = $request->getAttribute('shop_id');
        if (!$shop_id || !$row || !$status || $status > 6 || $status < 1) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $startDate = $request->input('start_time');
        !$startDate && $startDate = '1970-01-01';
        $endDate = $request->input('end_time');
        !$endDate && $endDate = '2080-01-01';
        $where = [
            ['shop_id', '=', $shop_id],
            ['create_at', '>', "$startDate 00:00:00"],
            ['create_at', '<', "$endDate 23:59:59"],
        ];
        if ($status == 1) {
            $where[] = ['status', '=', 2];
            $where[] = ['order_type_status', '<>', 2];
        }
        if ($status == 2) {
            $where[] = ['store_order.status', '=', 3];
            $where[] = ['store_order.order_type_status', '<>', 2];
            $where[] = ['store_order.deal_type', '=', 1];
        }
        if ($status == 3) {
            $where[] = ['store_order.status', '=', 3];
            $where[] = ['store_order.order_type_status', '<>', 1];
            $where[] = ['store_order.deal_type', '=', 2];
        }
        if ($status == 4) {
            $where[] = ['store_order.status', '=', 4];
        }
        if ($status == 5) {
            $where[] = ['handle_type', '=', 1];
            $ret = $this->shopOrderService->getRefundList($where, $row);
        } elseif ($status == 6) {
            $where[] = ['handle_type', '=', 2];
            $ret = $this->shopOrderService->getRefundList($where, $row);
        } else {
            $ret = $this->shopOrderService->getList($row, $where);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 订单搜索
     *
     * @RequestMapping(path="/v1/app/order/shop/search", methods="get")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:39
     * Updated_at: 2021/1/6 0006 13:48
     */
    public function search(RequestInterface $request)
    {
        $shop_id = $request->getAttribute('shop_id');
        $content = $request->input('content');
        $row = $request->input('row', 15);
        if (!$shop_id || !$content) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $where = $this->shopOrderService->searchWhere($shop_id, $content);
        $ret = $this->shopOrderService->getList(intval($row), $where);
        return $this->resultFormat($ret);
    }

    /**
     * 核销
     *
     * @RequestMapping(path="/v1/app/order/write_off", methods="get,put")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:40
     * Updated_at: 2021/1/6 0006 13:48
     */
    public function writeOff(RequestInterface $request)
    {
        $code = $request->input('code');
        $shop_id = $request->getAttribute('shop_id');
        if (!$code) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $ret = $this->shopOrderService->shopOrderInfo($shop_id, ['write_off_code' => $code]);
        if ($request->isMethod('put')) {
            $ret = $this->shopOrderService->writeOff($shop_id, $code);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 发货
     *
     * @RequestMapping(path="/v1/app/order/shop/ship", methods="put")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:39
     * Updated_at: 2020/12/29 0029 14:42
     */
    public function ship(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        if (!$order_no) {
            $ret['code'] = ErrorCode::PARAMS_INVALID;
        } else {
            $ret = $this->shopOrderService->ship($order_no);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 批量发货
     *
     * @RequestMapping(path="/v1/app/order/bulk_shipping", methods="put")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:39
     * Updated_at: 2020/12/29 0029 14:40
     */
    public function bulkShipping(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        if (!$order_no) {
            $ret['code'] = ErrorCode::PARAMS_INVALID;
        } else {
            $order_nos = explode(',', $order_no);
            $ret = $this->shopOrderService->bulkShipping($order_nos);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 确认送达
     *
     * @RequestMapping(path="/v1/app/order/arrival", methods="put")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:38
     * Updated_at: 2020/12/29 0029 14:41
     */
    public function arrival(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        if (!$order_no) {
            $ret['code'] = ErrorCode::PARAMS_INVALID;
        } else {
            $ret = $this->shopOrderService->arrival($order_no);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 申请售后退款页面
     *
     * @RequestMapping(path="/v1/app/order/after_sale", methods="get")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:38
     * Updated_at: 2020/12/29 0029 14:38
     */
    public function apply_after_sale(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        if (!$order_no) {
            $ret['code'] = ErrorCode::PARAMS_INVALID;
        } else {
            $ret = $this->shopOrderService->getDrivingRefundInfo($order_no);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 拒绝退款
     *
     * @RequestMapping(path="/v1/app/order/refusal_refund", methods="put")
     *
     * @param RequestInterface $request
     * @return array
     *
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:36
     * Updated_at: 2020/12/29 0029 14:37
     */
    public function refuseRefund(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        if (!$order_no) {
            $ret['code'] = ErrorCode::PARAMS_INVALID;
        } else {
            $ret = $this->shopOrderService->refuseRefund($order_no);
        }
        return $this->resultFormat($ret);
    }

    /**
     * 订单详情
     * @RequestMapping(path="/v1/app/order/detail", methods="get")
     * @Middlewares({
     *     @Middleware(AesMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return array
     * User: mengchenchen
     * Created_at: 2020/12/29 0029 14:35
     * Updated_at: 2020/12/29 0029 14:42
     */
    public function shopOrderDetail(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        $refund_no = $request->input('refund_no', '');// 缺少导致多次售后数据混乱
        $shop_id = $request->getAttribute('shop_id');
        if (!$order_no) {
            $ret['code'] = ErrorCode::PARAMS_INVALID;
        } else {
            $ret = $this->shopOrderService->shopOrderInfo($shop_id, ['order_no' => $order_no], $refund_no);
        }
        return $this->resultFormat($ret);
    }



    /**
     * APP小组件
     * @RequestMapping(path="/v1/app/order/widget", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function orderWidget(RequestInterface $request)
    {

        $params = $request->all();
        $shop_id = (int)$params['shop_id'];
        if(empty($shop_id)){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderBusinessService->orderWidget($shop_id);
        return $this->success($res,'成功');
    }

    /**
     * APP首页改版
     * @RequestMapping(path="/v1/app/home/index", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function homeIndex(RequestInterface $request)
    {
        $shop_id = $request->getAttribute('shop_id');
        if(empty($shop_id)){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->orderBusinessService->homeIndex($shop_id);
        return $this->success($res,'成功');
    }

}
