<?php


namespace App\Order\Repository;


use App\Order\Model\EntrustOrderGoodsModel;
use App\Resource\Model\GoodsListModel;
use App\Resource\Model\GoodsModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class EntrustOrderGoodsRepository implements BaseRepositoryInterface
{
    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectMoreUnionList(array $primaryField,array $sonField,$grandField,array $condition,int $page,int $limit,array $orderBy,bool $isPage=true,bool $isArray=true)
    {
        $entrustOrderGoods =(new EntrustOrderGoodsModel())->getTable();
        $goods =(new GoodsModel())->getTable();
        $goodsList =(new GoodsListModel())->getTable();
        $primaryField =array_map(function ($item) use ($entrustOrderGoods) {
            return $entrustOrderGoods.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($goods) {
            return $goods.'.'.$item;
        },$sonField);
        $grandField =array_map(function ($item) use ($goodsList ) {
            return $goodsList .'.'.$item;
        },$grandField);
        $connect = EntrustOrderGoodsModel::query()->from("{$entrustOrderGoods}")
            ->select(array_merge($primaryField,$sonField,$grandField))
            ->leftJoin("{$goodsList}", $entrustOrderGoods.'.goods_id', '=', $goodsList.'.goods_id')
            ->leftJoin("{$goods}", $entrustOrderGoods.'.goods_id', '=', $goods.'.id')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereNotIn']??[], function ($query) use($condition) {
                return $query->whereNotIn($condition['whereNotIn'][0],$condition['whereNotIn'][1]);
            })
            ->when($condition['whereIn']??[], function ($query) use($condition) {
                return $query->whereIn($condition['whereIn'][0],$condition['whereIn'][1]);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($goods) {
                return $query->orderBy($goods.'.sort','desc');
            });
        if($isPage){
            if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()->toArray()];
            return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()];
        }
        if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()->toArray()];
        return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()];
    }

}