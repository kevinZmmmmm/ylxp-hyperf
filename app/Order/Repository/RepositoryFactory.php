<?php


namespace App\Order\Repository;

use App\Resource\Repository\ShopRepository;
use Hyperf\Di\Annotation\Inject;

class RepositoryFactory
{

    /**
     * @Inject()
     * @var OrderGoodsRepository
     */
    private $orderGoods;

    /**
     * @Inject()
     * @var OrderRepository
     */
    private $order;

    /**
     * @Inject()
     * @var ShopRepository
     */
    private $shop;

    /**
     * @Inject()
     * @var EntrustOrderRepository
     */
    private $entrustOrder;

    /**
     * @Inject()
     * @var EntrustOrderGoodsRepository
     */
    private $entrustOrderGoods;

    public function getRepository($repoName){
        return $this->$repoName;
    }
}