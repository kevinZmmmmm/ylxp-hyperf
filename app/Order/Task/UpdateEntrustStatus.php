<?php

declare(strict_types=1);

namespace App\Order\Task;

use App\Order\Service\Order\OrderBusinessService;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Task\Annotation\Task;

class UpdateEntrustStatus
{
    /**
     * @Task
     * @param $entrust_order_id
     * @author 1iu
     * @date 2021-03-12 10:08
     */
    public function handle($entrust_order_id)
    {
        $container = ApplicationContext::getContainer();
        $where = ['id' => $entrust_order_id];
        $update = ['status' => 1];
        $container->get(OrderBusinessService::class)->updateEntrust($where, $update);
    }
}
