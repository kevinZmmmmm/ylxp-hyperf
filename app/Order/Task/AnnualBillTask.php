<?php
declare(strict_types=1);

namespace App\Order\Task;
use App\Order\Model\OrderModel;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;


/**
 * 年度账单
 * @Crontab(name="billTask", rule="40 17 30 12 *", callback="task")
 */
class AnnualBillTask
{



    public function task()
    {
        $container = ApplicationContext::getContainer();
        $this->container = $container;
        $redis = $this->container->get(RedisFactory::class)->get('resource');
        $year ='2020';
        $count = [];
        $i=0;
        while (true){
            $_count = OrderModel::where(['is_pay' => 1])
                ->selectRaw('count(*) as num,mid')
                ->whereRaw("create_at > '$year'")->groupBy(['mid'])->offset($i)->limit(1000)->orderBy('num', 'asc')->get()->toArray();
            if ($_count) {
                $count =  array_merge($count, $_count);
            }else{
                break;
            }
            $i +=1000;
        }
        if ($count) {
            $redis->set('order:countByMidAll', json_encode($count));
        }

    }
}