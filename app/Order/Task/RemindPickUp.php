<?php
declare(strict_types=1);

namespace App\Order\Task;

use App\Order\Model\OrderModel;
use App\Third\Service\Sms\SmsService;
use EasyWeChat\Factory;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

/**
 * 提醒提货
 *
 * @Crontab(name="RemindPickUp", rule="00 00 09 * * *", callback="task")
 * Class RemindPickUp
 * @package App\Order\Task
 */
class RemindPickUp
{

    /**
     * @Inject
     * @var SmsService
     */
    protected $smsService;

    /**
     * @Inject
     * @var Factory
     */
    protected $wechat;

    /**
     * 微信
     *
     * @var \EasyWeChat\OfficialAccount\Application
     */
    protected $app;

    public function __construct()
    {
        $this->app = $this->wechat::officialAccount([
            'app_id'        => config('wechat.official_account.app_id'),
            'secret'        => config('wechat.official_account.secret'),
            'response_type' => 'array',
        ]);
    }

    public function task()
    {
        $where = "appointment_time REGEXP '.. (" . date('Y') . "-)?" . date('m-d') . " [0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}' and status = 3 and order_source = 1";
        OrderModel::query()->whereRaw($where)->chunk(1000, function ($orders) {
            foreach ($orders as $order) {
                $order_goods_title = $order->goods()->groupBy(['order_no'])->get([Db::raw('group_concat(goods_title ORDER BY number DESC SEPARATOR "、") as summary')]);
                if ($order->member->wx_openid) {
                    $this->app->template_message->send([
                        'touser'      => $order->member->wx_openid,
                        'template_id' => 'iX1OvpW_qS9Yh0XIpEXW2HFuGGhnfpOYWN9ibNbFwk4',
                        'miniprogram' => [
                            'appid'    => 'wx43a215392b89d41f',
                            'pagepath' => 'pages/order-group/order-group?status=2',
                        ],
                        'data'        => [
                            'first'    => '亲，您的商品已经准备出库了，请在指定日期内凭取货密码前往领取。',
                            'keyword1' => [
                                'value' => $order->order_no,
                                'color' => 'blue',
                            ],
                            'keyword2' => $order_goods_title[0]['summary'] ?? '',
                            'keyword3' => $order->appointment_time,
                            'keyword4' => $order->shop->address,
                            'keyword5' => $order->write_off_code,
                            'remark'   => '任何疑问可直接在公众号与客服对话，服务时间：上午9:00-下午5:30，感谢你的支持！',
                        ],
                    ]);
                } elseif (preg_match("/1[23456789]{1}\d{9}$/", $order->member->phone)) {
                    $content = [
                        '@1@=' . $order->order_no,
                        '@2@=' . $order_goods_title[0]['summary'] ?? '',
                        '@3@=' . $order->appointment_time,
                        '@4@=' . $order->shop->address,
                        '@5@=' . $order->write_off_code,
                    ];
                    $this->smsService->sendOut($order->member->phone, implode(',', $content), 'XC10263-0006');
                }
            }
        });
    }
}