<?php

declare(strict_types=1);

namespace App\Order\Task;

use App\Activity\Service\Free\FreeMiniService;
use App\Common\Constants\Stakeholder;
use App\Order\Model\OrderModel;
use Hyperf\Task\Annotation\Task;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerInterface;

class UpdateFreeStatus
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Task
     * @param $order
     * @return bool
     * @author 1iu
     * @date 2021-03-25 13:55
     */
    public function paymentHandle($order){
        //检测上级是否成功
        if( !empty($order->free_parent_order_no) ){
            // 上级订单信息 --创建订单时检验活动时间
            $porder = OrderModel::query()->where('order_no',$order->free_parent_order_no)->first();
            $refund_status = [Stakeholder::ORDER_REFUNDED, Stakeholder::ORDER_PENDING];
            if ($porder->is_pay == Stakeholder::ORDER_PAID and !in_array($porder->status, $refund_status)){
                $node = OrderModel::query()
                    ->select(['appointment_date'])
                    ->where(['order_type'=>Stakeholder::ORDER_TYPE_FREE, 'is_pay'=>Stakeholder::ORDER_PAID, 'free_parent_order_no'=>$order->free_parent_order_no])
                    ->whereNotIn('status', $refund_status)
                    ->oldest('pay_at')
                    ->get()->toArray();
                if (count($node) < $porder->need_invited_num){
                    $porder->is_freed = 0;
                    $porder->free_step = 6;
                }
                if (count($node) >= $porder->need_invited_num){
                    $porder->is_freed = 2;
                    $porder->free_step = 4;
                    $porder->free_success_time = $node[$porder->need_invited_num - 1]['appointment_date'];
                }
                $porder->save();
            }
        }
        $this->container()->get(FreeMiniService::class)->freeStartMessage($order);
        return true;
    }

    /**
     * @param array $order
     * @param object $act
     * @return bool
     * @author 1iu
     * @date 2021-03-25 17:03
     */
    public function refundHandle(array $order, object $act){
        $now = time();
        $activity_prams = json_decode($act->activity_prams, true);
        $is_second = $activity_prams['is_second'];
        $act_end = strtotime($act->end_date);
        if (!empty($order['free_parent_order_no'])) {
            $porder = OrderModel::query()->where('order_no', $order['free_parent_order_no'])->first();//上级订单
            $pay_time = strtotime($porder->pay_at);
            $refund_status = [Stakeholder::ORDER_REFUNDED, Stakeholder::ORDER_PENDING];
            if ($porder->is_pay == Stakeholder::ORDER_PAID and !in_array($porder->status, $refund_status)) {
                // 剩余子节点
                $node = OrderModel::query()->select(['appointment_date'])
                    ->where(['order_type' => Stakeholder::ORDER_TYPE_FREE, 'is_pay' => Stakeholder::ORDER_PAID, 'free_parent_order_no' => $order['free_parent_order_no']])
                    ->whereNotIn('status', $refund_status)
                    ->where('order_no', '!=', $order['order_no'])
                    ->oldest('pay_at')
                    ->get()->toArray();
                if (count($node) < $porder->need_invited_num) {
                    $first = $pay_time + $activity_prams['first_invite_time'] * 3600;
                    if ($porder->free_activation_time)
                        $second = strtotime($porder->free_activation_time) + $activity_prams['second_invite_time'] * 3600;
                    if ($now < $act_end){
                        //首次未过时间6，首次已过时间2，二次未过时间1，二次已过时间3
                        if ($porder->free_num == 1 and $now < $first) {$is_freed = 0;$free_step = 6;}
                        if ($porder->free_num == 1 and $now > $first) {
                            if ($is_second == 1){
                                $_second_act = $pay_time + ($activity_prams['second_activation'] + $activity_prams['first_invite_time']) * 3600;
                                if ($now < $_second_act){
                                    $is_freed = 1;$free_step = 2;
                                }
                            }
                            if ($is_second == 0){$is_freed = 3;$free_step = 3;}
                        }
                        if ($porder->free_num == 2 and $now < $second) {$is_freed = 0;$free_step = 1;}
                        if ($porder->free_num == 2 and $now > $second) {$is_freed = 3;$free_step = 3;}
                    }else{
                        $is_freed = 3;
                        $free_step = 5;
                    }
                    $porder->is_freed = $is_freed;
                    $porder->free_step = $free_step;
                    $porder->free_success_time = null;
                }
                if (count($node) >= $porder->need_invited_num) {
                    $porder->free_success_time = $node[$porder->need_invited_num - 1]['appointment_date'];
                }
                $porder->save();
            }
        }
        $this->container()->get(FreeMiniService::class)->freeFailMessage($order['order_no']);
        return true;
    }

    /**
     * @Task
     * @param $order_no
     * @author 1iu
     * @date 2021-03-27 15:51
     */
    public function sendRefundSuccessMsgForFree($order_no){
//        $og = OrderGoodsModel::query()->where('order_no', $order_no)->first();
//        ActivityGoodsModel::query()->where('id', $og->activity_goods_id)->increment('free_num');
        $this->container()->get(FreeMiniService::class)->freeMessage($order_no);
    }

    protected function container(){
        if ($this->container instanceof ContainerInterface){
            return $this->container;
        }
        return $this->container = ApplicationContext::getContainer();
    }

}
