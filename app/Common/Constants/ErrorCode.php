<?php

declare(strict_types=1);

namespace App\Common\Constants;


class ErrorCode
{
    const SUCCESS = 0;

    const AUTH_ERROR = 100;
    const AUTH_TOKEN_ERROR = 1000;
    const AUTH_OPEN_ASE_ERROR = 4000;
    const AUTH_ASE_INVALID_ERROR = 5000;
    const HEADER_KEY_ERROR = 6000;
    const HEADER_KEY_INVALID_ERROR = 7000;
    const PERMISSION_ERROR = 101;
    const ADMIN_IS_GOD = 102;
    const ADD_ROLE_FAILE = 103;
    const BIND_SHOP_FAILE = 104;
    const PHONE_CODE_INVALID = 105;
    const PHONE_CODE_ERROR = 106;
    const ORIGINAL_PASSWORD_ERROR = 107;
    const PASSWORD_LENGTH_SIX = 108;
    const IMAGE_INVALID = 301;
    const SYSTEM_INVALID = 400;
    const SORT_INVALID = 401;
    const PARAMS_INVALID = 402;
    const TIME_INVALID = 403;
    const NOT_EXIST = 404;
    const NOT_IN_FORCE = 405;
    const FORBID_BIND = 406;
    const PRICE_INVALID = 407;
    const PHONE_EXIST = 408;
    const NOT_SETTLE = 409;
    const ACTIVITY_CAN_NOT_EDIT = 410;
    const PRICE_ERROR = 411;
    const GOODS_SPEC_ERROR = 412;
    const SERVER_ERROR = 500;
    const DELETE_REFUSE = 501;
    const GOODS_IN_ACTIVITY = 502;
    const COOKBOOK_DELETE = 503;
    const CATEGORY_DELETE = 504;
    const FORBID_UPDATE_GOODS = 600;
    const TAG_INCLUDE_MEMBER = 666;
    const SHARER_TEAM_LEADER_STATUS_FAILED = 700;
    const TEAM_LEADER_STATUS_FAILED = 701;
    const HAS_BEEN_TEAM_LEADER = 702;
    const WITHDRAW_CERTIFICATE_INVALID = 703;
    const BALANCE_OF_ACCOUNT_INSUFFICIENT = 704;
    const TEAMLEADE_STATUS_REJECT = 705;
    const TEAMLEADE_STATUS_SUBMIT = 706;
    const TEAMLEADE_STATUS_CAN_NOT_CHECK = 707;
    const TEAMLEADE_WITHDRAW_AMOUNT_ERROR = 708;
    const INSUFFICIENT_ACCOUNT_BALANCE = 709;
    const CAN_T_ADVANCE_TAKE = 710;
    const ORDER_VERIFICATION_STATUS = 711;
    const WRITE_OFF_DATA_IS_ILLEGAL = 712;
    const LEADER_INCOMPLETE_INFORMATION = 713;
    const SAME_TIME_AND_SHOP = 800;
    const CRD_GOODS_BEYOND_LIMIT_NUM = 9527;
    const ERROR_TYPE = 90001;
    const ERROR_USERNAME = 20001;
    const ERROR_PASSWORD = 20002;
    const ERROR_MINI_USER = 20003;
    const NOT_HAVE_NEAR_SHOP = 10000;
    const MISSING_PARAMETER = 10001;
    const EMPTY_PHONE = 10002;
    const NOT_CONTENT_CHANGE = 10013;
    const PHONE_ERROR = 10015;
    const ERROR_BY_USER_BY_SHOP = 10017;
    const GOODS_NUM_OVER = 30001;
    const GOODS_INVALID = 30002;
    const GOODS_SELL_OUT = 30003;
    const GOODS_SELL_OUT_PART = 30004;
    const CRD_GOODS_NUM_OVER = 30005;
    const SALE_GOODS_INVALID = 30006;
    const SALE_STOCK_INVALID = 30007;
    const GOODS_UNDER_STOCK = 30008;
    const ACTIVITY_GOODS_UNDER_STOCK = 30009;
    const ACTIVITY_ENDED = 30010;
    const ACTIVITY_SHOP_PART_NOT_IN = 30011;
    const ACTIVITY_NOT_IN_HAND = 30012;
    const CART_NULL = 40001;
    const CART_CLEAR_FAIL = 40002;
    const CART_NOT_SELECTED = 40003;
    const ENTRUST_NULL = 40004;
    const SESSION_CODE_ERROR = 50001;
    const ORDER_DEAL_ERROR = 60001;
    const ORDER_ADDRESS_ERROR = 60002;
    const ORDER_PRICE_ERROR = 60003;
    const ORDER_PAY_TYPE_ERROE = 60004;
    const ORDER_GOODS_STOCK_ERROR = 60005;
    const SHOP_STATUS_NO_OPEN = 70000;
    const SHOP_STATUS_ERROR = 70001;
    const SHOP_STATUS_CLOSE = 70002;
    const SHOP_DEAL_TYPE = 70003;
    const SHOP_AREA_ERROR = 70004;
    const GOODS_SEARCH_ERROR = 70005;
    const SHOP_MONEY_ERROR =70006;
    const ERROR_BY_PASSWORD = 70007;
    const LEADER_ERROR = 70008;
    const GOODS_DELETE = 70009;
    const GOODS_STATUS = 70010;
    const GOODS_PLATFORM_STATUS = 70011;
    const LINK_EXPIRED = 70012;
    const ORDER_COUPON_PAYKEY_ERROR = 80000;

    public static $messages
        = [
            self::AUTH_ERROR => '用户名或密码错误',
            self::PERMISSION_ERROR => '无权进行此操作',
            self::ADMIN_IS_GOD => '不能对超级管理员进行此操作',
            self::ADD_ROLE_FAILE => '分配角色失败',
            self::BIND_SHOP_FAILE => '绑定门店失败',
            self::PHONE_CODE_INVALID => '无效的验证码，请重新获取',
            self::PHONE_CODE_ERROR => '验证码输入错误',
            self::ORIGINAL_PASSWORD_ERROR => '原密码不正确',
            self::PASSWORD_LENGTH_SIX => '密码最少 6 位数',
            self::IMAGE_INVALID => '当前文件后缀不被允许',
            self::SYSTEM_INVALID => '系统服务异常:缺少必要参数',
            self::SORT_INVALID => '权重值只能在 0-100 范围内',
            self::PARAMS_INVALID => '系统参数错误~',
            self::TIME_INVALID => '所选择的时间不正确',
            self::NOT_EXIST => '当前条目不存在',
            self::NOT_IN_FORCE => '操作未生效',
            self::FORBID_BIND => '当前账号或店铺已进行过绑定，不能重复绑定',
            self::PRICE_INVALID => '商品活动价不能大于等于划线价或原价',
            self::PHONE_EXIST => '手机号已经存在',
            self::SERVER_ERROR => 'Server Error',
            self::DELETE_REFUSE => '目前不能执行删除操作，该项正被使用中，可能存在子项,或已售出',
            self::GOODS_IN_ACTIVITY => '检测到所选商品已存在于其他活动中，不能再被添加到专题活动',
            self::COOKBOOK_DELETE => '已启用的菜谱不能执行删除操作',
            self::CATEGORY_DELETE => '分类下存在商品，不能删除',
            self::FORBID_UPDATE_GOODS => '该商品已产生订单，不能被编辑或删除',
            self::TAG_INCLUDE_MEMBER => '该标签下有会员,不能删除',
            self::SHARER_TEAM_LEADER_STATUS_FAILED => '分享人当前还不是团长，请到个人中心自行申请团长',
            self::TEAM_LEADER_STATUS_FAILED => '该用户当前还不是团长',
            self::HAS_BEEN_TEAM_LEADER => '您已经是团长了，不能重复申请',
            self::WITHDRAW_CERTIFICATE_INVALID => '请上传转账凭证',
            self::BALANCE_OF_ACCOUNT_INSUFFICIENT => '提现金额不能大于用户已申请金额总计',
            self::TEAMLEADE_STATUS_REJECT => '驳回操作请填写驳回原因',
            self::TEAMLEADE_STATUS_SUBMIT => '团长申请已提交，请耐心等待审核',
            self::TEAMLEADE_STATUS_CAN_NOT_CHECK => '当前用户没有提交审核请求，不能进行审核操作',
            self::TEAMLEADE_WITHDRAW_AMOUNT_ERROR => '提现金额不能小于 100 元',
            self::INSUFFICIENT_ACCOUNT_BALANCE => '账户余额不足',
            self::ORDER_VERIFICATION_STATUS => '当前订单已经被核销过了',
            self::CAN_T_ADVANCE_TAKE => '无法进行提前核销',
            self::WRITE_OFF_DATA_IS_ILLEGAL => '核销数据不合法',
            self::LEADER_INCOMPLETE_INFORMATION => '该团长资料不全，还不能营业',
            self::SAME_TIME_AND_SHOP => '当前活动的时间和其它活动时间重叠，且存在相同的店铺',
            self::ERROR_TYPE => '传输类型错误',
            self::ERROR_USERNAME => '用户不存在',
            self::ERROR_PASSWORD => '密码输入有误',
            self::ERROR_MINI_USER => '您的登陆信息已过期,请重新登陆尝试~',
            self::NOT_HAVE_NEAR_SHOP => '附件没有店铺',
            self::MISSING_PARAMETER => '参数不正确',
            self::EMPTY_PHONE => '请输入手机号',
            self::PHONE_ERROR => '请输入正确的手机号',
            self::NOT_CONTENT_CHANGE => '内容未发生改变,请修改后在提交',
            self::ERROR_BY_USER_BY_SHOP => '该用户没有跟店铺绑定,请联系平台管理员',
            self::GOODS_NUM_OVER => '超出限购数量',
            self::GOODS_INVALID => '商品已失效',
            self::GOODS_SELL_OUT_PART => '部分商品已失效',
            self::GOODS_SELL_OUT => '亲该商品已售罄哦~',
            self::GOODS_UNDER_STOCK => '菜品库存不足,请联系小跑运营团队,给您带来不便敬请谅解',
            self::ACTIVITY_ENDED => '活动已结束,给您带来不便敬请谅解',
            self::ACTIVITY_NOT_IN_HAND => '活动未在进行中,请联系小跑运营团队,给您带来不便敬请谅解',
            self::ACTIVITY_SHOP_PART_NOT_IN => '该门店没有参与此活动,请您选着相应门店进行下单,给您带来不便敬请谅解',
            self::ACTIVITY_GOODS_UNDER_STOCK => '活动菜品库存不足,请联系小跑运营团队,给您带来不便敬请谅解',
            self::CART_NULL => '您的购物车没有菜品,请您将想要的菜品放入购物车进行下单',
            self::CART_NOT_SELECTED => '购物车商品未勾选',
            self::ENTRUST_NULL => '代客下单模板中没有商品信息',
            self::CART_CLEAR_FAIL => '购物车清空失败',
            self::SESSION_CODE_ERROR => 'Code换取SessionKey失败',
            self::ORDER_DEAL_ERROR => '配送方式异常',
            self::ORDER_ADDRESS_ERROR => '收货地址不能为空',
            self::ORDER_PRICE_ERROR => '菜品支付金额异常,请联系小跑运营团队,给您带来不便敬请谅解',
            self::ORDER_GOODS_STOCK_ERROR =>'菜品库存不足,请联系小跑运营团队,给您带来不便敬请谅解',
            self::ORDER_PAY_TYPE_ERROE => '支付方式有误',
            self::SHOP_STATUS_NO_OPEN =>'该门店未营业',
            self::SHOP_STATUS_ERROR => '该门店不在营业时间内',
            self::SHOP_STATUS_CLOSE => '门店系统已关闭',
            self::SHOP_DEAL_TYPE => '该门店只能自提',
            self::SHOP_AREA_ERROR => '超出店铺配送范围',
            self::NOT_SETTLE => '暂无结算商品',
            self::ACTIVITY_CAN_NOT_EDIT => '只能编辑还未开始的活动',
            self::PRICE_ERROR => '价格必须大于 0',
            self::GOODS_SPEC_ERROR => '商品规格必须大于 0',
            self::CRD_GOODS_NUM_OVER => '每人每天限购',
            self::GOODS_SEARCH_ERROR => '搜索关键字不能为空',
            self::CRD_GOODS_BEYOND_LIMIT_NUM => '次日达订单列表立即支付超出限购',
            self::SHOP_MONEY_ERROR => '菜品金额有误,请联系小跑运营团队,给您带来不便敬请谅解',
            self::ERROR_BY_PASSWORD => '原密码与新密码相同',
            self::LEADER_ERROR => '团长信息不存在',
            self::SALE_GOODS_INVALID => '秒杀商品已下架',
            self::SALE_STOCK_INVALID => '秒杀商品库存不足',
            self::GOODS_DELETE => '店铺商品已删除',
            self::GOODS_STATUS => '店铺商品已下架',
            self::GOODS_PLATFORM_STATUS => '平台商品已下架',
            self::AUTH_TOKEN_ERROR =>'token身份已过期,请尝试重新登录~',
            self::AUTH_OPEN_ASE_ERROR =>'已开启AES加密认证',
            self::AUTH_ASE_INVALID_ERROR =>'AES加密认证失败',
            self::HEADER_KEY_ERROR=>'已开启Header平台通讯凭证',
            self::HEADER_KEY_INVALID_ERROR=>'无效请求,请稍后再试',
            self::LINK_EXPIRED=>'链接已过期',
            self::ORDER_COUPON_PAYKEY_ERROR=>'优惠劵使用缺少PayKey参数',
        ];

    public static function getMessage($code)
    {
        $message = ErrorCode::$messages[$code] ?? '未知错误';

        return $message;
    }
}
