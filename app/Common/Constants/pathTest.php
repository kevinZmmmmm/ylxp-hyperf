<?php

declare(strict_types=1);

namespace App\Common\Constants;


class pathTest
{
    public static $path
        = [
            // 商品分类
            'App\Resource\Controller\CategoryController\store' => '新增及时达商品分类',
            'App\Resource\Controller\CategoryController\store?shop_type=1' => '新增次日达商品分类',
            'App\Resource\Controller\CategoryController\update' => '编辑及时达商品分类',
            'App\Resource\Controller\CategoryController\update?shop_type=1' => '编辑次日达商品分类',
            'App\Resource\Controller\CategoryController\delete' => '修改及时达商品分类',
            'App\Resource\Controller\CategoryController\delete?shop_type=1' => '修改次日达商品分类',
            'App\Resource\Controller\CategoryController\editStatus' => '修改及时达商品分类状态——',
            'App\Resource\Controller\CategoryController\editStatus?shop_type=1' => '修改次日达商品分类状态——',
            'App\Resource\Controller\CategoryController\editHome' => '修改及时达商品分类首页状态——',
            'App\Resource\Controller\CategoryController\editHome?shop_type=1' => '修改次日达商品分类首页状态——',
            'App\Resource\Controller\CategoryController\editSort' => '修改及时达商品分类排序',
            'App\Resource\Controller\CategoryController\editSort?shop_type=1' => '修改次日达商品分类排序',
            // 商品单位
            'App\Resource\Controller\GoodsUnitController\store' => '新增商品单位',
            'App\Resource\Controller\GoodsUnitController\update' => '编辑商品单位',
            'App\Resource\Controller\GoodsUnitController\delete' => '修改商品单位',
            // 商品基础资料
            'App\Resource\Controller\GoodsController\store' => '新增商品',
            'App\Resource\Controller\GoodsController\update' => '编辑商品',
            'App\Resource\Controller\GoodsController\delete' => '修改商品',
            'App\Resource\Controller\GoodsController\editstatus' => '商品全局状态——',
            'App\Resource\Controller\GoodsController\hot' => '商品首页热销操作——',
            // 活动
            'App\Activity\Controller\GroupActivityController\store' => '新增团购活动',
            'App\Activity\Controller\QuotaActivityController\store' => '新增限购活动',
            'App\Activity\Controller\SpecialActivityController\store' => '新增专题活动',
            'App\Activity\Controller\FollowActivityController\store' => '新增接龙活动',
            'App\Activity\Controller\SeckillActivityController\store' => '新增秒杀活动',
            'App\Activity\Controller\FreeActivityController\store' => '新增分享免单活动',
            'App\Activity\Controller\GroupActivityController\update' => '编辑团购活动',
            'App\Activity\Controller\QuotaActivityController\update' => '编辑限购活动',
            'App\Activity\Controller\SpecialActivityController\update' => '编辑专题活动',
            'App\Activity\Controller\FollowActivityController\update' => '编辑接龙活动',
            'App\Activity\Controller\SeckillActivityController\update' => '编辑秒杀活动',
            'App\Activity\Controller\FreeActivityController\update' => '编辑分享免单活动',
            'App\Activity\Controller\GroupActivityController\delete' => '删除团购活动',
            'App\Activity\Controller\QuotaActivityController\delete' => '删除限购活动',
            'App\Activity\Controller\SpecialActivityController\delete' => '删除专题活动',
            'App\Activity\Controller\FollowActivityController\delete' => '删除接龙活动',
            'App\Activity\Controller\SeckillActivityController\delete' => '删除秒杀活动',
            'App\Activity\Controller\SeckillActivityController\editStatus' => '修改秒杀活动状态——',
            'App\Activity\Controller\FreeActivityController\editStatus' => '修改分享免单活动状态——',
            'App\Activity\Controller\BaseActivityController\editStatus' => '修改活动状态——',
            'App\Activity\Controller\BaseActivityController\bindShop' => '活动新增店铺',
            'App\Activity\Controller\SeckillActivityController\addShop' => '秒杀活动新增店铺',
            'App\Activity\Controller\FreeActivityController\addShop' => '分享免单活动新增店铺',
            // 首页管理
            'App\Resource\Controller\NavigationController\store' => '新增及时达图文导航',
            'App\Resource\Controller\NavigationController\store?shop_type=1' => '新增次日达图文导航',
            'App\Resource\Controller\RotationController\store' => '新增及时达轮播图',
            'App\Resource\Controller\RotationController\store?shop_type=1' => '新增次日达轮播图',
            'App\Resource\Controller\PopupController\store' => '新增及时达弹窗广告',
            'App\Resource\Controller\PopupController\store?shop_type=1' => '新增次日达弹窗广告',
            'App\Resource\Controller\CouponController\store' => '新增及时达优惠券',
            'App\Resource\Controller\CouponController\store?shop_type=1' => '新增次日达优惠券',
            'App\Resource\Controller\NoticeController\store' => '新增及时达公告管理',
            'App\Resource\Controller\NoticeController\store?shop_type=1' => '新增次日达公告管理',
            'App\Resource\Controller\LandingController\store' => '新增及时达落地页',
            'App\Resource\Controller\LandingController\store?shop_type=1' => '新增次日达落地页',
            'App\Resource\Controller\NavigationController\update' => '编辑及时达图文导航',
            'App\Resource\Controller\NavigationController\update?shop_type=1' => '编辑次日达图文导航',
            'App\Resource\Controller\RotationController\update' => '编辑及时达轮播图',
            'App\Resource\Controller\RotationController\update?shop_type=1' => '编辑次日达轮播图',
            'App\Resource\Controller\PopupController\update' => '编辑及时达弹窗广告',
            'App\Resource\Controller\PopupController\update?shop_type=1' => '编辑次日达弹窗广告',
            'App\Resource\Controller\CouponController\update' => '编辑及时达优惠券',
            'App\Resource\Controller\CouponController\update?shop_type=1' => '编辑次日达优惠券',
            'App\Resource\Controller\NoticeController\update' => '编辑及时达公告管理',
            'App\Resource\Controller\NoticeController\update?shop_type=1' => '编辑次日达公告管理',
            'App\Resource\Controller\LandingController\update' => '编辑及时达落地页',
            'App\Resource\Controller\LandingController\update?shop_type=1' => '编辑次日达落地页',
            'App\Resource\Controller\NavigationController\delete' => '删除及时达图文导航',
            'App\Resource\Controller\NavigationController\delete?shop_type=1' => '删除次日达图文导航',
            'App\Resource\Controller\RotationController\delete' => '删除及时达轮播图',
            'App\Resource\Controller\RotationController\delete?shop_type=1' => '删除次日达轮播图',
            'App\Resource\Controller\PopupController\delete' => '删除及时达弹窗广告',
            'App\Resource\Controller\PopupController\delete?shop_type=1' => '删除次日达弹窗广告',
            'App\Resource\Controller\CouponController\delete' => '删除及时达优惠券',
            'App\Resource\Controller\CouponController\delete?shop_type=1' => '删除次日达优惠券',
            'App\Resource\Controller\NoticeController\delete' => '删除及时达公告管理',
            'App\Resource\Controller\NoticeController\delete?shop_type=1' => '删除次日达公告管理',
            'App\Resource\Controller\LandingController\delete' => '删除及时达落地页',
            'App\Resource\Controller\LandingController\delete?shop_type=1' => '删除次日达落地页',
            'App\Resource\Controller\NavigationController\editStatus' => '修改及时达图文导航状态——',
            'App\Resource\Controller\NavigationController\editStatus?shop_type=1' => '修改次日达图文导航状态——',
            'App\Resource\Controller\RotationController\editStatus' => '修改及时达轮播图状态——',
            'App\Resource\Controller\RotationController\editStatus?shop_type=1' => '修改次日达轮播图状态——',
            'App\Resource\Controller\PopupController\editStatus' => '修改及时达弹窗广告状态——',
            'App\Resource\Controller\PopupController\editStatus?shop_type=1' => '修改次日达弹窗广告状态——',
            'App\Resource\Controller\CouponController\editStatus' => '修改及时达优惠券状态——',
            'App\Resource\Controller\CouponController\editStatus?shop_type=1' => '修改次日达优惠券状态——',
            'App\Resource\Controller\NoticeController\editStatus' => '修改及时达公告管理状态——',
            'App\Resource\Controller\NoticeController\editStatus?shop_type=1' => '修改次日达公告管理状态——',
            'App\Resource\Controller\LandingController\editStatus' => '修改及时达落地页状态——',
            'App\Resource\Controller\LandingController\editStatus?shop_type=1' => '修改次日达落地页状态——',
            'App\Resource\Controller\NoticeController\editSort' => '修改及时达公告排序',
            'App\Resource\Controller\LandingController\editSort' => '修改及时达落地页排序',
            // 跳转组件
            'App\Resource\Controller\JumpController\store' => '新增跳转组件',
            'App\Resource\Controller\JumpController\update' => '编辑跳转组件',
            'App\Resource\Controller\JumpController\delete' => '删除跳转组件',
            // 店铺
            'App\Resource\Controller\ShopController\store' => '新增店铺',
            'App\Resource\Controller\ShopController\update' => '编辑店铺',
            'App\Resource\Controller\ShopController\editStatus' => '修改店铺状态——',
            'App\Resource\Controller\ShopController\unBindGoods' => '及时达商品部分店铺下架',
            'App\Resource\Controller\ShopController\unBindGoods?shopType=1' => '次日达商品部分店铺下架',
            // 店铺账号
            'App\User\Controller\ShopAccountController\store' => '新增店铺账号',
            'App\User\Controller\ShopAccountController\update' => '编辑店铺账号',
            'App\User\Controller\ShopAccountController\editStatus' => '修改店铺账号状态——',
            'App\User\Controller\ShopAccountController\shopAccountBindShopId' => '店铺账号绑定店铺',
            // 店群管理
            'App\Resource\Controller\ShopGroupController\store' => '新增店群',
            'App\Resource\Controller\ShopGroupController\update' => '编辑店群',
            // 标签管理
            'App\User\Controller\TagController\store' => '新增标签',
            'App\User\Controller\TagController\update' => '编辑标签',
            'App\User\Controller\TagController\delete' => '删除标签',
//            'App\User\Controller\MemberController\update' => '会员加标签',  // 会员加标签，和小程序共用接口
            // 团长
            'App\Resource\Controller\TeamLeaderController\edit' => '编辑团长',
            'App\Resource\Controller\TeamLeaderController\withdrawApprove' => '团长提现审核',
            'App\Resource\Controller\TeamLeaderController\check' => '团长资质审核',
            // 次日达商城配置
            'App\Resource\Controller\SystemController\CrdShopConfig' => '次日达截单时间配置',
            'App\Resource\Controller\SystemController\CrdUpdateShareConfig' => 'APP分享设置',
            'App\Activity\Controller\FreeActivityController\setFreeRule' => '编辑免单规则',
            // 地推人员
            'App\Resource\Controller\GroundPromotionUserController\add' => '新增地推人员',
            'App\Resource\Controller\GroundPromotionUserController\update' => '编辑地推人员',
            'App\Resource\Controller\GroundPromotionUserController\delete' => '删除地推人员',
            'App\Resource\Controller\GroundPromotionUserController\editStatus' => '修改地推人员状态——',
            // 菜谱
            'App\Resource\Controller\CookbookController\store' => '新增菜谱',
            'App\Resource\Controller\CookbookController\update' => '编辑菜谱',
            'App\Resource\Controller\CookbookController\delete' => '删除菜谱',
            'App\Resource\Controller\CookbookController\editstatus' => '修改菜谱状态——',
            'App\Resource\Controller\CookbookController\editSort' => '编辑菜谱排序权重',
            // 平台管理员
            'App\User\Controller\UserController\store' => '新增平台管理员',
            'App\User\Controller\UserController\update' => '编辑平台管理员',
            'App\User\Controller\UserController\delete' => '删除平台管理员',
            'App\User\Controller\UserController\destroy' => '强删除平台管理员',
            'App\User\Controller\UserController\restore' => '恢复被软删除的管理员',
            'App\User\Controller\UserController\editStatus' => '修改管理员状态——',
//            'App\User\Controller\UserController\addRole' => '修改管理员角色',
            'App\User\Controller\UserController\editPassword' => '修改管理员密码',
            // 平台角色
            'App\User\Controller\RoleController\store' => '新增平台角色',
            'App\User\Controller\RoleController\update' => '编辑平台角色',
            'App\User\Controller\RoleController\delete' => '删除平台角色',
            // 权限菜单
            'App\User\Controller\PermissionController\store' => '新增一级或子节点',
            'App\User\Controller\PermissionController\update' => '编辑节点',
            'App\User\Controller\PermissionController\delete' => '删除节点',
            // 系统参数
            'App\Resource\Controller\SystemController\update' => '编辑版本参数配置',
            'App\Resource\Controller\StoreController\update' => '编辑商城参数',

        ];

    public static function getpath($path)
    {
        $message = self::$path[$path] ?? '未找到相匹配的操作描述';

        return $message;
    }
}
