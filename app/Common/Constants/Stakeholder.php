<?php
declare(strict_types=1);

namespace App\Common\Constants;

class Stakeholder
{
    /**
     * 付款方式ID---微信支付
     */
    const PAYMENT_METHOD_WECHAT = 1;

    /**
     * 付款方式ID---余额支付
     */
    const PAYMENT_METHOD_BALANCE = 2;

    /**
     * 订单支付状态---未支付
     */
    const ORDER_UNPAID = 0;

    /**
     * 订单支付状态---已支付
     */
    const ORDER_PAID = 1;

    /**
     * 会员支付系统--未开启
     */
    const PAY_SYSTEM_TYPE_CLOSE = 0;

    /**
     * 会员支付系统--客至余额
     */
    const PAY_SYSTEM_TYPE_KZ = 1;

    /**
     * 会员支付系统--吾享微信
     */
    const PAY_SYSTEM_TYPE_WX = 2;

    /**
     * 送货方式 --配送订单
     */
    const ORDER_DELIVERY = 1;

    /**
     * 送货方式--自提订单
     */
    const ORDER_SELF_PICK = 2;

    /**
     * 退款订单---整单退
     */
    const WHOLE_REFUND = 0;

    /**
     * 退款订单---部分退
     */
    const PART_REFUND = 1;

    /**
     * 订单状态--已取消
     */
    const ORDER_CANCEL = 0;

    /**
     * 订单状态--待付款
     */
    const ORDER_PENDING_PAYMENT = 1;

    /**
     * 订单状态--待发货
     */
    const ORDER_PENDING_SHIP = 2;

    /**
     * 订单状态--待取货
     */
    const ORDER_PENDING_PICK = 3;

    /**
     * 订单状态--已收货
     */
    const ORDER_RECEIVED = 4;

    /**
     * 订单状态--已退款
     */
    const ORDER_REFUNDED = 5;

    /**
     * 订单状态--已拒绝
     */
    const ORDER_REFUND_REFUSE = 6;

    /**
     * 订单状态--待处理
     */
    const ORDER_PENDING = 7;

    /**
     * 订单类型--普通订单
     */
    const ORDER_TYPE_NORMAL = 0;

    /**
     * 订单类型--普通自提订单
     */
    const ORDER_TYPE_NORMAL_SELF = 2;

    /**
     * 订单类型--团购自提订单
     */
    const ORDER_TYPE_GROUP = 6;

    /**
     * 订单类型--接龙订单
     */
    const ORDER_TYPE_DRAGON = 7;

    /**
     * 订单类型--次日达小跑精选
     */
    const ORDER_TYPE_NEXT_DAY = 8;

    /**
     * 订单类型--秒杀订单
     */
    const ORDER_TYPE_SECKILL = 9;

    /**
     * 订单类型--免单订单
     */
    const ORDER_TYPE_FREE = 10;

    /**
     * 订单是否使用优惠劵--未使用
     */
    const ORDER_IS_COUPON_NO = 0;

    /**
     * 订单是否使用优惠劵--已使用
     */
    const ORDER_IS_COUPON_YES = 1;

    /**
     * 次日达订单
     */
    const NEXT_DAY_ORDER = 1;

    /**
     * 及时达订单
     */
    const TIMELY_ORDER = 0;

    /*******************************************************************************************************/

    /**
     * 退款状态--申请退款
     */
    const REFUND_REQUEST = 0;

    /**
     * 退款状态--已拒绝
     */
    const REFUND_REFUSE = 1;

    /**
     * 退款状态--已退款
     */
    const REFUND_COMPLETE = 2;

    /**
     * 退款处理状态--待处理
     */
    const REFUND_NOT_PROCESSED = 1;

    /**
     * 退款处理状态--已处理
     */
    const REFUND_PROCESSED = 2;

    /*****************************************************************************************************/

    /**
     * 客至支付方式--账户余额
     */
    const KZ_PAY_BALANCE = 1;

    /**
     * 客至支付方式--积分
     */
    const KZ_PAY_INTEGRAL = 3;

    /**
     * 客至支付方式--现金支付
     */
    const KZ_PAY_CASH = 4;

    /**
     * 客至支付方式--优惠券
     */
    const KZ_PAY_COUPON = 5;

    /**
     * 客至支付方式--微信支付
     */
    const KZ_PAY_WECHAT = 88;

    /********************************************************************************/
    /**
     * 商品种类--标品
     */
    const GOODS_STANDARD = 0;

    /**
     * 商品种类--非标品
     */
    const GOODS_NON_STANDARD = 1;
    /**********************************************************************************/

    /**
     * 销售状态,0已下架，1销售中
     */
    const GOODS_STATUS_0 = 0;
    const GOODS_STATUS_1 = 1;

    /**
     *  活动是否删除--未删除
     */
    const ACTIVITY_NON_DELETED = 0;

    /**
     *  活动是否删除--已删除
     */
    const ACTIVITY_DELETED = 1;

    /**
     *  活动类型--团购
     */
    const ACTIVITY_GROUP = 0;

    /**
     *  活动类型--限购
     */
    const ACTIVITY_LIMIT = 1;

    /**
     *  活动类型--秒杀
     */
    const ACTIVITY_SECKILL = 4;

    /**
     *     活动状态--禁用
     */
    const ACTIVITY_DISABLE_STATUS = 0;

    /**
     *     活动状态--启用
     */
    const ACTIVITY_ENABLE_STATUS = 1;

    /**
     *     活动状态--秒杀活动进行中
     */
    const ACTIVITY_SECKILL_ING_STATUS = 2;

    /**
     *     活动状态--进行中
     */
    const ACTIVITY_OPEN_STATUS = 2;

    /********************************************************************************/

    /**
     * 会员地址--未删除
     */
    const MEMBER_ADDRESS_NOT_DELETED = 0;

    /**
     * 会员地址--已删除
     */
    const MEMBER_ADDRESS_DELETED = 1;

    /********************************************************************************/
    /**
     * 购物车--已选
     */
    const CART_SELECTED = 1;

    /**
     * 购物车--未选
     */
    const CART_NOT_SELECTED = 0;


    /********************************************************************************/
    /*
     * 团长身份状态--审核中
     */
    const LEADER_STATUS_UNDER_REVIEW = 1;

    /*
     * 团长身份状态--被驳回
     */
    const LEADER_STATUS_REJECTED = 2;

    /*
     * 团长身份状态--审核通过（营业中）
     */
    const LEADER_STATUS_IN_BUSINESS = 3;

    /*
     * 团长身份状态--审核通过（歇业中）
     */
    const LEADER_STATUS_OUT_OF_BUSINESS = 4;


    /*
     * 团长提现审核状态--未审核
     */
    const LEADER_WITHDRAW_STATUS_NO = 0;

    /*
     * 团长提现审核状态--已审核
     */
    const LEADER_WITHDRAW_STATUS_YES = 1;

    /**
     * 店铺状态：0：未营业 1：营业中  2 关闭
     */
    const  SHOP_STATUS_0 = 0;
    const  SHOP_STATUS_1 = 1;
    const  SHOP_STATUS_2 = 2;

    /**
     * 团长资金类型--提现
     */
    const WITHDRAW_TYPE   = 1;

    /**
     * 团长资金类型--订单核销收益
     */
    const ORDER_WRITE_OFF   = 2;

    /**
     * 团长资金类型--退款退佣金
     */
    const COMMISSION_REFUND   = 3;

    /**
     * pos状态
     */
    const POS_STATUS_NORMAL  = 1;    // 正常
    const POS_STATUS_PERVERT = 2;    // 反常

    /**
     * 订单支付方式
     */
    const ORDER_PAY_TYPE_WECHAT  = 1;   // 微信支付
    const ORDER_PAY_TYPE_BALANCE = 2;   // 余额支付

    /**
     * 平台订单来源
     */
    const ORDER_PLATFORM_TYPE_1 = 1;   // 1、其他订单
    const ORDER_PLATFORM_TYPE_2 = 2;   // 2、团长订单
    const ORDER_PLATFORM_TYPE_3 = 3;   // 3、代客单

    /**
     * 退款处理状态
     */
    const REFUND_HANDLE_STATUS_1 = 1;   // 待处理
    const REFUND_HANDLE_STATUS_2 = 2;   // 已处理

    /**
     * 区分团长 店铺    店铺1  团长2
     */
    const TAG_1 =1;
    const TAG_2 =2;
    /**
     *1 查询订单表2查询返款表
     */
    const  FINANCE_ORDER = 1;
    const  FINANCE_CITIC = 2;
    /**
     * 商品分组
     */
    const GROUP_ID_O = 0;//普通商品
    const GROUP_ID_1 = 1;//小跑拼团
    const GROUP_ID_2 = 2;//会员专区
    const GROUP_ID_3 = 3;//限时抢购
    const GROUP_ID_4 = 4;//次日达秒杀
}
