<?php

declare(strict_types=1);

namespace App\Common\Middleware;

use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Hyperf\Contract\ConfigInterface;

class EnvMiddleware implements MiddlewareInterface
{
    /**
     * @var HttpResponse
     */
    protected HttpResponse $response;

    /**
     * @var ConfigInterface
     */
    protected ConfigInterface $config;

    public function __construct(HttpResponse $response, ConfigInterface $config)
    {
        $this->response = $response;
        $this->config = $config;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // 此处功能应校验 用户mid 的环境 防止不法分子 那测试的mid 访问正式的数据
        $serverEnv = env('APP_ENV');
        // 没啥用
//        switch ($serverEnv) {
//            case 'dev':
//                $this->config->set('APP_ENV', 'dev');
//                $this->config->set('DB_HOST', '39.106.25.235');
//                $this->config->set('DB_DATABASE', 'shopylxp');
//                $this->config->set('DB_USERNAME', 'shopylxp');
//                $this->config->set('DB_PASSWORD', 'wtH4MTFcdr2keRDZ');
//                $this->config->set('DB_DATABASE_STATISTICS', 'qpplatformdb');
//                $this->config->set('DB_DATABASE_LOG', 'qpplatformlogdb');
//                $this->config->set('REDIS_HOST', '39.106.25.235');
//                $this->config->set('REDIS_AUTH', 'ylxpredis@2020@');
//                break;
//            case 'pro':
//                if (env('APP_ENV') == 'dev'){
//                    $data = [
//                        'code' => 403,
//                        'msg' => '对不起，环境不一致不能访问',
//                        'data' => [],
//                    ];
//                    return $this->response->json($data);
//                }
//                break;
//            default:
//                $data = [
//                    'code' => 403,
//                    'msg' => '对不起，你的通行证不正确',
//                    'data' => [],
//                ];
//                return $this->response->json($data);
//        }
        return $handler->handle($request);
    }
}
