<?php

declare(strict_types=1);

namespace App\Common\Controller;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;

class PingController
{
    public function heartbeat(RequestInterface $request, ResponseInterface $response)
    {
        $key = $request->input('key');
        if ($key === 'PING') return $response->raw('PONG');
    }

}
