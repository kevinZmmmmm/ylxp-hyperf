<?php

declare(strict_types=1);

namespace App\Common\Controller;


use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Request\ImageRequest;
use App\Common\Service\UploadFileService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class UploadFileController extends AbstractController
{
    /**
     * @Inject()
     * @var UploadFileService
     */
    private $uploadFileService;

    /**
     * 上传图片至阿里云 OSS
     * @RequestMapping(path="/v1/uploadFile", methods="post")
     * @param ImageRequest $request
     *
     * @return array
     */
    public function uploadFile(ImageRequest $request)
    {
        $params = $request->validated();
        $res = $this->uploadFileService->uploadImage($params['upload']);
        if (empty($res['code'])) {
            return $this->success($res, '操作成功');
        } else {
            return $this->failed($res['code'], $res['msg']);
        }

    }

}
