<?php


namespace App\Common\Process;


use Hyperf\AsyncQueue\Process\ConsumerProcess;
use Hyperf\Process\Annotation\Process;

/**
 * @Process()
 */
class LogProcess extends ConsumerProcess
{
    protected $queue = 'log';
}