<?php

declare(strict_types=1);

namespace App\Common\Exception;

use App\Common\Constants\ErrorCode;
use Hyperf\Server\Exception\ServerException;
use Throwable;

class ErrorException extends ServerException
{
    public function __construct(int $code = 0, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = ErrorCode::getMessage($code);
        }

        parent::__construct($message, $code, $previous);
    }
}