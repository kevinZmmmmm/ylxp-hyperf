<?php

declare(strict_types=1);

namespace App\Common\Exception;

use Hyperf\Server\Exception\ServerException;

class RefundException extends ServerException
{

}
