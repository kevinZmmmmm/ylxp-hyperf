<?php


namespace App\Common\Job;
use App\Order\Model\CrmStockLogModel;
use App\Order\Model\OrderLogModel;
use App\Resource\Model\CartLogModel;
use Hyperf\AsyncQueue\Job;

class LogJob extends Job
{
    public $params;

    /**
     * 任务执行失败后的重试次数，即最大执行次数为 $maxAttempts+1 次
     *
     * @var int
     */
    protected $maxAttempts = 2;

    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * type = 1  库存记录 type=2订单记录  type = 3购物车记录
     */
    public function handle()
    {
        switch ($this->params['type']) {
            case 1:
                CrmStockLogModel::query()->insert([
                    $this->params['data']
                ]);
                break;
            case 2:
                OrderLogModel::query()->insert(
                    $this->params['data']
                );
                break;
            case 3:
                CartLogModel::query()->insert(
                    $this->params['data']
                );
                break;
            default:
                break;
        }


    }
}