<?php
declare(strict_types=1);

namespace App\Common\Service;

use Hyperf\Guzzle\ClientFactory;

class DingDing extends BaseService
{
    /**
     * @var \Hyperf\Guzzle\ClientFactory
     */
    private $clientFactory;

    public function __construct(ClientFactory $clientFactory)
    {
        parent::__construct();
        $this->clientFactory = $clientFactory;
    }

    public function send(?string $accessToken, string $msg = '未知的异常')
    {
        $webhook = config('dingding.webhook');
        $accessToken = empty($accessToken) ? config('dingding.DING_ACCESS_TOKEN_DEV') : $accessToken;
        $webhook = $webhook . '?access_token=' . $accessToken;
        $msg = env('APP_ENV') == 'dev' ? "开发环境\n[狗子][狗子][狗子]\n$msg" : "生产环境\n[二哈][捂脸哭][二哈]\n$msg";
        $data = [
            'msgtype' => 'text',
            'text' => [
                'content' => $msg
            ]
        ];
        $headers = [
            'headers' => ['Content-Type' => 'application/json;charset=utf-8']
        ];
        $options = [
            'body' => json_encode($data)
        ];
        $client = $this->clientFactory->create($headers);
        return $client->post($webhook, $options);
    }

}
