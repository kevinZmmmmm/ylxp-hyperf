<?php

declare(strict_types=1);

namespace App\Common\Request;

use Hyperf\Validation\Request\FormRequest;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'upload' => 'image|mimes:jpg,jpeg,gif,png|between:1,2048',
        ];
    }

    public function messages(): array
    {
        return [
            'upload' => '上传文件',
        ];
    }
}
