<?php
/**
 * Created by PhpStorm.
 * User: ran
 * Date: 2020/8/13
 * Time: 14:35
 */

namespace App\Common\Model;


use Hyperf\DbConnection\Model\Model;

class ShopAnalysis extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_analysis';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'statistics';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function shop()
    {
        return $this->hasOne(ShopModel::class, 'shop_id', 'shop_id');
    }

}
