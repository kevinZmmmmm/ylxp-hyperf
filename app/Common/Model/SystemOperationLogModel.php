<?php

declare (strict_types=1);

namespace App\Common\Model;

use App\User\Model\UserModel;
use Hyperf\DbConnection\Model\Model;

class SystemOperationLogModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_system_operation_log';
    const UPDATED_AT = null;

    protected $connection = 'log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

//    public function user()
//    {
//        return $this->belongsTo(UserModel::class);
//    }


}
