<?php

declare(strict_types=1);

namespace App\Common\Aspect;

use App\Common\Service\DingDing;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Psr\Container\ContainerInterface;

/**
 * @Aspect
 */
class DingAspect extends AbstractAspect
{
    // 要切入的类，可以多个，亦可通过 :: 标识到具体的某个方法，通过 * 可以模糊匹配
    public $classes = [
        'App\Third\Service\Kz\KzMainService::refund',
        'App\Third\Service\Kz\KzMainService::pay',
        'App\Third\Service\Wx\WxPayService::comPayCancel',
        'App\Third\Service\Wx\WxPayService::comPaySubmit',
        'App\Third\Service\Wx\WxMainDeliverService::refundDeliveryOfflineMapping',
        'App\Third\Service\Wx\WxMainDeliverService::deliveryInsertOfflineMapping',
        'App\Pay\Service\PayService::checkSignature',
        'App\Pay\Service\PayService::weChatPayment',
        'App\Pay\Service\PayService::balancePayment',
        'App\Pay\Service\PayService::updateOrderGoodsDiscountInfo',
        'App\Pay\Listener\NotifyListener::process',
    ];

    // 要切入的注解，具体切入的还是使用了这些注解的类，仅可切入类注解和类方法注解
    public $annotations = [
    ];

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $res = $proceedingJoinPoint->process();
        if (isset($res['code']) && ($res['code'] == 0) && !empty($res['throw'])) {
            $access_token = env('APP_ENV') == 'dev' ? config('dingding.access_token_dev') : config('dingding.access_token_prod');
            $this->container->get(DingDing::class)->send($access_token, $res['throw']);
        }
        return $res;
    }
}
