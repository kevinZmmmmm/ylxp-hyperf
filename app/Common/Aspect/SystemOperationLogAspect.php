<?php

namespace App\Common\Aspect;

use App\Common\Constants\pathTest;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Common\Service\BaseService;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\HttpServer\Router\Dispatched;
use Phper666\JWTAuth\JWT;
use Psr\Container\ContainerInterface;
use Hyperf\Validation\Request\FormRequest;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Aspect
 */
class SystemOperationLogAspect extends AbstractAspect
{
    /**
     * @Inject()
     * @var JWT
     */
    protected $jwt;

    /**
     * @Inject
     * @var RequestInterface
     */
    protected $request;

    /**
     * @Inject
     * @var pathTest
     */
    protected $pathTest;

    /**
     * @Inject
     * @var FormRequest
     */
    protected $formRequest;

    /**
     * @Inject
     * @var Request
     */
    protected $httpFoundationRequest;


    // 要切入的类，可以多个，亦可通过 :: 标识到具体的某个方法，通过 * 可以模糊匹配
    public $classes = [
        // 通用操作
        'App\Common\Service\BaseService::editStatus',
        // 商品分类
        'App\Resource\Service\CategoryService::add',
        'App\Resource\Service\CategoryService::update',
        'App\Resource\Service\CategoryService::delete',
        'App\Resource\Service\CategoryService::editHome',
        'App\Resource\Service\CategoryService::editManySort',
        // 商品单位
        'App\Resource\Service\GoodsUnitService::add',
        'App\Resource\Service\GoodsUnitService::update',
        'App\Resource\Service\GoodsUnitService::delete',
        // 商品
        'App\Resource\Service\GoodsService::add',
        'App\Resource\Service\GoodsService::update',
        'App\Resource\Service\GoodsService::delete',
        'App\Common\Service\BaseService::editHotOrHome',
        // 活动
        'App\Activity\Service\ActivityService::add',
        'App\Activity\Service\ActivityService::update',
        'App\Activity\Service\ActivityService::delete',
        'App\Activity\Service\ActivityService::seckillStatus',
        'App\Activity\Service\ActivityService::freeStatus',
        'App\Activity\Service\ActivityService::activityBindShop',
        'App\Activity\Service\ActivityService::addShop',
        // 首页管理
        'App\Resource\Service\NavigationService::add',
        'App\Resource\Service\NavigationService::update',
        'App\Resource\Service\NavigationService::delete',
        'App\Resource\Service\NavigationService::editSort',
        // 跳转组件
        'App\Resource\Service\JumpService::add',
        'App\Resource\Service\JumpService::update',
        'App\Resource\Service\JumpService::delete',
        // 店铺
        'App\Resource\Service\ShopService::add',
        'App\Resource\Service\ShopService::update',
        'App\Resource\Service\ResourceService::unBindGoods',
        // 店铺账号/管理员
        'App\User\Service\UserService::add',
        'App\User\Service\UserService::update',
        'App\User\Service\UserService::bindUidWithShop',
        // 店群管理
        'App\Resource\Service\ShopGroupService::add',
        'App\Resource\Service\ShopGroupService::update',
        // 标签管理
        'App\User\Service\TagService::store',
        'App\User\Service\TagService::update',
        'App\User\Service\TagService::destroy',
//        'App\User\Service\MemberService::update', // 会员加标签，和小程序共用接口
        // 团长
        'App\Resource\Service\TeamLeaderService::edit',
        'App\Resource\Service\TeamLeaderService::withdrawApprove',
        'App\Resource\Service\TeamLeaderService::check',
        // 次日达商城配置
        'App\Resource\Service\SystemService::CrdShopConfig',
        'App\Resource\Service\SystemService::UpdateShareConfig',
        // 地推人员
        'App\Resource\Service\GroundPromotionUserService::add',
        'App\Resource\Service\GroundPromotionUserService::update',
        'App\Resource\Service\GroundPromotionUserService::delete',
        // 菜谱
        'App\Resource\Service\CookbookService::add',
        'App\Resource\Service\CookbookService::update',
        'App\Resource\Service\CookbookService::delete',
        'App\Resource\Service\CookbookService::editSort',
        // 平台管理员
        'App\User\Service\UserService::add',
        'App\User\Service\UserService::update',
        'App\User\Service\UserService::delete',
        'App\User\Service\UserService::forceDelete',
        'App\User\Service\UserService::restore',
        'App\User\Service\PermissionService::restore',
        'App\User\Service\UserService::editPassword',
        // 平台角色/权限菜单
        'App\User\Service\PermissionService::add',
        'App\User\Service\PermissionService::update',
        'App\User\Service\PermissionService::delete',
        // 系统参数
        'App\Resource\Service\SystemService::update',
        'App\Resource\Service\StoreService::update',


    ];

    // 要切入的注解，具体切入的还是使用了这些注解的类，仅可切入类注解和类方法注解
    public $annotations = [
    ];

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $res = $proceedingJoinPoint->process();
        if (!$res['code']){
            $interfacePath = $this->request->getAttribute(Dispatched::class)->handler->callback;    // 获取请求接口地址
            $params = $this->formRequest->getParsedBody();

            $shopType = $params['shop_type'] ?? null;
            $operationResults = $res['info']['operation_results'] ?? null;
            if($shopType){
                $res['info']['operation_describe'] = $this -> pathTest::getpath($interfacePath[0].'\\'.$interfacePath[1].'?shop_type=1').$operationResults;
            }else{
                $res['info']['operation_describe'] = $this -> pathTest::getpath($interfacePath[0].'\\'.$interfacePath[1]).$operationResults;
            }

            $res['info']['uid'] = $this->jwt->setScene('default')->getParserData()['uid'];
            $res['info']['ip'] = $this->request->getServerParams()['remote_addr'];   // $this -> httpFoundationRequest -> getClientIp()
//            $res['info']['ip'] = $this->request->server('remote_addr');   // $this -> httpFoundationRequest -> getClientIp()
            $res['info']['class_name'] = $interfacePath[0];
            $res['info']['method_name'] = $interfacePath[1];
            $res['info']['request_param'] = json_encode($this->request->all());

//            var_dump($res['info']);die;

            $this->container->get(BaseService::class)->changeStatusLog($res['info']);
        }
        return $res;
    }
}
