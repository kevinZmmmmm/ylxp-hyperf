<?php

namespace App\Common\Redis;

use Hyperf\Redis\Redis;

class ActivityGroupRedis extends Redis
{
    // 对应的 Pool 的 key 值
    protected $poolName = 'group';
}