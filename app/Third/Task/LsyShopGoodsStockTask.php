<?php

namespace App\Third\Task;
use App\Resource\Model\CrontabLog;
use App\Third\Service\Lsy\PullLsyStockService;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Di\Annotation\Inject;
use Psr\Container\ContainerInterface;

/**
 * @Crontab(name="LsyShopGoodsStockTask", rule="0 *\/20 * * * *", callback="task", memo="20分钟全量同步集团下门店库存")
 * Class LsyShopGoodsStockTask
 * @package App\Third\Task
 */

class LsyShopGoodsStockTask
{

    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;

    public function task()
    {
        if(env('APP_ENV') != 'dev') {
            $resMsg = '获取全量同步集团下门店库存-';
            $beginTime = (int)(microtime(true) * 1000);
            try {
                $this->container->get(PullLsyStockService::class)->pullLsyStockToRedis();
            }catch (\Exception $e){
                $overTime = (int)(microtime(true) * 1000);
                $resMsg .= '-消耗时间' . ($overTime - $beginTime) . 'ms';
                $data = [
                    'name' => 'LsyShopGoodsStockTask',
                    'info' => $e->getMessage(),
                    'status' => 1,
                    'job_rule' => '0 0/20 * * *',
                    'job_desc' => $resMsg,
                    'created_at' => date('Y-m-d H:i:s')
                ];
                CrontabLog::query()->insert($data);
            }
        }
    }

}
