<?php

declare(strict_types=1);

namespace App\Third\Task;


use App\Third\Service\Lsy\LsyShopGoodsStockService;
use Hyperf\Task\Annotation\Task;
use Hyperf\Utils\ApplicationContext;

class InsertLsyStockTask
{
    /**
     * @Task
     * @author 1iu
     * @date 2021-02-07 15:46
     */
    public function handle()
    {
        $container = ApplicationContext::getContainer();
        $container->get(LsyShopGoodsStockService::class)->insertFromSql();
    }
}
