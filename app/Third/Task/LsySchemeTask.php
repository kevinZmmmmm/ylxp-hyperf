<?php

declare(strict_types=1);

namespace App\Third\Task;

use App\Third\Service\Lsy\LsyMainGoodsService;
use App\Third\Service\Lsy\LsyShopSchemeService;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * @Crontab(name="LsySchemeTask", rule="0 6 * * *", callback="task", memo="每天6点拉取销售方案")
 * Class LsySchemeTask
 * @package App\Third\Task
 */
class LsySchemeTask
{

    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerFactory $loggerFactory)
    {
        $this->logger = $loggerFactory->get('拉取方案', 'crmScheme');
    }


    public function task()
    {
        if (env('APP_ENV') != 'dev') {
            $resMsg = '获取龙首银有效方案ID执行完成-';
            $beginTime = (int)(microtime(true) * 1000);
            $goods_data = $this->container->get(LsyMainGoodsService::class)->CompletePullArchInfo(8);
            $menu_arr = [];
            $e_week = date('N');
            $now_time = time();
            $day_time = date('H:i', time());
            foreach ($goods_data['itemList'] as $value) {
                $plShiftList = $value['plShiftList'];
                $weekArr = $value['plWeekList'];
                $shopArr = $value['shops'];
                $startTime = array_column($plShiftList, 'startTime')[0] ?? '00:00';
                $endTime = array_column($plShiftList, 'endTime')[0] ?? '24:00';
                $beginDay = strtotime($value['beginDay'] ?? '2070-00-00 00:00:00');
                $endDay = strtotime($value['endDay'] ?? '1970-00-00 00:00:00');
                $week = array_column($weekArr, 'week');
                $shops = array_column($shopArr, 'shopId');
                if (in_array($e_week, $week) && $now_time > $beginDay && $now_time < $endDay && $day_time > $startTime && $day_time < $endTime) {
                    $menu_arr[] = [
                        'schemeID' => $value['id'],
                        'menu_name' => $value['name'],
                        'shops' => implode(',', $shops),
                        'weekList' => implode(',', $week),
                        'startTime' => $startTime,
                        'endTime' => $endTime,
                        'beginDay' => $value['beginDay'],
                        'endDay' => $value['endDay'],
                        'is_enable' => 1,
                    ];
                } else {
                    //方案是否开启
                    if ($value['isEnable'] && $value['hasItem'] == 1) {
                        $menu_arr[] = [
                            'schemeID' => $value['id'],
                            'menu_name' => $value['name'],
                            'shops' => implode(',', $shops),
                            'weekList' => '长期有效',
                            'startTime' => $startTime,
                            'endTime' => $endTime,
                            'is_enable' => 1,
                        ];
                    }
                }
            }
            $scheme = $this->container->get(LsyShopSchemeService::class);
            $scheme->lsySchemeDelete(['is_enable' => 1]);
            $scheme->saveAll($menu_arr);
            $overTime = (int)(microtime(true) * 1000);
            $resMsg .= '-消耗时间' . ($overTime - $beginTime) . 'ms';
            $this->logger->info('定时拉去销售方案成功', ['pull success' => $resMsg]);
        }
    }
}
