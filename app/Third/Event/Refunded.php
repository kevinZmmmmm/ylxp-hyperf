<?php

declare(strict_types=1);

namespace App\Third\Event;


class Refunded
{
    public $order;
    public $type;

    public function __construct($order, $type)
    {
        $this->order = $order;
        $this->type = $type;
    }
}
