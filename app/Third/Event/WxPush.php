<?php

declare(strict_types=1);

namespace App\Third\Event;

class WxPush
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
}