<?php

declare(strict_types=1);

namespace App\Third\Event;


class RefundedPush
{
    public $order_no;
    public $orderRefundGoods;
    public $needRefundMoney;
    public $refundType;
    public $partRefundFlg;
    public $note;

    public function __construct($order_no, $orderRefundGoods, $needRefundMoney, $refundType, $partRefundFlg = 0, $note = '')
    {
        $this->order_no = $order_no;
        $this->orderRefundGoods = $orderRefundGoods;
        $this->needRefundMoney = $needRefundMoney;
        $this->refundType = $refundType;
        $this->partRefundFlg = $partRefundFlg;
        $this->note = $note;
    }
}
