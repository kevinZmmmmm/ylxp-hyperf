<?php

declare(strict_types=1);

namespace App\Third\Event;


class KzRefunded
{
    public $order;
    public $res;
    public $type;

    public function __construct($order, $KzRes)
    {
        $this->order = $order;
        $this->res = $KzRes;
    }
}
