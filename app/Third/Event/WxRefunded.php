<?php

declare(strict_types=1);

namespace App\Third\Event;


class WxRefunded
{
    public $order;
    public $kzRes;
    public $wxRes;

    public function __construct($order, $kzRes, $wxRes)
    {
        $this->order = $order;
        $this->kzRes = $kzRes;
        $this->wxRes = $wxRes;
    }
}
