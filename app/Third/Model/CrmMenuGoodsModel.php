<?php

declare (strict_types=1);

namespace App\Third\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class CrmMenuGoodsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crm_menu_goods';
    const UPDATED_AT = null;
    const CREATED_AT = 'create_at';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
