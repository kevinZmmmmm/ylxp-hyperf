<?php

declare (strict_types=1);

namespace App\Third\Model;

use Hyperf\DbConnection\Model\Model;

class WxDedoLogModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'qb_wx_dedo_log';

    protected $connection = 'log';

    const UPDATED_AT = null;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];


    public function getTypeAttribute($value)
    {
        if($value == 1){
            return '成功';
        }elseif($value == 2){
            return '异常';
        }else{
            return '未知';
        }
    }


    public function getPosTypeAttribute($value)
    {
        if($value == 1){
            return '外卖推送下单';
        }elseif($value == 2){
            return '外卖推送退单';
        }else{
            return '未知';
        }
    }
}
