<?php
/**
 * Created by PhpStorm.
 * User: ran
 * Date: 2020/12/15
 * Time: 11:27
 */

namespace App\Third\Model;


use Hyperf\DbConnection\Model\Model;

class ShopLsyGoodsStockModel extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_lsy_goods_stock';

    protected $connection = 'statistics';
    /**
     * The attributes that are mass assignable.
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];

}
