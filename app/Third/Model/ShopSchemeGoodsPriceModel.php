<?php

declare (strict_types=1);

namespace App\Third\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class ShopSchemeGoodsPriceModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_scheme_goods_price';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'statistics';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'schemeID' => 'string'
    ];
}
