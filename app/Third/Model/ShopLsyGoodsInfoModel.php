<?php

declare (strict_types=1);

namespace App\Third\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class ShopLsyGoodsInfoModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_lsy_goods_info';

    protected $connection = 'statistics';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $casts = [
        'id' => 'string'
    ];

}
