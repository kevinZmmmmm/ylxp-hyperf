<?php
namespace App\Third\Service\Lsy;


use App\Common\Service\BaseService;
use App\Third\Model\ShopSchemeModel;

class LsyShopSchemeService extends BaseService
{

    /**
     * @param $ShopNo
     * @return \Hyperf\Utils\HigherOrderTapProxy|mixed|void
     */
    public static function getLsySchemeIdByShopId($ShopNo)
    {
        return ShopSchemeModel::query()->whereRaw('FIND_IN_SET(:ShopNo,shops)', ['ShopNo' => $ShopNo])->value('schemeID');
    }
    /**
     * @param $where
     * @return mixed
     */
    public static function lsySchemeDelete($where)
    {
        return ShopSchemeModel::query()->where($where)->delete();
    }
    public function saveAll(array $data){
        ShopSchemeModel::query()->insert($data);
    }

}