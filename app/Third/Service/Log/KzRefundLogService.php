<?php
declare(strict_types=1);

namespace App\Third\Service\Log;


use App\Common\Service\BaseService;
use App\Order\Model\KzRefundLogModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class KzRefundLogService extends BaseService
{
    /**
     * @param array|null $order_no
     * @return array
     */
    public function refundInfo(?array $order_no)
    {
        return KzRefundLogModel::query()
            ->selectRaw('SUM(refund_balance_money) as refund_balance_money,order_no')
            ->whereIn('order_no', $order_no)
            ->groupBy(['order_no'])
            ->get()
            ->toArray();
    }

    /**
     * 商品分析---客至退款金额对比
     * @param array $params
     * @return array
     */
    public function refundAmtInfo(array $params)
    {
        $basic = KzRefundLogModel::query()
            ->selectRaw('SUM(refund_balance_money) as refund_balance_money')
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('shop_id', $shop_id);
            })
            ->whereBetween('create_time', $params['date'])
            ->first()
            ->toArray();

        $contrast = KzRefundLogModel::query()
            ->selectRaw('SUM(refund_balance_money) as refund_balance_money')
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('shop_id', $shop_id);
            })
            ->whereBetween('create_time', $params['contrasttime'])
            ->first()
            ->toArray();

        return ['basic' => $basic['refund_balance_money'], 'contrast' => $contrast['refund_balance_money']];
    }

    /**
     * @param array $params
     * @return array
     */
    public function refundAmtInfoByDateRange(array $params)
    {
        if ($params['flag'] == 1) {
            $selectRaw = "DATE_FORMAT(create_time, '%Y-%m-%d') as date,SUM(refund_balance_money) as refund_balance_money";
        } else {
            $selectRaw = "DATE_FORMAT(create_time, '%Y-%m') as date,SUM(refund_balance_money) as refund_balance_money";
        }
        $basic = KzRefundLogModel::query()
            ->selectRaw($selectRaw)
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('shop_id', $shop_id);
            })
            ->whereBetween('create_time', $params['date'])
            ->groupBy(['date'])
            ->get()
            ->toArray();

        $contrast = KzRefundLogModel::query()
            ->selectRaw($selectRaw)
            ->when(is_numeric($params['order_source']), function ($query) use ($params) {
                return $query->where('order_source', $params['order_source']);
            })
            ->when(is_numeric($params['order_type']), function ($query) use ($params) {
                return $query->where('order_type', $params['order_type']);
            })
            ->when($params['shop_id'] ?? 0, function ($query, $shop_id) {
                return $query->where('shop_id', $shop_id);
            })
            ->whereBetween('create_time', $params['contrasttime'])
            ->groupBy(['date'])
            ->get()
            ->toArray();

        return ['basic' => $basic, 'contrast' => $contrast];
    }

    /**
     * 新增数据
     * @param array $data
     * @return Builder|Model
     */
    public function create(array $data)
    {
        return KzRefundLogModel::query()->create($data);
    }

}