<?php

declare(strict_types=1);

namespace App\Third\Listener;

use App\Common\Constants\ErrorCode;
use App\Common\Constants\Stakeholder;
use App\Common\Exception\RefundException;
use App\Common\Service\BaseService;
use App\Order\Service\OrderService;
use App\Third\Event\WxRefunded;
use App\Third\Service\Log\WxRefundLogService;
use Exception;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Psr\Container\ContainerInterface;

/**
 * @Listener
 */
class WxRefundedListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            WxRefunded::class
        ];
    }

    public function process(object $event)
    {
        $orderInfo = $event->order;
        $KzRes = $event->kzRes;
        $wxRes = $event->wxRes;

        try {

            $refundTicketCodeArr = [];
            $kzRefundCouponTotalMoney = $kzRefundWeChatTotalMoney = 0;
            if (isset($KzRes['data']['refundSubInfo'])) {
                foreach ($KzRes['data']['refundSubInfo'] as $arr) {
                    if ($arr['refundType'] == Stakeholder::KZ_PAY_WECHAT) {
                        $kzRefundWeChatTotalMoney += $arr['fee'];
                    }
                    if ($arr['refundType'] == Stakeholder::KZ_PAY_COUPON) {
                        $refundTicketCodeArr[] = $arr['ticketCode'];
                        $kzRefundCouponTotalMoney += $arr['fee'];
                    }
                }
            }
            $insertData = [
                'shop_id' => $orderInfo['shop_id'],
                'order_no' => $orderInfo['order_no'],
                'order_type' => $orderInfo['order_type'],
                'order_source' => $orderInfo['order_source'],
                'pay_at' => $orderInfo['pay_at'],
                'platform_type' => $orderInfo['platform_type'],
                'leader_id' => $orderInfo['leader_id'],
                'refund_commission_money' => $orderInfo['refundCommission'] ?? 0,
                'refund_total_money' => bcdiv((string)$KzRes['data']['refundMoney'] ?? '0', '100', 2),
                'refund_wx_money' => $wxRes['data']['realMoney'],
                'refund_wx_principal_money' => bcdiv((string)$KzRes['data']['storePrincipalMoney'] ?? '0', '100', 2),
                'refund_wx_grants_money' => bcdiv((string)$KzRes['data']['storeGrantsMoney'] ?? '0', '100', 2),
                'refund_coupon_money' => bcdiv((string)$kzRefundCouponTotalMoney, '100', 2),
                'coupon_ticket_code' => $refundTicketCodeArr ? implode(',', $refundTicketCodeArr) : '',
                'res_detail' => json_encode($KzRes, JSON_UNESCAPED_UNICODE),
                'wx_res_detail' => json_encode($wxRes, JSON_UNESCAPED_UNICODE),
                'create_time' => date('Y-m-d H:i:s')
            ];
            // 写入日志
            $this->container->get(WxRefundLogService::class)->create($insertData);

            $transaction_id = isset($wxRes['data']['transaction_id']) ? $wxRes['data']['transaction_id'] : '';
            $orderUpdateData = ['transaction_id' => $transaction_id];
            $this->container->get(OrderService::class)->update(['order_no' => $orderInfo['order_no']], $orderUpdateData);

        } catch (Exception $e) {
            $param = json_encode(['orderInfo' => $orderInfo, 'kzRes' => $KzRes, 'wxRes' => $wxRes, 'error' => $e->getMessage()]);
            $this->container->get(BaseService::class)->write('吾享退款成功,写入日志错误', $param);
            throw new RefundException('吾享退款成功,写入日志失败', ErrorCode::SYSTEM_INVALID);
        }
    }
}
