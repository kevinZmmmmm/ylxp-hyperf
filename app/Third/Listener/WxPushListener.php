<?php

declare(strict_types=1);

namespace App\Third\Listener;

use App\Third\Event\WxPush;
use App\Third\Model\WxDedoLogModel;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class WxPushListener implements ListenerInterface
{
    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            WxPush::class,
        ];
    }

    /**
     * @param object $event
     */
    public function process(object $event)
    {
        $data = $event->data;
        WxDedoLogModel::query()->create(
            [
                'order_no' => $data['order_no'],
                'shop_id' => $data['shop_id'],
                'params_detail' => $data['params_detail'],
                'res_detail' => $data['res_detail'],
                'deId' => $data['deId'],
                'deNo' => $data['deNo'],
                'type' => $data['type'],
                'pay_type' => $data['pay_type'],
                'pos_type' => $data['pos_type'],
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
    }
}
