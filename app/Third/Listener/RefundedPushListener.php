<?php

declare(strict_types=1);

namespace App\Third\Listener;

use App\Third\Event\RefundedPush;
use App\Third\Service\Wx\WxDeliverService;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Psr\Container\ContainerInterface;

/**
 * @Listener
 */
class RefundedPushListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            RefundedPush::class
        ];
    }

    public function process(object $event)
    {
        $order_no = $event->order_no;
        $orderRefundGoods = $event->orderRefundGoods;
        $needRefundMoney = $event->needRefundMoney;
        $refundType = $event->refundType;
        $partRefundFlg = $event->partRefundFlg;
        $note = $event->note;
        $this->container->get(WxDeliverService::class)
            ->refundedPush($order_no, $orderRefundGoods, $needRefundMoney, $refundType, $partRefundFlg, $note);

    }
}
