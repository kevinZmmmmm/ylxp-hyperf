<?php


namespace App\Third\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Third\Service\Sms\SmsService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 */
class SmsController extends AbstractController
{
    /**
     * @Inject()
     * @var SmsService
     */
    private $smsService;

    /**
     * 发送手机验证码
     * @PostMapping(path="/v1/sms/send", methods="post")
     * @param RequestInterface $request
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(RequestInterface $request)
    {
        $phone = $request -> input('phone');
        if(!$phone){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $code = mt_rand(100000, 999999);
        $res = $this->smsService->send($phone, $code);
        if (!$res['code']) {
            return $this->success($res['data'], '发送成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
