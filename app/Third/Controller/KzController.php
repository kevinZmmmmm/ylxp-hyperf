<?php

declare(strict_types=1);

namespace App\Third\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Third\Service\Kz\KzMainService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\JwtAuthMiddleware;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * Class KzController
 * @package App\Third\Controller
 * @Controller()
 */
class KzController extends AbstractController
{
    /**
     * @Inject()
     * @var KzMainService
     */
    private $kzMainService;

    /**
     * 领劵中心===优惠券列表
     * @RequestMapping(path="/v1/cus/coupon", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function couponList(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $shopId = (string)$request->input('shop_id', '');
        $couponType = (int)$request->input('coupon_type', 2);
//        $goodsId = (string)$request->input('goods_id', 0);
        $pageNum = (int)$request->input('page', 1);
        $pageSize = (int)$request->input('perpage', 50);
        if (empty($shopId) || empty($couponType)){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->kzMainService->couponCenter($mid, $shopId, $couponType, $pageNum, $pageSize);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data']['list'], $res['msg'], $res['data']['count']);
    }
    /**
     * 领取优惠券
     * @RequestMapping(path="/v1/cus/coupon/receive", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function receiveCoupon(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $shop_id = (string)$request->input('shop_id', '');
        $coupon_activity_id = (string)$request->input('coupon_activity_id', '');
        $draw_coupon_id = (string)$request->input('draw_coupon_id', '');
        $subcode = (string)$request->input('subcode', '');
        if (empty($shop_id) || empty($coupon_activity_id) || empty($draw_coupon_id) || empty($subcode)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, "shop_id/coupon_activity_id/draw_coupon_id/subcode参数错误");
        }
        $res = $this->kzMainService->receiveCouponDo($mid, $shop_id, $coupon_activity_id, $draw_coupon_id, $subcode);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }
    /**
     * 优惠券详情
     * @RequestMapping(path="/v1/cus/coupon/detail", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function couponDetail(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $subcode = (string) $request->input('subcode'); //券ID
        if (empty($subcode)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, 'subcode参数错误');
        }
        $res = $this->kzMainService->getCouponDetail((int)$mid, $subcode);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }
    /**
     * 优惠券可用门店
     * @RequestMapping(path="/v1/cus/coupon/shop", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function stores(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $subcode = (string) $request->input('subcode'); //券ID
        if (empty($subcode)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, 'subcode参数错误');
        }
        $res = $this->kzMainService->getStores((int)$mid, $subcode);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }
    /**
     * 商品适用优惠券
     * @RequestMapping(path="/v1/cus/coupon/goods", methods="get")
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function goodsCoupon(){
        $shopId = (string)$this->request->input('shop_id', '');
        $couponType = (int)$this->request->input('coupon_type', 2);
        $goodsId = (string)$this->request->input('kz_goods_id', 0);
        $typeId = (string)$this->request->input('kz_type_id', 0);
        $brandId = (string)$this->request->input('kz_brand_id', 0);
        $pageNum = (int)$this->request->input('page', 1);
        $pageSize = (int)$this->request->input('perpage', 50);
        if (empty($shopId)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, 'shop_id错误');
        }
        $goodsId = is_numeric($goodsId) ? $goodsId:'0';
        $res = $this->kzMainService->getGoodsCouponList($shopId, $couponType, $goodsId, $typeId, $brandId, $pageNum, $pageSize);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data']['list'], $res['msg'], $res['data']['count'] );
    }
    /**
     * 订单可用优惠券
     * @RequestMapping(path="/v1/cus/coupon/order", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orderCoupon(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $shopId = (string)$request->input('shop_id', '');
        $orderSource = $request->input('order_source');
        $goodsList = $request->input('goods_list');
        $orderIntegral = (string)$request->input('order_integral', '0'); // 订单积分
//        $buildTime = (string)$request->input('build_time', date('YmdHis')); // 下单时间
        $freightFee = (string)$request->input('freight', '0'); // 运费
        $entrustId = $request->input('entrust_id', null);
        $goodsListArr = json_decode($goodsList, true);
        if (!is_array($goodsListArr) || empty($shopId) || !is_numeric($orderSource)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, 'goods_list/shop_id/order_source错误');
        }
        $res = $this->kzMainService->getOrderCouponList($mid, $shopId, $orderIntegral, $freightFee, $goodsListArr, $orderSource, $entrustId);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }
    /**
     * 订单使用优惠券
     * @RequestMapping(path="/v1/cus/coupon/use", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function useCoupon(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $payKey = (string)$request->input('pay_key');
        $orderSource = $request->input('order_source');
        $shopId = (string)$request->input('shop_id', ''); // 店铺ID
        $goodsList = $request->input('goods_list'); // 商品列表
        $coupon = $request->input('coupon'); // 优惠券信息
        $goodsListArr = json_decode($goodsList, true);
        $couponArr = json_decode($coupon, true);
//        $couponDeductedMoney = (string)$request->input('coupon_deducted_money');//优惠券已抵扣的总金额
        $usePayType = (int)$request->input('use_type', 2); // 使用类型:2取消，1使用，3清除
        $freightFee = (string)$request->input('freight', '0'); // 运费
        $entrustId = $request->input('entrust_id', null);
        if (!is_array($goodsListArr) || !is_array($couponArr) || empty($shopId) || !is_numeric($orderSource)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, 'goods_list/coupon/shop_id/order_source错误');
        }
        if (empty($payKey)) return $this->failed(ErrorCode::SYSTEM_INVALID, '缺少payKey');
        $res = $this->kzMainService->useCouponDo($mid, $shopId, $goodsListArr, $couponArr, $usePayType, $freightFee, $payKey,$orderSource,$entrustId);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);

    }
    /**
     * 新人专属--优惠券
     * @RequestMapping(path="/v1/cus/coupon/newcomer", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function newcomerCoupon(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $isEffect = (string)$request->input('is_effect', '1'); // 是否过期
        $fState = (string)$request->input('state', '1'); // 方案状态
        $fActivityName = '新人';
        $pageNum = (int)$request->input('page', 1);
        $pageSize = (int)$request->input('perpage', 50);
        if (!is_numeric($mid)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, '登陆信息失效,请重新登陆噢');
        }
        $res = $this->kzMainService->newcomerCoupon($mid, $isEffect, $fState, $fActivityName, $pageNum, $pageSize);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data']['list'], $res['msg'], $res['data']['total']);
    }
    /**
     *新人专属--一键领取优惠券
     * @RequestMapping(path="/v1/cus/coupon/collect", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function batchCollectNewcomerCoupon(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $shopId = (string)$request->input('shop_id', '');
        $coupons = $request->input('coupons'); // 优惠券信息
        if (is_string($coupons)) $coupons = json_decode($coupons, true);
        if (empty($shopId) || empty($coupons)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, '店铺ID或优惠券信息错误');
        }
        $res = $this->kzMainService->batchCollectNewcomerCoupon($mid, $shopId, $coupons);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);

    }
    /**
     * 新人专属--活动规则
     * @RequestMapping(path="/v1/cus/coupon/rule", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function newcomerActivityRule(){
        $request = $this->request;
        $mid = (int)$request->getAttribute('mid');
        $isEffect = (string)$this->request->input('is_effect', '1');
        $fState = (string)$this->request->input('state', '1');
        $fActivityName = '新人';
        $pageNum = (int)$this->request->input('page', 1);
        $pageSize = (int)$this->request->input('perpage', 50);
        if (!is_numeric($mid)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, '登陆信息失效,请重新登陆噢');
        }
        $res = $this->kzMainService->newcomerActivityRule($isEffect, $fState, $fActivityName, $pageNum, $pageSize);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }

    /**
     * 年度账单赠优惠券
     * @RequestMapping(path="/v1/api/bill/give", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class)
     * })
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function yearBillGiveCoupon(){
        $mid = (int) $this->request->getAttribute('mid');
        $shopId = '20200606322061';
        if (!is_numeric($mid)){
            return $this->failed(ErrorCode::SYSTEM_INVALID, '用户信息错误');
        }
        $res = $this->kzMainService->yearBillGiveCoupon($mid, $shopId);
        if ($res['code'] == 0) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg']);
    }

}
