<?php


namespace App\Third\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Third\Service\Trade\SourceService;
use App\Third\Service\Wx\WxDeliverService;
use App\Third\Service\Wx\WxPayService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class WxController extends AbstractController
{
    /**
     * @Inject()
     * @var SourceService
     */
    private $thirdService;

    /**
     * 获取吾享商品编码列表
     * @param RequestInterface $request
     * @RequestMapping(path="/v1/wx/goods", methods="get")
     * @return array
     */
    public function getWxGoodsList(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : '';
        $field = ['name', 'itemCode'];
        $list = $this->thirdService->getCrmGoodsList($params, $perPage, $field, 'wx');
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }
    }
    /**
     * 同步吾享编
     * @RequestMapping(path="/v1/wx/code", methods="get")
     * @return array
     */
    public function syncWxCode()
    {
        $res = $this->thirdService->getCrmGoodsInfoCode();
        if ($res) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
    /**
     * @RequestMapping(path="/v1/wx/pay/test", methods="post")
     */
    public function wxPay()
    {
        $orderNo = 'CSXP' . date('mdHis') . mt_rand(11111, 99999);
        $openId = 'o3s--4nbiGZP9FSSRfHr1eg01EbE';
        $this->container->get(WxPayService::class)->comPaySubmit($orderNo, $openId, 0.01);
    }

    /**
     * @RequestMapping(path="/v1/wx/refund/test", methods="post")
     */
    public function wxRefund()
    {
        $orderNo = 'CSXP20200818174158405381QMV3210';
        $this->container->get(WxPayService::class)->comPayCancel($orderNo, 0.01);
    }
    /**
     * @RequestMapping(path="/v1/wx/service/env", methods="get")
     */
    public function serviceEnv()
    {
        return file_get_contents(BASE_PATH . '/.env');

    }
    /**
     * 吾享异常外卖订单
     * @RequestMapping(path="/v1/wx/order/abnormal", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function abnormalOrder(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field  = ['order_no','shop_id','mid','deal_type','address_id','is_whole','total_price','goods_price','freight_price','is_pay','is_whole','pay_type','appointment_time','status','order_type','pay_at','create_at','order_phone'];
        $res = $this->thirdService->wxOrderLog($params, (int)$perPage, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }
    /**
     * 推送异常外卖单
     * @RequestMapping(path="/v1/wx/push/abnormal", methods="post")
     * @param RequestInterface $request
     * @return array
     */
    public function pushAbnormalOrder(RequestInterface $request)
    {
        $order_no = $request->input('order_no');
        $res = $this->container->get(WxDeliverService::class)->payPush($order_no);
        if ($res['code'] == 1) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['errorCode'], $res['errorMsg']);
        }
    }

    /**
     * 手动推送外卖退单
     * @RequestMapping(path="/v1/wx/push/chargeback", methods="post")
     * @param RequestInterface $request
     * @return array
     */
    public function manualTakeOutChargeBack(RequestInterface $request)
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->thirdService->manualPushTakeOutChargeBack((int)$params['id']);
        if(!$res['code']) return $this->success($res['data'],'系统处理成功');
        return $this->failed($res['code'], $res['msg']);
    }

}
