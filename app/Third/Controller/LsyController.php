<?php


namespace App\Third\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Third\Service\Lsy\LsyMainGoodsService;
use App\Third\Service\Trade\SourceService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 */
class LsyController extends AbstractController
{

    /**
     * @Inject()
     * @var SourceService
     */
    private $sourceService;

    /**
     * @Inject()
     * @var LsyMainGoodsService
     */
    private $lsyMainGoodsService;

    /**
     * 龙收银商品资料
     * @RequestMapping(path="/v1/crm/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getCrmGoodsList(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 30;
        $field = ['*'];
        $list = $this->sourceService->getCrmGoodsList($params, (int)$perPage, $field, 'lsy');
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }
    }
    /**
     * 龙收银应用信息详情
     * @RequestMapping(path="/v1/crm/goods/scheme", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getCrmSchemeGoodsList(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 30;
        $field = ['*'];
        $list = $this->sourceService->getCrmGoodsList($params, (int)$perPage, $field, 'lsyScheme');
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }
    }
    /**
     * 获取龙收银库存余量接口
     * @RequestMapping(path="/v1/crm/lsyStock", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getCompletePullArchInfoQuantity(RequestInterface $request)
    {
        $goods_crm_code = $request -> input('goods_crm_code');
        $lsy_shop_no = $request -> input('lsy_shop_no');
        if(!$goods_crm_code || !$lsy_shop_no){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $goods_Info  = $this->lsyMainGoodsService->SinglePullArchInfoStock($goods_crm_code,$lsy_shop_no);
        $list =[
            'surplusQuantity'=>$goods_Info['surplusQuantity'] ?? 0,
            'salesQuantity'=>$goods_Info['salesQuantity'] ?? 0,
        ];
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list, '获取成功');
        }
    }
    /**
     * 获取龙收银店铺编码 商品ID 获取库存
     * @RequestMapping(path="/v1/crm/goodsStock", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getSinglePullArchInfoStock(RequestInterface $request)
    {
        $goods_crm_code = $request -> input('goods_crm_code');
        $lsy_shop_no = $request -> input('lsy_shop_no');
        if(!$goods_crm_code || !$lsy_shop_no){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $goods_Info  = $this->lsyMainGoodsService->SinglePullArchInfoStock($goods_crm_code,$lsy_shop_no);
        $list[] =[
            'surplusQuantity'=>$goods_Info['surplusQuantity'] ?? 0,
            'salesQuantity'=>$goods_Info['salesQuantity'] ?? 0,
        ];
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list, '获取成功');
        }
    }
    /**
     * 获取商品价格
     * @RequestMapping(path="/v1/crm/goodsPrice", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function SinglePullArchInfoStock(RequestInterface $request)
    {
        $goods_crm_code = $request -> input('goods_crm_code');
        $shop_id = $request -> input('shop_id');
        if(!$goods_crm_code || !$shop_id){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $goods_Info  = $this->lsyMainGoodsService->SinglePullArchInfoStock($goods_crm_code,$shop_id);
        $list =[
            'surplusQuantity'=>$goods_Info['surplusQuantity'] ?? 0,
            'salesQuantity'=>$goods_Info['salesQuantity'] ?? 0,
            'del_flg'=>$goods_Info['delflg'],
            'is_GuQing'=>$goods_Info['isGuQing'],
        ];
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list, '获取成功');
        }
    }
    /**
     * 获取龙收银商品信息
     * @RequestMapping(path="/v1/crm/goodsInfo", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getCrmMenuGoodsInfo(RequestInterface $request)
    {
        $goods_crm_code = $request -> input('goods_crm_code');
        $goods_id = $request -> input('goods_id');
        $shop_id = $request -> input('shop_id');
        if(!$goods_crm_code || !$goods_id || !$shop_id){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $list = $this->sourceService->getLsyGoodsInfo($goods_crm_code,$goods_id,$shop_id);
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data'], '获取成功');
        }
    }
    /**
     * 获取龙收银应用方案
     * @RequestMapping(path="/v1/crm/scheme", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getLsyApplicationScheme(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 30;
        $field = ['schemeID', 'menu_name', 'shops','weekList','update_date'];
        $list = $this->sourceService->getLsyApplicationScheme($params, (int)$perPage, $field);
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }
    }
    /**
     * 根据当前龙收银方案同步价格
     * @RequestMapping(path="/v1/crm/scheme/{id:\d+}", methods="get")
     * @param $id
     * @return array
     */
    public function lsyApplicationSchemeSync($id)
    {
        $list = $this->sourceService->getCrmSchemeGoodsPriceBySchemeId($id);
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success([], "获取成功,共耗时{$list['data']}秒");
        }
    }
    /**
     * 全量拉取龙收银商品信息
     * @RequestMapping(path="/v1/crm/lsyGoodsInfo", methods="get")
     * @param $id
     * @return array
     */
    public function GetLsyGoodsInfo()
    {
        $list = $this->sourceService->getCrmLsyGoodsInfo();
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success([], "获取成功,共耗时{$list['data']}秒");
        }
    }
    /**
     * 用户全量获取余量接口（1.0 老库存）
     * @RequestMapping(path="/v1/crm/lsyGoodsStockByShopId", methods="get")
     * @return array
     */
    public function getCompletePullArchInfoStock()
    {
        $list = $this->sourceService->getCompletePullArchInfoStock();
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success([], "获取成功,共耗时{$list['data']}秒");
        }
    }
    /**
     * 龙收银拉取时间段内店铺合计数据
     * @RequestMapping(path="/v1/external/shop/gross", methods="post")
     * @return array
     */
    public function shopOrderInfoToLsy(){
        $params = $this->request->all();
        $start_time = trim($params['start_time']);
        $end_time = trim($params['end_time']);
        if (!strtotime($start_time) || !strtotime($end_time) || empty($params['shop_code'])){
            return $this->failed(ErrorCode::MISSING_PARAMETER);
        }
        if (strtotime($start_time) > strtotime($end_time) ){
            return $this->failed(ErrorCode::MISSING_PARAMETER);
        }
        $shop_code = json_decode($params['shop_code'], true);
        if (!$shop_code){
            return $this->failed(ErrorCode::MISSING_PARAMETER);
        }

        $res = $this->sourceService->getShopOrderInfoToLsy($start_time, $end_time, $shop_code);
        if (!$res['code']) {
            return $this->failed($res['errorCode'], $res['msg']);
        }
        return $this->success($res['data'], $res['msg'], count($res['data']));
    }
    /**
     * 龙收银商品库存信息
     * @RequestMapping(path="/v1/crm/goodsStockInfo", methods="get")
     * @return array
     */
    public function goodsStockInfo(){
        $params = $this->request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 30;
        $field = ['*'];
        $list = $this->sourceService->goodsStockInfo($params, (int)$perPage, $field);
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }
    }

}
