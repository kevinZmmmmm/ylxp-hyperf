<?php


namespace App\Third\Repository;


use App\Third\Model\ShopSchemeGoodsPriceModel;
use App\Third\Model\ShopSchemeModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

/**
 * 提供数据能力
 * Class LsyPriceRepository
 * @package App\Third\Repository
 */

class LsyPriceRepository implements BaseRepositoryInterface
{

    /**
     * 主表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findThirdFirst(array $field = ['*'],array $where = [],string $whereRaw='',bool $isArray=true)
    {
        $connect = ShopSchemeModel::query()->select($field)->where($where)
            ->when($whereRaw, function ($query, $whereRaw) {
                return $query->whereRaw($whereRaw);
            })
            ->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }

    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectUnionListNoPage(array $primaryField,array $sonField,string $lsy_shop_no,bool $isArray=true)
    {
        $scheme =(new ShopSchemeModel())->getTable();
        $schemePrice =(new ShopSchemeGoodsPriceModel())->getTable();
        $primaryField =array_map(function ($item) use ($scheme) {
            return $scheme.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($schemePrice) {
            return $schemePrice.'.'.$item;
        },$sonField);
        $connect = shopSchemeGoodsPriceModel::query()->from("{$schemePrice}")
            ->select(array_merge($primaryField,$sonField))
            ->leftJoin("{$scheme}", $scheme.'.schemeID', '=', $schemePrice.'.schemeID')
            ->whereRaw('FIND_IN_SET(?,'."{$scheme}".'.shops)', [$lsy_shop_no])
            ->get();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }


    /**
     * 根据主键ID主表子表两表联查获取数据
     * @param int $PrimaryId
     * @param array $PrimaryField
     * @param array $SonField
     * @return array|Builder|Model|\Hyperf\Database\Query\Builder|object
     * @author zhangzhiyuan
     */
    public function getThirdSingleSonByPrimaryId(string $primaryId,string $lsy_shop_no,array $primaryField=['*'], array $sonField=['*'],bool $isArray=true)
    {
        $scheme =(new ShopSchemeModel())->getTable();
        $schemePrice =(new ShopSchemeGoodsPriceModel())->getTable();
        $primaryField =array_map(function ($item) use ($scheme) {
            return $scheme.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($schemePrice) {
            return $schemePrice.'.'.$item;
        },$sonField);
        $connect = ShopSchemeGoodsPriceModel::query()->from("{$schemePrice}")
            ->select(array_merge($primaryField,$sonField))
            ->leftJoin("{$scheme}", $scheme.'.schemeID', '=', $schemePrice.'.schemeID')
            ->where($schemePrice.'.crm_goods_id', $primaryId)
            ->whereRaw('FIND_IN_SET(?,'."{$scheme}".'.shops)', [$lsy_shop_no])
            ->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }




}