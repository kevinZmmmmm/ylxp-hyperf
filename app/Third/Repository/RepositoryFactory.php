<?php


namespace App\Third\Repository;

use Hyperf\Di\Annotation\Inject;

class RepositoryFactory
{
    /**
     * @Inject()
     * @var LsyPriceRepository
     */
    private LsyPriceRepository $lsyPrice;

    public function getRepository($repoName){
        return $this->$repoName;
    }

}