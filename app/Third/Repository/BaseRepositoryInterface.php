<?php


namespace App\Third\Repository;


interface BaseRepositoryInterface
{
    public function findThirdFirst(array $field,array $where,string $whereRaw);
    public function getThirdSingleSonByPrimaryId(string $primaryId,string $lsy_shop_no,array $primaryField, array $sonField,bool $isArray);
}