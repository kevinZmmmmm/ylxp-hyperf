<?php

declare(strict_types=1);

namespace App\Social\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Social\Service\CollectService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 */
class CollectController extends AbstractController
{
    /**
     * @Inject()
     * @var CollectService
     */
    private $collectService;


    /**
     * 小程序用户收藏列表
     * @RequestMapping(path="/v1/collect", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function collectIndex(RequestInterface $request)
    {
        $params = $request->all();
        if (!$params['mid'] || !$params['type']) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $res = $res = $this->collectService->collectIndex($params, (int)$perPage);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }


    /**
     * 用户收藏/取消收藏菜谱
     * @RequestMapping(path="/v1/collect", methods="post")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function collect(RequestInterface $request)
    {
        $params = $request->all();
        if (!$params['mid'] || !$params['cookbook_id'] || !$params['type']) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $res = $this->collectService->collect((int)$params['mid'], (int)$params['cookbook_id'], (int)$params['type']);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

}
