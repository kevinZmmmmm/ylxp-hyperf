<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Social\Model;

use Hyperf\DbConnection\Model\Model;

class CollectModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_collect';
    protected $primaryKey = null;
    const UPDATED_AT = null;
    const CREATED_AT = null;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['shop_id'];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

}
