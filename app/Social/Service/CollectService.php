<?php

declare(strict_types=1);

namespace App\Social\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Resource\Service\CookbookService;
use App\Social\Model\CollectModel;
use Hyperf\Di\Annotation\Inject;

class CollectService extends BaseService
{
    const COLLECT = 1;

    /**
     * @Inject
     * @var CookbookService
     */
    protected $cookbookService;

    /**
     * @param array $where  type=1,菜谱收藏
     * @param int $perPage
     * @return array
     */
    public function collectIndex(array $where, int $perPage = 15)
    {
        switch ($where['type']){
            case 1:
                $list = $this -> cookbookService -> cookbookSimple($where, $perPage);
                break;
            default:
                // TODO 其他收藏
                $list = $this -> cookbookService -> cookbookSimple($where, $perPage);
        }

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }

    /**
     * @param int $mid
     * @param int $cookbook_id
     *
     * @return array
     */
    public function collect(int $mid, int $cookbook_id, int $type)
    {
        $map = [
            'mid' => $mid,
            'entity_id' => $cookbook_id,
            'type' => $type
        ];
        $is_exist = CollectModel::query()->where($map)->exists();
        if ($is_exist) {
            //取消收藏
            $res = CollectModel::query()->where($map)->delete();
        } else {
            //添加收藏
            $res = CollectModel::insert($map);
        }
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => []];
        }

        return ['code' => ErrorCode::NOT_IN_FORCE];
    }
}