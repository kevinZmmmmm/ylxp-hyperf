<?php

declare(strict_types=1);

namespace App\Activity\Task;

use App\Activity\Model\ActivityModel;
use App\Activity\Service\Free\FreeMiniService;
use Psr\Container\ContainerInterface;
use App\Order\Model\OrderModel;
use Hyperf\Crontab\Annotation\Crontab;


class FreeStatusTask
{
    /**
     * @var ContainerInterface
     */
    private $container;
    public function status()
    {
        // 获取已结束的活动
        $over = ActivityModel::query()
            ->where('activityType', 5)
            ->where('is_deleted', 0)
            ->whereIn('status', [0,3])
            ->get(['activityID'])->toArray();
        if($over){
            OrderModel::query()->where('is_pay',1)->whereNotIn('is_freed',[2,4])->whereIn('activity_id',$over)->update(['is_freed' => 3,'free_step' => 5]);
        }
        // 获取正在进行中的活动
        $list = ActivityModel::query()
            ->where('activityType', 5)
            ->where('is_deleted', 0)
            ->where('status', 2)
            ->get(['activityID as activity_id', 'activity_prams'])
            ->toArray();
        foreach ($list as $key => $val) {
            $list[$key]['activity_prams'] = json_decode($list[$key]['activity_prams'],true);
            $list[$key]['first_invite_time'] = $list[$key]['activity_prams']['first_invite_time'];
            $list[$key]['is_second'] = $list[$key]['activity_prams']['is_second'];
            $list[$key]['second_activation'] = $list[$key]['activity_prams']['second_activation'];
            $list[$key]['second_invite_time'] = $list[$key]['activity_prams']['second_invite_time'];
            unset($list[$key]['activity_prams']);
        }
        $activity_id = array_column($list,'activity_id');
        $act_list = array_column($list,null,'activity_id');
        $order_list = OrderModel::query()->whereIn('activity_id',$activity_id)->where(['order_source'=>1,'order_type'=>10,'is_pay'=>1])->get(['activity_id','order_no','pay_at','free_parent_order_no','is_freed','free_num','free_step','need_invited_num','free_activation_time','free_success_time','refund_at'])->toArray();
        $parent_order_no = array_column($order_list,'order_no');
        $free_parent = OrderModel::query()->where(['order_source'=>1,'order_type'=>10,'is_pay'=>1])->where('refund_at','=',null)->whereIn('free_parent_order_no',$parent_order_no)->selectRaw('COUNT(order_no) as num,free_parent_order_no as order_no')->groupBy(['free_parent_order_no'])->get()->toArray();
        $free_parent_num = array_column($free_parent,null,'order_no');
        $order = array_column($order_list,null,'order_no');
        foreach ($order as $s => &$i){
            if(isset($free_parent_num[$i['order_no']])){
                $i['free_parent_num'] = $free_parent_num[$i['order_no']]['num'];
            }else{
                $i['free_parent_num'] = 0;
            }
        }
        foreach ($order as $k => &$v) {
            if (!isset($act_list[$v['activity_id']])) {
                unset($order[$k]);
                continue;
            }
            if(isset($act_list[$v['activity_id']])){
                $v['first_invite_time'] = $act_list[$v['activity_id']]['first_invite_time'];
                $v['is_second'] = $act_list[$v['activity_id']]['is_second'];
                $v['second_activation'] = $act_list[$v['activity_id']]['second_activation'];
                $v['second_invite_time'] = $act_list[$v['activity_id']]['second_invite_time'];
            }
        }
        $time = time();
        foreach ($order as $ke => $va){
            if(!empty($order[$ke]['refund_at'])){
                if(!in_array($order[$ke]['is_freed'],[2,4])){
                    if($order[$ke]['is_freed'] <> 3 && $order[$ke]['free_step'] <> 3){
                        OrderModel::query()->where(['order_no'=>$order[$ke]['order_no']])->update(['is_freed' => 3,'free_step' => 3]);
                    }
                }
            }
            if((strtotime($order[$ke]['pay_at']) < $time - ($order[$ke]['first_invite_time'] * 3600)) && $order[$ke]['is_second'] == 0 && $order[$ke]['need_invited_num'] > $order[$ke]['free_parent_num']){
                if(!in_array($order[$ke]['is_freed'],[2,4])) {
                    if ($order[$ke]['is_freed'] <> 3 && $order[$ke]['free_step'] <> 3) {
                        OrderModel::query()->where(['order_no' => $order[$ke]['order_no']])->update(['is_freed' => 3, 'free_step' => 3]);
                    }
                }
            }
            if($order[$ke]['is_second'] == 1 && $order[$ke]['free_num'] <> 2 && (strtotime($order[$ke]['pay_at']) < $time - ($order[$ke]['first_invite_time'] * 3600)) && $time > strtotime($order[$ke]['pay_at']) + ($order[$ke]['first_invite_time'] * 3600) + ($order[$ke]['second_activation'] * 3600) && $order[$ke]['need_invited_num'] > $order[$ke]['free_parent_num']) {
                if(!in_array($order[$ke]['is_freed'],[2,4])) {
                    if ($order[$ke]['is_freed'] <> 3 && $order[$ke]['free_step'] <> 3) {
                        OrderModel::query()->where(['order_no' => $order[$ke]['order_no']])->update(['is_freed' => 3, 'free_step' => 3]);
                    }
                }
            }elseif($order[$ke]['is_second'] == 1 && $order[$ke]['free_num'] <> 2 && (strtotime($order[$ke]['pay_at']) < $time - ($order[$ke]['first_invite_time'] * 3600)) && $order[$ke]['need_invited_num'] > $order[$ke]['free_parent_num']){
                if(!in_array($order[$ke]['is_freed'],[2,4])) {
                    if ($order[$ke]['is_freed'] <> 1 && $order[$ke]['free_step'] <> 2) {
                        OrderModel::query()->where(['order_no' => $order[$ke]['order_no']])->update(['is_freed' => 1, 'free_step' => 2]);
                    }
                }
            }
            if($order[$ke]['free_num'] == 2 && (strtotime($order[$ke]['free_activation_time']) < $time - ($order[$ke]['second_invite_time'] * 3600)) && $order[$ke]['need_invited_num'] > $order[$ke]['free_parent_num']){
                if(!in_array($order[$ke]['is_freed'],[2,4])) {
                    if ($order[$ke]['is_freed'] <> 3 && $order[$ke]['free_step'] <> 3) {
                        OrderModel::query()->where(['order_no' => $order[$ke]['order_no']])->update(['is_freed' => 3, 'free_step' => 3]);
                    }
                }
            }
        }

    }

}
