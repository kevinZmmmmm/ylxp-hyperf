<?php

declare(strict_types=1);

namespace App\Activity\Task;

use App\Activity\Model\ActivityModel;
use App\Order\Model\OrderModel;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="FreeTask", rule="*\/30 * * * * *", callback="execute", memo="定时检测修改免单活动的状态")
 */
class FreeTask
{
    public function execute()
    {
        $operation = [];    // 收集要变更为进行中的活动 ID
        $closed = [];    // 收集要变更为已结束的活动 ID

        // 获取今天未被禁用的免单活动，并根据当前时间判断活动状态是否需要改变
        $list = ActivityModel::query()
            ->where('activityType', 5)
            ->where('is_deleted', 0)
            ->whereIn('status', [1, 2])
            ->get(['activityID', 'status', 'begin_date', 'end_date']);

        if($list->isEmpty()){
            return;
        }

        foreach ($list as $key => $val) {
            $checkTime = $this->checkTime($val['begin_date'], $val['end_date']);
            if ($checkTime == 2) {
                array_push($operation, $val['activityID']);
            } elseif ($checkTime == 3) {
                array_push($closed, $val['activityID']);
            }
        }

        if (!empty($operation)) {
            ActivityModel::query()->whereIn('activityID', $operation)->update(['status' => 2]);
        }
        if (!empty($closed)) {
            ActivityModel::query()->whereIn('activityID', $closed)->update(['status' => 3]);
            OrderModel::query()->where('is_pay',1)->whereNotIn('is_freed',[2,4])->whereIn('activity_id',$closed)->update(['is_freed' => 3,'free_step' => 5]);
        }

        // 获取已结束和禁用的活动
        $over = ActivityModel::query()
            ->where('activityType', 5)
            ->where('is_deleted', 0)
            ->whereIn('status', [0,3])
            ->get(['activityID'])->toArray();
        if($over){
            OrderModel::query()->where('is_pay',1)->whereNotIn('is_freed',[2,4])->whereIn('activity_id',$over)->update(['is_freed' => 3,'free_step' => 5]);
        }
    }

    /**
     * @param $start
     * @param $end
     * @return int
     *
     * 活动已过期，变更为禁用
     * 活动未开始，保持为未生效
     * 活动进行中，变更为进行中
     *
     * 判断当前时间是否处于给定的时间段中
     */
    public function checkTime($start, $end): int
    {
        $timeBegin = strtotime($start);
        $timeEnd = strtotime($end);
        $currentTime = time();
        if($currentTime >= $timeBegin && $currentTime < $timeEnd){
            return 2;   // 进行中
        }elseif ($currentTime < $timeBegin){
            return 1;   // 活动未开始
        }elseif ($currentTime >= $timeEnd){
            return 3;   // 活动已结束
        }

    }

}
