<?php


namespace App\Activity\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class SpecialActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => ['required', 'between:1,25', Rule::unique('store_activity')->where(function ($query) {
                        $query->where('is_deleted', 0);
                    })],
                    'activity_img' => 'required',
                    'share_img' => 'required',
                    'goods_id' => 'required',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'title' => [
                        'required',
                        'between:1,25',
                        Rule::unique('store_activity')->ignore($id, 'activityID')->where(function ($query){
                            $query->where('is_deleted', 0);
                        }),
                    ],
                    'activity_img' => 'required',
                    'share_img' => 'required',
                    'goods_id' => 'required',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '专题活动名称',
            'activity_img' => '专题活动图片',
            'share_img' => '专题活动分享图片',
            'goods_id' => '专题商品',
        ];
    }
}
