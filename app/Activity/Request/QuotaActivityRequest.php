<?php


namespace App\Activity\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class QuotaActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => ['required', 'between:1,25', Rule::unique('store_activity')->where(function ($query) {
                        $query->where('is_deleted', 0);
                    })],
                    'start_time' => 'required|date',
                    'end_time' => 'required|date|after:start_time',
                    'number' => 'sometimes|integer',
                    'stock' => 'required|integer',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'title' => [
                        'required',
                        'between:1,25',
                        Rule::unique('store_activity')->ignore($id, 'activityID')->where(function ($query){
                            $query->where('is_deleted', 0);
                        }),
                    ],
                    'start_time' => 'required|date',
                    'end_time' => 'required|date|after:start_time',
                    'number' => 'sometimes|integer',
                    'stock' => 'required|integer',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '限购活动名称',
            'start_time' => '限购开始时间',
            'end_time' => '限购结束时间',
            'number' => '限购作弊数',
            'shop_id' => '所选门店',
            'goods_id' => '商品编码',
            'crossed_price' => '划线价',
            'group_price' => '限购价',
            'stock' => '限购人数',
        ];
    }
}
