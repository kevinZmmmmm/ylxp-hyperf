<?php


namespace App\Activity\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;


class SeckillActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => ['required', 'between:1,20', Rule::unique('store_activity')->where(function ($query) {
                        $query->where('is_deleted', 0);
                    })],
                    'start_time' => 'required|date',
                    's_time' => 'required|date_format:"H:i:s"',
                    'e_time' => 'required|date_format:"H:i:s"',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
                ];

            case 'PUT':
                 $id = $this->route('id');
                return [
                    'title' => [
                        'required',
                        'between:1,20',
                        Rule::unique('store_activity')->ignore($id, 'activityID')->where(function ($query){
                            $query->where('is_deleted', 0);
                        }),
                    ],
                    'start_time' => 'required|date',
                    's_time' => 'required|date_format:"H:i:s"',
                    'e_time' => 'required|date_format:"H:i:s"',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '秒杀活动名称',
            'start_time' => '秒杀活动日期',
            's_time' => '秒杀开始时间',
            'e_time' => '秒杀结束时间',
            'shop_id' => '所选门店',
            'goods_id' => '秒杀商品',
        ];
    }

//    public function messages(): array
//    {
//        return [
//            'th_start_time.after' => '拼团提货开始时间必须在拼团结束时间 24 小时之后',
//        ];
//    }
}
