<?php


namespace App\Activity\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class GroupActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => ['required', 'between:1,25', Rule::unique('store_activity')->where(function ($query) {
                        $query->where('is_deleted', 0);
//                        $query->where('activityType', 3);
                    })],
                    'start_time' => 'required|date',
                    'end_time' => 'required|date|after:start_time',
                    'th_start_time' => 'required|date',
                    'th_end_time' => 'required|date',
                    'sort' => 'sometimes|integer',
                    'need_people' => 'sometimes|integer',
                    'number' => 'sometimes|integer',
                    'activity_img' => 'required',
                    'share_img' => 'required',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
//                    'crossed_price' => 'required',
//                    'group_price'   => 'required|lt:crossed_price',
//                    'stock'         => 'required',
                ];

            case 'PUT':
                $id = $this->route('id');
                return [
                    'title' => [
                        'required',
                        'between:1,25',
                        Rule::unique('store_activity')->ignore($id, 'activityID')->where(function ($query){
                            $query->where('is_deleted', 0);
                        }),
                    ],
                    'start_time' => 'required|date',
                    'end_time' => 'required|date|after:start_time',
//                    'th_start_time' => 'required|date|after:end_time',
//                    'th_end_time' => 'required|date|after:th_start_time',
                    'th_start_time' => 'required|date',
                    'th_end_time' => 'required|date',
                    'sort' => 'sometimes|integer',
                    'need_people' => 'sometimes|integer',
                    'number' => 'sometimes|integer',
                    'activity_img' => 'required',
                    'share_img' => 'required',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
//                    'crossed_price' => 'required',
//                    'group_price'   => 'required|lt:crossed_price',
//                    'stock'         => 'required',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '活动名称',
            'start_time' => '活动开始时间',
            'end_time' => '活动结束时间',
            'th_start_time' => '提货开始时间',
            'th_end_time' => '提货结束时间',
            'sort' => '排序',
            'need_people' => '活动人数',
            'number' => '活动作弊数',
            'activity_img' => '活动图片',
            'share_img' => '活动分享图片',
            'shop_id' => '所选门店',
            'goods_id' => '商品编码',
            'crossed_price' => '划线价',
            'group_price' => '活动价',
            'stock' => '库存',
            'tomorrow' => '明日',
        ];
    }

//    public function messages(): array
//    {
//        return [
//            'th_start_time.after' => '拼团提货开始时间必须在拼团结束时间 24 小时之后',
//        ];
//    }
}
