<?php


namespace App\Activity\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class FreeActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => ['required', 'between:1,15', Rule::unique('store_activity')->where(function ($query) {
                        $query->where('is_deleted', 0);
                    })],
                    'activity_time' => 'required',
                    'first_invite_time' => 'required',
                    'is_second' => 'required',
                    'second_activation' => 'required_if:is_second,1',
                    'second_invite_time' => 'required_if:is_second,1',
                    'activity_img' => 'required',
                    'special_image' => 'required',
                    'share_image' => 'required',
                    'share_text' => 'required',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
                ];

            case 'PUT':
                $id = $this->route('id');
                return [
                    'title' => [
                        'required',
                        'between:1,15',
                        Rule::unique('store_activity')->ignore($id, 'activityID')->where(function ($query){
                            $query->where('is_deleted', 0);
                        }),
                    ],
                    'activity_time' => 'required',
                    'first_invite_time' => 'required',
                    'is_second' => 'required',
                    'second_activation' => 'required_if:is_second,1',
                    'second_invite_time' => 'required_if:is_second,1',
                    'activity_img' => 'required',
                    'share_image' => 'required',
                    'special_image' => 'required',
                    'share_text' => 'required',
                    'shop_id' => 'required',
                    'goods_id' => 'required',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '免单活动名称',
            'activity_time' => '免单可参与时间',
            'first_invite_time' => '首次邀请好友时效',
            'is_second' => '是否允许二次激活',
            'second_activation' => '二次激活时效',
            'second_invite_time' => '第二次邀请好友时效',
            'activity_img' => '次日达首页入口展示图',
            'special_img' => '免单活动专题头部展示图',
            'share_img' => '分享图片',
            'share_text' => '分享文案',
            'shop_id' => '应用门店',
            'goods_id' => '商品信息',
            'stock' => '库存',
            'group_price' => '活动价',
            'spell_num' => '免单基数',
            'need_invited_num' => '需要请支付人数',

        ];
    }

    public function messages(): array
    {
        return [
            'second_activation.required_if' => '允许二次激活时，二次激活时效必须填写',
            'second_invite_time.required_if' => '允许二次激活时，第二次邀请好友时效必须填写',
        ];
    }
}
