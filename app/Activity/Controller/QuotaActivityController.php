<?php

declare(strict_types=1);

namespace App\Activity\Controller;

use App\Activity\Request\QuotaActivityRequest;
use App\Activity\Service\ActivityService;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class QuotaActivityController extends AbstractController
{

    /**
     * @Inject()
     * @var ActivityService
     */
    private $actService;
    const QUOTAACTIVITY = 1;


    /**
     * 获取限购活动列表
     * @RequestMapping(path="/v1/activity/quota", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['activityID', 'title', 'begin_date', 'end_date', 'status', 'create_at', 'activity_prams', 'shop_ids'];
        $res = $this->actService->getList($params, (int)$perPage, self::QUOTAACTIVITY, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取限购活动详情
     * @RequestMapping(path="/v1/activity/{id:\d+}/quota", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $field = ['activityID', 'title', 'begin_date', 'end_date', 'activity_prams'];
        $res = $this->actService->getInfoById($id, self::QUOTAACTIVITY, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加限购活动
     * @RequestMapping(path="/v1/activity/quota", methods="post")
     * @param QuotaActivityRequest $request
     *
     * @return array
     */
    public function store(QuotaActivityRequest $request)
    {
        $params = $request->validated();
        $res = $this->actService->add($params, self::QUOTAACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑限购活动
     * @PutMapping(path="/v1/activity/{id:\d+}/quota", methods="put")
     * @param int $id
     * @param QuotaActivityRequest $request
     *
     * @return array
     */
    public function update(int $id, QuotaActivityRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->actService->update($params, self::QUOTAACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除限购
     * @DeleteMapping(path="/v1/activity/{id:\d+}/quota", methods="delete")
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->actService->delete($id, self::QUOTAACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }


}
