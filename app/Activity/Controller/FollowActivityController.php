<?php
declare(strict_types=1);

namespace App\Activity\Controller;

use App\Activity\Request\GroupActivityRequest;
use App\Activity\Service\ActivityService;
use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * 接龙控制器
 * @Controller()
 */
class FollowActivityController extends AbstractController
{

    /**
     * TODO 1、首页商品列表的次日达商品、2、分类商品接口
     * TODO 1、申请退款的 接龙类型 字段，2、后台 接龙 退款列表的接口
     * TODO
     */

    /**
     * @Inject()
     * @var ActivityService
     */
    private $actService;

    const TYPE = 3;

    /**
     * 获取接龙活动列表
     * @RequestMapping(path="/v1/activity/follow", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['activityID', 'title', 'begin_date', 'end_date', 'activity_prams', 'status', 'create_at', 'shop_ids'];
        $res = $this->actService->getList($params, (int)$perPage, self::TYPE, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取接龙活动详情
     * @RequestMapping(path="/v1/activity/{id:\d+}/follow", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $field = ['activityID', 'title', 'activity_prams', 'begin_date', 'end_date', 'sort', 'image'];
        $res = $this->actService->getInfoById($id, self::TYPE, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加接龙活动
     * @PostMapping(path="/v1/activity/follow", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param GroupActivityRequest $request
     *
     * @return array
     */
    public function store(GroupActivityRequest $request)
    {
        $params = $request->validated();
        $res = $this->actService->add($params, self::TYPE);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑接龙活动
     * @PutMapping(path="/v1/activity/{id:\d+}/follow", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @param GroupActivityRequest $request
     *
     * @return array
     */
    public function update(int $id, GroupActivityRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->actService->update($params, self::TYPE);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除接龙活动
     * @DeleteMapping(path="/v1/activity/{id:\d+}/follow", methods="delete")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->actService->delete($id, self::TYPE);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 挑好物商品列表
     * @RequestMapping(path="/v1/api/follow/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function goodsList(RequestInterface $request)
    {
        $params = $request->all();
        $shop_id = (int)$params['shop_id'];
        $title = $params['title'] ?? '';
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        if (empty($shop_id)) {
            $shop_id =20200606322061;
        }
        $where = [
            'store_activity.status' => 1,
            'store_activity.is_deleted' => 0,
            'store_activity.activityType' => 3
        ];
        $field = [
            'store_activity.activityID',
            'store_activity.activity_prams',
            'store_activity.activityType',
            'ag.id as activity_goods_id',
            'ag.group_id',
            'g.scant_id',
            'g.ratio',
            'ag.spell_num',
            'store_activity.begin_date',
            'store_activity.end_date',
            'g.id as goods_id',
            'g.title as goods_title',
            'g.introduction',
            'g.logo',
            'store_activity.image',
            'ag.goods_spec',
            'ag.price_selling',
            'ag.costprice',
            'ag.stock',
            'ag.spell_num',
            'ag.commission',
        ];
        $list = $this->actService->getActivityListByShopId($where, $field, (int)$perPage, $shop_id, (string)$title);
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }

    }

    /**
     * 商品详情
     * @RequestMapping(path="/v1/api/follow/{id:\d+}/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function goodsInfo(int $id)
    {
        $activityId = (int)$id;//活动ID
        if (empty($activityId)) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $field = [
            'store_activity.activityID',
            'store_activity.activityType',
            'store_activity.activity_prams',
            'ag.id as activity_goods_id',
            'ag.group_id',
            'g.scant_id',
            'g.ratio',
            'g.introduction',
            'g.content',
            'ag.spell_num',
            'store_activity.begin_date',
            'store_activity.end_date',
            'g.id as goods_id',
            'g.title as goods_title',
            'g.logo',
            'g.image',
            'ag.goods_spec',
            'ag.price_selling',
            'ag.costprice',
            'ag.stock',
            'ag.spell_num',
            'ag.commission',
        ];
        $where = [
            'activityID' => $activityId,
        ];
        $dataInfo = $this->actService->getGoodsByActivityId($where, $field);
        return $this->success($dataInfo, '获取成功');
    }
}
