<?php
declare(strict_types=1);

namespace App\Activity\Controller;

use App\Activity\Service\ActivityService;
use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 */
class BaseActivityController extends AbstractController
{

    /**
     * @Inject()
     * @var ActivityService
     */
    private $actService;


    /**
     * 活动添加店铺
     * @PostMapping(path="/v1/activity/bind/shop", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     *
     * @return array
     */
    public function bindShop(RequestInterface $request)
    {
        $activityId = $request->input('activity_id') ?? '';
        $shopId = $request->input('shop_id') ?? '';
        $type = $request->input('type') ?? 0;
        if (!$activityId || !isset($type)) return $this->failed(ErrorCode::PARAMS_INVALID);

        $res = $this->actService->activityBindShop((int)$activityId, $shopId, (int)$type);

        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 禁用/启用
     * @PutMapping(path="/v1/activity/{id:\d+}/status", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     *
     * @return array
     */
    public function editStatus(int $id)
    {
        $res = $this->actService->editStatus('activity', (int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 根据活动 ID 查询店铺信息
     * @RequestMapping(path="/v1/activity/shop", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function getShopInfoByActivityId(RequestInterface $request)
    {
        $acticityId = $request->input('acticityId');
        if (!$acticityId) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->actService->getShopInfoByActivityId((int)$acticityId);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

}
