<?php
declare(strict_types=1);


namespace App\Activity\Controller\Free;

use App\Activity\Service\Free\FreeMiniService;
use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Order\Service\Order\OrderMiniService;
use App\Resource\Service\Goods\GoodsMiniService;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Service\SystemService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller
 * Class FreeMiniController
 * @package App\Activity\Controller\Free
 */
class FreeMiniController extends AbstractController
{

    /**
     * @Inject()
     * @var SystemService
     */
    protected SystemService $systemService;

    /**
     * @Inject()
     * @var FreeMiniService
     */
    protected FreeMiniService $freeMiniService;

    /**
     * @Inject()
     * @var OrderMiniService
     */
    protected OrderMiniService $orderMiniService;

    /**
     * 最新的活动入口
     * @RequestMapping(path="/v1/api/activity/free/latest", methods="get")
     * @param int $shop_id
     * @return array
     */
    public function latestActivity()
    {
        $shop_id = $this->request->input('shop_id');
        if (!$shop_id) return $this->failed(ErrorCode::PARAMS_INVALID);
        $orders = $this->freeMiniService->getFreeOrderActivity($shop_id);
        return $this->success((object)$orders, '获取成功');
    }
    /**
     * 活动详情
     * @RequestMapping(path="/v1/api/activity/{id}/free", methods="get")
     * @param int $id
     * @return array
     * @author ran
     * @date 2021-04-01 9:30
     * mailbox 466180170@qq.com
     */
    public function freeOrderDetail(int $id)
    {
        $res = $this->freeMiniService->detail($id);
        if (!$res['code']) {
            return $this->failed(400, $res['msg']);
        } else {
            return $this->success($res['data'], '获取成功');
        }
    }
    /**
     * 获取两条我的免单
     * @RequestMapping(path="/v1/api/activity/free/my", methods="get")
     * @return array
     * @author ran
     * @date 2021-04-01 9:30
     * mailbox 466180170@qq.com
     */
    public function myFreeOrder()
    {
//        $mid = (int)$this->request->getAttribute('mid');
        $mid = (int)$this->request->input('mid', 0);
        $activity_id = (int)$this->request->input('activity_id');
        $limit = (int)$this->request->input('limit', 2);
        if (!$activity_id || $limit <= 0) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $orders = $this->orderMiniService->getInviteOrder((int)$mid, 1000)->items();
        $orders = collect($orders)->groupBy('activity_id')->toArray();
        $result = array_slice($orders[$activity_id] ?? [], 0, $limit);
        return $this->success($result, 2, '获取成功');
    }
    /**
     * 获取免单活动规则
     * @RequestMapping(path="/v1/api/activity/free/rules", methods="get")
     * @return array
     * @author ran
     * @date 2021-04-01 9:28
     * mailbox 466180170@qq.com
     */
    public function freeOrderRules()
    {
        $res = $this->systemService->getSystemInfo(['name' => 'free_order_rules']);
        return $this->success($res, '获取成功');
    }
    /**
     * 获取成功的免单列表
     * @RequestMapping(path="/v1/api/activity/free/success", methods="get")
     * @return array
     */
    public function getSuccessOrderList()
    {
        $filed = ['m.nickname', 'm.face_url', 'og.goods_title', 'ag.price_selling', 'o.is_freed', 'o.pay_at'];
        $res = $this->freeMiniService->freeOrderList(['o.is_freed' => 2], implode(',', $filed), 50);
        return $this->success($res, '获取成功');
    }
    /**
     * 首页免单状态
     * @RequestMapping(path="/v1/api/free/status", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-04-01 9:29
     * mailbox 466180170@qq.com
     */
    public function freeStatus(RequestInterface $request)
    {
        //$mid = $request->getAttribute('mid');
        $mid = $request->input('mid', 0);
        if (!$mid) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->freeMiniService->getFreeStatus($mid);
        if(!$res){
            return $this->success([],'操作成功');
        }elseif($res['code']==200){
            return $this->success($res['data'],'你有免单正在进行中');
        }elseif ($res['code']==400){
            return $this->success($res['data'],'你有免单失败了');
        }
    }

    /**
     * CS
     * @RequestMapping(path="/v1/api/cs", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function freeOrderStatus(RequestInterface $request)
    {
        $res = $this->freeMiniService->freeOrderStatus('CSXP20210407145536652806QPQ6406',2);
        if(!$res){
            return $this->success([],'操作成功');
        }elseif($res['code']==200){
            return $this->success($res['data'],'你有免单正在进行中');
        }elseif ($res['code']==400){
            return $this->success($res['data'],'你有免单失败了');
        }
    }

}
