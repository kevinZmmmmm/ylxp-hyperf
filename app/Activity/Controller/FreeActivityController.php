<?php

declare(strict_types=1);

namespace App\Activity\Controller;

use App\Activity\Request\FreeActivityRequest;
use App\Activity\Service\ActivityService;
use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Service\SystemService;
use Carbon\Carbon;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller
 * Class FreeActivityController
 * @package App\Activity\Controller
 */
class FreeActivityController extends AbstractController
{
    /**
     * @Inject()
     * @var ActivityService
     */
    private $actService;

    /**
     * @Inject()
     * @var SystemService
     */
    private $systemService;

    const FREEACTIVITY = 5;

    /**
     * 获取免单活动列表
     * @RequestMapping(path="/v1/activity/free", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request): array
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['activityID', 'title', 'activity_prams', 'begin_date', 'end_date', 'status', 'image', 'create_at', 'shop_ids', 'updated_at', 'updated_uid'];
        $res = $this->actService->getList($params, (int)$perPage, self::FREEACTIVITY, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取免单活动详情
     * @RequestMapping(path="/v1/activity/{id:\d+}/free", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     *
     * @param int $id
     * @param RequestInterface $request
     * @return array
     */
    public function info(int $id, RequestInterface $request): array
    {
        $params = $request->all();
        $field = ['activityID', 'title', 'begin_date', 'end_date', 'activity_prams', 'image', 'shop_ids'];
        $res = $this->actService->getInfoById($id, self::FREEACTIVITY, $field, $params);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加免单活动
     * @RequestMapping(path="/v1/activity/free", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param FreeActivityRequest $request
     *
     * @return array
     */
    public function store(FreeActivityRequest $request): array
    {
        $params = $request->validated();
        $params['start_time'] = trim(substr($params['activity_time'], 0, 19));
        $params['end_time'] = trim(substr($params['activity_time'], -19));

        $res = $this->actService->add($params, self::FREEACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            if(!empty($res['msg'])){
                return $this->failed($res['code'], $res['msg']);
            }
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑免单活动
     * @PutMapping(path="/v1/activity/{id:\d+}/free", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     *
     * @param int $id
     * @param FreeActivityRequest $request
     * @return array
     */
    public function update(int $id, FreeActivityRequest $request): array
    {
        $params = $request->validated();
        $params['id'] = $id;
        $params['start_time'] = trim(substr($params['activity_time'], 0, 19));
        $params['end_time'] = trim(substr($params['activity_time'], -19));
        $res = $this->actService->update($params, self::FREEACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            if(!empty($res['msg'])){
                return $this->failed($res['code'], $res['msg']);
            }
            return $this->failed($res['code']);
        }
    }

    /**
     * 禁用/启用
     * @PutMapping(path="/v1/activity/status/{id:\d+}/free", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @return array
     */
    public function editStatus(int $id): array
    {
        $res = $this->actService->freeStatus((int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 分享免单规则设置
     * @PutMapping(path="/v1/free/rule", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return array
     */
    public function setFreeRule(RequestInterface $request): array
    {
        $params['free_order_rules'] = $request -> input('free_order_rules');
        $res = $this->systemService->CrdShopConfig($params);
        if ($res) {
            return $this->success([], '操作成功');
        } else {
            return $this->failed(0);
        }
    }

    /**
     * 分享免单规则获取
     * @RequestMapping(path="/v1/free/rule", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @return array
     */
    public function getFreeRule(): array
    {
        $res = $this->systemService->getSystemConfig(['free_order_rules']);
        if ($res) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 免单活动新增门店
     * @PutMapping(path="/v1/activity/shop/{id:\d+}/free", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @return array
     */
    public function addShop(int $id, RequestInterface $request)
    {
        $shopIds = $request->input('shop_ids');
        if(!$id || !$shopIds){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->actService->addShop((int)$id,$shopIds);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
