<?php

declare(strict_types=1);

namespace App\Activity\Controller;

use App\Activity\Request\SpecialActivityRequest;
use App\Activity\Service\ActivityService;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class SpecialActivityController extends AbstractController
{

    /**
     * @Inject()
     * @var ActivityService
     */
    private $actService;
    const SPECIALACTIVITY = 2;


    /**
     * 获取专题活动列表
     * @RequestMapping(path="/v1/activity/special", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['activityID', 'title', 'create_at'];
        $res = $this->actService->getList($params, (int)$perPage, self::SPECIALACTIVITY, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取专题活动详情
     * @RequestMapping(path="/v1/activity/{id:\d+}/special", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $field = ['activityID', 'title', 'image', 'activity_prams'];
        $res = $this->actService->getInfoById($id, self::SPECIALACTIVITY, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加专题活动
     * @RequestMapping(path="/v1/activity/special", methods="post")
     * @param SpecialActivityRequest $request
     *
     * @return array
     */
    public function store(SpecialActivityRequest $request)
    {
        $params = $request->validated();
        $res = $this->actService->add($params, self::SPECIALACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑专题活动
     * @PutMapping(path="/v1/activity/{id:\d+}/special", methods="put")
     * @param int $id
     * @param SpecialActivityRequest $request
     *
     * @return array
     */
    public function update(int $id, SpecialActivityRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->actService->update($params, self::SPECIALACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除专题活动
     * @RequestMapping(path="/v1/activity/{id:\d+}/special", methods="delete")
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->actService->delete($id, self::SPECIALACTIVITY);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
