<?php


namespace App\Activity\Service\Seckill;


class SeckillMiniService extends SeckillBaseService
{
    /**
     * 业务场景 (目前作用域订单)
     * @return array
     * @author ran
     * @date 2021-04-06 16:46
     * mailbox 466180170@qq.com
     */
    public function getActivityGoodsSeckillListToOrder(array $activityGoodsIds):array
    {
        $needExceptKey=['activityType','begin_date','end_date','create_at','is_deleted','sort','shop_ids','title','status','activity_prams','is_view','image','updated_at','updated_uid'];
        $whereIn =['store_activity_goods.id',$activityGoodsIds];
        $extraCondition=['whereIn'=>$whereIn];
        $ActivityGoodsArray=$this->getActivityGoodsCommonSeckillList($needExceptKey,$extraCondition,1,20,false);
        return $ActivityGoodsArray['list'];
    }

}