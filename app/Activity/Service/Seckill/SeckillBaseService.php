<?php


namespace App\Activity\Service\Seckill;

use App\Activity\Repository\RepositoryFactory;
use App\Common\Service\BaseService;

class SeckillBaseService extends BaseService
{
    /**
     * 公共字段集合
     */
    const ACTIVITYFIELD=['activityID','activityType','begin_date','end_date','sort','shop_ids','status'];
    const ACTIVITYGOODSFIELD=['id','activityID', 'goods_id', 'goods_cate','goods_title', 'goods_logo','goods_spec','costprice','price_selling','stock','spell_num','group_id','per_can_buy_num'];

    const ACTIVITY_TYPE=4;

    /**
     * Repository
     */
    private $activity;
    private $activitygoods;

    /**
     * QuotaBaseService constructor.
     * @param RepositoryFactory $repositoryFactory
     */
    public function __construct(RepositoryFactory $repositoryFactory)
    {
        parent::__construct();
        $this->activity = $repositoryFactory->getRepository("activity");
        $this->activitygoods = $repositoryFactory->getRepository("activityGoods");
    }

    /**
     * 业务场景 获取秒杀多条记录
     * @param array $needExceptKey
     * @param array $extraCondition
     * @param array $orderBy
     * @return array|mixed
     * @author ran
     * @date 2021-03-23 17:23
     * mailbox 466180170@qq.com
     */
    protected function getActivityGoodsCommonSeckillList(array $needExceptKey=[],array $extraCondition=[],int $page=1,int $limit=20,bool $isPage=true,array $orderBy=[]):array
    {
        $where =['store_activity.activityType'=>self::ACTIVITY_TYPE];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereIn'=>$extraCondition['whereIn']??[],'whereRaw'=>$extraCondition['whereRaw']??[],'whereOp'=>$extraCondition['whereOp']??[]];
        $activityArr =$this->activitygoods->selectSonUnionList(self::ACTIVITYGOODSFIELD,self::ACTIVITYFIELD,$condition,$page,$limit,$orderBy,$isPage);
        return $this->arr->exceptArrKey($activityArr,$needExceptKey);
    }

}