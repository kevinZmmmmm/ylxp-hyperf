<?php


namespace App\Activity\Service\Group;

use App\Activity\Repository\RepositoryFactory;
use App\Activity\Model\ActivityModel;
use App\Common\Service\BaseService;

class GroupBaseService extends BaseService
{

    /**
     * 公共字段集合
     */
    const ACTIVITYFIELD=['activityID','activityType','begin_date','end_date','create_at','is_deleted','sort','shop_ids','title','status','activity_prams','is_view','image','updated_at','updated_uid'];
    const ACTIVITYGOODSFIELD=['id','activityID', 'goods_id', 'goods_cate','goods_title', 'goods_logo','goods_spec','costprice','price_selling','stock','spell_num','group_id','activityType','commission','current_cost_price','per_can_buy_num','click_num','mini_app_code'];

    const ACTIVITY_TYPE=0;

    /**
     * 删除状态 0未删除，1已删除
     */
    const ACTIVITY_IS_DELETED_0 = 0;
    const ACTIVITY_IS_DELETED_1 = 1;
    /**
     * 活动状态 0禁用，1启用
     */
    const ACTIVITY_STATUS_0 = 0;
    const ACTIVITY_STATUS_1 = 1;
    /**
     * Repository
     */
    private $activity;
    private $activitygoods;

    /**
     * QuotaBaseService constructor.
     * @param RepositoryFactory $repositoryFactory
     */
    public function __construct(RepositoryFactory $repositoryFactory)
    {
        parent::__construct();
        $this->activity = $repositoryFactory->getRepository("activity");
        $this->activitygoods = $repositoryFactory->getRepository("activityGoods");
    }

    /**
     * 获取小程序拼团商品数据
     * @param $shop_id
     * @param false $IsNeedCount
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-20 17:12
     * mailbox 466180170@qq.com
     */
    public function getActivityGroupGoodsList($shop_id, $IsNeedCount = false, $page = 1, $limit = 6)
    {
        $model = ActivityModel::query()
            ->from('store_activity as a')
            ->select([
                'a.activityID',
                'ag.id as activity_goods_id',
                'a.image',
                'ag.goods_title as title',
                'ag.goods_id',
                'ag.goods_spec',
                'ag.price_selling',
                'ag.costprice as price_market',
                'ag.stock',
            ])
            ->leftJoin('store_activity_goods as ag', 'a.activityID', '=', 'ag.activityID')
            ->where('a.status', 1)
            ->where('a.is_deleted', 0)
            ->where('a.activityType', 0)
            ->where('a.end_date', '>', date('Y-m-d H:i:s'))
            ->whereRaw('FIND_IN_SET(' . $shop_id . ',a.shop_ids)')
            ->orderByRaw('a.sort desc,a.end_date desc');
        $totalNum = $model->count();
        $list = $model->forPage($page, $limit)->get();
        $list = $list ? $list->toArray() : [];
        if ($IsNeedCount) {
            return [
                'count' => $totalNum,
                'list' => $list,
            ];
        }
        return $list;
    }

}