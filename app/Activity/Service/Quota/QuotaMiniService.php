<?php


namespace App\Activity\Service\Quota;


class QuotaMiniService extends QuotaBaseService
{
    /**
     * 业务场景 获取首页限购活动数据 (目前只适用小程序首页)
     * @author ran
     * @date 2021-03-23 16:27
     * mailbox 466180170@qq.com
     */
    public function getActivityHomeQuotaGoodsList(int $shop_id):array
    {
        $needExceptKey=['activityType','begin_date','end_date','create_at','is_deleted','sort','shop_ids','title','status','activity_prams','is_view','image','updated_at','updated_uid'];
        $whereOp =[['end_date', '>=', date('Y-m-d H:i:s')]];
        $whereRaw ='!FIND_IN_SET(' . $shop_id . ',shop_ids)';
        $extraCondition =['whereRaw'=>$whereRaw,'whereOp'=>$whereOp];
        return $this->getActivityCommonQuotafirst($needExceptKey,$extraCondition,['end_date','asc']);
    }

    /**
     * 业务场景 (目前作用域订单)
     * @return array
     * @author ran
     * @date 2021-04-06 16:46
     * mailbox 466180170@qq.com
     */
    public function getActivityGoodsQuotaListToOrder(array $activityGoodsIds):array
    {
        $needExceptKey=['activityType','begin_date','end_date','create_at','is_deleted','sort','shop_ids','title','status','activity_prams','is_view','image','updated_at','updated_uid'];
        $whereIn =['store_activity_goods.id',$activityGoodsIds];
        $extraCondition=['whereIn'=>$whereIn];
        return $this->getActivityGoodsCommonQuotaList($needExceptKey,$extraCondition,1,20,false);
    }

}