<?php


namespace App\Activity\Service\Quota;


use App\Activity\Repository\RepositoryFactory;
use App\Common\Service\BaseService;

class QuotaBaseService extends BaseService
{

    /**
     * 公共字段集合
     */
    const ACTIVITYFIELD=['activityID','activityType','begin_date','end_date','create_at','is_deleted','sort','shop_ids','title','status','activity_prams','is_view','image','updated_at','updated_uid'];
    const ACTIVITYGOODSFIELD=['id','activityID', 'goods_id', 'goods_cate','goods_title', 'goods_logo','goods_spec','costprice','price_selling','stock','spell_num','group_id','activityType','commission','current_cost_price','per_can_buy_num','click_num','mini_app_code'];

    const ACTIVITY_TYPE=1;

    /**
     * 删除状态 0未删除，1已删除
     */
    const ACTIVITY_IS_DELETED_0 = 0;
    const ACTIVITY_IS_DELETED_1 = 1;
    /**
     * 活动状态 0禁用，1启用
     */
    const ACTIVITY_STATUS_0 = 0;
    const ACTIVITY_STATUS_1 = 1;
    /**
     * Repository
     */
    private $activity;
    private $activitygoods;

    /**
     * QuotaBaseService constructor.
     * @param RepositoryFactory $repositoryFactory
     */
    public function __construct(RepositoryFactory $repositoryFactory)
    {
        parent::__construct();
        $this->activity = $repositoryFactory->getRepository("activity");
        $this->activitygoods = $repositoryFactory->getRepository("activityGoods");
    }

    /**
     * 业务场景 获取限时抢购活动单条记录公共服务
     * @param array $needExceptKey
     * @param array $extraCondition
     * @param array $orderBy
     * @return array|mixed
     * @author ran
     * @date 2021-03-23 17:23
     * mailbox 466180170@qq.com
     */
    public function getActivityCommonQuotafirst(array $needExceptKey=[],array $extraCondition=[],array $orderBy=[]):array
    {
        $where =['activityType'=>self::ACTIVITY_TYPE];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereRaw'=>$extraCondition['whereRaw']??[],'whereOp'=>$extraCondition['whereOp']??[]];
        $activityArr =$this->activity->findFirst(self::ACTIVITYFIELD,$condition,$orderBy);
        return $this->arr->exceptArrKey($activityArr,$needExceptKey);
    }

    /**
     * 业务场景 获取限时抢购多条记录
     * @param array $needExceptKey
     * @param array $extraCondition
     * @param array $orderBy
     * @return array|mixed
     * @author ran
     * @date 2021-03-23 17:23
     * mailbox 466180170@qq.com
     */
    protected function getActivityGoodsCommonQuotaList(array $needExceptKey=[],array $extraCondition=[],int $page=1,int $limit=20,bool $isPage=true,array $orderBy=[]):array
    {
        $where =['store_activity.activityType'=>self::ACTIVITY_TYPE];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereRaw'=>$extraCondition['whereRaw']??[],'whereOp'=>$extraCondition['whereOp']??[]];
        $activityArr =$this->activitygoods->selectSonUnionList(self::ACTIVITYGOODSFIELD,self::ACTIVITYFIELD,$condition,$page,$limit,$orderBy,$isPage);
        return $this->arr->exceptArrKey($activityArr,$needExceptKey);
    }

    /**
     * @author ran
     * @date 2021-03-23 17:29
     * mailbox 466180170@qq.com
     */
    public function ex(){
        /*$sale_limit = ActivityModel::query()
            ->where([
                ['end_date', '>=', date('Y-m-d H:i:s')],
                ['status', '=', 1],
                ['activityType', '=', 1],
                ['is_deleted', '=', 0],
            ])
            ->selectRaw('activityID, activityType, begin_date, end_date, activity_prams')
            ->whereRaw('FIND_IN_SET(' . $shop_id . ',shop_ids)')
            ->orderBy('begin_date')
            ->first();
        if (!$sale_limit) {
            return [];
        }
        $sale_limit = $sale_limit->toArray();
        $sale_limit['time_desc'] = ActivityModel::getSaleActivityTime($sale_limit['begin_date'], $sale_limit['end_date']);
        $sale_limit['goods_list'] = ActivityGoodsModel::query()
            ->from('store_activity_goods as ag')
            ->select([
                'g.id as goods_id',
                'ag.id as activity_goods_id',
                'g.title',
                'g.image',
                'ag.goods_spec as specs',
                'ag.group_id',
                'ag.costprice as price_market',
                'ag.price_selling',
            ])
            ->where('g.status', 1)
            ->where('g.is_deleted', 0)
            ->where('activityID', $sale_limit['activityID'])
            ->leftJoin('store_goods as g', 'ag.goods_id', '=', 'g.id')
            ->get()
            ->toArray();
        return [$sale_limit];*/

    }

}