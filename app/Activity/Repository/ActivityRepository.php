<?php


namespace App\Activity\Repository;


use App\Activity\Model\ActivityGoodsModel;
use App\Activity\Model\ActivityModel;
use App\Resource\Model\GoodsListModel;
use App\Resource\Model\GoodsModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class ActivityRepository implements BaseRepositoryInterface
{

    /**
     * 主表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findFirst(array $field = ['*'],array $condition,array $orderBy,bool $isArray=true)
    {
        $connect = ActivityModel::query()->select($field)
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query){
                return $query->orderBy('sort','desc');
            })->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }
    /**
     * 子表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function selectSonUnionList(array $primaryField,array $sonField,array $condition,array $orderBy,bool $isArray=true)
    {
        $activityGoods =(new ActivityGoodsModel())->getTable();
        $goodsList =(new GoodsListModel())->getTable();
        $primaryField =array_map(function ($item) use ($activityGoods) {
            return $activityGoods.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($goodsList) {
            return $goodsList.'.'.$item;
        },$sonField);
        $connect = ActivityGoodsModel::query()->from("{$activityGoods}")
            ->select(array_merge($primaryField,$sonField))
            ->leftJoin("{$goodsList}", $activityGoods.'.goods_id', '=', $goodsList.'.goods_id')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($activityGoods) {
                return $query->orderBy($activityGoods.'.stock','desc');
            })->get();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }
    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectList(array $field,array $where,string $whereRaw,array $whereIn,array $whereNotIn,int $page,int $limit,array $orderBy,bool $isArray=true)
    {
        $connect = ActivityModel::query()->select($field)->where($where)
            ->when($whereRaw, function ($query, $whereRaw) {
                return $query->whereRaw($whereRaw);
            })
            ->when($whereNotIn, function ($query, $whereNotIn) {
                return $query->whereNotIn($whereNotIn[0],$whereNotIn[1]);
            })
            ->when($whereIn, function ($query, $whereIn) {
                return $query->whereIn($whereIn[0],$whereIn[1]);
            })->orderBy($orderBy[0],$orderBy[1])->forPage($page, $limit)->get();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }

}