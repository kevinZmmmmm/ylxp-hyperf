<?php


namespace App\Activity\Repository;


use App\Activity\Model\ActivityGoodsModel;
use App\Activity\Model\ActivityModel;
use App\Resource\Model\GoodsListModel;
use App\Resource\Model\GoodsModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class ActivityGoodsRepository implements BaseRepositoryInterface
{

    /**
     * 子表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findSonUnionFirst(array $primaryField,array $sonField,array $condition,array $orderBy,bool $isArray=true)
    {
        $activityGoods =(new ActivityGoodsModel())->getTable();
        $activity =(new ActivityModel())->getTable();
        $primaryField =array_map(function ($item) use ($activityGoods) {
            return $activityGoods.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($activity) {
            return $activity.'.'.$item;
        },$sonField);
        $connect = ActivityGoodsModel::query()->from("{$activityGoods}")
            ->select(array_merge($primaryField,$sonField))
            ->leftJoin("{$activity}", $activityGoods.'.activityID', '=', $activity.'.activityID')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($activityGoods) {
                return $query->orderBy($activityGoods.'.stock','desc');
            })->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }

    /**
     * 子表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findSonMoreUnionFirst(array $primaryField,array $sonField,array $grandSonField,array $condition,array $orderBy,bool $isArray=true)
    {
        $activityGoods =(new ActivityGoodsModel())->getTable();
        $goods =(new GoodsModel())->getTable();
        $goodsList =(new GoodsListModel())->getTable();
        $primaryField =array_map(function ($item) use ($activityGoods) {
            return $activityGoods.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($goodsList) {
            return $goodsList.'.'.$item;
        },$sonField);
        $grandSonField =array_map(function ($item) use ($goods) {
            return $goods.'.'.$item;
        },$grandSonField);
        $connect = ActivityGoodsModel::query()->from("{$activityGoods}")
            ->select(array_merge($primaryField,$sonField,$grandSonField))
            ->leftJoin("{$goodsList}", $activityGoods.'.goods_id', '=', $goodsList.'.goods_id')
            ->leftJoin("{$goods}", $activityGoods.'.goods_id', '=', $goods.'.id')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($activityGoods) {
                return $query->orderBy($activityGoods.'.stock','desc');
            })->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }

    /**
     * 子表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function selectSonUnionList(array $primaryField,array $sonField,array $condition,int $page,int $limit,array $orderBy,bool $isPage=true,bool $isArray=true)
    {
        $activityGoods =(new ActivityGoodsModel())->getTable();
        $activity =(new ActivityModel())->getTable();
        $primaryField =array_map(function ($item) use ($activityGoods) {
            return $activityGoods.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($activity) {
            return $activity.'.'.$item;
        },$sonField);
        $connect = ActivityGoodsModel::query()->from("{$activityGoods}")
            ->select(array_merge($primaryField,$sonField))
            ->leftJoin("{$activity}", $activityGoods.'.activityID', '=', $activity.'.activityID')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($condition['whereIn']??[], function ($query) use($condition) {
                return $query->whereIn($condition['whereIn'][0],$condition['whereIn'][1]);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($activityGoods) {
                return $query->orderBy($activityGoods.'.stock','desc');
            });
        if($isPage){
            if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()->toArray()];
            return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()];
        }
        if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()->toArray()];
        return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()];
    }

}