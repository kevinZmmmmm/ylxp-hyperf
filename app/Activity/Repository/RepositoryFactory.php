<?php


namespace App\Activity\Repository;
use Hyperf\Di\Annotation\Inject;


class RepositoryFactory
{
    /**
     * @Inject()
     * @var ActivityRepository
     */
    private $activity;
    /**
     * @Inject()
     * @var ActivityGoodsRepository
     */
    private $activityGoods;

    public function getRepository($repoName){
        return $this->$repoName;
    }

}