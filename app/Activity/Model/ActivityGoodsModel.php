<?php


namespace App\Activity\Model;

use Hyperf\DbConnection\Model\Model;

class ActivityGoodsModel extends Model
{
//    protected $table = 'hf_activity_goods';
    protected $table = 'store_activity_goods';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function activity()
    {
        return $this->belongsTo(ActivityModel::class, 'activityID', 'activityID');
    }
}
