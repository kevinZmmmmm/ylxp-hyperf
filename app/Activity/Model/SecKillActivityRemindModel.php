<?php

declare (strict_types=1);

namespace App\Activity\Model;

use App\Resource\Model\GoodsModel;
use App\User\Model\MemberModel;
use Hyperf\DbConnection\Model\Model;

/**
 */
class SecKillActivityRemindModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_seckill_activity_remind';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function member()
    {
        return $this->belongsTo(MemberModel::class, 'mid', 'mid');
    }

    public function activity_goods()
    {
        return $this->belongsTo(ActivityGoodsModel::class, 'id', 'activity_goods_id');
    }
}
