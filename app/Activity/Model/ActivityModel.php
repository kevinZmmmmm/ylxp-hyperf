<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Activity\Model;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $mobile
 * @property string $realname
 */
class ActivityModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
//    protected $table = 'hf_activity';
    protected $table = 'store_activity';
    protected $primaryKey = 'activityID';
//    const UPDATED_AT = null;
    const CREATED_AT = 'create_at';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['title', 'st', 'realname'];
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes = [
        'sort' => 0,
//        'number' => 0,
        'status' => 1,
    ];

    public function activityGoods()
    {
        return $this->hasMany(ActivityGoodsModel::class, 'activityID', 'activityID');
    }

    /**
     * 获取限时抢购状态
     *
     * @param $begin_date
     * @param $end_date
     * @return array
     */
    public static function getSaleActivityTime($begin_date, $end_date)
    {
        $now_time = time();
        $begin_time = strtotime($begin_date);
        $end_time = strtotime($end_date);
        $sale_type = '';
        $remark = '';
        $remaining_time = '';
        if ($now_time <= $begin_time) {
            $remark = '活动暂未开始';
            $sale_type = 1;
            $remaining_time = $begin_time - $now_time;
        }
        if ($now_time > $begin_time && $now_time < $end_time) {
            $remark = '活动进行中';
            $sale_type = 2;
            $remaining_time = $end_time - $now_time;
        }
        if ($now_time >= $end_time) {
            $remark = '活动已结束';
            $sale_type = 3;
            $remaining_time = $now_time - $end_time;
        }
        $data = [
            'sale_type'      => $sale_type,
            'remaining_time' => $remaining_time,
            'remark'         => $remark,
        ];
        return $data;
    }
}
