<?php

declare(strict_types=1);

namespace App\Activity\Process;

use Hyperf\AsyncQueue\Process\ConsumerProcess;
use Hyperf\Process\Annotation\Process;

/**
 * @Process()
 */
class SubscribeConsumerProcess extends ConsumerProcess
{
    /**
     * @var string
     */
    protected $queue = 'subscribe';
}
