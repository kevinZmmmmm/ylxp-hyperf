<?php

declare(strict_types=1);

namespace App\Activity\Event;


class Group
{
    public $acId;
    public $needPeople;

    public function __construct($acId, $needPeople)
    {
        $this->acId = $acId;
        $this->needPeople = $needPeople;
    }
}
