<?php

declare(strict_types=1);

namespace App\Activity\Aspect;

use App\Activity\Service\ActivityService;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Psr\Container\ContainerInterface;
use Hyperf\Di\Aop\ProceedingJoinPoint;

/**
 * @Aspect
 */
class SecKillAspect extends AbstractAspect
{

    public $classes = [
//        'App\Activity\Service\ActivityService::addShop',
//        'App\Activity\Service\ActivityService::insertOrUpdate',
//        'App\Activity\Service\ActivityService::delete',
//        'App\Activity\Service\ActivityService::seckillStatus'
    ];

    public $annotations = [
    ];

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $res = $proceedingJoinPoint->process();
        if (isset($res['data']) && !empty($res['data'])){
            $this->container->get(ActivityService::class)->generateSecKillActDataForEveryStore();
        }
        return $res;
    }
}
