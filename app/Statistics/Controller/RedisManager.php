<?php

declare(strict_types=1);

namespace App\Statistics\Controller;

use App\Statistics\Service\RedisManagerService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class)
 * })
 * Class RedisManager
 * @package App\Statistics\Controller
 */
class RedisManager extends AbstractController
{

    /**
     * @Inject()
     * @var RedisManagerService
     */
    private $redisService;

    /**
     * @RequestMapping(path="/v1/redis/manager", methods="get")
     */
    public function index(){
        $res = $this->redisService->confList();
        return $this->success($res, 'success', count($res));
    }

    /**
     * @RequestMapping(path="/v1/redis/db/{dbName}", methods="get")
     * @param string $dbName
     * @return array
     */
    public function keys(string $dbName){
        $res = $this->redisService->keys($dbName);
        return $this->success($res, 'success');
    }

    /**
     * @RequestMapping(path="/v1/redis/key/value", methods="get")
     * @return array
     */
    public function value(){
        $dbName = $this->request->input('dbName');
        $keyName = $this->request->input('keyName');
        $res = $this->redisService->value($dbName, $keyName);
        return $this->success($res, 'success');
    }

}
