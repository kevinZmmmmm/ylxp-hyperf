<?php
/**
 * Created by PhpStorm.
 * User: ran
 * Date: 2020/8/13
 * Time: 11:00
 */

namespace App\Statistics\Controller;

use App\Statistics\Service\HomeService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use App\Common\Controller\AbstractController;
/**
 * @Controller()
 * 后台-首页统计
 * Class HomeController
 * @package App\Common\Controller
 */
class HomeController extends AbstractController
{
    /**
     * @Inject()
     * @var HomeService
     */
    private $homeService;

    /**
     * 实时门店销售数据分析
     * @RequestMapping(path="/v1/statistics/home/shop", methods="get")
     * @return array
     */
    public function HomeShopStatistics()
    {
        $field = ['shop_id', 'time', 'customer_price', 'kz_refund_recharge', 'wx_refund_recharge', 'recharge_freight_price', 'kz_recharge', 'wx_recharge',
            'recharge_order_count', 'recharge_goods_price', 'recharge', 'kz_refund_recharge_order_count', 'wx_refund_recharge_order_count', 'recharge_count'];
        $res = $this->homeService->getHomeShopData([], $field);
        return $this->success($res, '获取信息成功');
    }

    /**
     * 首页数据分析
     * @RequestMapping(path="/v1/statistics/home", methods="get")
     * @return array
     */
    public function HomeStatistics()
    {
        $homeData = $this->homeService->platformHomeStatistics();
        return $this->success($homeData, '获取信息成功');
    }


}