<?php

declare(strict_types=1);

namespace App\Resource\Listener;

use App\Resource\Event\UpdateGoodsCache;
use App\Resource\Model\GoodsListModel;
use App\Resource\Model\GoodsModel;
use App\Resource\Service\BuildGoodsService;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class UpdateGoodsCacheListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            UpdateGoodsCache::class
        ];
    }

    public function process(object $event)
    {
        $type = $event->type;
        $goods_id = $event->goods_id;
        $info = $event->info;
        $buildGoodsService = $this->container->get(BuildGoodsService::class);
        switch ($type){
            case 'create':
                $info['id'] = $goods_id;
                $info['spell_num'] = 0;
                $info['shop_ids'] = '';
                $info['crd_shop_ids'] = '';
                $info['number_sales'] = 0;
                $info['status'] = isset($info['status']) ? $info['status'] : 1;
                $info['is_home'] = isset($info['is_home']) ? $info['is_home'] : 0;
                $info['ratio'] = 0;
                $info['group_id'] = $info['member_goods'] == 1 ? 2 : 0;
                if ($info['scant_id'] == 1) $info['ratio'] = $info['spec'];
                if ($info['err'] > 0) {
                    $info['goods_spec'] = $info['scant_id'] == 1 ? $info['spec'] . "g±{$info['err']}g/" . $info['unit']:
                        $info['spec'] . "±{$info['err']}g/" . $info['unit'];
                } else {
                    $info['goods_spec'] = $info['scant_id'] == 1 ? $info['spec'] . 'g/' . $info['unit'] : $info['spec'] . '/' . $info['unit'];
                }
                $buildGoodsService->createEvent($info);
                break;
            case 'update':
                $goodsField = ['goods.id','goods.title','goods.group_id','goods.goods_crm_code','goods.spell_num','goods.cate_id',
                    'goods.scant_id','goods.ratio','goods.logo','goods.read_pos_price','goods.read_pos_stock','goods.is_today',
                    'goods.safety_stock','goods.kz_goods_id','goods.sort','goods.is_next_day','goods.is_home','goods.status',
                    'goods.shop_ids','goods.crd_shop_ids'];
                $goodsListField = ['gl.goods_spec','gl.price_selling','gl.price_market','gl.number_stock','gl.number_virtual',
                    'gl.number_sales','gl.commission','gl.cost_price','gl.goods_pick_time'];
                $info = GoodsModel::query()
                    ->from("store_goods as goods")
                    ->leftJoin("store_goods_list as gl", 'goods.id', '=', 'gl.goods_id')
                    ->select(array_merge($goodsField, $goodsListField))
                    ->where('goods.id', $goods_id)->first()->toArray();
                $buildGoodsService->updateEvent($info, $type);
                break;
            case 'is_sale':
            case 'shop_sale':
            case 'is_home':
                $info = $info->toArray();
                if ($info['status'] == 1){
                    $goodsListField = ['goods_spec','price_selling','price_market','number_stock','number_virtual',
                        'number_sales','commission','cost_price','goods_pick_time'];
                    $extGoods = GoodsListModel::query()->select($goodsListField)->where('goods_id',$goods_id)->first()->toArray();
                    $info = array_merge($info, $extGoods);
                }
                $buildGoodsService->updateEvent($info, $type);
                break;
            default:
        }
    }
}
