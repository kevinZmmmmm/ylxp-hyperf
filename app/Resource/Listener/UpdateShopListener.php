<?php


namespace App\Resource\Listener;

use App\Resource\Event\UpdateShop;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Redis\RedisFactory;
use Psr\Container\ContainerInterface;

/**
 * @Listener
 */
class UpdateShopListener implements ListenerInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            UpdateShop::class,
        ];
    }

    /**
     * @param object $event
     *
     * 由于现在有多个 Redis 键存储了附近店铺列表，
     * 因此当修改或删除的时候直接将它们删除
     */
    public function process(object $event)
    {
        $resourceRedis = $this->container->get(RedisFactory::class)->get('resource');
//        if($event->data['func'] == 'add'){
//            $resourceRedis -> geoadd('nearShop',$event->data['longitude'],$event->data['latitude'],$event->data['shop_id']);
//        }elseif ($event->data['func'] == 'edit'){
//            $resourceRedis -> zrem('nearShop', $event->data['shop_id']);
//            $resourceRedis -> geoadd('nearShop',$event->data['longitude'],$event->data['latitude'],$event->data['shop_id']);
            // TODO 删除操作
//        }

        $type = $event->data['cate'] ?? null;
        switch ($type)
        {
            case 'crd':
                $resourceRedis -> del('nearShop-crd');
                break;
            default:
                $keys = $resourceRedis -> keys('nearShop*');
                $resourceRedis -> del($keys);
        }
    }
}
