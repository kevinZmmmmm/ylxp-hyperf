<?php

declare(strict_types=1);

namespace App\Resource\Event;

class UpdateGoodsCache
{
    public $type;
    public $goods_id;
    public $info;

    public function __construct($type, $goods_id, $info)
    {
        $this->type = $type;
        $this->goods_id = $goods_id;
        $this->info = $info;
    }
}
