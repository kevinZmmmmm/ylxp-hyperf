<?php


namespace App\Resource\Repository;

interface BaseRepositoryInterface
{
    public function findFirst(array $field,array $where,string $whereRaw,bool $isArray);

    //public function selectList(array $field,array $where,string $whereRaw,array $whereIn,array $whereNotIn,int $page,int $limit,array $orderBy,bool $isArray);

    public function getResourceSingleSonByPrimaryId(int $PrimaryId,array $PrimaryField,array $SonField);

}