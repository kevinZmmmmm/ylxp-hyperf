<?php


namespace App\Resource\Repository;

use App\Third\Repository\LsyPriceRepository;
use Hyperf\Di\Annotation\Inject;

class RepositoryFactory
{

    /**
     * @Inject()
     * @var GoodsRepository
     */
    private GoodsRepository $goods;

    /**
     * @Inject()
     * @var CartRepository
     */
    private CartRepository $cart;

    /**
     * @Inject()
     * @var CookBookRepository
     */
    private CookBookRepository $cookbook;

    /**
     * @Inject()
     * @var ShopRepository
     */
    private ShopRepository $shop;

    /**
     * @Inject()
     * @var LsyPriceRepository
     */
    private LsyPriceRepository $lsyPrice;

    /**
     * @Inject()
     * @var CategoryRepository
     */
    private CategoryRepository $category;


    public function getRepository($repoName){
        return $this->$repoName;
    }
}