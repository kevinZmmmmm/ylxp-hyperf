<?php


namespace App\Resource\Repository;


use App\Resource\Model\CookbookModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class CookBookRepository implements BaseRepositoryInterface
{

    /**
     * 主表查询单条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function findFirst(array $field = ['*'],array $where = [],string $whereRaw='',bool $isArray=true)
    {
        $connect = CookbookModel::query()->select($field)->where($where)
            ->when($whereRaw, function ($query, $whereRaw) {
                return $query->whereRaw($whereRaw);
            })
            ->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }
    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectList(array $field,array $where,string $whereRaw,array $whereIn,array $whereNotIn,int $page,int $limit,array $orderBy,bool $isArray=true)
    {
        $connect = CookbookModel::query()->select($field)->where($where)
            ->when($whereRaw, function ($query, $whereRaw) {
                return $query->whereRaw($whereRaw);
            })
            ->when($whereNotIn, function ($query, $whereNotIn) {
                return $query->whereNotIn($whereNotIn[0],$whereNotIn[1]);
            })
            ->when($whereIn, function ($query, $whereIn) {
                return $query->whereIn($whereIn[0],$whereIn[1]);
            })->orderBy($orderBy[0],$orderBy[1])->forPage($page, $limit)->get();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }

    public function getResourceSingleSonByPrimaryId(int $PrimaryId, array $PrimaryField, array $SonField)
    {
        // TODO: Implement getMergeSingleSonByPrimaryId() method.
    }
}