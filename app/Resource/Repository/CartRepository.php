<?php


namespace App\Resource\Repository;


use App\Activity\Model\ActivityGoodsModel;
use App\Activity\Model\ActivityModel;
use App\Resource\Model\CartModel;
use App\Resource\Model\GoodsListModel;
use App\Resource\Model\GoodsModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class CartRepository implements BaseRepositoryInterface
{

    /**
     * 主表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findFirst(array $field = ['*'],array $where = [],string $whereRaw='',bool $isArray=true)
    {
        $connect = CartModel::query()->select($field)->where($where)
            ->when($whereRaw, function ($query, $whereRaw) {
                return $query->whereRaw($whereRaw);
            })
            ->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }

    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectMoreUnionList(array $primaryField,array $sonField,$grandSonField,array $condition,int $page,int $limit,array $orderBy,bool $isPage=true,bool $isArray=true)
    {
        $cart =(new CartModel())->getTable();
        $goods =(new GoodsModel())->getTable();
        $goodsList =(new GoodsListModel())->getTable();
        $primaryField =array_map(function ($item) use ($cart) {
            return $cart.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($goods) {
            return $goods.'.'.$item;
        },$sonField);
        $grandSonField =array_map(function ($item) use ($goodsList) {
            return $goodsList.'.'.$item;
        },$grandSonField);
        $connect = CartModel::query()->from("{$cart}")
            ->select(array_merge($primaryField,$sonField,$grandSonField))
            ->leftJoin("{$goodsList}", $cart.'.goods_id', '=', $goodsList.'.goods_id')
            ->leftJoin("{$goods}", $cart.'.goods_id', '=', $goods.'.id')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereNotIn']??[], function ($query) use($condition) {
                return $query->whereNotIn($condition['whereNotIn'][0],$condition['whereNotIn'][1]);
            })
            ->when($condition['whereIn']??[], function ($query) use($condition) {
                return $query->whereIn($condition['whereIn'][0],$condition['whereIn'][1]);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($goods) {
                return $query->orderBy($goods.'.sort','desc');
            });
        if($isPage){
            if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()->toArray()];
            return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()];
        }
        if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()->toArray()];
        return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()];
    }

    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectMoreOtherUnionList(array $primaryField,array $sonField,$grandField,$grandSonField,array $condition,int $page,int $limit,array $orderBy,bool $isPage=true,bool $isArray=true)
    {
        $cart =(new CartModel())->getTable();
        $goods =(new GoodsModel())->getTable();
        $activityGoods =(new ActivityGoodsModel())->getTable();
        $activity =(new ActivityModel())->getTable();
        $primaryField =array_map(function ($item) use ($cart) {
            return $cart.'.'.$item;
        },$primaryField);
        $sonField =array_map(function ($item) use ($activityGoods) {
            return $activityGoods.'.'.$item;
        },$sonField);
        $grandField =array_map(function ($item) use ($activity) {
            return $activity.'.'.$item;
        },$grandField);
        $grandSonField =array_map(function ($item) use ($goods) {
            return $goods.'.'.$item;
        },$grandSonField);
        $connect = CartModel::query()->from("{$cart}")
            ->select(array_merge($primaryField,$sonField,$grandField,$grandSonField))
            ->leftJoin("{$activityGoods}", $cart.'.activity_goods_id', '=', $activityGoods.'.id')
            ->leftJoin("{$activity}", $activityGoods.'.activityID', '=', $activity.'.activityID')
            ->leftJoin("{$goods}", $cart.'.goods_id', '=', $goods.'.id')
            ->when($condition['where']??[], function ($query) use ($condition){
                return $query->where($condition['where']);
            })
            ->when($condition['whereOp']??[], function ($query) use ($condition){
                return $query->where($condition['whereOp']);
            })
            ->when($condition['whereRaw']??'', function ($query) use($condition){
                return $query->whereRaw($condition['whereRaw']);
            })
            ->when($condition['whereNotIn']??[], function ($query) use($condition) {
                return $query->whereNotIn($condition['whereNotIn'][0],$condition['whereNotIn'][1]);
            })
            ->when($condition['whereIn']??[], function ($query) use($condition) {
                return $query->whereIn($condition['whereIn'][0],$condition['whereIn'][1]);
            })
            ->when($orderBy, function ($query, $orderBy){
                return $query->orderBy($orderBy[0],$orderBy[1]);
            }, function ($query) use ($activityGoods) {
                return $query->orderBy($activityGoods.'.stock','desc');
            });
        if($isPage){
            if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()->toArray()];
            return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->forPage($page, $limit)->get()];
        }
        if($isArray) return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()->toArray()];
        return empty($connect) ? ['count' => $connect->count(), 'list' =>[]]:['count' => $connect->count(), 'list' => $connect->get()];
    }

    public function getResourceSingleSonByPrimaryId(int $PrimaryId, array $PrimaryField, array $SonField)
    {
        // TODO: Implement getMergeSingleSonByPrimaryId() method.
    }

}