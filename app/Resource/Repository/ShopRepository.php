<?php


namespace App\Resource\Repository;


use App\Resource\Model\StoreShopModel;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class ShopRepository implements BaseRepositoryInterface
{
    /**
     * 主表查询
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findFirst(array $field = ['*'],array $where = [],string $whereRaw='',bool $isArray=true)
    {
        $connect = StoreShopModel::query()->select($field)->where($where)
            ->when($whereRaw, function ($query, $whereRaw) {
                return $query->whereRaw($whereRaw);
            })
            ->first();
        if($isArray) return empty($connect) ? array():$connect->toArray();
        return $connect;
    }
    /**
     * 单个字段值
     * @param array $where
     * @param array $field
     * @param bool $isArray
     * @return array|Builder|Model|null|object
     * @author zhangzhiyuan
     */
    public function findFirstValue(string $valueName,array $where = [])
    {
       return StoreShopModel::query()->where($where)->value($valueName);
    }

    /**
     * 主表查询多条
     * @param array $where
     * @param array|string[] $field
     * @param bool $isArray
     * @return array|Builder|Model|object|null
     * @author zhangzhiyuan
     */
    public function selectList(array $field,array $where,string $whereRaw,array $whereIn,array $whereNotIn,int $page,int $limit,array $orderBy,bool $isArray=true)
    {

    }

    public function getResourceSingleSonByPrimaryId(int $PrimaryId, array $PrimaryField, array $SonField)
    {
        // TODO: Implement getMergeSingleSonByPrimaryId() method.
    }

}