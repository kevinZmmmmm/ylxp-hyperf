<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Resource\Model;

use App\Order\Model\OrderGoodsModel;
use Hyperf\DbConnection\Model\Model;

class GoodsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_goods';
    protected $primaryKey = 'id';
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden
        = [

        ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes = [
        'group_id' => 0,
        'type_id' => 0,
        'status' => 1,
        'sort' => 0,
        'is_home' => 0,
        'is_deleted' => 0,
        'hot_id' => 0,
        'safety_stock' => 2,
    ];

    public function goodsList()
    {
        return $this->hasOne(GoodsListModel::class, 'goods_id', 'id');
    }

    /**
     * 商品分类关联
     */
    public function goodsCate()
    {
        return $this->hasOne(CategoryModel::class, 'id', 'cate_id');
    }

    public function orderGoods()
    {
        return $this->hasMany(OrderGoodsModel::class, 'goods_id', 'id');
    }

    /**
     * 获取正常商品的 where
     *
     * @return \Hyperf\Database\Model\Builder
     */
    public static function normalWhere()
    {
        return self::query()->where('is_deleted', 0)->where('status', 1);
    }

}
