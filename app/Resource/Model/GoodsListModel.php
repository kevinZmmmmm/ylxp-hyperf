<?php
/**
 * Created by PhpStorm.
 * User: ran
 * Date: 2020/7/18
 * Time: 16:09
 */

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

class GoodsListModel extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_goods_list';
    protected $primaryKey = 'id';
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden
        = [

        ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

}
