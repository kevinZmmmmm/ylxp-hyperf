<?php

declare (strict_types=1);

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class MenuModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_menu';
    const CREATED_AT = 'create_at';
    const UPDATED_AT = null;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'pid',
            'title',
            'node',
            'icon',
            'url',
            'params',
            'target',
            'sort',
            'status',
        ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes
        = [
            'node' => '',
            'params' => '',
            'target' => '_self',
            'sort' => 0,
            'status' => 1,
        ];
}
