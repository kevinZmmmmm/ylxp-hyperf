<?php

declare (strict_types=1);

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class NavigationModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_navigation';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'img', 'jump_type', 'sort', 'status', 'jump_url', 'start_time', 'end_time', 'type', 'content'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes = [
        'sort' => 0,
        'status' => 1,
    ];

    const TYPE_NAV        = 1; // 图文导航
    const TYPE_PLAY_IMAGE = 2; // 轮播图
    const TYPE_POPUP      = 3; // 弹窗广告
    const TYPE_NOTICE     = 4; // 公告管
    const TYPE_LANDING    = 5; // 落地页
    const TYPE_COUPON     = 6; // 优惠券
}
