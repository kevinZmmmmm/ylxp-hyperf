<?php

declare (strict_types=1);

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class CookbookModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_cookbook';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable
//        = [
//            'title',
//            'take_time',
//            'difficulty_level',
//            'delicious_level',
//            'detailed_list',
//            'image',
//            'rotation_image',
//            'detail',
//            'sort',
//            'status',
//            'main_materials',
//            'second_materials',
//            'matching_materials',
//        ];
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes
        = [
            'sort' => 0,
        ];

}
