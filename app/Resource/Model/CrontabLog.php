<?php


namespace App\Resource\Model;


use Hyperf\DbConnection\Model\Model;





    /**
     */
class CrontabLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'qb_crontab_log';
    protected $connection = 'log';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = [];
    protected $guarded = ['id'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];


}