<?php

declare (strict_types=1);

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

class TeamLeaderCommissionRecord extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_team_leader_commission_record';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'serial_no' => 'string'
    ];

    protected $attributes = [];

    const TYPE_WITHDRAW          = 1;   // 提现
    const TYPE_INCOME_COMMISSION = 2;   // 收益
    const TYPE_BACK_COMMISSION   = 3;   // 退佣金
}