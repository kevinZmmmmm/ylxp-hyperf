<?php

declare (strict_types=1);

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

class CrdCategoryModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_crd_goods_cate';
    const CREATED_AT = 'create_at';
    const UPDATED_AT = null;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['logo', 'title', 'desc', 'status', 'sort'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes = [
        'status' => 1,
        'is_home' => 0,
        'sort' => 0
    ];

    /**
     *关联模型
     */
//    public function goods()
//    {
//        return $this->hasMany(GoodsModel::class, 'cate_id', 'id');
//    }
}