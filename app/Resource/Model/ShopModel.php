<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;


class ShopModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_shop';
    protected $primaryKey = 'shop_id';
    const UPDATED_AT = null;
    const CREATED_AT = 'create_at';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['shop_id'];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * 商品分类关联
     */
//    public function shopGroup()
//    {
//        return $this->hasOne(ShopGroupModel::class,'id', 'shop_group_id');
//    }
}
