<?php

declare (strict_types=1);

namespace App\Resource\Model;

use App\User\Model\MemberModel;
use Hyperf\DbConnection\Model\Model;

class TeamLeaderModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_team_leader';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $attributes = [
        'status' => 1,
    ];

    /**
     * 银行列表
     *
     * @return array
     */
    public static function banks()
    {
        return [
            ['id' => 1, 'name' => '中国工商银行',],
            ['id' => 2, 'name' => '招商银行',],
            ['id' => 3, 'name' => '中国农业银行',],
            ['id' => 4, 'name' => '中国建设银行',],
            ['id' => 5, 'name' => '中国银行',],
            ['id' => 6, 'name' => '中国民生银行',],
            ['id' => 7, 'name' => '中国光大银行',],
            ['id' => 8, 'name' => '中信银行',],
            ['id' => 9, 'name' => '交通银行',],
            ['id' => 10, 'name' => '兴业银行',],
            ['id' => 11, 'name' => '上海浦东发展银行',],
            ['id' => 12, 'name' => '中国人民银行',],
            ['id' => 13, 'name' => '华夏银行',],
            ['id' => 14, 'name' => '深圳发展银行',],
            ['id' => 15, 'name' => '广东发展银行',],
            ['id' => 16, 'name' => '国家开发银行',],
            ['id' => 17, 'name' => '厦门国际银行',],
            ['id' => 18, 'name' => '中国进出口银行',],
            ['id' => 19, 'name' => '中国农业发展银行',],
            ['id' => 20, 'name' => '北京银行',],
            ['id' => 21, 'name' => '上海银行',],
            ['id' => 22, 'name' => '中国邮政储蓄银行',],
        ];
    }

    public function member()
    {
        return $this->hasOne(MemberModel::class, 'mid', 'mid');
    }
}