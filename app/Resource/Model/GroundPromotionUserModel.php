<?php

declare (strict_types=1);

namespace App\Resource\Model;

use App\User\Model\UserModel;
use Hyperf\DbConnection\Model\Model;

/**
 */
class GroundPromotionUserModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_ground_promotion_user';
    const CREATED_AT = 'create_at';
    const UPDATED_AT = null;
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    protected $guarded = [];

    public function getAdminUserIdAttribute($value)
    {

        return UserModel::where(['id' => $value])->value('username');

    }

    public function getCodeUrlAttribute($value)
    {

        return $value?explode(',',$value):[];

    }
}
