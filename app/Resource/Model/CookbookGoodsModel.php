<?php

declare (strict_types=1);

namespace App\Resource\Model;

use Hyperf\DbConnection\Model\Model;

class CookbookGoodsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hf_cookbook_goods';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
