<?php

declare (strict_types=1);

namespace App\Resource\Model;


use Hyperf\DbConnection\Model\Model;

class CrdNavigationShopModel extends Model
{
    protected $table = 'hf_crd_navigation_shop';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}