<?php


namespace App\Resource\Controller\Category;


use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Service\Category\CategoryBusinessService;
use App\Resource\Service\Category\CategoryMiniService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * Class CategoryBusinessController
 * @package App\Resource\Controller\Category
 */
class CategoryBusinessController extends AbstractController
{

    /**
     * @Inject()
     * @var CategoryBusinessService
     */
    protected CategoryBusinessService $categoryBusinessService;

    /**
     * 业务场景（商家端及时达获取分类接口）
     * @RequestMapping(path="/v1/app/timely/category", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     * mailbox 466180170@qq.com
     */
    public function getResourcAppTimelyArriveCategoryList(): array
    {
        $res = $this->categoryBusinessService->getResourceTimelyArriveCategoryList();
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（商家端次日达获取分类接口）
     * @RequestMapping(path="/v1/app/overnight/category", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     * mailbox 466180170@qq.com
     */
    public function getResourcMiniOverNightArriveCategoryList(): array
    {
        $res = $this->categoryBusinessService->getResourceOverNightArriveCategoryList();
        return $this->success($res['list'], '获取成功',$res['count']);
    }

}