<?php


namespace App\Resource\Controller\Category;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Service\Category\CategoryMiniService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * Class CategoryMiniController
 * @package App\Resource\Controller\Category
 */

class CategoryMiniController extends AbstractController
{
    /**
     * @Inject()
     * @var CategoryMiniService
     */
    protected CategoryMiniService $categoryMiniService;


    /**
     * 业务场景（及时达小程序获取分类接口）
     * @RequestMapping(path="/v1/api/timely/category", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     * mailbox 466180170@qq.com
     */
    public function getResourcMiniTimelyArriveCategoryList(): array
    {
        $res = $this->categoryMiniService->getResourceTimelyArriveCategoryList();
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（次日达小程序获取分类接口）
     * @RequestMapping(path="/v1/api/overnight/category", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     * mailbox 466180170@qq.com
     */
    public function getResourcMiniOverNightArriveCategoryList(): array
    {
        $res = $this->categoryMiniService->getResourceOverNightArriveCategoryList();
        return $this->success($res['list'], '获取成功',$res['count']);
    }


}