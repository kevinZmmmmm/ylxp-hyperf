<?php

declare(strict_types=1);

namespace App\Resource\Controller\Cart;


use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Service\Cart\CartMiniService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class)
 * })
 * Class CartMiniController
 * @package App\Resource\Controller\Cart
 */
class CartMiniController extends AbstractController
{
    /**
     * @Inject()
     * @var CartMiniService
     */
    protected CartMiniService $cartMiniService;

    /**
     * * 业务场景（及时达购物车）
     * @RequestMapping(path="/v1/api/timely/single/join_cart", methods="post")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-13 14:52
     * mailbox 466180170@qq.com
     */
    public function updateSingleResourceMiniTimelyCart(RequestInterface $request){
        $params = $request->all();
        $mid =(int)$request->getAttribute('mid');
        $count =isset($params['count'])?empty($params['count'])?1:(int)$params['count']:$params['count']??1;
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($mid) || empty($params['shop_id']) || empty($params['id']) || !isset($params['goods'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $goods = json_decode($params['goods'], true);
        if (!is_array($goods)) return $this->failed(ErrorCode::ERROR_TYPE);
        $res = $this->cartMiniService->updateSingleResourceTimelyCart((int)$params['shop_id'],(int)$mid,(string)$params['id'],(array)$goods,(int)$count);
        if(!$res['code']) return $this->success($res['data'],'加入购物车成功');
        return $this->failed($res['code'], $res['msg']);
    }

}