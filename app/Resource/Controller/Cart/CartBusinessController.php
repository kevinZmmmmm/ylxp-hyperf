<?php

declare(strict_types=1);

namespace App\Resource\Controller\Cart;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\AesMiddleware;
use App\Resource\Service\Cart\CartBusinessService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(AesMiddleware::class)
 * })
 */
class CartBusinessController extends AbstractController
{
    /**
     * @Inject()
     * @var CartBusinessService
     */
    private $cartBusinessService;

    /**
     * 获取预设订单
     *
     * @RequestMapping(path="/v1/app/order/preset", methods="get")
     *
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @throws \Exception
     */
    public function presetOrder(RequestInterface $request)
    {
        $type = $request->input('type', 1);
        if (!in_array($type, [1, 2])) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $res = $this->cartBusinessService->getPresetOrder($type);
        $res['goodsList'] = array_values($res['goodsList']);
        return $this->success($res, '获取成功');
    }

    /**
     * 更新预设订单
     *
     * @RequestMapping(path="/v1/app/order/preset", methods="put")
     *
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @throws \Exception
     */
    public function updatePreset(RequestInterface $request)
    {
        $goods_id = $request->input('goods_id');
        $type = $request->input('type', 1);
        $number = intval($request->input('number', 1));
        if (!$goods_id || $number < 0 || !in_array($type, [1, 2])) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $res = $this->cartBusinessService->singleRefresh($goods_id, $number, $type);
        if (!$res['code']) {
            return $this->success($res['data'], '更新成功');
        } else {
            return $this->failed($res['code'], $res['msg']);
        }
    }

    /**
     * 计算优惠后预设订单
     *
     * @RequestMapping(path="/v1/app/order/preset/discount", methods="put")
     *
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @throws \Exception
     */
    public function afterDiscount(RequestInterface $request)
    {
        $discount = $request->input('discount');
        $type = $request->input('type', 1);
        if (!is_numeric($discount) || $discount < 0 || !in_array($type, [1, 2])) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }
        $res = $this->cartBusinessService->afterDiscount($discount, $type);
        if (!$res['code']) {
            return $this->success($res['data'], '计算成功');
        } else {
            return $this->failed($res['code'], $res['msg']);
        }
    }


}