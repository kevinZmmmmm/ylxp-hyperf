<?php

declare(strict_types=1);


namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Service\CrdCartService;
use App\Resource\Service\CartService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
/**
 * 及时达购物车相关模块服务
 * @Controller()
 * @Middlewares({
 *    @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 * Class CrdCartController
 * @package App\Resource\Controller
 */
class CrdCartController extends AbstractController
{
    /**
     * @Inject()
     * @var CrdCartService
     */
    private $crdCartService;

    /**
     * @Inject()
     * @var CartService
     */
    private $cartService;
    /**
     * 根据商品id更新次日达购物车信息
     * @RequestMapping(path="/v1/api/tomorrow_cart_update/{id:\d+}", methods="put")
     * @param int $id
     * @param RequestInterface $request
     * @return array|bool
     * @throws \Exception
     */
    public function cartUpdateByGoodsId(int $id,RequestInterface $request)
    {
        $params = $request->all();
        $goodsId = $id;
        $shop_id = (int)$params['shop_id'];
        $mid = $request->getAttribute('mid');
        $num = (int)$params['num'] ?? 0;
        $isSelected = (int)$params['is_selected'] ?? 0;
        $group_id = (int)$params['group_id'] ?? 0;
        $activityGoodsId = (int)$params['activity_goods_id'] ?? 0;
        if (!$goodsId || !$shop_id || !$mid) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $updateOrAddData = ['goods_num' => $num, 'is_selected' => $isSelected];
        $cartRes = $this->crdCartService->setInfoCrdGoodsId($mid, $goodsId, $updateOrAddData, $group_id, $shop_id,$activityGoodsId);
        if (!$cartRes['code']) {
            return $this->success('', '更新购物车成功');
        } elseif(isset($cartRes['stock'])) {
            return $this->failed(30005,'每人每天限购'.$cartRes['stock'].''.'件');//每人每天限购
        }else{
            return $this->failed($cartRes['code']);
        }
    }

    /**
     * 批量添加次日达购物车
     * @RequestMapping(path="/v1/api/tomorrow_cart_batch", methods="put")
     * @param RequestInterface $request
     * @return array
     */
    public function batchCart(RequestInterface $request)
    {
        $params = $request->all();
        $mid = $request->getAttribute('mid');
        $shopId = (int)$params['shop_id'];
        $goodsListArr = json_decode($params['goods_list'], true);
        if (!is_array($goodsListArr)) {
            return $this->failed(ErrorCode::ERROR_TYPE);
        }
        if(!isset($goodsListArr[0]['group_id'])){
            $goodsListArr[0]['group_id'] = 0;
        }
        if(!isset($goodsListArr[0]['activity_goods_id'])){
            $goodsListArr[0]['activity_goods_id'] = 0;
        }
        $field = [
            'mid',
            'shop_id',
            'source',
            'group_id',
            'activity_goods_id',
            'goods_id',
            'goods_num',
            'is_selected'
        ];
        $res = $this->crdCartService->batchCart($mid, $shopId, $goodsListArr, $field);
        if (!$res['code']) {
            return $this->success('', '加入购物车成功');
        } elseif(isset($res['stock'])) {
            return $this->failed(30005,'每人每天限购'.$res['stock'].''.'件');
        }else{
            return $this->failed($res['code']);
        }
    }

    /**
     * 清空次日达购物车
     * @RequestMapping(path="/v1/api/tomorrow_cart_clear", methods="delete")
     * @param RequestInterface $request
     * @return array
     */
    //需添加shop_id
    public function clearCartAll(RequestInterface $request)
    {
        $params = $request->all();
        $mid = $request->getAttribute('mid');
        $shop_id = (int)$params['shop_id'];
        if (!$mid || !$shop_id) {
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $cartDel = $this->cartService::deleteCart(['mid' => $mid,'shop_id'=>$shop_id,'source' => 2]);
        if (!$cartDel) {
            return $this->failed(ErrorCode::CART_CLEAR_FAIL);
        }
        return $this->success('', '购物车已清空');
    }

}
