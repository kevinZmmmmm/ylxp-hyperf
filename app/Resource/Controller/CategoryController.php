<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\CategoryRequest;
use App\Resource\Service\CategoryService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller()
 */
class CategoryController extends AbstractController
{
    /**
     * @Inject()
     * @var CategoryService
     */
    private $categoryService;

    /**
     * 获取分类列表
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @RequestMapping(path="/v1/category", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 100;
        $field = [
            'id',
            'title',
            'create_at',
            'sort',
        ];
        $res = $this->categoryService->getList($params, $perPage, $field);

        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /** 小程序获取分类列表api
     * @RequestMapping(path="/v1/api/category", methods="get")
     * @return array
     */
    public function getCategoryList(RequestInterface $request) //要废弃
    {
        $where = [];
        $where['is_deleted'] = 0;
        $where['status'] = 1;
        $field = [
            'id',
            'title'
        ];
        $shopType = $request -> input('shop_type') ?? null;
        $res = $this->categoryService->getCategoryList($where, $field, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取分类信息
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @RequestMapping(path="/v1/category/{id:\d+}", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id, RequestInterface $request)
    {
        $shopType = $request -> input('shop_type') ?? null;
        $res = $this->categoryService->getInfoById((int)$id, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加分类
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PostMapping(path="/v1/category", methods="post")
     * @param CategoryRequest $request
     *
     * @return array
     */
    public function store(CategoryRequest $request)
    {
        $params = $request->validated();
        $res = $this->categoryService->add($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑分类
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PutMapping(path="/v1/category/{id:\d+}", methods="put")
     * @param int $id
     * @param CategoryRequest $request
     *
     * @return array
     */
    public function update(int $id, CategoryRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->categoryService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除分类（软删除）
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @DeleteMapping(path="/v1/category/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id, RequestInterface $request)
    {
        $shopType = $request -> input('shop_type') ?? null;
        $res = $this->categoryService->delete((int)$id, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }


    /**
     * 分类状态更改
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PutMapping(path="/v1/category/{id:\d+}/status", methods="put")
     * @param int $id
     *
     * @return array
     */
    public function editStatus(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        $res = $this->categoryService->editStatus('category', (int)$id, '', $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 分类首页显示更改
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PutMapping(path="/v1/category/{id:\d+}/home", methods="put")
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function editHome(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        $res = $this->categoryService->editHome((int)$id, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 批量修改菜单权重值
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PostMapping(path="/v1/category/sort", methods="post")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function editSort(RequestInterface $request)
    {
        $ids = $request->input('par');
        if (!$ids) {
            return $this->failed(ErrorCode::SERVER_ERROR);
        }
        $shopType = $request->input('shop_type') ?? null;
        $res = $this->categoryService->editManySort($ids, $shopType);

        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * App门店分享分类列表头
     * @RequestMapping(path="/v1/app/store_share", methods="get")
     * @return array
     */
    public function storeShare()
    {
        $where = [];
        $where['is_deleted'] = 0;
        $where['status'] = 1;
        $field = [
            'id',
            'title'
        ];
        $res = $this->categoryService->getAppCategoryList($where, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
