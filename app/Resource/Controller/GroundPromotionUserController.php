<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\GroundPromotionUserRequest;
use App\Resource\Service\GroundPromotionUserService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Phper666\JWTAuth\JWT;


/**
 * @Controller()
 */
class GroundPromotionUserController extends AbstractController
{
    /**
     * @Inject()
     * @var GroundPromotionUserService
     */
    private $groundService;
    /**
     * @Inject()
     * @var JWT
     */
    private $jwt;
    /**
     * 获取地推列表
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @RequestMapping(path="/v1/ground", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 100;
        $field = [
            'id',
            'name',
            'tel',
            'code_url',
            'create_at',
            'status',
            'admin_user_id'
        ];
        $res = $this->groundService->getList($params, $perPage, $field);

        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功',$res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }



    /**
     * 添加地推人员
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PostMapping(path="/v1/ground", methods="post")
     * @param GroundPromotionUserRequest $request
     *
     * @return array
     */
    public function add(GroundPromotionUserRequest $request)
    {
        $jwtData = $this->jwt->getParserData();
        $params = $request->validated();
        $res = $this->groundService->add($params,$jwtData['uid']);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑地推人员
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PutMapping(path="/v1/ground/{id:\d+}", methods="put")
     * @param int $id
     * @param GroundPromotionUserRequest $request
     *
     * @return array
     */
    public function update(int $id, GroundPromotionUserRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->groundService->update((int)$id,$params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除分类（软删除）
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @DeleteMapping(path="/v1/ground/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->groundService->delete((int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }


    /**
     * 分类状态更改
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @PutMapping(path="/v1/ground/{id:\d+}/status", methods="put")
     * @param int $id
     *
     * @return array
     */
    public function editStatus(int $id)
    {
        $res = $this->groundService->editStatus('ground', (int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
