<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\ShopGroupRequest;
use App\Resource\Service\ShopGroupService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class ShopGroupController extends AbstractController
{
    /**
     * @Inject()
     * @var ShopGroupService
     */
    private $shopGroupService;


    /**
     * 获取店群列表
     * @RequestMapping(path="/v1/shop/group", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? (int)$params['perpage'] : 15;
        $list = $this->shopGroupService->getList($params, $perPage);
        if ($list['code']) {
            return $this->failed($list['code']);
        } else {
            return $this->success($list['data']->items(), '获取成功', $list['data']->total());
        }
    }

    /**
     * 获取店群信息及其下所有店铺
     * @RequestMapping(path="/v1/shop/{id:\d+}/group", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $res = $this->shopGroupService->getInfoById($id);
        if ($res['code']) {
            return $this->failed($res['code']);
        } else {
            return $this->success($res['data'], '获取成功');
        }
    }

    /**
     * 新增店群
     * @PostMapping(path="/v1/shop/group", methods="post")
     * @param ShopGroupRequest $request
     *
     * @return array
     */
    public function store(ShopGroupRequest $request)
    {
        $params = $request->all();
        $res = $this->shopGroupService->add($params);
        if ($res['code']) {
            return $this->failed($res['code']);
        } else {
            return $this->success($res['data'], '操作成功');
        }
    }

    /**
     * 编辑店群
     * @PutMapping(path="/v1/shop/{id:\d+}/group", methods="put")
     * @param int $id
     * @param ShopGroupRequest $request
     *
     * @return array
     */
    public function update(int $id, ShopGroupRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->shopGroupService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取所有请求店群下的所有店铺
     * @RequestMapping(path="/v1/shop/group/shops", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function getShopListByGroup(RequestInterface $request)
    {
        $params = $request -> all();
        $res = $this->shopGroupService->getShopListByGroup($params);
        if (!$res['code']) {
            return $this->success($res['data']);
        } else {
            return $this->failed($res['code']);
        }

    }
}
