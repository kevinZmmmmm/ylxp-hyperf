<?php
declare(strict_types=1);

namespace App\Resource\Controller\Navigation;


use App\Common\Controller\AbstractController;
use App\Common\Constants\ErrorCode;
use App\Resource\Service\Goods\GoodsMiniService;
use App\Resource\Service\Home\HomeMiniService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

class NavigationMiniController extends AbstractController
{

}