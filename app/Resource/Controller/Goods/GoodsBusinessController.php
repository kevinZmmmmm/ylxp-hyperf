<?php

declare(strict_types=1);

namespace App\Resource\Controller\Goods;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Service\Goods\GoodsBusinessService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(AesMiddleware::class)
 * })
 * Class GoodsBusinessController
 * @package App\Resource\Controller\Goods
 */

class GoodsBusinessController extends AbstractController
{
    /**
     * @Inject()
     * @var GoodsBusinessService
     */
    protected GoodsBusinessService $goodsBusinessService;
    /**
     * 业务场景（商家端及时达商城类别ID获取商品列表）
     * @RequestMapping(path="/v1/app/timely/category/{category_id:\d+}/goods", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceAppTimelyArriveCategoryGoodsList(int $category_id,RequestInterface $request): array
    {
        $params = $request->all();
        $shopId =(int)$request->getAttribute('shop_id');
        $type =$params['type']??1;
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($category_id) || empty($shopId) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->goodsBusinessService->getResourceTimelyArriveCategoryGoodsList((int)$category_id,(int)$shopId,(int)$params['page'],(int)$params['limit'],(int)$type);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（商家端及时达商品档案信息列表）
     * @RequestMapping(path="/v1/app/timely/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-09 17:02
     * mailbox 466180170@qq.com
     */
    public function getResourceAppTimelyArriveGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        $shopId =(int)$request->getAttribute('shop_id');
        $type =$params['type']??1;
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($shopId) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->goodsBusinessService->getResourceTimelyArriveGoodsList((int)$shopId,(int)$params['page'],(int)$params['limit'],(int)$type);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（商家端及时达商品档案搜索服务）
     * @RequestMapping(path="/v1/app/timely/search/goods", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceAppTimelyArriveSearchGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        $shopId =(int)$request->getAttribute('shop_id');
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($shopId) || !isset($params['page']) || empty($params['limit']) || !isset($params['goods_name'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        if(empty($params['goods_name'])) return $this->failed(ErrorCode::GOODS_SEARCH_ERROR,'请输入您想要的商品名称噢');
        $res = $this->goodsBusinessService->getResourceTimelyArriveSearchGoodsList((int)$shopId,(int)$params['page'],(int)$params['limit'],(string)$params['goods_name']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（商家端次日达分类商品档案信息列表）
     * @RequestMapping(path="/v1/app/overnight/category/{category_id:\d+}/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-09 17:02
     * mailbox 466180170@qq.com
     */
    public function getResourceAppOverNightArriveCategoryGoodsList(int $category_id,RequestInterface $request): array
    {
        $params = $request->all();
        $shopId =(int)$request->getAttribute('shop_id');
        $type =$params['type']??1;
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($category_id) || empty($shopId) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->goodsBusinessService->getResourceOverNightArriveCategoryGoodsList((int)$category_id,(int)$shopId,(int)$params['page'],(int)$params['limit'],(int)$type);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（商家端次日达商品档案信息列表）
     * @RequestMapping(path="/v1/app/overnight/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-09 17:02
     * mailbox 466180170@qq.com
     */
    public function getResourceAppOverNightArriveGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        $shopId =(int)$request->getAttribute('shop_id');
        $type =$params['type']??1;
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($shopId) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->goodsBusinessService->getResourceOverNightArriveGoodsList((int)$shopId,(int)$params['page'],(int)$params['limit'],(int)$type);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（商家端及时达商品档案搜索服务）
     * @RequestMapping(path="/v1/app/overnight/search/goods", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceAppOverNightArriveSearchGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        $shopId =(int)$request->getAttribute('shop_id');
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($shopId) || !isset($params['page']) || empty($params['limit']) || !isset($params['goods_name'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        if(empty($params['goods_name'])) return $this->failed(ErrorCode::GOODS_SEARCH_ERROR,'请输入您想要的商品名称噢');
        $res = $this->goodsBusinessService->getResourceOverNightArriveSearchGoodsList((int)$shopId,(int)$params['page'],(int)$params['limit'],(string)$params['goods_name']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

}