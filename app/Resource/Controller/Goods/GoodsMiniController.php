<?php
declare(strict_types=1);

namespace App\Resource\Controller\Goods;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Service\Goods\GoodsMiniService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * Class GoodsMiniController
 * @package App\Resource\Controller\Goods
 */

class GoodsMiniController extends AbstractController
{

    /**
     * @Inject()
     * @var GoodsMiniService
     */
    protected GoodsMiniService $GoodsMiniService;

    /**
     * 业务场景（及时达商城首页精选产品）
     * @RequestMapping(path="/v1/api/goods/timely/fine_choice", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-02-24 13:34
     * mailbox 466180170@qq.com
     */
    public function getResourceMiniTimelyArriveFineChoiceGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceTimelyArriveFineChoiceGoodsList((int)$params['shop_id'],(int)$params['page'],(int)$params['limit']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（及时达商城热门搜索）
     * @RequestMapping(path="/v1/api/goods/timely/hot_search", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-02-24 13:34
     * mailbox 466180170@qq.com
     */
    public function getResourceMiniTimelyArriveHotSearchGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceTimelyArriveHotSearchGoodsList((int)$params['shop_id'],(int)$params['page'],20);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（小程序及时达商城类别ID获取商品列表）
     * @RequestMapping(path="/v1/api/timely/category/{category_id:\d+}/goods", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniTimelyArriveCategoryGoodsList(int $category_id,RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($category_id )|| empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceTimelyArriveCategoryGoodsList((int)$category_id,(int)$params['shop_id'],(int)$params['page'],(int)$params['limit']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（及时达商城搜索）
     * @RequestMapping(path="/v1/api/goods/timely/search", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-02-24 13:34
     * mailbox 466180170@qq.com
     */
    public function getResourceMiniTimelyArriveSearchGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit']) || !isset($params['goods_name'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        if(empty($params['goods_name'])) return $this->failed(ErrorCode::GOODS_SEARCH_ERROR,'请输入您想要的商品名称噢');
        $res = $this->GoodsMiniService->getResourceTimelyArriveSearchGoodsList((int)$params['shop_id'],(int)$params['page'],(int)$params['limit'],(string)$params['goods_name']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }
    /**
     * 业务场景（及时达商城会员商品更多）
     * @RequestMapping(path="/v1/api/goods/timely/member_more", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-02-24 13:34
     * mailbox 466180170@qq.com
     */
    public function getResourceMiniTimelyArriveMemberMoreGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceTimelyArriveMemberMoreGoodsList((int)$params['shop_id'],(int)$params['page'],(int)$params['limit']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 小程序 获取商品档案详情 业务场景（及时达商城）
     * @RequestMapping(path="/v1/api/goods/timely/{id:\d+}/info", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniTimelyArriveGoodsMsg(int $id, RequestInterface $request): array
    {
        $shop_id =$request->input('shop_id');
        if (!$shop_id) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceTimelyArriveGoodsMsg((int)$id, (int)$shop_id);
        return $this->success($res, '获取成功');
    }
    /**
     * 小程序 获取商品档案详情 业务场景（及时达商城）
     * @RequestMapping(path="/v1/api/goods/{id:\d+}/info", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniTimelyArriveGoodsMsgBak(int $id, RequestInterface $request): array
    {
        $shop_id =$request->input('shop_id');
        if (!$shop_id) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceTimelyArriveGoodsMsg((int)$id, (int)$shop_id);
        return $this->success($res, '获取成功');
    }
    /**
     * 小程序获取商品档案详情 业务场景（次日达商城）
     * @RequestMapping(path="/v1/api/goods/overnight/{id:\d+}/info", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniOverNightArriveGoodsMsg(int $id, RequestInterface $request): array
    {
        $shop_id =$request->input('shop_id');
        if (!$shop_id) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceOverNightArriveGoodsMsg((int)$id);
        return $this->success($res, '获取成功');
    }

    /**
     * 小程序次日达商城小跑精选 业务场景（次日达商城）
     * @RequestMapping(path="/v1/api/goods/overnight", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniOverNightArriveFineChoiceGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceOverNightArriveFineChoiceGoodsList((int)$params['shop_id'],(int)$params['page'],(int)$params['limit']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（次日达商城小程序次日达商城热销搜索）
     * @RequestMapping(path="/v1/api/goods/overnight/hot_search", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniOverNightArriveHotSearchGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceOverNightArriveHotSearchGoodsList((int)$params['shop_id'],(int)$params['page'],(int)$params['limit']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（小程序次日达商品档案搜索服务）
     * @RequestMapping(path="/v1/api/overnight/search/goods", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniOverNightArriveSearchGoodsList(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($params['shop_id']) || empty($params['page']) || empty($params['limit']) || !isset($params['goods_name'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        if(empty($params['goods_name'])) return $this->failed(ErrorCode::GOODS_SEARCH_ERROR,'请输入您想要的商品名称噢');
        $res = $this->GoodsMiniService->getResourceOverNightArriveSearchGoodsList((int)$params['shop_id'],(int)$params['page'],(int)$params['limit'],(string)$params['goods_name']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

    /**
     * 业务场景（次日达商城小程序次日达商城类别ID获取商品列表）
     * @RequestMapping(path="/v1/api/overnight/category/{category_id:\d+}/goods", methods="get")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniOverNightArriveCategoryGoodsList(int $category_id,RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (empty($category_id) || empty($params['shop_id']) || empty($params['page']) || empty($params['limit'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->GoodsMiniService->getResourceOverNightArriveCategoryGoodsList((int)$category_id,(int)$params['shop_id'],(int)$params['page'],(int)$params['limit']);
        return $this->success($res['list'], '获取成功',$res['count']);
    }

}