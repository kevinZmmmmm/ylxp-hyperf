<?php

declare(strict_types=1);

namespace App\Resource\Controller\Cook;


use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\CookbookRequest;
use App\Resource\Service\Cook\CookGoodsMiniService;
use App\Resource\Service\Cook\CookMiniService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 */

class CookMiniController extends  AbstractController
{

    /**
     * @Inject()
     * @var CookMiniService
     */
    protected CookMiniService $CookMiniService;

    /**
     * 小程序商品详情匹配喜欢的菜谱
     * @RequestMapping(path="/v1/api/cook/goods", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     * 平台功能服务命名方法名字
     * 备注（不同业务场景下的服务不同方法（针对不同的业务逻辑）可以不写到interface接口服务中去,相同的业务逻辑必须写到interface接口服务）
     * 获取资源
     * get+模块名称+业务模块名称+自定义业务方法意义
     * 更新资源
     * update+模块名称+业务模块名称+自定义业务方法意义
     * 删除资源
     * delete+模块名称+业务模块名称+自定义业务方法意义
     * 新增资源
     * insert+模块名称+业务模块名称+自定义业务方法意义
     */
    public function getResourceMiniCookById(RequestInterface $request): array
    {
        $params = $request->all();
        if (empty($params['id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        if (empty($params['shop_id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $field = ['id','title','image','main_materials'];
        $res = $this->CookMiniService->getResourceCookById((int)$params['id'], (int)$params['shop_id']);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小程序订单推荐商品列表
     * @RequestMapping(path="/v1/api/cook/order", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author zhangzhiyuan
     */
    public function getResourceMiniCookOrderGoodsListByIds(RequestInterface $request): array
    {
        $params = $request->all();
        if (empty($params['ids'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        if (empty($params['shop_id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->CookMiniService->getResourceCookOrderGoodsListByIds((string)$params['ids'], (int)$params['shop_id']);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

}