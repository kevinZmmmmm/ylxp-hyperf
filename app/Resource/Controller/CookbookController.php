<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\CookbookRequest;
use App\Resource\Service\CookbookService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 */
class CookbookController extends AbstractController
{
    /**
     * @Inject()
     * @var CookbookService
     */
    private $cookbookService;


    /**
     * 菜谱列表
     * @RequestMapping(path="/v1/cookbook", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 15;
        $field = ['id', 'image', 'title', 'main_materials', 'second_materials', 'matching_materials', 'difficulty_level', 'delicious_level', 'take_time', 'sort', 'status', 'created_at'];
        $res = $this->cookbookService->getList($params, $perPage, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取菜谱详情
     * @RequestMapping(path="/v1/cookbook/{id:\d+}", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $field = ['id', 'title', 'image', 'rotation_image', 'detailed_list', 'detail', 'main_materials', 'second_materials', 'matching_materials', 'difficulty_level', 'delicious_level', 'take_time'];
        $res = $this->cookbookService->getInfoById($id, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加菜谱
     * @PostMapping(path="/v1/cookbook", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param CookbookRequest $request
     *
     * @return array
     */
    public function store(CookbookRequest $request)
    {
        $params = $request->validated();
        $res = $this->cookbookService->add($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑菜谱
     * @PutMapping(path="/v1/cookbook/{id:\d+}", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @param CookbookRequest $request
     *
     * @return array
     */
    public function update(int $id, CookbookRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->cookbookService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除菜谱（可批量）
     * @DeleteMapping(path="/v1/cookbook", methods="delete")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     *
     * @return array
     */
    public function delete(RequestInterface $request)
    {
        $params = $request->all();
        if (empty($params['id'])) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->cookbookService->delete($params['id']);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 修改菜谱状态
     * @RequestMapping(path="/v1/cookbook/{id:\d+}/status", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     *
     * @return array
     */
    public function editstatus(int $id)
    {
        $res = $this->cookbookService->editStatus('cookbook', $id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 修改菜谱权重
     * @RequestMapping(path="/v1/cookbook/{id:\d+}/sort", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     *
     * @return array
     */
    public function editSort(int $id, RequestInterface $request)
    {
        $sort = $request->input('sort') ?? 0;
        if ($sort < 0 || $sort > 100) {
            return $this->failed(ErrorCode::SORT_INVALID);
        }
        $res = $this->cookbookService->editSort($id, (int)$sort);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取菜谱中的商品
     * @RequestMapping(path="/v1/cookbook/{id:\d+}/goods", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id 菜谱 ID
     *
     * @return array
     */
    public function getGoodsInCookbook(int $id)
    {
        $res = $this->cookbookService->getGoodsInCookbook($id);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小程序菜谱列表
     * @RequestMapping(path="/v1/api/cookbook", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function getCookBookList(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : 15;
        $field = ['id', 'image', 'title', 'difficulty_level', 'delicious_level', 'take_time', 'sort', 'status', 'created_at'];
        $res = $this->cookbookService->getCookBookList($params, $perPage, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小程序菜谱详情
     * @RequestMapping(path="/v1/api/cookbook/{cook_id:\d+}", methods="get")
     * @param int $cook_id
     * @param RequestInterface $request
     * @return array
     */
    public function getCookBookInfo(int $cook_id,RequestInterface $request)
    {
        $params = $request->all();
        $mid = $params['mid'];
        $shop_id = $params['shop_id'];
        $field = ['id', 'image', 'title', 'difficulty_level','detailed_list','delicious_level', 'main_materials', 'second_materials', 'matching_materials', 'detail', 'take_time', 'sort', 'status', 'created_at'];
        $res = $this->cookbookService->getCookBookInfo((int)$mid, $cook_id, (int)$shop_id, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }


}
