<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\MenuRequest;
use App\Resource\Service\MenuService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class MenuController extends AbstractController
{
    /**
     * @Inject()
     * @var MenuService
     */
    private $menuService;

    /**
     * 获取菜单列表
     * @RequestMapping(path="/v1/menu", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $list = $this->menuService->getList($params);
        if ($list['code']) {
            return $this->failed($list['code']);
        }
        return $this->success($list['data'], '获取成功', count($list['data']));
    }

    /**
     * 获取可选的父级菜单
     * @RequestMapping(path="/v1/menu/parent", methods="get")
     *
     * @return array
     */
    public function getPar()
    {
        $list = $this->menuService->getParIndex();
        if ($list['code']) {
            return $this->failed($list['code']);
        }
        return $this->success($list['data'], '获取成功');
    }

    /**
     * 添加菜单
     * @PostMapping(path="/v1/menu", methods="post")
     * @param MenuRequest $request
     *
     * @return array
     */
    public function store(MenuRequest $request)
    {
        $params = $request->validated();
        $res = $this->menuService->add($params);
        if (!$res['code']) {
            return $this->success($res['data']);
        }
        return $this->failed($res['code']);
    }

    /**
     * 编辑菜单
     * @PutMapping(path="/v1/menu/{id:\d+}", methods="put")
     * @param int $id
     * @param MenuRequest $request
     *
     * @return array
     */
    public function update(int $id, MenuRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->menuService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        }
        return $this->failed($res['code']);
    }

    /**
     * 删除菜单
     * @RequestMapping(path="/v1/menu/{id:\d+}", methods="delete")
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function delete(int $id, RequestInterface $request)
    {
        $res = $this->menuService->delete((int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 禁用/启用
     * @RequestMapping(path="/v1/menu/{id:\d+}/status", methods="put")
     * @param int $id
     *
     * @return array
     */
    public function editStatus(int $id)
    {
        $res = $this->menuService->editStatus('menu', $id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
