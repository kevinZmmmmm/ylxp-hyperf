<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\NavigationRequest;
use App\Resource\Service\NavigationService;
use App\Resource\Service\ResourceService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class PopupController extends AbstractController
{
    /**
     * @Inject()
     * @var NavigationService
     */
    private $navigationService;

    /**
     * @Inject()
     * @var ResourceService
     */
    private $resourceService;

    const POPUP = 3;


    /**
     * 获取弹窗广告列表
     * @RequestMapping(path="/v1/popup", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['id', 'img', 'created_at', 'jump_type', 'jump_url', 'sort'];
        $res = $this->navigationService->getList($params, (int)$perPage, self::POPUP, $field);
        if ($res['code']) {
            return $this->failed($res['code']);
        } else {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        }
    }

    /**
     * 获取弹窗广告信息
     * @RequestMapping(path="/v1/popup/{id:\d+}", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        if (!$id) return $this->failed(ErrorCode::SERVER_ERROR);
        $field = ['id', 'img', 'jump_type', 'jump_url', 'start_time', 'end_time'];
        $res = $this->navigationService->getInfoById((int)$id, self::POPUP, $field, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加广告弹窗
     * @PostMapping(path="/v1/popup", methods="post")
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function store(NavigationRequest $request)
    {
        $params = $request->validated();
        $res = $this->navigationService->add($params, self::POPUP);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑广告弹窗
     * @PutMapping(path="/v1/popup/{id:\d+}", methods="put")
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function update(NavigationRequest $request)
    {
        $params = $request->validated();
        $res = $this->navigationService->update($params, self::POPUP);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除广告弹窗
     * @DeleteMapping(path="/v1/popup/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id, RequestInterface $request)
    {
        $shopType = $request -> input('shop_type') ?? null;
        $res = $this->navigationService->delete($id, self::POPUP, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 解绑应用该广告弹窗的店铺
     * @PostMapping(path="/v1/popup/unbind/shop", methods="post")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function unBindShop(RequestInterface $request)
    {
        $navigationId = $request->input('navigation_id');
        $shopId = $request->input('shop_ids');
        if (!$navigationId || !$shopId) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }

        $shopType = $request -> input('shop_type') ?? null;

        $res = $this->resourceService->unBindShop((int)$navigationId, (int)$shopId, self::POPUP, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
