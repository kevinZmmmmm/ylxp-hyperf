<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\NavigationRequest;
use App\Resource\Service\NavigationService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\Rule;

/**
 * @Controller()
 */
class LandingController extends AbstractController
{
    /**
     * @Inject()
     * @var NavigationService
     */
    private $navigationService;

    /**
     * @Inject()
     * @var ValidatorFactoryInterface
     */
    protected $validationFactory;

    const LANDING = 5;

    /**
     * 落地页列表
     * @RequestMapping(path="/v1/landing", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['id', 'title', 'content', 'sort', 'status', 'created_at'];
        $res = $this->navigationService->getList($params, (int)$perPage, self::LANDING, $field);
        if ($res['code']) {
            return $this->failed($res['code']);
        } else {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        }
    }

    /**
     * 获取落地页详情
     * @RequestMapping(path="/v1/landing/{id:\d+}", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function info(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        $field = ['id', 'title', 'content', 'sort'];
        $res = $this->navigationService->getInfoById($id, self::LANDING, $field, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加落地页
     * @PostMapping(path="/v1/landing", methods="post")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function store(RequestInterface $request)
    {
        $validator = $this->validationFactory->make(
            $request->all(),
            [
//                'title' => 'required|max:25|unique:hf_navigation,title',
                'title' => 'required|max:25',
                'content' => 'required',
                'sort' => 'sometimes',
                'status' => 'sometimes',
                'shop_type' => 'sometimes',
            ],
            [],
            [
                'title' => '落地页标题',
                'content' => '落地页内容',
                'sort' => '排序权重',
                'status' => '状态',
            ]
        );
        if ($validator->fails()) {
            $errorMessage = $validator->errors()->first();
            return $this->failed(ErrorCode::PARAMS_INVALID, $errorMessage);
        }
        $res = $this->navigationService->add($request->all(), self::LANDING);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑落地页
     * @PutMapping(path="/v1/landing/{id:\d+}", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function update(int $id, RequestInterface $request)
    {
        $validator = $this->validationFactory->make(
            $request->all(),
            [
                'id' => 'required',
                'title' => [
                    'required',
                    'max:25',
//                    Rule::unique('hf_navigation')->ignore($id),
                ],
                'content' => 'required',
                'sort' => 'sometimes',
                'status' => 'sometimes',
                'shop_type' => 'sometimes',
            ],
            [],
            [
                'title' => '落地页标题',
                'content' => '落地页内容',
                'sort' => '排序权重',
                'status' => '状态',
            ]
        );
        if ($validator->fails()) {
            $errorMessage = $validator->errors()->first();
            return $this->failed(ErrorCode::PARAMS_INVALID, $errorMessage);
        }
        $params = $request->all();
        $params['id'] = $id;
        $res = $this->navigationService->update($params, self::LANDING);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除落地页
     * @param RequestInterface $request
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @DeleteMapping(path="/v1/landing/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        $res = $this->navigationService->delete($id, self::LANDING, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 禁用/启用
     * @PutMapping(path="/v1/landing/{id:\d+}/status", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     *
     * @return array
     */
    public function editStatus(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        if (!$id) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->navigationService->editStatus('navigation', (int)$id, (string)self::LANDING, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 修改权重
     * @RequestMapping(path="/v1/landing/{id:\d+}/sort", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param int $id
     * @param RequestInterface $request
     *
     * @return array
     */
    public function editSort(int $id, RequestInterface $request)
    {
        $sort = $request->input('sort') ?? 0;
        if ($sort < 0 || $sort > 100) {
            return $this->failed(ErrorCode::SORT_INVALID);
        }
        $shopType = $request->input('shop_type') ?? null;
        $res = $this->navigationService->editSort($id, (int)$sort, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
