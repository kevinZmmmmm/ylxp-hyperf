<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Resource\Request\SystenRequest;
use App\Resource\Service\SystemService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;

/**
 * @Controller()
 */
class SystemController extends AbstractController
{
    /**
     * @Inject()
     * @var SystemService
     */
    private $systemService;

    /**
     * 系统参数配置
     * @RequestMapping(path="/v1/system/config", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @return array
     */
    public function systemConfig()
    {
        $res = $this->systemService->systemConfig();
        return $this->success($res);
    }
    /**
     * 编辑系统参数配置
     * @PutMapping(path="/v1/system/update", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return array
     */
    public function update(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->systemService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }


    /**
     * 配置次日达商城相关参数
     * @PutMapping(path="/v1/crd/config", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param SystenRequest $request
     * @return array
     */
    public function CrdShopConfig(SystenRequest $request)
    {
        $params = $request->validated();
        $res = $this->systemService->CrdShopConfig($params);
        if ($res) {
            return $this->success([], '操作成功');
        } else {
            return $this->failed(0);
        }
    }

    /**
     * 获取次日达商城相关参数
     * @RequestMapping(path="/v1/crd/config", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @return array
     */
    public function CrdShopConfigInfo()
    {
        $res = $this->systemService->CrdShopConfigInfo();
        if ($res) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * APP分享相关参数配置
     * @RequestMapping(path="/v1/crd/share/config", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @return array
     */
    public function CrdShareConfig()
    {
        $res = $this->systemService->ShareConfig();
        if ($res) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed(ErrorCode::NOT_IN_FORCE);
        }
    }

    /**
     * APP分享相关参数配置编辑
     * @PutMapping(path="/v1/crd/share/update/config", methods="put")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return array
     */
    public function CrdUpdateShareConfig(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->systemService->UpdateShareConfig($params);
        if (!$res['code']) {
            return $this->success($res, '操作成功');
        } else {
            return $this->failed(ErrorCode::NOT_IN_FORCE);
        }
    }

    /**
     * 获取服务条款，隐私政策，关于我们
     * @RequestMapping(path="/v1/api/agreement", methods="get")
     * @return array
     * @author liule
     */
    public function agreement(){
        $params = $this->request->all();
        if (!in_array($params['type'], ['service_agreement', 'privacy_policy', 'about_us'])){
            return $this->failed(ErrorCode::SYSTEM_INVALID);
        }
        $res = $this->systemService->getServiceAgreement($params);
        return $this->success($res, '操作成功');
    }

    /**
     * 获取后台系统操作日志
     * @RequestMapping(path="/v1/system/OperationLog", methods="get")
     * @Middlewares({
     *     @Middleware(JwtAuthMiddleware::class),
     *     @Middleware(PermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return array
     */
    public function getSystemOperationLog(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? (int)$params['perpage'] : 15;
        $field = ['*'];
        $res = $this->systemService->getSystemOperationLog($params, $perPage, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed(ErrorCode::NOT_IN_FORCE);
        }
    }


}
