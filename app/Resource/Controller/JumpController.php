<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\JumpRequest;
use App\Resource\Service\JumpService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class JumpController extends AbstractController
{
    /**
     * @Inject()
     * @var JumpService
     */
    private $jumpService;

    /**
     * 获取跳转组件列表
     * @RequestMapping(path="/v1/jump", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = isset($params['perpage']) ? (int)($params['perpage']) : null;
        $field = ['*'];
        $res = $this->jumpService->getList($params, $perPage, $field);
        if (!$res['code']) {
            if(isset($perPage) && !empty($perPage)){
                return $this->success($res['data']->items(), '获取成功', $res['data']->total());
            }else{
                return $this->success($res['data'], '获取成功');
            }
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 跳转组件详情
     * @RequestMapping(path="/v1/goods/{id:\d+}/jump", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $res = $this->jumpService->getInfoById($id);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加跳转组件
     * @PostMapping(path="/v1/jump", methods="post")
     * @param JumpRequest $request
     * @return array
     */
    public function store(JumpRequest $request)
    {
        $params = $request->validated();
        $res = $this->jumpService->add($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑跳转组件
     * @PutMapping(path="/v1/jump/{id:\d+}", methods="put")
     * @param int $id
     * @param JumpRequest $request
     * @return array
     */
    public function update(int $id, JumpRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->jumpService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除跳转组件
     * @DeleteMapping(path="/v1/jump/{id:\d+}", methods="delete")
     * @param int $id
     * @param RequestInterface $request
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->jumpService->delete((int)$id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}