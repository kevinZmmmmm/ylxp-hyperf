<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Service\RegionService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class RegionController extends AbstractController
{
    /**
     * @Inject()
     * @var RegionService
     */
    private $regionService;

    /**
     * 地区三级联动
     *
     * @param RequestInterface $request
     * @RequestMapping(path="/v1/region", methods="get")
     *
     * @return array
     */
    public function getRegionJson(RequestInterface $request)
    {
        $data = $request->all();
        $type = isset($data['type']) ? (int)$data['type'] : 0;
        $pid = isset($data['pid']) ? (int)$data['pid'] : 0;
        if (!$type || !$pid) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->regionService->getRegionByParentId($pid, $type);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
