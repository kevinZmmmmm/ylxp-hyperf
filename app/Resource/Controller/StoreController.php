<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Service\ShopService;
use App\Resource\Service\StoreService;
use App\Order\Service\OrderService;
use App\Resource\Service\StoreMemberService;
use App\Order\Service\OrderRefundService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller()
 */
class StoreController extends AbstractController
{
    /**
     * @Inject()
     * @var StoreService
     */
    private $storeService;

    /**
     * @Inject()
     * @var ShopService
     */
    private $shopService;

    /**
     * @Inject()
     * @var OrderService
     */
    private $orderService;
    /**
     * @Inject()
     * @var StoreMemberService
     */
    private $storeMemberService;
    /**
     * @Inject()
     * @var OrderRefundService
     */
    private $orderRefundService;

    /**
     * 系统参数配置
     * @RequestMapping(path="/v1/store/config", methods="get")
     * @return array
     */

    public function storeConfig()
    {
        $res = $this->storeService->storeConfig();
        return $this->success($res,'操作成功');
    }

    /**
     * 编辑系统参数配置
     * @PutMapping(path="/v1/store/update", methods="put")
     * @param RequestInterface $request
     * @return array
     */
    public function update(RequestInterface $request)
    {
        $params = $request->all();
        $res = $this->storeService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 小程序商家首页
     *
     * @RequestMapping(path="/v1/api/shop/home/{shop_id:\d+}", methods="get")
     * @param $shop_id
     * @return array
     */
    public function home($shop_id)
    {
        $res = $this->shopService->getMiniAppHomeData(intval($shop_id));
        return $this->success($res, '获取成功');
    }

    /**
     * 商家APP首页信息 (弃用)
     * @RequestMapping(path="/v1/app/index", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $shop_id = (int)$params['shop_id'];
        $order_data = $this->orderService->storesIncomes($shop_id,$this->orderService::ORDER_IS_PAY_1);
        if(empty($order_data['sum_income'])){
            $order_data['sum_income'] = "0";
        }
        $type = 1;
        $member_data = $this->storeMemberService->storesIncome($type);
        $month_data = $this->orderService->currentMonth($shop_id,$this->orderService::ORDER_IS_PAY_1);
        if(empty($month_data['month_sum'])){
            $month_data['month_sum'] = "0";
        }
        // 退款表退款额(当天)
        $StoreOrderRefundDayMoney = $this->orderRefundService->refundAmountDay($shop_id);
        // 退款表退款额(当月)
        $StoreOrderRefundMonthMoney = $this->orderRefundService->refundAmountMonth($shop_id);
        $avgPrice = $this->orderService->avgPrice($shop_id,$this->orderService::ORDER_IS_PAY_1);
        $home_info = [
            'shop_id'=>$shop_id,
            'sum_income'=>bcsub($order_data['sum_income'],(string)$StoreOrderRefundDayMoney, 2),
            'count_income'=>$order_data['count_income'],
            'member_num' => $member_data['num'],
            'unit_price' => round($avgPrice, 2),
            'month_sum' =>bcsub($month_data['month_sum'],(string)$StoreOrderRefundMonthMoney,2),
            'month_count' => $month_data['month_count']
        ];
        return $this->success($home_info, '操作成功');
    }
    /**
     * 商家APP首页流程图展示  (弃用)
     * @RequestMapping(path="/v1/app/flowChart", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function flowChart(RequestInterface $request)
    {
        $params = $request->all();
        $shop_id = (int)$params['shop_id'];
        $month_money_sum = $this->orderService->monthMoneySums($shop_id,$this->orderService::ORDER_IS_PAY_1);
        return $this->success($month_money_sum, '操作成功');
    }

    /**
     * 版本
     * @RequestMapping(path="/v1/app/versionUpdate", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function versionUpdatebak(RequestInterface $request)
    {
        $params = $request->all();
        $versions = $params['phone_model_type'];
        if (empty($versions)) {
            $this->failed(ErrorCode::MISSING_PARAMETER);
        }
        $versionArr = [];
        if ($versions == 1) {
            $versionArr = $this->android_version();
        } elseif ($versions == 2) {
            $versionArr = $this->ios_version();
        }
        return $this->success($versionArr, '成功');
    }

    /**
     * 版本
     * @RequestMapping(path="/v1/app/version", methods="get")
     * @param RequestInterface $request
     * @return array
     */
    public function versionUpdate(RequestInterface $request)
    {
        $params = $request->all();
        $versions = $params['phone_model_type'];
        if (empty($versions)) {
            $this->failed(ErrorCode::MISSING_PARAMETER);
        }
        $versionArr = [];
        if ($versions == 1) {
            $versionArr = $this->android_version();
        } elseif ($versions == 2) {
            $versionArr = $this->ios_version();
        }
        return $this->success($versionArr, '成功');
    }

    /**
     * 安卓机型
     * @return array
     */
    public function android_version()
    {
        $res = $this->storeService->android_version();
        $data = [
            'version_name' => $res['version_name'],
            'version' => $res['version'],
            'link' => $res['link'],
            'content' => $res['content'],
            'is_version' => $res['is_version'],
            'version_update' => $res['version_update'],
        ];
        return $data;
    }

    /**
     * ios机型
     * @return array
     */
    public function ios_version()
    {
        $res = $this->storeService->ios_version();
        $data = [
            'version_name' => $res['version_name'],
            'version' => $res['version'],
            'link' => $res['link'],
            'content' => $res['content'],
            'is_version' => $res['is_version'],
            'version_update' => $res['version_update'],
        ];
        return $data;
    }
}
