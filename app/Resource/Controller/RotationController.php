<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\NavigationRequest;
use App\Resource\Service\NavigationService;
use App\Resource\Service\ResourceService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class RotationController extends AbstractController
{
    /**
     * @Inject()
     * @var NavigationService
     */
    private $navigationService;

    /**
     * @Inject()
     * @var ResourceService
     */
    private $resourceService;

    const ROTATION = 2;

    /**
     * 获取轮播图列表
     * @RequestMapping(path="/v1/rotation", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field = ['id', 'sort', 'img', 'jump_type', 'jump_url', 'created_at'];
        $res = $this->navigationService->getList($params, (int)$perPage, self::ROTATION, $field);
        if ($res['code']) {
            return $this->failed($res['code']);
        } else {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        }
    }

    /**
     * 获取轮播图信息
     * @RequestMapping(path="/v1/rotation/{id:\d+}", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        $field = ['id', 'img', 'sort', 'jump_type', 'jump_url', 'start_time', 'end_time'];
        $res = $this->navigationService->getInfoById((int)$id, self::ROTATION, $field, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加轮播图
     * @PostMapping(path="/v1/rotation", methods="post")
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function store(NavigationRequest $request)
    {
        $params = $request->validated();
        $res = $this->navigationService->add($params, self::ROTATION);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑轮播图
     * @PutMapping(path="/v1/rotation/{id:\d+}", methods="put")
     * @param int $id
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function update(int $id, NavigationRequest $request)
    {
        $params = $request->validated();
        $params['id'] = $id;
        $res = $this->navigationService->update($params, self::ROTATION);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除轮播图
     * @DeleteMapping(path="/v1/rotation/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id, RequestInterface $request)
    {
        $shopType = $request->input('shop_type') ?? null;
        $res = $this->navigationService->delete($id, self::ROTATION, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 解绑应用该轮播的店铺
     * @PostMapping(path="/v1/rotation/unbind/shop", methods="post")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function unBindShop(RequestInterface $request)
    {
        $navigationId = $request->input('navigation_id');
        $shopId = $request->input('shop_ids');
        if (!$navigationId || !$shopId) {
            return $this->failed(ErrorCode::PARAMS_INVALID);
        }

        $shopType = $request -> input('shop_type') ?? null;

        $res = $this->resourceService->unBindShop((int)$navigationId, (int)$shopId, self::ROTATION, $shopType);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
