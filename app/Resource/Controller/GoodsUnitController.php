<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Request\GoodsUnitRequest;
use App\Resource\Service\GoodsUnitService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class GoodsUnitController extends AbstractController
{
    /**
     * @Inject()
     * @var GoodsUnitService
     */
    private $goodsUnitService;

    /**
     * 获取单位列表
     * @RequestMapping(path="/v1/goods/unit", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params = $request->all();
        $perPage = !empty($params['perpage']) ? (int)$params['perpage'] : 15;
        $field = ['id', 'title'];
        $res = $this->goodsUnitService->getList($params, $perPage, $field);
        if (!$res['code']) {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 获取单位信息
     * @RequestMapping(path="/v1/goods/{id:\d+}/unit", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        $res = $this->goodsUnitService->getInfoById($id);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加分类
     * @PostMapping(path="/v1/goods/unit", methods="post")
     * @param GoodsUnitRequest $request
     *
     * @return array
     */
    public function store(GoodsUnitRequest $request)
    {
        $params = $request->validated();
        $res = $this->goodsUnitService->add($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑单位
     * @PutMapping(path="/v1/goods/{id:\d+}/unit", methods="put")
     * @param int $id
     * @param GoodsUnitRequest $request
     *
     * @return array
     */
    public function update(int $id, GoodsUnitRequest $request)
    {
        $params = $request->all();
        $params['id'] = $id;
        $res = $this->goodsUnitService->update($params);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除单位
     * @DeleteMapping(path="/v1/goods/{id:\d+}/unit", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->goodsUnitService->delete($id);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }
}
