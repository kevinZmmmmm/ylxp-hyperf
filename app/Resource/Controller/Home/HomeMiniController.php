<?php
declare(strict_types=1);

namespace App\Resource\Controller\Home;


use App\Common\Controller\AbstractController;
use App\Common\Constants\ErrorCode;
use App\Resource\Service\Goods\GoodsMiniService;
use App\Resource\Service\Home\HomeMiniService;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use App\Common\Middleware\AesMiddleware;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * Class HomeMiniController
 * @package App\Resource\Controller\Home
 */
class HomeMiniController extends AbstractController
{
    /**
     * @Inject()
     * @var HomeMiniService
     */
    protected HomeMiniService $homeMiniService;

    /**
     * 业务场景（及时达商城首页集成接口（弹框广告、banner、公告、分类、优惠劵、拼团、限购、会员））
     * @RequestMapping(path="/v1/api/timely/home", methods="get")
     * @param RequestInterface $request
     * @return array
     * @author ran
     * @date 2021-03-04 16:38
     * mailbox 466180170@qq.com
     */
    public function getResourcMiniTimelyArriveHome(RequestInterface $request): array
    {
        $params = $request->all();
        if(empty($params)) return $this->failed(ErrorCode::PARAMS_INVALID);
        if (!$params['shop_id']) return $this->failed(ErrorCode::SYSTEM_INVALID);
        $res = $this->homeMiniService->getResourceTimelyArriveHome((int)$params['shop_id']);
        return $this->success($res, '获取成功',$res);
    }

}