<?php

declare(strict_types=1);

namespace App\Resource\Controller;

use App\Common\Constants\ErrorCode;
use App\Common\Controller\AbstractController;
use App\Common\Middleware\JwtAuthMiddleware;
use App\Common\Middleware\PermissionMiddleware;
use App\Resource\Model\NavigationModel;
use App\Resource\Request\NavigationRequest;
use App\Resource\Service\NavigationService;
use App\Resource\Service\ResourceService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(JwtAuthMiddleware::class),
 *     @Middleware(PermissionMiddleware::class)
 * })
 */
class CouponController extends AbstractController
{
    /**
     * @Inject()
     * @var NavigationService
     */
    private $navigationService;

    /**
     * 获取优惠券导航列表
     * @RequestMapping(path="/v1/coupon", methods="get")
     * @param RequestInterface $request
     *
     * @return array
     */
    public function index(RequestInterface $request)
    {
        $params  = $request->all();
        $perPage = !empty($params['perpage']) ? $params['perpage'] : 15;
        $field   = ['id', 'img', 'created_at', 'jump_type', 'jump_url', 'sort'];
        $res     = $this->navigationService->getList($params, (int)$perPage, NavigationModel::TYPE_COUPON, $field);
        if ($res['code']) {
            return $this->failed($res['code']);
        } else {
            return $this->success($res['data']->items(), '获取成功', $res['data']->total());
        }
    }

    /**
     * 获取优惠券导航信息
     * @RequestMapping(path="/v1/coupon/{id:\d+}", methods="get")
     * @param int $id
     *
     * @return array
     */
    public function info(int $id)
    {
        if (!$id)
            return $this->failed(ErrorCode::SERVER_ERROR);
        $field = ['id', 'img', 'jump_type', 'jump_url', 'start_time', 'end_time'];
        $res   = $this->navigationService->getInfoById((int)$id, NavigationModel::TYPE_COUPON, $field);
        if (!$res['code']) {
            return $this->success($res['data'], '获取成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 添加优惠券导航
     * @PostMapping(path="/v1/coupon", methods="post")
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function store(NavigationRequest $request)
    {
        $params = $request->validated();
        $res    = $this->navigationService->add($params, NavigationModel::TYPE_COUPON);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 编辑优惠券导航
     * @PutMapping(path="/v1/coupon/{id:\d+}", methods="put")
     * @param NavigationRequest $request
     *
     * @return array
     */
    public function update(NavigationRequest $request)
    {
        $params = $request->validated();
        $res    = $this->navigationService->update($params, NavigationModel::TYPE_COUPON);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * 删除优惠券导航
     * @DeleteMapping(path="/v1/coupon/{id:\d+}", methods="delete")
     * @param int $id
     *
     * @return array
     */
    public function delete(int $id)
    {
        $res = $this->navigationService->delete($id, NavigationModel::TYPE_COUPON);
        if (!$res['code']) {
            return $this->success($res['data'], '操作成功');
        } else {
            return $this->failed($res['code']);
        }
    }

    /**
     * @RequestMapping(path="/v1/api/coupon/pic", methods="get")
     * @return array
     */
    public function couponPic(){
//        $shopId = (string) $this->request->input('shop_id');
//        if (!is_numeric($shopId)) return $this->failed(ErrorCode::SYSTEM_INVALID, '店铺信息错误');
        $res = $this->navigationService->couponPic();
        return $this->success($res['data'], $res['msg']);
    }

}
