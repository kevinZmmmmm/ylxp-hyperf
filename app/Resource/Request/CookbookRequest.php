<?php


namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class CookbookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => 'required|between:1,30|unique:hf_cookbook',
                    'take_time' => 'required|integer',
                    'difficulty_level' => 'required|numeric',
                    'delicious_level' => 'required|numeric',
                    'detailed_list' => 'sometimes',
                    'image' => 'required',
                    'rotation_image' => 'required',
                    'detail' => 'sometimes',
                    //                    'sort'               => 'sometimes',
                    'main_materials' => 'required',
                    'second_materials' => 'required',
                    'matching_materials' => 'required',
                    'status' => 'required',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'title' => [
                        'required',
                        'between:1,30',
                        Rule::unique('hf_cookbook')->ignore($id),
                    ],
                    'take_time' => 'required|integer',
                    'difficulty_level' => 'required|numeric',
                    'delicious_level' => 'required|numeric',
                    'detailed_list' => 'sometimes',
                    'image' => 'required',
                    'rotation_image' => 'required',
                    'detail' => 'sometimes',
                    //                    'sort'               => 'sometimes',
                    'main_materials' => 'required',
                    'second_materials' => 'required',
                    'matching_materials' => 'required',
                    'status' => 'required',
                ];
        }
    }

    public function attributes(): array
    {
        return [
            'title' => '菜谱名称',
            'take_time' => '耗时',
            'difficulty_level' => '上手程度',
            'delicious_level' => '美味等级',
            'detailed_list' => '食材清单',
            'image' => '菜谱封面',
            'rotation_image' => '菜谱轮播图',
            'detail' => '菜谱详情',
            //            'sort'               => '排序',
            'main_materials' => '主料商品',
            'second_materials' => '辅料商品',
            'matching_materials' => '搭配食材',
            'status' => '菜谱状态',
            //            'collection' => '收藏数量',
        ];
    }
}
