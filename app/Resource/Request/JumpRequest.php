<?php


namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class JumpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => 'required|max:25|unique:hf_jump_type,title',
                    'is_two_level' => 'required|integer',
                    'url' => 'sometimes',
                    'msg' => 'sometimes',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'title' => ['required', 'max:25', Rule::unique('hf_jump_type')->ignore($id)],
                    'is_two_level' => 'required|integer',
                    'url' => 'sometimes',
                    'msg' => 'sometimes',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '跳转组件名称',
            'is_two_level' => '跳转组件二级选项',
            'url' => '跳转地址',
            'msg' => '提示语',
        ];
    }
}
