<?php

declare(strict_types=1);

namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class TeamLeaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'mid' => 'required',
                    'sid' => 'sometimes',
                    'name' => ['required', 'between:1,8', 'regex:/^[\x{4e00}-\x{9fa5}A-Za-z0-9_]+$/u'], // 数字字母汉字
                    'delivery_address' => 'required',
                    'detail_address' => ['required', 'between:1,20', 'regex:/^[\x{4e00}-\x{9fa5}A-Za-z0-9_]+$/u'],
                    'longitude' => 'required',
                    'latitude' => 'required',
                    'phone' => 'required|integer|digits:11',
                    'phone_code' => 'required|integer',
                    'shop_id' => 'required|integer',
                    'status' => 'sometimes',
                    'reject_reason' => 'sometimes',
                ];

            case 'PUT':
                $params = $this->getParsedBody();
                return [
                    'name' => ['required', 'between:1,8', 'regex:/^[\x{4e00}-\x{9fa5}A-Za-z0-9_]+$/u'], // 数字字母汉字
                    'delivery_address' => 'required',
                    'detail_address' => ['required', 'between:1,20', 'regex:/^[\x{4e00}-\x{9fa5}A-Za-z0-9_]+$/u'],
                    'longitude' => 'required',
                    'latitude' => 'required',
                    'phone' => ['required', 'integer', 'digits:11',Rule::unique('hf_team_leader')->ignore($params['id'])],
//                    'phone_code' => 'required|integer',
                    'shop_id' => 'required|integer',
                    'status' => 'required',
                    'reject_reason' => 'sometimes',
                ];
        }
    }

    public function attributes(): array
    {
        return [
            'mid' => '申请人 ID',
            'sid' => '分享人 ID',
            'name' => '姓名',
            'delivery_address' => '小区/自提点',
            'detail_address' => '街道、门牌号',
            'longitude' => '经度',
            'latitude' => '纬度',
            'phone' => '手机号码',
            'phone_code' => '验证码',
            'shop_id' => '绑定店铺',
            'status' => '团长状态',
            'reject_reason' => '驳回原因',
        ];
    }

}
