<?php


namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class GoodsUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => 'required|alpha_dash|max:25|unique:hf_goods_unit,title',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'title' => ['required', 'max:25', 'alpha_dash', Rule::unique('hf_goods_unit')->ignore($id)],
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '商品单位',
        ];
    }
}
