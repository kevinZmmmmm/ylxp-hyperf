<?php

declare(strict_types=1);

namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ShopGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'shop_group_name' => ['required', 'max:25', Rule::unique('hf_shop_group')->where(function ($query) {
                        $query->where('deleted_at', null);
                    })],

                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'shop_group_name' => ['required', 'max:25', Rule::unique('hf_shop_group')->ignore($id)->where(function ($query) {
                        $query->where('deleted_at', null);
                    })],
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'shop_group_name' => '店群名称',

        ];
    }

//    public function messages(): array
//    {
//        return [
//            'username.unique'   => '用户名已被占用，请重新填写',
//            'username.regex'    => '用户名只支持英文、数字、横杆和下划线。',
//            'username.between'  => '用户名必须介于 3 - 25 个字符之间。',
//            'username.required' => '用户名不能为空。',
//        ];
//    }
}
