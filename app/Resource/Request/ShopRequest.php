<?php

declare(strict_types=1);

namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'shop_name' => ['required', 'between:3,25', Rule::unique('store_shop')->where(function ($query) {
                        $query->where('is_deleted', 0);
                    })],
                    'shop_man' => 'required',
                    'shop_no' => 'required',
                    'lsy_shop_no' => 'required',
                    'crm_mcid' => 'required',
                    'start_time' => 'required',
                    'end_time' => 'required',
                    'shop_desc' => 'required',
                    'deliver_money' => 'required',
                    'free_deliver_money' => 'required',
                    'shop_freight' => 'required',
                    'deliver_type' => 'required',
                    'deliver_kilometre' => 'required_if:deliver_type,1',
                    'deliver_range' => 'required_if:deliver_type,2',
                    'shop_tel' => 'required',
                    'province' => 'required|integer|min:1',
                    'city' => 'required|integer|min:1',
                    'country' => 'required|integer|min:1',
                    'address' => 'required',
                    'status' => 'sometimes|integer',
                    'is_must' => 'sometimes|integer',
                    'latitude' => 'sometimes',
                    'longitude' => 'sometimes',
                    'shop_group_id' => 'required',
                    'shop_sdp_no' => 'sometimes',
                    'shop_sdp_name' => 'sometimes',
                    'shop_sdp_user_code' => 'sometimes',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'shop_name' => [
                        'required',
                        'between:3,25',
                        Rule::unique('store_shop')->ignore($id, 'shop_id')->where(function ($query){
                            $query->where('is_deleted', 0);
                        })
                    ],
                    'shop_man' => 'required',
                    'shop_no' => 'required',
                    'lsy_shop_no' => 'required',
                    'crm_mcid' => 'required',
                    'start_time' => 'required',
                    'end_time' => 'required',
                    'shop_desc' => 'required',
                    'deliver_money' => 'required',
                    'free_deliver_money' => 'required',
                    'shop_freight' => 'required',
                    'deliver_type' => 'required',
                    'deliver_kilometre' => 'required_if:deliver_type,1',
                    'deliver_range' => 'required_if:deliver_type,2',
                    'shop_tel' => 'required',
                    'province' => 'required|integer|min:1',
                    'city' => 'required|integer|min:1',
                    'country' => 'required|integer|min:1',
                    'address' => 'required',
                    'status' => 'sometimes|integer',
                    'is_must' => 'sometimes|integer',
                    'latitude' => 'sometimes',
                    'longitude' => 'sometimes',
                    'shop_group_id' => 'required',
                    'shop_sdp_no' => 'sometimes',
                    'shop_sdp_name' => 'sometimes',
                    'shop_sdp_user_code' => 'sometimes',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'shop_name' => '店铺名称',
            'shop_man' => '联系人',
            'shop_no' => '客至店铺编码',
            'lsy_shop_no' => '收银-天财（龙收银）店铺编码',
            'crm_mcid' => '吾享店铺编码',
            'start_time' => '营业开始时间',
            'end_time' => '营业结束时间',
            'shop_desc' => '营业时间描述',
            'deliver_money' => '起送金额',
            'free_deliver_money' => '免运费金额',
            'shop_freight' => '运费（门店配送）',
            'deliver_type' => '配送范围描述方式',
            'deliver_kilometre' => '配送范围（单位：公里）',
            'deliver_range' => '配送范围（围栏）',
            'shop_tel' => '店铺电话',
            'address' => '门店地址',
            'status' => '店铺营业状态',
            'is_must' => '是否必须自提选项',
            'latitude' => '经度',
            'longitude' => '纬度',
            'shop_group_id' => '隶属店群',
            'shop_sdp_no' => '疏东坡店铺编码',
            'shop_sdp_name' => '疏东坡店铺名称',
            'shop_sdp_user_code' => '疏东坡用户编码',
        ];
    }

    public function messages(): array
    {
        return [
            'deliver_kilometre.required_if' => '请填写配送范围（公里）',
            'deliver_range.required_if' => '请绘制配送范围电子围栏',
        ];
    }

}
