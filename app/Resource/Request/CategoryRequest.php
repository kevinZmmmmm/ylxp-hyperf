<?php


namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $id = $this->route('id');
        $shopType = $this->input('shop_type',null);

        if($shopType){
            $tableName = 'store_crd_goods_cate';
        }else{
            $tableName = 'store_goods_cate';
        }

        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => Rule::unique($tableName)->where(function ($query) {
                        $query->where('is_deleted', 0);
                    }),
                    'logo' => 'sometimes',
                    'desc' => 'sometimes|max:80',
                    'status' => 'sometimes|integer',
                    'sort' => 'sometimes|integer',
                    'is_home' => 'sometimes',
                    'shop_type' => 'sometimes',
                ];

            case 'PUT':

                return [
                    'title' => ['required', Rule::unique($tableName)->ignore($id)->where(function ($query){
                            $query->where('is_deleted', 0);
                        })
                    ],
                    'logo' => 'sometimes',
                    'desc' => 'sometimes|max:80',
                    'status' => 'sometimes|integer',
                    'sort' => 'sometimes|integer',
                    'is_home' => 'sometimes',
                    'shop_type' => 'sometimes',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '分类名称',
            'logo' => '分类图标',
            'desc' => '分类描述',
            'status' => '分类状态',
            'sort' => '排序权重',
            'is_home' => '是否显示首页选项',
        ];
    }
}
