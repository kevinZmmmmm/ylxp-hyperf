<?php


namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class GroundPromotionUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**hf_ground_promotion_user
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'name' => 'required|max:30|unique:hf_ground_promotion_user,name',
                    'tel' =>'required|numeric||unique:hf_ground_promotion_user,tel',
                    'status' => 'sometimes|integer',
                ];

            case 'PUT':
                $params = $this->getParsedBody();
                return [
                    'name' => [
                        'required',
                        'max:100',
                        Rule::unique('hf_ground_promotion_user')->ignore($params['id']),
                    ],
                    'tel' => [
                        'required',
                        'numeric',
                        Rule::unique('hf_ground_promotion_user')->ignore($params['id']),
                    ],
                    'status' => 'sometimes|integer',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'name' => '姓名',
            'tel' => '手机号',
            'status' => '状态',
        ];
    }
}
