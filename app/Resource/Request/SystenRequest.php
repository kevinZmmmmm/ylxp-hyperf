<?php

declare(strict_types=1);

namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class SystenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {

            case 'PUT':

                return [
                    'cut_off_time' => 'date_format:"H:i"|required',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'cut_off_time' => '截单时间',
        ];
    }

    public function messages(): array
    {
        return [
            'cut_off_time.date_format' => '时间格式不正确，正确参考：21:00'
        ];
    }

}
