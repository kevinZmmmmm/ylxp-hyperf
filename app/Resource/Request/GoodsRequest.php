<?php


namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;

class GoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => 'required|max:60',   // 商品有标品非标品以及其它规格，因此名称不检测重复
                    'introduction' => 'sometimes|max:80',
                    'scant_id' => 'required|integer',
                    'spec' => 'required',
                    'unit' => 'required',
                    'unit_id' => 'required',
                    'err' => 'sometimes',
                    'sort' => 'sometimes',
                    'handle' => 'sometimes|max:80',
                    'number_virtual' => 'sometimes|integer',
                    'goods_crm_code' => 'required|integer',
                    'lsy_goods_name' => 'sometimes',
                    'lsy_unit_name' => 'sometimes',
                    'lsy_class_name' => 'sometimes',
                    'wx_crm_code' => 'required',
                    'kz_type_id' => 'required',
                    'kz_goods_id' => 'required',
                    'cookbook' => 'sometimes',
                    "is_today" => 'required|integer',
                    "is_next_day" => 'required|integer',
                    'cate_id' => 'required_if:is_today,1',  // 及时达商城的商品分类
                    'read_pos_price' => 'required|integer',
                    'price_selling' => 'required',    //商品售价，若read_pos_price为0，则必填
                    'member_goods' => 'required',
                    'member_func' => 'required_if:member_goods,1',    //若member_goods为1，则该字段必填，1为按折扣，2位按固定金额
                    'discount' => 'required_if:member_func,1',    //若member_func 为1，则必填
                    'member_price' => 'required_if:member_func,2',    //若member_func为2，则必填
                    'read_pos_stock' => 'required|integer',
                    'number_stock' => 'sometimes',  //若read_pos_stock为0，则必填
                    "safety_stock" => 'sometimes',
                    'hot_id' => 'required|integer',
                    'is_home' => 'required|integer',
                    'crd_cate_id' => 'required_if:is_next_day,1',  // 次日达商城的商品分类
                    'limite_num_per_day' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'cost_price' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'price_market' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'commission' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'goods_pick_time' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'logo' => 'required',
                    'image' => 'required',   //可填多个，用 | 隔开
                    'share_image' => 'required',
                    'content' => 'sometimes',
                    'status' => 'sometimes',  // 【保存商品】按钮传0，【保存并上架】传1
                ];

            case 'PUT':
                return [
                    'title' => [
                        'required',
                        'max:60',
                    ],
                    'introduction' => 'sometimes|max:80',
                    'scant_id' => 'required|integer',
                    'spec' => 'required',
                    'unit' => 'required',
                    'unit_id' => 'required',
                    'err' => 'sometimes',
                    'sort' => 'sometimes',
                    'handle' => 'sometimes|max:80',
                    'number_virtual' => 'sometimes|integer',
                    'goods_crm_code' => 'required|integer',
                    'lsy_goods_name' => 'sometimes',
                    'lsy_unit_name' => 'sometimes',
                    'lsy_class_name' => 'sometimes',
                    'wx_crm_code' => 'required',
                    'kz_type_id' => 'required',
                    'kz_goods_id' => 'required',
                    'cookbook' => 'sometimes',
                    "is_today" => 'required|integer',
                    "is_next_day" => 'required|integer',
                    'cate_id' => 'required_if:is_today,1',
                    'read_pos_price' => 'required|integer',
                    'price_selling' => 'required',    //商品售价，若read_pos_price为0，则必填
                    'member_goods' => 'required',
                    'member_func' => 'required_if:member_goods,1',    //若member_goods为1，则该字段必填，1为按折扣，2位按固定金额
                    'discount' => 'required_if:member_func,1',    //若member_func 为1，则必填
                    'member_price' => 'required_if:member_func,2',    //若member_price为2，则必填
                    'read_pos_stock' => 'required|integer',
                    'number_stock' => 'sometimes',  //若read_pos_stock为0，则必填
                    "safety_stock" => 'sometimes',
                    'hot_id' => 'required|integer',
                    'is_home' => 'required|integer',
                    'crd_cate_id' => 'required_if:is_next_day,1',  // 次日达商城的商品分类
                    'limite_num_per_day' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'cost_price' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'price_market' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'commission' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'goods_pick_time' => 'required_if:is_next_day,1',    // 商品选择次日达商城时，该项为必填项
                    'logo' => 'required',
                    'image' => 'required',   //可填多个，用 | 隔开
                    'share_image' => 'required',
                    'content' => 'sometimes',
                    'status' => 'sometimes',  // 【保存商品】按钮传0，【保存并上架】传1
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '商品名称',
            'err' => '规格误差',
            'introduction' => '商品简介',
            'cate_id' => '及时达商品分类',
            'scant_id' => '商品种类',
            'spec' => '商品规格',
            'unit' => '商品单位',
            'unit_id' => '商品单位 ID',
            'handle' => '处理方式',
            'number_virtual' => '已售基数',
            'goods_crm_code' => '龙收银编码',
            'lsy_goods_name' => '龙收银菜品名称',
            'lsy_unit_name' => '龙收银单位名称',
            'lsy_class_name' => '龙收银类别名称',
            'wx_crm_code' => '吾享商品编码',
            'kz_type_id' => '客至类别 ID',
            'kz_goods_id' => '客至商品 ID',
            'cookbook' => '菜谱绑定',
            'price_selling' => '商品价格',
            'member_goods' => '会员商品',
            'member_func' => '会员价',
            'discount' => '折扣比例',
            'member_price' => '会员价固定金额',
            'read_pos_price' => '读取POS价格',
            'read_pos_stock' => '读取POS库存',
            'status' => '销售状态',
            'hot_id' => '热销商品',
            'is_home' => '首页展示',
            'logo' => '商品主图',
            'share_image' => '分享图片',
            'image' => '商品图片',
            'content' => '商品详情',
            'number_stock' => '单独库存',
            "is_next_day" => '次日达',
            "is_today" => '及时达',
            "safety_stock" => '安全库存',
            'limite_num_per_day' => '每人每天限购数量',    // 商品选择次日达商城时，该项为必填项
            'cost_price' => '成本价',    // 商品选择次日达商城时，该项为必填项
            'commission' => '分佣金额',    // 商品选择次日达商城时，该项为必填项
            'goods_pick_time' => '商品自提时间',    // 商品选择次日达商城时，该项为必填项
            'price_market' => '商品划线价',    // 商品选择次日达商城时，该项为必填项
            'crd_cate_id' => '次日达商品分类',    // 商品选择次日达商城时，该项为必填项
        ];
    }

    public function messages(): array
    {
        return [
            'price_selling.required_if' => '不读 POS 价格，或者选择次日达商品时，商品售价必须填写',
            'member_func.required_if' => '会员商品必须选择折扣方式',
            'discount.required_if' => '折扣方式为按照折扣比例时，折扣比例必须填写',
            'member_price.required_if' => '折扣方式为按照固定金额时，会员价金额必须填写',
            'number_stock.required_if' => '不读取 POS 库存时，单独库存必须填写',
            'limite_num_per_day.required_if' => '每人每天限购数量必须填写当商品类别为次日达时',
            'cost_price.required_if' => '商品成本价必须填写当商品类别为次日达时',
            'goods_pick_time.required_if' => '商品自提时间必须填写当商品类别为次日达时',
            'cate_id.required_if' => '商品分类必须填写当商品类别为及时达时',
            'crd_cate_id.required_if' => '次日达商品分类必须填写当商品类别为次日达时',
            'price_market.required_if' => '商品划线价必须填写当选择次日达商品时',
            'commission.required_if' => '分佣金额必须填写当选择次日达商品时',

        ];
    }
}
