<?php

namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'pid' => 'required|integer',
                    'title' => 'required|alpha_dash|max:30|unique:system_menu,title',
                    'url' => 'required',
                    'icon' => 'sometimes',
                    'tag' => 'sometimes',
                ];

            case 'PUT':
                $id = $this->route('id');

                return [
                    'title' => [
                        'required',
                        'alpha_dash',
                        'max:30',
                        Rule::unique('system_menu')->ignore($id),
                    ],
                    'pid' => 'required|integer',
                    'url' => 'required',
                    'icon' => 'sometimes',
                    'tag' => 'sometimes',
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'pid' => '上级菜单',
            'title' => '菜单名称',
            'url' => '菜单链接',
            'icon' => '菜单图标',
        ];
    }
}
