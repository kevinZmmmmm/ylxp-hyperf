<?php

declare(strict_types=1);

namespace App\Resource\Request;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class NavigationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case
            'POST':
                return [
                    'title' => 'sometimes|max:25',
                    'img' => 'required',
                    'jump_type' => 'required|integer',
                    'jump_url' => 'sometimes',
                    'sort' => 'sometimes|integer',
                    'status' => 'sometimes|integer',
                    'type' => 'sometimes|integer',
                    'start_time' => 'sometimes',
                    'end_time' => 'sometimes',
                    'shop_ids' => 'sometimes',
                    'shop_type' => 'sometimes'
                ];

            case 'PUT':
                return [
                    'title' => [
                        'sometimes',
                        'max:25',
                    ],
                    'img' => 'required',
                    'jump_type' => 'required|integer',
                    'jump_url' => 'sometimes',
                    'sort' => 'sometimes|integer',
                    'status' => 'sometimes|integer',
//                    'type'       => 'required|integer',
                    'start_time' => 'sometimes',
                    'end_time' => 'sometimes',
                    'shop_ids' => 'sometimes',
                    'shop_type' => 'sometimes'
                ];
        }

    }

    public function attributes(): array
    {
        return [
            'title' => '名称',
            'img' => '图片',
            'jump_type' => '跳转类型',
            'sort' => '权重值',
            'status' => '状态',
            'type' => '类型',
            'start_time' => '有效期开始时间',
            'end_time' => '有效期结束时间',
            'shop_type' => '商城类型，普通商城或次日达商城',
        ];
    }
}
