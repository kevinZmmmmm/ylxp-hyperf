<?php
declare(strict_types=1);

namespace App\Resource\Task;

use App\Resource\Model\CrdCartModel;
use App\Resource\Model\HfCartModel;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="DeleteSaleTask", rule="5 0 * * *", callback="task", memo="每天0点05删除秒杀购物车内前一日的商品")
 * Class DeleteSaleTask
 * @package App\Resource\Task
 */
class DeleteSaleTask
{

    public function task()
    {
        CrdCartModel::query()->where('group_id','=',4)->delete();
        HfCartModel::query()->where('group_id','=',4)->delete();
    }
}