<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Resource\Model\StoreShopMessageModel;
use Hyperf\DbConnection\Db;

class StoreShopMessageService extends BaseService
{
    /**
     * 新增反馈内容
     * @param $shop_id
     * @param array $data
     * @return int|string|array
     */
    public static function getMessage(int $shop_id, array $data)
    {
        $res = StoreShopMessageModel::query()->where(['shop_id' => $shop_id])->first();
        if($res){
            $data['update_time'] = date('Y-m-d H:i:s');
            $date =StoreShopMessageModel::query()->where(['shop_id' => $shop_id])->update($data);
            return $date;
        }else{
            $data['create_time'] = date('Y-m-d H:i:s');
            $date=StoreShopMessageModel::query()->insert($data);
            return $date;
        }
    }
}
