<?php

declare(strict_types=1);

namespace App\Resource\Service;
use App\Activity\Model\ActivityGoodsModel;
use App\Activity\Model\ActivityModel;
use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Order\Model\OrderGoodsModel;
use App\Order\Model\OrderModel;
use App\Resource\Model\CartModel;
use App\Resource\Model\GoodsListModel;
use App\Resource\Model\GoodsModel;
use App\Resource\Model\StoreGoodsListModel;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;
use function Symfony\Component\Translation\t;


class CrdCartService extends BaseService
{
    /**
     * 商品是否有效 0有效商品 1无效商品
     */
    const GOODS_STATUS_0 = 0;
    const GOODS_STATUS_1 = 1;
    private $log;
    /**
     *  1次日达
     */
    const CART_SOURCE_1 = 1;

    /**
     * @Inject()
     * @var CartService
     */
    private $cartService;

    public function __construct(LoggerFactory $loggerFactory)
    {
        parent::__construct();
        $this->log = $loggerFactory->get('cart.log', 'default');
    }

    /**
     * 根据会员id及商品id 更新/新增 次日达购物车信息
     * @param int $mid
     * @param int $goodsId
     * @param array $updateOrAddData
     * @param int $group_id
     * @param int $shop_id
     * @param int $activityGoodsId
     * @return bool|array
     * @throws \Exception
     */
    public function setInfoCrdGoodsId(int $mid, int $goodsId, array $updateOrAddData, int $group_id, int $shop_id, int $activityGoodsId)
    {
        //判断商品是否删除
        if (!self::isEffectiveDelete($goodsId,$shop_id)){
            return ['code' => ErrorCode::GOODS_DELETE];
        }
        //判断平台是否下架
        $data = self::isEffectivePlatform($goodsId);
        if($data){
            return ['code' => ErrorCode::GOODS_PLATFORM_STATUS];
        }
        //判断店铺是否下架
        if (!self::isEffectiveByGoodsId($goodsId, $shop_id)) {
            return ['code' => ErrorCode::GOODS_STATUS];
        }
        //判断秒杀活动是否有效
        if($group_id == 4){
            //活动商品已下架
            if(!self::saleAct($activityGoodsId)){
                return ['code' => ErrorCode::SALE_GOODS_INVALID];
            }
            //活动商品库存不足
            if(!self::saleActStock($activityGoodsId,$updateOrAddData['goods_num'])){
                return ['code' => ErrorCode::SALE_STOCK_INVALID];
            }
        }
        $datetime = date('Y-m-d H:i:s');
        $cartObjs = CartModel::query()->where(['mid' => $mid, 'goods_id' => $goodsId,'shop_id' => $shop_id,'source' => 2, 'group_id' => $group_id,'activity_goods_id'=>$activityGoodsId])->first();
        //每人每天限购判断
        if($group_id == 0){
            if(!self::is_stock($updateOrAddData['goods_num'],$goodsId)){
                return ['code' => ErrorCode::GOODS_UNDER_STOCK];
            }
            if(self::is_overflow((int)$updateOrAddData['goods_num'], $mid, $goodsId)){
                return ['code' => ErrorCode::CRD_GOODS_NUM_OVER, 'stock' => self::getGoodsLimit($goodsId)];
            }
        }
        if ($cartObjs) {
            if (isset($updateOrAddData['goods_num'])) {
                $goodsNum = $updateOrAddData['goods_num'];
                if ($updateOrAddData['goods_num'] == 0 || $goodsNum <= 0) {
                    if (CartModel::query()->where(['mid' => $mid, 'goods_id' => $goodsId, 'shop_id' => $shop_id, 'source' => 2, 'group_id' => $group_id, 'activity_goods_id' => $activityGoodsId])->delete()) {
                        return ['code' => ErrorCode::SUCCESS];
                    } else {
                        return ['code' => ErrorCode::CART_CLEAR_FAIL];
                    }
                }
                $updateOrAddData['goods_num'] = $goodsNum;
            }
            $updateOrAddData['update_at'] = $datetime;
            if (CartModel::query()->where(['mid' => $mid, 'goods_id' => $goodsId, 'shop_id' => $shop_id, 'source' => 2, 'group_id' => $group_id, 'activity_goods_id' => $activityGoodsId])->update($updateOrAddData)) {
                return ['code' => ErrorCode::SUCCESS];
            }
        }else {
            $updateOrAddData['mid'] = $mid;
            $updateOrAddData['shop_id'] = $shop_id;
            $updateOrAddData['goods_id'] = $goodsId;
            $updateOrAddData['source'] = 2;
            $updateOrAddData['create_at'] = $datetime;
            $updateOrAddData['group_id'] = $group_id;
            $updateOrAddData['activity_goods_id'] = $activityGoodsId;
            if ($this->cartService->cartInsert($updateOrAddData)){
                return ['code' => ErrorCode::SUCCESS];
            }
        }
    }

    /**
     * 批量加入次日达购物车
     * @param int $mid
     * @param int $shopId
     * @param array $goods_list
     * @param array|string[] $field
     * @return array|mixed
     */
    public function batchCart(int $mid, int $shopId,array $goods_list, array $field = ['*'])
    {
        $goodsIdsArr = array_column($goods_list, 'goods_id');
        $group_id = array_column($goods_list,'group_id');
        $goods_num = array_column($goods_list,'goods_num');
        $activity_goods_id = array_column($goods_list,'activity_goods_id');
        //判断商品是否删除
        if (!self::isEffectiveDelete($goodsIdsArr[0],$shopId)){
            return ['code' => ErrorCode::GOODS_DELETE];
        }
        //判断平台是否下架
        $data = self::isEffectivePlatform($goodsIdsArr[0]);
        if($data){
            return ['code' => ErrorCode::GOODS_PLATFORM_STATUS];
        }
        //判断店铺是否下架
        if (!self::isEffectiveByGoodsId($goodsIdsArr[0], $shopId)) {
            return ['code' => ErrorCode::GOODS_STATUS];
        }
        //判断秒杀活动是否有效
        if($group_id[0] == 4){
            //秒杀商品是否失效
            if(!self::shopSaleAct($activity_goods_id[0],$shopId)){
                return ['code' => ErrorCode::GOODS_INVALID];
            }
            //秒杀商品已下架
            if(!self::saleAct($activity_goods_id[0])){
                return ['code' => ErrorCode::SALE_GOODS_INVALID];
            }
            //活动商品库存不足
            if(!self::saleActStock($activity_goods_id[0],$goods_num[0])){
                return ['code' => ErrorCode::SALE_STOCK_INVALID];
            }
        }
        $cartListArr = CartModel::query()->select($field)->where(['mid' => $mid,'shop_id'=>$shopId,'group_id'=> $group_id[0],'activity_goods_id'=>$activity_goods_id[0],'source'=>2])->whereIn('goods_id', $goodsIdsArr)->get()->toArray();
        $cartListArrByMid = $updateOrAddData = [];
        if ($cartListArr) {
            $cartListArrByMid = array_column($cartListArr, null, 'goods_id');
        }
        $datetime = date('Y-m-d H:i:s');
        Db::beginTransaction();
        try {
            foreach ($goods_list as $arr) {
                $num = $arr['goods_num'];
                if (isset($cartListArrByMid[$arr['goods_id']]) || isset($cartListArrByMid[$arr['goods_id']]['group_id'])) {
                    $num = $cartListArrByMid[$arr['goods_id']]['goods_num'] + $num;
                    $tempArr = [
                        'goods_num' => $cartListArrByMid[$arr['goods_id']]['goods_num'] + $arr['goods_num'],
                    ];
                    if ($arr['is_selected']) {
                        $tempArr['is_selected'] = $arr['is_selected'];
                    }
                    $tempArr['update_at'] = $datetime;
                    //判断秒杀库存
                    if($group_id[0]==4){
                        if(!self::saleActStock($activity_goods_id[0],$num)){
                            Db::rollBack();
                            return ['code' => ErrorCode::SALE_STOCK_INVALID,'stock'=>'秒杀库存不足'];
                        }
                    }
                    CartModel::query()->where(['mid' => $mid, 'goods_id' => $arr['goods_id'],'shop_id'=>$shopId,'group_id' => $group_id[0],'activity_goods_id'=>$activity_goods_id[0],'source'=>2])->update($tempArr);
                    $this->cartService->saveCartLog('success',$tempArr,self::CART_SOURCE_1,'成功');
                } else {
                    $updateOrAddData[] = [
                        'mid' => $mid,
                        'shop_id' => $shopId,
                        'goods_id' => $arr['goods_id'],
                        'goods_num' => $arr['goods_num'],
                        'activity_goods_id' => $arr['activity_goods_id'],
                        'group_id' => $arr['group_id'] ?? 0,
                        'is_selected' => $arr['is_selected'],
                        'create_at' => $datetime,
                        'source' => 2
                    ];
                }
                //每人每天限购
                if($group_id[0] ==0 ||$activity_goods_id[0] == 0){
                    if (!self::is_stock($num,(int)$arr['goods_id'])){
                        Db::rollBack();
                        return ['code' => ErrorCode::GOODS_UNDER_STOCK];
                    }
                    if (self::is_overflow($num, $mid, (int)$arr['goods_id'])) {
                        Db::rollBack();
                        return ['code' => ErrorCode::CRD_GOODS_NUM_OVER, 'stock' => self::getGoodsLimit($arr['goods_id'])];
                    }
                }
            }
            if ($updateOrAddData) {
                $this->cartService->cartInsert($updateOrAddData);
                $this->cartService->saveCartLog('success',$updateOrAddData,self::CART_SOURCE_1,'成功');
            }
            Db::commit();
            return ['code' => ErrorCode::SUCCESS];
        } catch (\Throwable $ex) {
            Db::rollBack();
            $this->cartService->saveCartLog('error',[$mid,$shopId,$goods_list],self::CART_SOURCE_1,$ex->getMessage());
            return ['code' => ErrorCode::NOT_IN_FORCE];
        }
    }

    /**
     * 商品库存
     * @param int $num
     * @param int $goodsId
     * @return bool
     */
    public function is_stock(int $num,int $goodsId){
        $stock = GoodsListModel::query()->where(['goods_id'=>$goodsId])->value('number_stock');
        if($num>$stock){
            return false;
        }
        return true;
    }
    /**
     * 校验商品是否删除
     * @param int $goodsId
     * @param int $shop_id
     * @return bool
     */
    public static function isEffectiveDelete(int $goodsId,int $shop_id)
    {
        $goodsObj = GoodsModel::query()->where(['is_deleted' => 0,'id'=>$goodsId,'is_next_day'=>'1'])->whereRaw('!FIND_IN_SET(?,crd_shop_ids)', [$shop_id])->value('id');
        if ($goodsObj) {
            return true;
        }
        return false;
    }

    /**
     * 校验商品是否在店铺下架
     * @param int $goodsId
     * @param int $shop_id
     * @return bool
     */
    public static function isEffectiveByGoodsId(int $goodsId, int $shop_id)
    {
        $goodsObj = GoodsModel::query()->where(['status' => 1,'id'=>$goodsId,'is_next_day'=>'1'])->whereRaw('!FIND_IN_SET(?,crd_shop_ids)', [$shop_id])->value('id');
        if ($goodsObj) {
            return true;
        }
        return false;
    }

    /**
     * 校验商品是否在平台下架
     * @param int $goodsId
     * @return array
     */
    public static function isEffectivePlatform(int $goodsId)
    {
        $goodsObj = GoodsModel::query()->where(['status' => 0,'id'=>$goodsId])->value('id');
        return $goodsObj;
    }
    /**
     * 秒杀商品是否有效
     * @param int $activityId
     * @return bool
     */
    public static function saleAct(int $activityId)
    {
        $goodsObj = ActivityGoodsModel::query()->where(['id'=>$activityId,'group_id'=>4,'is_deleted'=>0])->first();
        if ($goodsObj) {
            return true;
        }
        return false;
    }
    /**
     * 秒杀商品在当前店铺是否有效
     * @param int $activityId
     * @param int $shopId
     * @return bool
     */
    public static function shopSaleAct(int $activityId,int $shopId)
    {
        $goodsObj = ActivityGoodsModel::query()->where(['id'=>$activityId,'group_id'=>4,'is_deleted'=>0])->value('activityID');
        $goodsRes = ActivityModel::query()->where(['activityID'=>$goodsObj,'is_deleted'=>1])->first();
        if(!$goodsRes){
            $goodsArr = ActivityModel::query()->where(['activityID'=>$goodsObj])->whereRaw('FIND_IN_SET(?,shop_ids)', [$shopId])->first();
            if ($goodsArr){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    /**
     * 检验秒杀商品库存
     * @param int $activityId
     * @param int $num
     * @return bool
     */
    public static function saleActStock(int $activityId,int $num)
    {
        $goodsObj = ActivityGoodsModel::query()->where(['id'=>$activityId,'group_id'=>4,'is_deleted'=>0])->where('stock','>=',$num)->first();
        if ($goodsObj) {
            return true;
        }
        return false;
    }

    /**
     * 是否超出限购
     *
     * @param int $want
     *
     *
     * @param int $mid
     * @param int $goodsId
     *
     * var_dump('商品限购：' . $limit);
    var_dump('要买：' . $want);
    var_dump('已买：' . $hasNumber);
    var_dump('剩余：限购 - (已买 + 要买) ：' . $surplus);
    var_dump('是否超出：' . ($surplus >= 0 ? '未超出' : '已超出'));
     *
     * @return bool
     */
    public static function is_overflow(int $want, int $mid, int $goodsId)
    {
        $limit = self::getGoodsLimit($goodsId);
        if ($want <= $limit) {
            $nos = OrderModel::query()->where(['mid' => $mid, 'is_pay' => 1])->whereDate('pay_at', date('Y-m-d'))->pluck('order_no');
            $hasNumber = $nos ? OrderGoodsModel::query()->whereIn('order_no', $nos->toArray())->where('goods_id', $goodsId)->sum('number') : 0;
            $surplus = $limit - ($want + $hasNumber);
            if ($surplus >= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取商品限购数量
     *
     * @param $goodsId
     * @return \Hyperf\Utils\HigherOrderTapProxy|int|mixed|void
     */
    public static function getGoodsLimit($goodsId)
    {
        $limit = GoodsListModel::query()->where('goods_id', $goodsId)->value('limite_num_per_day');
        return $limit ;
    }

    /**
     * 获取商品 (status =0 is_deleted=1  店铺下架)
     * $type=0有效 $type =1  无效
     * @param array $cartList
     * @param array $goodsId
     * @param int $shop_id
     * @param int $mid
     * @param int $type
     * @return array
     */
    public static function getCartGoodsAll(array $cartList, array $goodsId, int $shop_id, int $mid, int $type = 0)
    {
        //用户当前店铺购物车普通次日达商品
        $goods = CartModel::query()->where('group_id','<>',4)->where(['mid'=>$mid,'shop_id'=>$shop_id,'source' =>2])->whereIn('goods_id',$goodsId)->select('goods_id')->get()->toArray();
        //普通商品id集合
        $goodsIds = array_column($goods,'goods_id');
        //用户当前店铺购物车秒杀商品
        $cartSaleGoods = CartModel::query()->where(['group_id'=>4,'mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->whereIn('goods_id',$goodsId)->select('goods_id','activity_goods_id')->get()->toArray();
        //秒杀活动商品id集合
        $activityGoodsId = array_column($cartSaleGoods,'activity_goods_id');
        //活动商品id集合
        $sellingId = array_column($cartSaleGoods,'goods_id');
        $goodsIdGather = GoodsModel::query()
            ->whereIn('id', $goodsIds)
            ->where(['is_deleted' => 0, 'status' => 1, 'is_next_day'=>'1'])
            ->whereRaw('!FIND_IN_SET(?,crd_shop_ids)', [$shop_id])
            ->pluck('id')
            ->toArray();
        //普通次日达商品用户当天已购买量
        $crd_order_Num =OrderModel::query()->from('store_order as o')->join('store_order_goods as g', 'o.order_no', '=', 'g.order_no')
            ->whereIn('o.status', [1, 2, 3, 4, 6, 7, 8])
            ->where(['o.mid' => $mid,'o.order_source'=>1])
            ->where(['g.group_id' => 0])
            ->whereIn('g.goods_id',$goodsIdGather)
            ->whereBetween('o.pay_at', [date('Y-m-d'), date('Y-m-d 23:59:59')])
            ->groupBy(['g.goods_id'])
            ->selectRaw('sum(g.number - g.refund_number - g.shop_refund_number) as num,goods_id')
            ->get()->toArray();
        $orderNum = array_column($crd_order_Num,null,'goods_id');
        //每人每天商品限购数
        $quotaNum = StoreGoodsListModel::query()->whereIn('goods_id',$goodsIdGather)->select('goods_id','limite_num_per_day')->get()->toArray();
        $q_Num = array_column($quotaNum,null,'goods_id');
        //当前用户当天商品还能购买数量
        $midNumber = [];
        foreach ($quotaNum as $k => $v){
            if(isset($orderNum[$quotaNum[$k]['goods_id']])){
                $midNumber[$k]['number'] = $q_Num[$quotaNum[$k]['goods_id']]['limite_num_per_day'] - $orderNum[$quotaNum[$k]['goods_id']]['num'];
                $midNumber[$k]['goods_id'] = $q_Num[$quotaNum[$k]['goods_id']]['goods_id'];
            }else{
                $midNumber[$k]['number'] = $q_Num[$quotaNum[$k]['goods_id']]['limite_num_per_day'];
                $midNumber[$k]['goods_id'] = $q_Num[$quotaNum[$k]['goods_id']]['goods_id'];
            }
        }
        $midNumber = array_column($midNumber,'number','goods_id');
        //用户在当前店铺购物车商品数量
        $validCart = CartModel::query()->where(['group_id'=>0,'mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->whereIn('goods_id',$goodsIdGather)->select('goods_num','goods_id')->get()->toArray();
        $validCart = array_column($validCart,'goods_num','goods_id');
        //当前用户在当前店铺有效的商品id
        $valid_goods_id = [];
        foreach ($midNumber as $k => $v){
            if($midNumber[$k] >= $validCart[$k] ){
                $valid_goods_id[] = $k;
            }
        }
        if ($type == self::GOODS_STATUS_0) {
            //店铺有效商品id
            $goodsIdArr = $valid_goods_id;
        } else {
            $goodsIdArr = CartModel::query()->where(['group_id'=>0,'mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->whereNotIn('goods_id',$valid_goods_id)->pluck('goods_id')->toArray();
        }
        //秒杀有效商品集合
        $saleGoodsIdRes = ActivityModel::query()->from('store_activity_goods as g')
            ->join('store_activity as s','s.activityID','=', 'g.activityID')
            ->join('store_goods as t','t.id','=','g.goods_id')
            ->where(['g.is_deleted' => 0, 's.is_deleted' => 0,'g.group_id' => 4])
            ->where('g.stock','>',0)
            ->where('s.status','<>',0)
            ->where('s.end_date','>=',date('Y-m-d H:i:s'))
            ->where(['t.is_deleted' => 0, 't.status' => 1, 't.is_next_day'=>'1'])
            ->whereIn('g.id',$activityGoodsId)
            ->whereIn('t.id',$sellingId)
            ->whereRaw('!FIND_IN_SET(?,t.crd_shop_ids)', [$shop_id])
            ->whereRaw('FIND_IN_SET(?,s.shop_ids)', [$shop_id])
            ->pluck('g.id')
            ->toArray();
        if($type == self::GOODS_STATUS_0){
            $saleGoodsIdArr = $saleGoodsIdRes;
        }else{
            $saleGoodsIdArr = CartModel::query()->where(['group_id'=>4,'mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->whereNotIn('activity_goods_id',$saleGoodsIdRes)->pluck('activity_goods_id')->toArray();
        }
        $goodsField = ['goods_id','goods_title','goods_logo','goods_spec','costprice','price_selling','per_can_buy_num','group_id','id'];
        //秒杀商品信息
        $_saleGoodsIdArr = ActivityGoodsModel::query()->where('group_id','=',4)->whereIn('id',$saleGoodsIdArr)->select($goodsField)->get()->toArray();
        //用户秒杀商品购买量
        $order_Num =OrderModel::query()->from('store_order as o')->join('store_order_goods as g', 'o.order_no', '=', 'g.order_no')
            ->whereIn('o.status', [1, 2, 3, 4, 6, 7, 8])
            ->where(['o.mid' => $mid,'o.order_source'=>1])
            ->whereIn('g.activity_goods_id',$saleGoodsIdArr)
            ->groupBy(['g.activity_goods_id'])
            ->selectRaw('sum(g.number - g.refund_number - g.shop_refund_number) as num,activity_goods_id')
            ->get()->toArray();
        $orderGoods = array_column($order_Num,'num','activity_goods_id');
        foreach ($_saleGoodsIdArr as $k => $v){
            if(isset($orderGoods[$_saleGoodsIdArr[$k]['id']])){
                $_saleGoodsIdArr[$k]['number'] = $orderGoods[$_saleGoodsIdArr[$k]['id']];
            }else{
                $_saleGoodsIdArr[$k]['number'] = 0;
            }
        }
        $saleId = array_column($_saleGoodsIdArr,'goods_id');
        $saleListArr = array_column($_saleGoodsIdArr,null,'goods_id');
        $saleCart = CartModel::query()->whereIn('goods_id',$saleId)->where(['group_id'=>4,'mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->select('id','goods_id')->get()->toArray();
        $salegoodsId = array_column($saleCart,'id','id');
        $goodsCart = CartModel::query()->whereIn('goods_id',$goodsIdArr)->where(['group_id'=>0,'mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->select('id','goods_id')->get()->toArray();
        $crdGoodsId = array_column($goodsCart,'id','id');
        $_goodsListArr = GoodsService::getCrdGoodsInfoData($goodsIdArr);
        $goodsListArr = array_column($_goodsListArr, null, 'goods_id');
        $goodsSale = CartModel::query()->where(['mid'=>$mid,'shop_id'=>$shop_id,'source'=>2])->whereIn('activity_goods_id',$saleGoodsIdArr)->pluck('goods_id')->toArray();
        $_salegoodsListArr = GoodsService::getCrdGoodsInfoData($goodsSale);
        $salegoodsListArr = array_column($_salegoodsListArr, null, 'goods_id');
        $goodsArr = array_merge($salegoodsId,$crdGoodsId);
        $goodsIdRes = array_flip($goodsArr);
        foreach ($cartList as $k => &$v) {
            if (!isset($goodsIdRes[$v['id']])) {
                unset($cartList[$k]);
                continue;
            }
            if (isset($salegoodsId[$v['id']])  ) {
                $v['title'] = $saleListArr[$v['goods_id']]['goods_title'];
                $v['logo'] = $saleListArr[$v['goods_id']]['goods_logo'];
                $v['goods_spec'] = $saleListArr[$v['goods_id']]['goods_spec'];
                $v['goods_pick_time'] = $salegoodsListArr[$v['goods_id']]['goods_pick_time'];
                $v['activity_goods_id'] = $saleListArr[$v['goods_id']]['id'];
                $v['sale_price_selling'] = bcmul($saleListArr[$v['goods_id']]['price_selling'], '100');
                $v['sale_price_market'] = bcmul($saleListArr[$v['goods_id']]['costprice'], '100');
                $v['per_can_buy_num'] = $saleListArr[$v['goods_id']]['per_can_buy_num'];
                $v['order_number'] = $saleListArr[$v['goods_id']]['number'];
                $v['sale_num'] = $v['per_can_buy_num'] - $v['order_number'];
            } else {
                $v['title'] = $goodsListArr[$v['goods_id']]['title'];
                $v['logo'] = $goodsListArr[$v['goods_id']]['logo'];
                $v['goods_spec'] = $goodsListArr[$v['goods_id']]['goods_spec'];
                $v['goods_pick_time'] = $goodsListArr[$v['goods_id']]['goods_pick_time'];
                $v['activity_goods_id'] = 0;
                $v['price_selling'] = bcmul($goodsListArr[$v['goods_id']]['price_selling'], '100');
                $v['price_market'] = bcmul($goodsListArr[$v['goods_id']]['price_market'], '100');
            }
        }
        return compact("cartList");
    }

    /**
     * 删除无效商品
     * @param int $shop_id
     * @param int $mid
     * @return int|mixed
     */
    public static function deleteNoCart(int $shop_id, int $mid)
    {
        $field = [
            'id',
            'mid',
            'goods_id',
            'goods_num',
            'is_selected',
            'activity_goods_id',
            'group_id',
            'source'
        ];
        $cartList = CartService::cartList(['mid' => $mid,'shop_id'=>$shop_id,'source'=>2], $field);
        $goodsId = array_column($cartList, 'goods_id');
        $arr = self::getCartGoodsAll($cartList, $goodsId, $shop_id, $mid,self::GOODS_STATUS_1);
        $goodsRes = array_column($arr['cartList'],'goods_id');
        if (!$goodsRes) {
            return true;
        }
        $res = CartModel::query()->where(['mid' => $mid,'shop_id'=>$shop_id,'source'=>2])->whereIn('goods_id', $goodsRes)->delete();
        return $res;
    }
}
