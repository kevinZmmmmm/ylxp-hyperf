<?php


namespace App\Resource\Service\Shop;


use App\Common\Service\BaseService;
use App\Resource\Repository\RepositoryFactory;

class ShopBaseService extends BaseService
{

    const SHOPFIELD= ['shop_id', 'shop_name', 'address', 'shop_tel', 'shop_desc', 'end_time', 'start_time','longitude','latitude'];

    public function __construct(RepositoryFactory $repoFactory)
    {
        parent::__construct();
        $this->shop = $repoFactory->getRepository("shop");
    }

    /**
     * 业务场景 获取限时抢购活动单条记录公共服务
     * @param array $needExceptKey
     * @param array $extraCondition
     * @param array $orderBy
     * @return array|mixed
     * @author ran
     * @date 2021-03-23 17:23
     * mailbox 466180170@qq.com
     */
    public function getfirst(array $needExceptKey=[],array $extraCondition=[],array $orderBy=[]):array
    {
        $where =['store_activity_goods.activityType'=>self::ACTIVITY_TYPE];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereRaw'=>$extraCondition['whereRaw']??[],'whereOp'=>$extraCondition['whereOp']??[]];
        $activityArr =$this->activitygoods->findSonMoreUnionFirst(self::ACTIVITYGOODSFIELD,self::GOODSLISTFIELD,self::GOODSFIELD,$condition,$orderBy);
        return $this->arr->exceptArrKey($activityArr,$needExceptKey);
    }

}