<?php


namespace App\Resource\Service\Cart;


use App\Common\Constants\Stakeholder;
use App\Common\Service\BaseService;
use App\Resource\Repository\RepositoryFactory;

class CartBaseService extends BaseService
{
    /**
     * 字段集合
     */
    const CARTFIELD=['id as cart_id','goods_num as cart_goods_number','group_id','activity_goods_id'];
    const GOODSFIELD=['id','title','status','ratio','scant_id','logo','wx_crm_code', 'kz_goods_id', 'kz_type_id', 'cate_id', 'crd_cate_id', 'lsy_goods_name', 'lsy_unit_name', 'lsy_class_name'];
    const GOODSLISTFIELD=['goods_spec','price_selling','price_market','number_stock','goods_pick_time','commission','limite_num_per_day'];
    /**
     * 公共字段集合
     */
    const ACTIVITYFIELD=['activityID','shop_ids as activity_shop_ids','status as activity_status','begin_date','end_date'];
    const ACTIVITYGOODSFIELD=['goods_id as id','goods_title as title', 'goods_logo as logo','goods_spec','costprice as activity_price_market','price_selling as activity_price_selling','stock as number_stock','per_can_buy_num','commission'];
    /**
     * redis
     */
    const CART_TIMELY_REDIS_KEY = 'cart:timely:goods';
    const CART_ATTRIBUTE_TIMELY_REDIS_KEY = 'cart:timely:attribute';
    const CART_SELECT_TIMELY_REDIS_KEY = 'cart:timely:select';
    const CART_OVERNIGHT_REDIS_KEY = 'cart:overnight';
    /**
     * 商品来源   1.及时达  2.次日达
     */
    const TIMELY_SOURCE = 1;
    const OVER_NIGHT_SOURCE = 2;

    protected $cart;
    /**
     * CartBaseService constructor.
     * @param RepositoryFactory $repoFactory
     */
    public function __construct(RepositoryFactory $repoFactory)
    {
        parent::__construct();
        $this->cart = $repoFactory->getRepository("cart");
    }
    /**
     * 次日达购物车商品信息服务
     * @param array $needExceptKey
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-02-24 13:46
     * mailbox 466180170@qq.com
     */
    protected  function getResourceCommonOverNightArriveCartGoodsList(array $needExceptKey=[],int $mid,int $shop_id,int $page=1,int $limit=20,array $extraCondition=[],bool $isPage=true,array $orderBy=[]): array
    {
        $where =['hf_cart.source'=>self::OVER_NIGHT_SOURCE,'hf_cart.shop_id'=>$shop_id,'hf_cart.mid'=>$mid];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereRaw'=>$extraCondition['whereRaw']??[],'whereNotIn'=>$extraCondition['whereNotIn']??[],'whereIn'=>$extraCondition['whereIn']??[]];
        $goodsArray= $this->cart->selectMoreUnionList(self::CARTFIELD,self::GOODSFIELD,self::GOODSLISTFIELD,$condition,$page,$limit,$orderBy,$isPage);
        $goodsArray['list']=$this->arr->exceptMoreArrKey($goodsArray['list'],$needExceptKey);
        return $goodsArray['list'];
    }

    /**
     * 次日达购物车活动商品信息服务
     * @param array $needExceptKey
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-02-24 13:46
     * mailbox 466180170@qq.com
     */
    protected  function getResourceCommonOverNightArriveCartActivityGoodsList(array $needExceptKey=[],int $mid,int $shop_id,int $page=1,int $limit=20,array $extraCondition=[],bool $isPage=true,array $orderBy=[]): array
    {
        $whereOp =[['hf_cart.group_id','>',Stakeholder::GROUP_ID_O]];
        $where =['hf_cart.is_selected' =>1,'hf_cart.source'=>self::OVER_NIGHT_SOURCE,'hf_cart.shop_id'=>$shop_id,'hf_cart.mid'=>$mid];
        $condition=['where'=>array_merge($where,$extraCondition['where']??[]),'whereRaw'=>$extraCondition['whereRaw']??[],'whereNotIn'=>$extraCondition['whereNotIn']??[],'whereIn'=>$extraCondition['whereIn']??[],'whereOp'=>array_merge($whereOp,$extraCondition['whereOp']??[])];
        $goodsArray= $this->cart->selectMoreOtherUnionList(self::CARTFIELD,self::ACTIVITYGOODSFIELD,self::ACTIVITYFIELD,self::GOODSFIELD,$condition,$page,$limit,$orderBy,$isPage);
        $goodsArray['list']=$this->arr->exceptMoreArrKey($goodsArray['list'],$needExceptKey);
        return $goodsArray['list'];
    }

}