<?php


namespace App\Resource\Service\Cart;


use App\Common\Constants\ErrorCode;
use App\Common\Constants\Stakeholder;
use Hyperf\DbConnection\Db;

/**
 * Class CartMiniService
 * @package App\Resource\Service\Cart
 */
class CartMiniService extends CartBaseService implements CartServiceInterface
{
    /**
     * 业务场景（及时达商城更新购物车服务） // 支持减法
     * @param int $shop_id
     * @param int $mid
     * @param string $id
     * @param array $goods
     * @param int $count
     * @return array
     * @author ran
     * @date 2021-03-17 14:43
     * mailbox 466180170@qq.com
     */
    public function updateSingleResourceTimelyCart(int $shop_id, int $mid, string $id, array $goods, int $count): array
    {
            $key1 = self::CART_TIMELY_REDIS_KEY.":{$shop_id}:{$mid}";
            $key2 = self::CART_ATTRIBUTE_TIMELY_REDIS_KEY.":{$shop_id}:{$mid}";
            $key3 = self::CART_SELECT_TIMELY_REDIS_KEY.":{$shop_id}:{$mid}";
            try {
                Db::transaction(function () use ($shop_id, $key1, $key2, $key3,$id,$goods,$count) {
                    //购物车存储属性
                    $this->resourceRedis->hSet($key2,$id, json_encode($goods));
                    //购物车增减
                    $this->resourceRedis->hIncrBy($key1,$id, $count);
                    //购物车选中
                    $this->resourceRedis->sadd($key3 , $id);
                });
            }catch (\Exception $e){
                return $this->error(ErrorCode::SYSTEM_INVALID,$e->getMessage());
            }
            return $this->success();
            //$commList = $this->resourceRedis->hMGet($key, $ret);
            /* $arr =$this->resourceRedis->hgetall($key);
            $s=$this->resourceRedis->hkeys($key);
            var_dump($s);
            var_dump($arr);
            foreach($arr as $key=>$v){
                var_dump($v);
                $this->resourceRedis->sadd($key2 , $id);
            }*/

            //$a =$this->resourceRedis->hIncrBy($key,$id, $count);
            /* $this->redis->sadd($key, $id);
            $arr =$this->redis->sMembers($key);*/
    }

    /**
     * 获取购物车商品信息服务
     * @param int $shop_id
     * @param int $mid
     * @param array $exceptKey
     * @return array
     * @author ran
     * @date 2021-04-05 11:17
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveCartGoodsList(int $shop_id,int $mid,$exceptKey=[]){
          $needExceptKey=[];
          $where =['hf_cart.is_selected' =>1,'hf_cart.group_id'=>Stakeholder::GROUP_ID_O];
          $extraCondition=['where'=>$where];
          return $this->getResourceCommonOverNightArriveCartGoodsList(array_merge($needExceptKey,$exceptKey),$mid,$shop_id,1,20,$extraCondition,false);
    }

    /**
     * 获取购物车活动信息服务
     * @param int $shop_id
     * @param int $mid
     * @param array $exceptKey
     * @return array
     * @author ran
     * @date 2021-04-05 11:17
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveCartActivityGoodsList(int $shop_id,int $mid,$exceptKey=[]){
        $needExceptKey=[];
        $whereRaw ='FIND_IN_SET(' . $shop_id . ',store_activity.shop_ids)';
        $where =['hf_cart.is_selected' =>1];
        $extraCondition=['where'=>$where,'whereRaw'=>$whereRaw];
        return $this->getResourceCommonOverNightArriveCartActivityGoodsList(array_merge($needExceptKey,$exceptKey),$mid,$shop_id,1,20,$extraCondition,false);
    }

}