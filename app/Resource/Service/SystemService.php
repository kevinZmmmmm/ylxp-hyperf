<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Model\SystemOperationLogModel;
use App\Common\Service\BaseService;
use App\Resource\Model\SystemModel;
use Hyperf\DbConnection\Db;

class SystemService extends BaseService
{
    /**
     * 系统配置参数
     * @return array
     */
    public function systemConfig()
    {
        $resArr = SystemModel::query()->select(['name','value'])->get()->toArray();
        $data = array_column($resArr, 'value','name');
        $res =[
            'android_version_link' => $data['android_version_link'],
            'android_version' => $data['android_version'],
            'ios_version' => $data['ios_version'],
            'version_content' => $data['version_content'],
            'is_version' => $data['is_version'],
            'version_name' => $data['version_name'],
            'ios_version_link' => $data['ios_version_link'],
            'version_update' => $data['version_update']
        ];
        return $res;
    }

    /**
     * 系统配置更新
     * @param $data
     * @return array
     */
    public function update($data)
    {
        $names = '';
        foreach($data as $k => $v){
            $names .= "'".$k."',";
        }
        $names = rtrim($names,',');
        $sql = "UPDATE system_config SET value = CASE name ";
        foreach ($data as $name => $value) {
            $sql .= sprintf("WHEN %s THEN %s ", '"'.$name.'"', '"'.$value.'"');
        }
        $sql .= "END WHERE name IN ($names)";
        $res = Db::update($sql);
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => [], 'info' => ['remarks' => json_encode($data)]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * @param array $params
     * @return array
     *
     * ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
     * ★★★★★★★★★★  所有系统配置编辑都走这一个方法，无需再重复写  ★★★★★★★★★★★★★★★
     * ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
     */
    public function CrdShopConfig($params = [])
    {
        $names = '';
        foreach($params as $k => $v){
            $names .= "'".$k."',";
        }
        $names = rtrim($names,',');
        $sql = "UPDATE system_config SET value = CASE name ";
        foreach ($params as $name => $value) {
            $sql .= sprintf("WHEN %s THEN %s ", '"'.$name.'"', '"'.$value.'"');
        }
        $sql .= "END WHERE name IN ($names)";
        $res = Db::update($sql);
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => [], 'info' => ['remarks' => $value]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * @param array $params
     * @return array
     */
    public function CrdShopConfigInfo()
    {
        // 后续其它配置字段可在这里配置
        $field = ['cut_off_time'];
        $res = SystemModel::query()->whereIn('name', $field)->select(['name','value'])->get();
        return ['code' => ErrorCode::SUCCESS, 'data' => $res];
    }

    /**
     * @return array
     */
    public function ShareConfig()
    {
        $field = ['share_title','share_img'];
        $res = SystemModel::query()->whereIn('name', $field)->select(['name','value'])->get();
        return ['code' => ErrorCode::SUCCESS, 'data' => $res];
    }

    /**
     * 获取系统配置参数
     * @param array $field  要获取的参数 name
     * @return array
     *
     * ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
     * ★★★★★★★★★★  所有系统配置获取都走这一个方法，无需再重复写  ★★★★★★★★★★★★★★★
     * ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
     */
    public function getSystemConfig(array $field): array
    {
//        $res = SystemModel::query()->whereIn('name', $field)->select(['name','value'])->get();
        $res = SystemModel::query()->whereIn('name', $field)->pluck('value', 'name');
        return ['code' => ErrorCode::SUCCESS, 'data' => $res];
    }

    /**
     * @param array $params
     * @return array
     */
    public function UpdateShareConfig(array $params)
    {
        $names = '';
        foreach($params as $k => $v){
            $names .= "'".$k."',";
        }
        $names = rtrim($names,',');
        $sql = "UPDATE system_config SET value = CASE name ";
        foreach ($params as $name => $value) {
            $sql .= sprintf("WHEN %s THEN %s ", '"'.$name.'"', '"'.$value.'"');
        }
        $sql .= "END WHERE name IN ($names)";
        $res = Db::update($sql);
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => [], 'info' => ['remarks' => json_encode($params)]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }


    // 获取服务条款，隐私政策，关于我们
    public function getServiceAgreement(array $params){
        return ['content' => SystemModel::query()->where(['name' => $params['type']])->value('value')];
    }

    /**
     * 获取单个配置
     * @param array $where
     * @return \Hyperf\Utils\HigherOrderTapProxy|mixed|void|null
     * @author lulongfei
     */
    public function getSystemInfo(array $where)
    {
      return  SystemModel::query()->where($where)->value('value');
    }

    /**
     * 系统后台操作日志列表
     * @param array $where
     * @param int $perPage
     * @param array|string[] $field
     * @return array
     */
    public function getSystemOperationLog(array $where, int $perPage = 15, array $field = ['*'])
    {
        $query = SystemOperationLogModel::query();
        if (!empty($where['time']) && strlen(trim($where['time'])) > 24) {
            $where['time'] = [
                trim(substr($where['time'], 0, 19)),
                trim(substr($where['time'], -19))
            ];
        }
        !empty($where['time']) && $query->whereBetween('created_at', $where['time']);
        $list = $query->latest()->paginate($perPage, $field);
        if ($list) {
            return ['code' => ErrorCode::SUCCESS, 'data' => $list];
        }
        return ['code' => ErrorCode::SERVER_ERROR];
    }

}
