<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Resource\Model\StoreShopDeliverModel;
use Hyperf\DbConnection\Db;

class StoreShopDeliverService extends BaseService
{

    /**
     * 新增ID
     * @param array $fields
     * @return int|string
     */
    public function shopInsert(array $fields)
    {
        return StoreShopDeliverModel::query()->insert($fields);
    }

    /**
     * 修改
     * @param int $shop_id
     * @param array $updateArr
     * @return int|string
     */
    public function shopUpdate(int $shop_id,array $updateArr)
    {
        return StoreShopDeliverModel::query()->where(['shop_id'=>$shop_id])->update($updateArr);
    }

    /**
     * 获取店铺配送信息
     * @param int $shop_id
     * @param array $field
     * @return int|string|array
     */
    public function getDeliverInfoByShopId(int $shop_id,array $field)
    {
        return StoreShopDeliverModel::query()->where(['shop_id'=>$shop_id])->select($field)->first()->toArray();
    }
}
