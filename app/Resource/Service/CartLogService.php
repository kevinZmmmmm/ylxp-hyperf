<?php

declare(strict_types=1);


namespace App\Resource\Service;


use App\Activity\Model\ActivityGoodsModel;
use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Common\Service\LogQueueService;
use App\Order\Model\OrderGoodsModel;
use App\Resource\Model\CartLogModel;
use App\Resource\Model\CartModel;
use App\Resource\Model\GoodsModel;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;


class CartLogService extends BaseService
{
    /**
     * 回调日志入库
     * @param string $event
     * @param array $params
     * @param array $response
     * @param string|null $type
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model
     */
    public function CartLog(string $event, array $params , array $response, string $type = null){
        $data = [
            'event' => $event,
            'params' => json_encode($params),
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE),
        ];
        if ($type == 'pay'){
            return CartLogModel::query()->create($data);
        }
        return NotifyLog::query()->create($data);
    }

}


