<?php

declare(strict_types=1);

namespace App\Resource\Service;
use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use Hyperf\DbConnection\Db;
use Swoole\FastCGI\Record\Data;
use Hyperf\Cache\Cache;
use Exception;
use Hyperf\Utils\HigherOrderTapProxy;
use App\Resource\Model\SystemModel;
class StoreService extends BaseService
{
    /**
     * 系统配置参数
     * @return array
     */
    public function storeConfig()
    {
        $resArr = SystemModel::query()->select(['name','value'])->get()->toArray();
        $data = array_column($resArr, 'value','name');
        $res =[
            'allow_order_type' => $data['allow_order_type'],
            'allow_order_time' => $data['allow_order_time'],
            'cut_order_time' => $data['cut_order_time'],
            'shop_status' => $data['shop_status'],
            'service_phone' => $data['service_phone'],
            'member_status' => $data['member_status'],
            'delivery_date_config' => $data['delivery_date_config'],
            'pick_date_config' => $data['pick_date_config'],
            'match_date_config' => $data['match_date_config'],
            'template_config' => $data['template_config'],
            'service_agreement' => $data['service_agreement'],
            'privacy_policy' => $data['privacy_policy'],
            'about_us' => $data['about_us']
        ];
        return $res;
    }

    /**
     * 系统配置更新
     * @param $data
     * @return array
     */
    public function update($data)
    {
        foreach ($data as $key =>$v) {
            SystemModel::query()->where(['name'=>$key])->update(['value'=>$v]);
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => [], 'info' => ['remarks' => json_encode($data)]];
    }

    /**
     * 安卓版本信息
     * @return array|int|string
     */
    public function android_version()
    {
        $resArr = SystemModel::query()->select(['name','value'])->get()->toArray();
        $data = array_column($resArr, 'value','name');
        $res =[
            'version_name' => $data['version_name'],
            'version' => $data['android_version'],
            'link' => $data['android_version_link'],
            'content' => $data['version_content'],
            'is_version' => $data['is_version'],
            'version_update' => $data['version_update'],

        ];
        return  $res;
    }

    /**
     * IOS版本信息
     * @return array|int|string
     */
    public function ios_version()
    {
        $resArr = SystemModel::query()->select(['name','value'])->get()->toArray();
        $data = array_column($resArr, 'value','name');
        $res =[
            'version_name' => $data['version_name'],
            'version' => $data['ios_version'],
            'link' => $data['ios_version_link'],
            'content' => $data['version_content'],
            'is_version' => $data['is_version'],
            'version_update' => $data['version_update'],

        ];
        return  $res;
    }
}
