<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Resource\Model\CategoryModel;
use App\Resource\Model\CrdCategoryModel;
use App\Resource\Model\JumpTypeModel;
use App\Third\Model\ShopLsyGoodsInfoModel;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class JumpService extends BaseService
{
    /**
     * @Inject
     * @var JumpTypeModel
     */
    protected $jumpTypeModel;

    /**
     * @param array $where
     * @param int $perPage
     * @param array|string[] $field
     *
     * @return array
     */
    public function getList(array $where, ?int $perPage = null, array $field = ['*'])
    {
        $list = jumpTypeModel::query()
            ->when($where['title'] ?? 0, function ($query, $title) {
                return $query->whereRaw('INSTR(title, ?) > 0', [$title]);
            })
            ->when($perPage ?? 0, function ($query, $perPage) use ($field) {
                return $query->paginate((int)$perPage, $field);
            }, function ($query) {
                return $query->get();
            });

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function getInfoById(int $id)
    {
        $res = jumpTypeModel::query()->where('id', $id)->first();
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => $res];
        }
        return ['code' => ErrorCode::NOT_EXIST];
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function add(array $params)
    {
        $res = jumpTypeModel::query()->create([
            'title' => $params['title'],
            'is_two_level' => $params['is_two_level'],
            'url' => $params['url'] ?? '',
            'msg' => $params['msg'] ?? '',
        ]);
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => $res, 'info' => ['target_id' => $res -> id]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * @param array $params
     * @return array
     */
    public function update(array $params)
    {
        $res = jumpTypeModel::where(['id' => $params['id']])->update([
            'title' => $params['title'],
            'is_two_level' => $params['is_two_level'],
            'url' => $params['url'] ?? '',
            'msg' => $params['msg'] ?? '',
        ]);
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => $res, 'info' => ['target_id' => $params['id']]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }

    /**
     * @param int $id
     * @return array
     */
    public function delete(int $id)
    {
        $res = jumpTypeModel::where('id', $id)->delete();
        if ($res) {
            return ['code' => ErrorCode::SUCCESS, 'data' => $res, 'info' => ['target_id' => $id]];
        }
        return ['code' => ErrorCode::NOT_IN_FORCE];
    }
}
