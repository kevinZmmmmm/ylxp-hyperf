<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Resource\Model\RegionModel;

class RegionService extends BaseService
{
    /**
     * @param int $pid
     * @param int $type
     * @return array
     */
    public function getRegionByParentId(int $pid, int $type)
    {
        $RegionData = [];
        $cityArr = RegionModel::where(['parent_id' => $pid, 'region_type' => $type])->get();
        foreach ($cityArr as $val) {
            unset($val['parent_id'], $val['region_type']);
            $RegionData[] = $val;
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => $RegionData];
    }
}
