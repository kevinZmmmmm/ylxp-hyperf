<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use App\Resource\Model\StoreMemberModel;
use Hyperf\DbConnection\Db;

class StoreMemberService extends BaseService
{
    /**
     * 通过店铺id获取店铺当天的会员新增人数
     * @param int $type
     * @return array|string
     */
    public function storesIncome(int $type)
    {
        return StoreMemberModel::query()
            ->where(['status'=>$type])
            ->selectRaw('count(openid) as num')
            ->whereDate('register_date', date('Y-m-d', time()))
            ->first()
            ->toArray();

    }
}
