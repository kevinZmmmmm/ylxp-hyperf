<?php

declare(strict_types=1);

namespace App\Resource\Service\Home;


use App\Activity\Service\Group\GroupBaseService;
use App\Activity\Service\Quota\QuotaBaseService;
use App\Activity\Service\Quota\QuotaMiniService;
use App\Resource\Service\Navigation\NavigationBaseService;
use Hyperf\Di\Annotation\Inject;

class HomeMiniService extends HomeBaseService implements HomeServiceInterface
{

    /**
     * @Inject()
     * @var NavigationBaseService
     */
    protected NavigationBaseService $navigationBaseService;

    /**
     * @Inject()
     * @var QuotaMiniService
     */
    protected QuotaMiniService $quotaMiniService;

    /**
     * @Inject()
     * @var GroupBaseService
     */
    protected GroupBaseService $groupBaseService;
    /**
     * 业务场景（及时达商城首页集成接口（弹框广告、banner、公告、分类、优惠劵、拼团、限购、会员））
     * @param int $shop_id
     * @return mixed
     * @author ran
     * @date 2021-03-20 11:29
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveHome(int $shop_id){
        // 导航类
        $result = $this->navigationBaseService->getNavList($shop_id);
        // 限时抢购
        $result['sale_limit'] = $this->quotaMiniService->getActivityHomeQuotaGoodsList($shop_id);
        // 小跑拼团
        $result['group_list'] = $this->groupBaseService->getActivityGroupGoodsList($shop_id);
        // 会员专区
        //$result['member_area'] = $this->goodsService->getShopGoodsList($shop_id, ['group_id' => 2], 5)['list'];
        return $result;

    }

}