<?php
declare(strict_types=1);

namespace App\Resource\Service\Cook;


use App\Common\Service\BaseService;
use App\Resource\Repository\RepositoryFactory;

abstract class CookBaseService extends BaseService
{
    const COOKBOOKFIELD= ['id','title','image','main_materials','sort'];

    const STATUS= 1;

    const LIMIT= 3;

    const COOK_LOVE_GOODS_REDIS_KEY = 'Cook:cookLoveGoodsInfo:';

    const COOK_LOVE_ORDER_GOODS_REDIS_KEY = 'Cook:cookLoveOrderGoodsList:';

    const COOK_LOVE_EXPIRE = 5; //过期时间一天

    protected $cookbook;

    public function __construct(RepositoryFactory $repoFactory)
    {
        parent::__construct();
        $this->cookbook = $repoFactory->getRepository("cookbook");
    }

    /**
     * 匹配推荐需要推荐菜品ids服务 （待优化公共调用） 业务场景 （匹配排序前三菜谱中不重复的菜品主键集合）
     * @param array $cookCollectArr 菜谱数组集合
     * @param array $needOrderGoodsIds
     * @return array
     * @author zhangzhiyuan
     */
    protected function getResourceCommonOrderGoodsIds(array $cookCollectArr, array $needOrderGoodsIds): array
    {
        $loveCookGoodIds=[];
        $cookCollectArr=array_values(array_column($cookCollectArr, NULL, 'id'));
        $cookCollectArr=array_slice($this->arr->arraySort($cookCollectArr,'sort'),0,self::LIMIT);
        foreach ($cookCollectArr as $value) {
            $loveCookGoodIds = array_merge($loveCookGoodIds, array_values(array_unique(explode(',', $value['main_materials']))));
        }
        return  array_diff(array_unique($loveCookGoodIds), $needOrderGoodsIds);
    }

}