<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Activity\Model\ActivityModel;
use App\Common\Constants\ErrorCode;
use App\Common\Constants\Stakeholder;
use App\Common\Service\BaseService;
use App\Order\Model\KzRefundLogModel;
use App\Order\Model\OrderGoodsModel;
use App\Order\Model\OrderModel;
use App\Order\Model\WxRefundLogModel;
use App\Order\Service\Order\OrderBusinessService;
use App\Order\Service\Order\OrderMiniService;
use App\Resource\Event\UpdateShop;
use App\Resource\Model\CrdCategoryModel;
use App\Resource\Model\CrdNavigationShopModel;
use App\Resource\Model\GoodsModel;
use App\Resource\Model\NavigationShopModel;
use App\Resource\Model\ShopDeliverModel;
use App\Resource\Model\ShopGroupModel;
use App\Resource\Model\ShopModel;
use App\Resource\Model\TeamLeaderCommissionRecord;
use App\Resource\Model\TeamLeaderModel;
use App\User\Model\MemberModel;
use App\User\Model\UserModel;
use App\User\Model\UserShopModel;
use App\User\Service\MemberService;
use Carbon\Carbon;
use Exception;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\HigherOrderTapProxy;
use Psr\EventDispatcher\EventDispatcherInterface;

class ShopService extends BaseService
{
    /**
     * @Inject
     * @var NavigationService
     */
    private $navigationService;

    /**
     * @Inject
     * @var GoodsService
     */
    private $goodsService;
    /**
     * @Inject()
     * @var MemberService
     */
    private $memberService;
    /**
     * @Inject()
     * @var OrderBusinessService
     */
    private $orderBusinessService;

    /**
     * 距离计算url
     */
    const WALKING_URL = 'ws/distance/v1/?mode=walking';
    /**
     * 请求密钥
     */
    protected $baseUrl;
    /**
     * 请求密钥
     */
    protected $Key;
    protected $container;

    public function __construct()
    {
        parent::__construct();
        $this->baseUrl = 'https://apis.map.qq.com/';
        $this->Key = 'LTGBZ-3VWCR-C45WP-W5IH7-M5IC3-T3BL7';
        $container = ApplicationContext::getContainer();
        $this->container = $container;
    }

    /**
     * @param array $where
     * @param int $perPage
     * @param array|string[] $field
     *
     * @return array
     */
    public function getList(array $where, int $perPage = 5000, array $field = ['*'])
    {
        $query = ShopModel::query();
        !empty($where['shop_name']) && $query->whereRaw('INSTR(shop_name, ?) > 0', [$where['shop_name']]);
        $query->where('is_deleted', '=', 0);
        // 可用于搜索条件的下拉框，效率更高
        if (isset($where['isTaxonomy']) && $where['isTaxonomy'] == 1) {
            $field = ['shop_id', 'shop_name', 'shop_group_id'];
            $list = $query->paginate($perPage, $field);
            return ['code' => ErrorCode::SUCCESS, 'data' => $list];
        }

        // 只获取营业中的店铺
        if (isset($where['mini']) && $where['mini'] == 1) {
            $query->where('status', '=', 1);
//            $list = $query->select($field)->get();
            $list = $query->paginate($perPage, $field);
//            return $list;
            return ['code' => ErrorCode::SUCCESS, 'data' => $list];
        }

        if (!empty($where['shop_group'])) {
            $shop_group_id = ShopGroupModel::query()->where(['shop_group_name' => $where['shop_group']])->value('id');
            $query->where('shop_group_id', '=', $shop_group_id);
        }
        if (!empty($where['shop_account'])) {
            $uid = UserModel::query()->where(['username' => $where['shop_account'], 'role' => 3])->value('id');
            $shop_id = UserShopModel::query()->where(['uid' => $uid, 'deleted_at' => null])->value('shop_id');
            $query->where('shop_id', '=', $shop_id);
        }
        if (isset($where['status']) && in_array($where['status'],['0','1','2'])) {
            $query->where('status', '=', $where['status']);
        }

        $list = $query->latest('sort')->paginate($perPage, $field);
        // 获取店铺账号和店群名称
        $dataArr = $list->toArray()['data'];
        $shopIds = array_column($dataArr, 'shop_id');
        $uids = isset($uid) ? [$shop_id => $uid] : UserShopModel::query()
            ->whereIn('shop_id', $shopIds)
            ->where(['deleted_at' => null])
            ->pluck('uid', 'shop_id')->toArray();
        $shopAccounts = UserModel::query()->whereIn('id', $uids)->where(['deleted_at' => null])->pluck('username', 'id')->toArray();
        $shopGroupIds = array_column($dataArr, 'shop_group_id', 'shop_id');
        $shopGroupName = ShopGroupModel::query()->whereIn('id', $shopGroupIds)->where('deleted_at', '=', null)->pluck('shop_group_name', 'id')->toArray();
        $shopAccount = [];
        foreach ($uids as $key => $val) {
            $shopAccount[$key] = $shopAccounts[$val] ?? '';
        }
        foreach ($shopGroupIds as $key => $val) {
            $shopGroupName[$key] = $shopGroupName[$val];
        }
        foreach ($list as $key => $val) {
            $list[$key]['shop_account'] = $shopAccount[$val->shop_id] ?? null;
            $list[$key]['group_name'] = $shopGroupName[$val->shop_id] ?? null;
        }

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function add(array $params)
    {
        $shop_id = $this->uniqidNumberCode(14);
        try {
            if(trim(substr($params['start_time'], 0, 5)) == '24:00'){
                $params['start_time'] = '00:00:00';
            }
            if(trim(substr($params['end_time'], 0, 5)) == '24:00'){
                $params['end_time'] = '23:59:59';
            }
            DB::transaction(function () use ($params, $shop_id) {
                ShopModel::query()->create([
                    'shop_id' => $shop_id,
                    'shop_name' => $params['shop_name'],
                    'shop_man' => $params['shop_man'],
                    'shop_no' => $params['shop_no'],
                    'lsy_shop_no' => $params['lsy_shop_no'],
                    'crm_mcid' => $params['crm_mcid'],
                    'start_time' => $params['start_time'],
                    'end_time' => $params['end_time'],
                    'shop_desc' => $params['shop_desc'],
                    'shop_tel' => $params['shop_tel'],
                    'province' => $params['province'],
                    'city' => $params['city'],
                    'country' => $params['country'],
                    'address' => $params['address'],
                    'status' => $params['status'],
                    'is_must' => $params['is_must'],
                    'latitude' => $params['latitude'],
                    'longitude' => $params['longitude'],
                    'shop_group_id' => $params['shop_group_id'],
                    'shop_sdp_no' => $params['shop_sdp_no'] ?? 0,
                    'shop_sdp_name' => $params['shop_sdp_name'] ?? 0,
                    'shop_sdp_user_code' => $params['shop_sdp_user_code'] ?? null,
                ]);
                ShopDeliverModel::query()->create([
                    'shop_id' => $shop_id,
                    'deliver_money' => $params['deliver_money'],
                    'free_deliver_money' => $params['free_deliver_money'],
                    'shop_freight' => $params['shop_freight'],
                    'deliver_type' => $params['deliver_type'] ?? 1,
                    'deliver_kilometre' => $params['deliver_kilometre'],
                    'deliver_range' => $params['deliver_range'],
                ]);

            });
        } catch (Exception $e) {
            return ['code' => ErrorCode::NOT_IN_FORCE];
        }

        // 更新附近店铺列表存储的 Redis 数据
        $params['shop_id'] = $shop_id;
        $params['func'] = 'add';
        $this->eventDispatcher->dispatch(new UpdateShop($params));


        return ['code' => ErrorCode::SUCCESS, 'data' => [], 'info' => ['target_id' => $shop_id]];
    }

    /**
     * @param int $shop_id
     *
     * @return array
     */
    public function getInfoById(int $shop_id)
    {
        $shop = ShopModel::query()->where('shop_id', $shop_id)->first();
        if (!$shop) {
            return ['code' => ErrorCode::NOT_EXIST];
        }
        $shop = $shop->toArray();
        $shop['group_name'] = self::getShopGroupNameById((int)$shop['shop_group_id']);
        $shopRes = ShopDeliverModel::query()->where('shop_id', $shop_id)->first();
        if (!$shopRes) {
            return ['code' => ErrorCode::NOT_EXIST];
        }
        $shopRes = $shopRes->toArray();
        $res = array_merge($shop, $shopRes);

        return ['code' => ErrorCode::SUCCESS, 'data' => $res];
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function update(array $params)
    {
        if(trim(substr($params['start_time'], 0, 5)) == '24:00'){
            $params['start_time'] = '00:00:00';
        }
        if(trim(substr($params['end_time'], 0, 5)) == '24:00'){
            $params['end_time'] = '23:59:59';
        }
        try {
            DB::transaction(function () use ($params) {
                ShopModel::query()->where(['shop_id' => $params['shop_id']])
                    ->update([
                        'shop_name' => $params['shop_name'],
                        'shop_man' => $params['shop_man'],
                        'shop_no' => $params['shop_no'],
                        'lsy_shop_no' => $params['lsy_shop_no'],
                        'crm_mcid' => $params['crm_mcid'],
                        'start_time' => $params['start_time'],
                        'end_time' => $params['end_time'],
                        'shop_desc' => $params['shop_desc'],
                        'shop_tel' => $params['shop_tel'],
                        'province' => $params['province'],
                        'city' => $params['city'],
                        'country' => $params['country'],
                        'address' => $params['address'],
                        'status' => $params['status'],
                        'is_must' => $params['is_must'],
                        'latitude' => $params['latitude'],
                        'longitude' => $params['longitude'],
                        'shop_group_id' => $params['shop_group_id'],
                        'shop_sdp_no' => $params['shop_sdp_no'],
                        'shop_sdp_name' => $params['shop_sdp_name'],
                        'shop_sdp_user_code' => $params['shop_sdp_user_code'] ?? null,
                    ]);
                ShopDeliverModel::query()->where(['shop_id' => $params['shop_id']])
                    ->update([
                        'deliver_money' => $params['deliver_money'],
                        'free_deliver_money' => $params['free_deliver_money'],
                        'shop_freight' => $params['shop_freight'],
                        'deliver_type' => $params['deliver_type'] ?? 1,
                        'deliver_kilometre' => $params['deliver_kilometre'],
                        'deliver_range' => $params['deliver_range'] ?? '',
                    ]);
            });
        } catch (Exception $e) {
            return ['code' => ErrorCode::NOT_IN_FORCE];
        }
        // 更新附近店铺列表存储的 Redis 数据项
        $params['func'] = 'edit';
        $this->eventDispatcher->dispatch(new UpdateShop($params));

        return ['code' => ErrorCode::SUCCESS, 'data' => [], 'info' => ['target_id' => $params['shop_id']]];
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public static function getShopGroupNameById(int $id)
    {
        return ShopGroupModel::query()->where(['id' => $id])->value('shop_group_name');
    }

    /**
     * @param int $shop_id
     * @param int $shop_group_id
     *
     * @return array
     */
    public function shopBindGroup(int $shop_id, int $shop_group_id)
    {
        $isHasGroup = ShopModel::query()->where(['shop_id' => $shop_id])->value('shop_group_id');
        if ($isHasGroup) {
            return ['code' => ErrorCode::FORBID_BIND];
        }
        $res = ShopModel::query()->where(['shop_id' => $shop_id])->update(['shop_group_id' => $shop_group_id]);
        if (!$res) {
            return ['code' => ErrorCode::NOT_IN_FORCE];
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => []];
    }

    /**
     * @param int $shop_id
     *
     * @return mixed
     */
    public function shopUnBindGroup(int $shop_id)
    {
        $res = ShopModel::query()->where(['shop_id' => $shop_id])->update(['shop_group_id' => null]);
        if (!$res) {
            return ['code' => ErrorCode::NOT_IN_FORCE];
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => $res];
    }

    /**
     * 店铺名称
     *
     * @return array
     */
    public function getShopNameData()
    {
        return ShopModel::query()
            ->where('is_deleted', 0)
            ->pluck('shop_name', 'shop_id')
            ->toArray();
    }

    /**
     * 店铺名恒模糊查询ID
     * @param string $shopName
     * @return array
     */
    public function getShopIdArrByShopName(string $shopName)
    {
        return ShopModel::query()
            ->whereRaw('INSTR(shop_name, ?) > 0', [$shopName])
            ->pluck('shop_id')
            ->toArray();
    }

    /**
     * 门店账号查门店ID
     *
     * @param string $shopAccount
     *
     * @return HigherOrderTapProxy|mixed|void|null
     */
    public function getShopIdByShopAccount(string $shopAccount)
    {
        $uid = UserModel::query()->where(['username' => $shopAccount])->value('id');

        return UserShopModel::query()->where(['uid' => $uid])->value('shop_id');
    }

    /**
     * 店群id => shop ids
     *
     * @param int $shopGroupId
     *
     * @return array
     */
    public function getShopIdsByShopGroupId(int $shopGroupId)
    {
        return ShopModel::query()->where(['shop_group_id' => $shopGroupId])->pluck('shop_id')->toArray();
    }

    /**
     * @param int $applicationId
     * @param $type
     * @param int $perPage
     * @param string $application
     * @param null $shopType
     *
     * @return array
     */
    public function getModelShops(int $applicationId, $type, int $perPage, string $application, $shopType = null)
    {
        $perPage = isset($perPage) ? (int)($perPage) : 5000;
        $field = [
            'shop_id',
            'shop_group_id',
            'shop_name',
            'shop_man',
            'shop_tel',
            'address',
            'shop_desc',
        ];

        switch ($application) {
            case 'activity':
                $bindShopsStr = ActivityModel::query()
                    ->where(['activityID' => $applicationId, 'activityType' => $type])
                    ->value('shop_ids');
                if (!$bindShopsStr) {
                    $bindShops = [];
                } else {
                    $bindShops = explode(',', $bindShopsStr);
                }
                break;
            case 'navigation':
                if ($shopType) {
                    //次日达
                    $bindShops = CrdNavigationShopModel::query()->where(['navigation_id' => $applicationId, 'type' => $type])->pluck('shop_id');
                } else {
                    //普通商城
                    $bindShops = NavigationShopModel::query()->where(['navigation_id' => $applicationId, 'type' => $type])->pluck('shop_id');
                }

                if ($bindShops->isEmpty()) {
                    $bindShops = [];
                } else {
                    $bindShops = $bindShops->toArray();
                }
                break;
        }
        $shopInfoArr = ShopModel::query()->whereIn('shop_id', $bindShops)->paginate($perPage, $field);
        foreach ($shopInfoArr as $key => $val) {
            $shopInfoArr[$key]['shop_group_name'] = $this->getGroupNameByGroupId($val['shop_group_id']) ?? '未知店群';
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => $shopInfoArr];
    }

    /**
     * @param int $shopId
     * @return HigherOrderTapProxy|mixed|void
     */
    public function getShopGroupByShopId(int $shopId)
    {
        $shopGroupId = ShopModel::query()->where('shop_id', $shopId)->value('shop_group_id');
        return $groupName = ShopGroupModel::query()->where('id', $shopGroupId)->value('shop_group_name');
    }

    /**
     * @param array $where
     * @param array|string[] $field
     * @return array
     */
    public function shopInfoByWhere(array $where, array $field = ['*'])
    {
        return ShopModel::query()
            ->select($field)
            ->where($where)
            ->first()
            ->toArray();
    }

    /**
     * @param array $array 店铺列表
     * @param float $longitude 当前位置经度
     * @param float $latitude 当前位置纬度
     * @param int $activityId 活动 ID
     *
     * @param int|null $isNextDay
     * @return array
     * 后台店铺操作添加和删除时，需要重新更新 Redis 存储。
     */
    public function getShopDistance(array $array, float $longitude, float $latitude, int $activityId = null, int $isNextDay = null)
    {
        //-----------------测试用------------------
//        $this -> resourceRedis -> del('nearShop-jsd');
//        $this -> resourceRedis -> del('nearShop-crd');
//        $res = $res = $this -> resourceRedis -> zrem('nearShop','69783456032115');
        //-----------------测试用------------------

        if ($activityId && !$isNextDay) {
            $redisKey = 'nearShop-actId:' . $activityId;
        } elseif ($isNextDay && !$activityId) {
            $redisKey = 'nearShop-crd';
        } elseif (!$activityId && !$isNextDay) {
            $redisKey = 'nearShop-jsd';
        } else {
            return ['code' => ErrorCode::NOT_HAVE_NEAR_SHOP];
        }
        $nearShopExist = $this->resourceRedis->exists($redisKey);

        if (!$nearShopExist) {
            // 将数据存入缓存，方便后续 georadius 调用
            foreach ($array as $key => $val) {
                $this->resourceRedis->geoadd($redisKey, $val['longitude'], $val['latitude'], $val['shop_id']);
            }
        }
        // 通过当前经纬度获取缓存中指定范围内每个对象的距离并进行排序
        $res = $this->resourceRedis->georadius($redisKey, $longitude, $latitude, 300000, 'm', ['WITHDIST', 'ASC']);
        $shopIds = array_column($res, 0);
        // 所有店铺的相关信息
        $sInfo = ShopModel::query()->whereIn('shop_id', $shopIds)->select(['shop_id', 'shop_name', 'shop_desc', 'address'])->get()->toArray();
        if ($isNextDay) {
            // 团长相关信息
            $teamInfo = TeamLeaderModel::query()
                ->from('hf_team_leader as leader')
                ->leftJoin('store_shop as shop', 'shop.shop_id', '=', 'leader.shop_id')
                ->whereIn('leader.id', $shopIds)
                ->select(['leader.id', 'leader.name', 'leader.opening_time', 'leader.closing_time', 'leader.delivery_address', 'leader.detail_address', 'leader.shop_id', 'shop.shop_name'])
                ->get()
                ->toArray();
            $tInfo = [];
            //整合团长的字段，和店铺字段保持一致
            foreach ($teamInfo as $key => $val) {
                $tInfo[$key]['shop_id'] = $val['id'];
                $tInfo[$key]['bind_shop_id'] = $val['shop_id']; // 团长绑定的 shop_id
                $tInfo[$key]['shop_name'] = $val['name'];   // 团长名称
                $tInfo[$key]['bind_shop_name'] = $val['shop_name']; // 团长绑定的店铺名称
                $tInfo[$key]['shop_desc'] = $val['opening_time'] . '~' . $val['closing_time'];
                $tInfo[$key]['address'] = $val['delivery_address'] . $val['detail_address'];
                $tInfo[$key]['tag'] = 2;
                $tInfo[$key]['leader_id'] = $val['id'];   // 团长 ID，redis 中这个字段的键改名为 shop_id
            }
            // 将团长和店铺信息合并一起
            $info = array_merge($sInfo, $tInfo);
        } else {
            $info = $sInfo;
        }
        $shopInfo = array_column($info, null, 'shop_id');
        $list = [];
        // 整合要返回的字段
        foreach ($res as $key => $val) {
            if (array_key_exists('tag', $shopInfo[$val[0]] ?? [])) {
                // 团长展示真实 shop_id
                $list[$key]['shop_id'] = $shopInfo[$val[0]]['bind_shop_id'];
                $list[$key]['leader_id'] = $shopInfo[$val[0]]['leader_id'];
                $list[$key]['bind_shop_name'] = $shopInfo[$val[0]]['bind_shop_name'];
            } else {
                // 店铺展示原本的 shop_id
                $list[$key]['shop_id'] = $val[0];
                $list[$key]['leader_id'] = '';
            }
            $list[$key]['distance'] = $this->m2Km($val[1]) ?? 0;
            $list[$key]['shop_name'] = $shopInfo[$val[0]]['shop_name'] ?? null;
            $list[$key]['shop_desc'] = $shopInfo[$val[0]]['shop_desc'] ?? null;
            $list[$key]['address'] = $shopInfo[$val[0]]['address'] ?? null;
            $list[$key]['tag'] = $shopInfo[$val[0]]['tag'] ?? 1;        // 该字段有值为团长，没值为店铺
        }
        if (empty($list)) {
            return ['code' => ErrorCode::NOT_HAVE_NEAR_SHOP];
        }

        return ['code' => ErrorCode::SUCCESS, 'data' => $list];
    }


    /**
     * @param int $acticityId
     * @return HigherOrderTapProxy|mixed|void
     */
    public function getShopIdsByActivityId(int $acticityId)
    {
        return ActivityModel::query()->where('activityID', $acticityId)->value('shop_ids');
    }

    /**
     * @param array $shopIdArr
     * @param array $field
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public function getShopInfobyShopIds(array $shopIdArr, array $field)
    {
        return ShopModel::query()->whereIn('shop_id', $shopIdArr)->where(['is_deleted' => 0, 'status' => 1])->select($field)->get();
    }

    /**
     * 获取首页导航数据
     *
     * @param int $shop_id
     * @return array
     */
    public function getMiniAppHomeData(int $shop_id)
    {
        // 导航类
        $result = $this->navigationService->getNavList($shop_id);
        // 限时抢购
        $result['sale_limit'] = $this->goodsService->getLimitSale($shop_id);
        // 小跑拼团
        $result['group_list'] = $this->goodsService->getShopHomeGroup($shop_id);
        // 会员专区
        $result['member_area'] = $this->goodsService->getShopGoodsList($shop_id, ['group_id' => 2], 5)['list'];

        return $result;
    }

    /**
     * 判断店铺是否正常营业 时间  是否自提  拼团订单(不限时间段)
     * @param int $shopId
     * @param int $order_type
     * @return array
     * @author lulongfei
     */
    public  function getShopCondition(int $shopId, int $order_type,int $deal_type,int $address_id)
    {
        $shopArr = ShopModel::query()->where(['shop_id' => $shopId])->select(['is_must', 'start_time', 'end_time', 'status','longitude', 'latitude'])->first()->toArray();
        $day_time = date('H:i:s', time());
        if ($order_type == OrderMiniService::ORDER_TYPE_0 || $order_type == OrderMiniService::ORDER_TYPE_2) {
            if ($day_time < $shopArr['start_time'] || $day_time > $shopArr['end_time']) {
                return ['code' => ErrorCode::SHOP_STATUS_ERROR];
            }
        }
        if ($shopArr['status'] != 1) {
            return ['code' => ErrorCode::SHOP_STATUS_CLOSE];
        }
        if($deal_type==OrderMiniService::ORDER_DEAL_TYPE_1){
            //判断店铺是否正常营业 时间  是否自提
            if($shopArr['is_must']==1)  return ['code' => ErrorCode::SHOP_DEAL_TYPE];
            //判断配送距离是否超出服务范围
            $addressArr = $this->memberService->getAddressInfo(['id' => $address_id],['longitude', 'latitude']);
            if(empty($addressArr)){
                return ['code' => ErrorCode::ORDER_ADDRESS_ERROR];
            }
            //两地距离
            $distance = round(($this->getServiceDistanceByWalking($shopArr, $addressArr)[0]['distance']) / 1000, 2);
            $deliver_kilometre = ShopDeliverModel::query()->where(['shop_id' => $shopId])->value('deliver_kilometre');
            if ($distance * 100 >= $deliver_kilometre * 100) {
                return ['code'=>ErrorCode::SHOP_AREA_ERROR];
            }
        }
        return ['code' => ErrorCode::SUCCESS];
    }

    /**
     * 获取步行距离
     * @param $ShopArr
     * @param $addressArr
     * @return mixed
     */
    public function getServiceDistanceByWalking(array $shopArr, array $addressArr)
    {
        $url = $this->baseUrl . self::WALKING_URL;
        $option = [
            'key' => $this->Key,
            'from' => [$addressArr['latitude'], $addressArr['longitude']],
            'to' => [$shopArr['latitude'], $shopArr['longitude']]
        ];
        $res = json_decode($this->do_request_post($url, json_encode($option)), true);
        if ($res['status'] == 0) {
            $dis = $res['result']['elements'];
            return $dis;
        }
    }

    /**
     * @param string $lng // 经度 x
     * @param string $lat // 纬度 y
     * @param string $shop_id // 根据店铺编号查出店铺
     *
     * @return false|int|string
     */
    public function checkPointInArea(string $lng, string $lat, string $shop_id)
    {
        $deliverRange = ShopDeliverModel::query()->where(['shop_id' => $shop_id])->value('deliver_range') ?? '';
        $deliverRangeArr = json_decode($deliverRange, true);    // 无数据为 NULL
        $deliverRangeArrPro = [];
        $deliverRangeArrPro[] = $deliverRangeArr;
        $area = new AreaService($deliverRangeArrPro);
        $res = $area->checkPoint($lng, $lat);
        return $res;
    }


    /**
     * 通过店铺id获取店铺信息
     * @param  $shopId
     * @param  $field
     * @return array
     * @author lulongfei
     */
    public static function getInfoByShopId(int $shopId, $field = ['*'])
    {
        $shop = ShopModel::query()->where(['shop_id' => $shopId])->select($field)->first();
        return $shop?$shop->toArray():[];
    }

    /**
     * 根据门店ID更新数据
     * @param $shopId
     * @param $updateData
     * @return array|bool
     */
    public static function setShopInfoByShopId($shopId, $updateData)
    {
        return ShopModel::query()->where(['shop_id' => $shopId])->update($updateData);

    }

    /**
     * 今日营收
     * @param array $where
     * @return array|string
     * @author lulongfei
     */
    public  function storesIncome(array $where)
    {
        return $this->orderBusinessService->storesIncome($where);
    }

    /**
     *
     * 根据分类获取商品列表 全部|售卖中
     * @param array $where
     * @param int $shopId
     * @return array
     * @author lulongfei
     */
    public static function getGoodsList(array $where,int $shopId,int $allGoods)
    {
        $goodsModel =  GoodsModel::where($where);
        if($allGoods==2){
            $goodsModel->whereRaw('!FIND_IN_SET(?,shop_ids)', [$shopId]);
        }
        $_goodsList =$goodsModel->select(['id','status','shop_ids'])->get();
        $goodsList = $_goodsList? $_goodsList->toArray():[];
        $goodsById = array_column($goodsList,null,'id');
        $goodsListArr = GoodsService::getGoodsInfoData($shopId,array_keys($goodsById),GoodsService::STOCK_TYPE_1);
        $data = [];
        foreach ($goodsListArr as $v){
            $data[] = [
                'id'=>$v['id'],
                'title'=>$v['title'],
                'cate_id'=>$v['cate_id'],
                'logo'=>$v['logo'],
                'price_selling'=>$v['price_selling'],
                'goods_spec'=>$v['goods_spec'],
//                'status'=>$goodsById[$v['goods_id']]['status'],
                'status'=>in_array($shopId,explode(',',$goodsById[$v['goods_id']]['shop_ids']??''))?2:1,
                'number_sales'=>$v['number_sales'],
            ];
        }
        return $data;
    }

    /**
     *  下架商品$flag =1店铺下架 =2 平台下架
     * @param array $where
     * @param int $shopId
     * @param int $flag
     * @param int $perPage
     * @param string $sort
     * @return array
     * @author lulongfei
     */
    public static function getSellGoodsList(array $where,int $shopId,int $flag,int $perPage, $orderBy = ['create_at', 'desc'])
    {
        if($flag==1){
            //门店下架
            $goodsIds = GoodsModel::where($where)->whereRaw('FIND_IN_SET(?,shop_ids)', [$shopId])->orderBy($orderBy[0], $orderBy[1])->select(['id'])->paginate($perPage);
        }else{
            //平台下架
            $goodsIds = GoodsModel::where($where)->orderBy($orderBy[0], $orderBy[1])->select(['id'])->paginate($perPage);
        }
        $goodsIdsArr =$goodsIds? array_column($goodsIds->toArray()['data'],'id'):[];
        $goodsList = GoodsService::getGoodsInfoData($shopId,$goodsIdsArr,GoodsService::STOCK_TYPE_0);
        $data = [];
        foreach ($goodsList as &$v){
            $data[] = [
                'id'=>$v['id'],
                'title'=>$v['title'],
                'cate_id'=>$v['cate_id'],
                'logo'=>$v['logo'],
                'price_selling'=>$v['price_selling'],
                'goods_spec'=>$v['goods_spec'],
                'status'=>$flag==1?1:0,//店铺是否下架（0：未下架；1：已下架）
                'number_sales'=>$v['number_sales'],
                'is_deleted'=> $flag==1?0:1//平台是否下架（0：未下架；1：已下架）
            ];
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => ['goodsList' => $data, 'total' => $goodsIds->total()]];
    }

    /**
     *
     * 商家APP 商品搜索
     * @param array $where
     * @param string $goodsName
     * @param int $shopId
     * @param string[] $orderBy
     * @return array
     * @author lulongfei
     */
    public static function GoodsSearchName(array $where,string $goodsName,int $shopId, $orderBy = ['create_at', 'desc'])
    {
        $goodsList = GoodsModel::where($where)->whereRaw('INSTR(title, ?) > 0', [$goodsName])->orderBy($orderBy[0], $orderBy[1])->select(['id','shop_ids'])->get();
        if($goodsList){
            $goodsById = array_column($goodsList->toArray(),'shop_ids','id');
        }else{
            return [];
        }
        $goodsList = GoodsService::getGoodsInfoData($shopId,array_keys($goodsById),GoodsService::STOCK_TYPE_0);
        $data = [];
        foreach ($goodsList as &$v){
            $data[] = [
                'id'=>$v['id'],
                'title'=>$v['title'],
                'cate_id'=>$v['cate_id'],
                'logo'=>$v['logo'],
                'price_selling'=>$v['price_selling'],
                'goods_spec'=>$v['goods_spec'],
                'status'=>$goodsById[$v['goods_id']]?(in_array($shopId,explode(',',$goodsById[$v['goods_id']]))?1:0):0,//店铺是否下架（0：未下架；1：已下架）
                'number_sales'=>$v['number_sales'],
            ];
        }
        return $data;
    }

    /**
     *
     * APP商品上下架
     * @param int $goodsId
     * @param int $shopId
     * @return int
     */
    public static function setGoodsInfoById(int $goodsId,int $shopId)
    {
        $shopIds = GoodsModel::where(['id'=>$goodsId])->value('shop_ids');
        $shopIdsArr =explode(',',$shopIds);
        if(empty($shopIds)){
            $shopIds = $shopId;
        }elseif(!in_array($shopId, $shopIdsArr)){
            $shopIds = $shopIds . ',' . $shopId;
        }else{
            foreach ($shopIdsArr as $k => $v) {
                if ($v == $shopId) {
                    unset($shopIdsArr[$k]);
                }
            }
            $shopIds = implode(',', $shopIdsArr);
        }
        return GoodsModel::where(['id'=>$goodsId])->update(['shop_ids'=>$shopIds]);
    }

    /**
     *
     * 获取用户列表
     * @param $perPage
     * @param string[] $field
     * @return \Hyperf\Contract\LengthAwarePaginatorInterface
     */
    public function getMemberList($perPage,$field = ['*'])
    {
        return MemberModel::where(['is_deleted'=>0])->select($field)->orderBy('register_date','desc')->paginate($perPage);
    }

    /**
     * 新增并插入记录并获取自增ID
     * @param array $data
     * @return int|string
     */
    public function shopInsertData(array $data)
    {
        return ShopModel::query()->insertGetId($data);
    }

    /**
     * 获取商家下面所有的团长id
     *
     * @param $shop_id
     * @return array
     */
    public function getShopPassLeaderIds($shop_id)
    {
        $ids = TeamLeaderModel::query()->where('shop_id', $shop_id)->whereIn('status', [
            Stakeholder::LEADER_STATUS_IN_BUSINESS,
            Stakeholder::LEADER_STATUS_OUT_OF_BUSINESS,
        ])->pluck('id');

        return $ids ? $ids->toArray() : [];
    }

    /**
     * 获取已经支付的订单
     *
     * @param array $leader_ids
     * @param array $where
     * @param false $toArray
     * @return array|\Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public function getLeaderPayOrder(array $leader_ids, $where = [], $toArray = false)
    {
        $list = OrderModel::query()->where($where)->where('is_pay', 1)->whereIn('leader_id', $leader_ids)->get();

        (count($list) && $toArray) && $list = $list->toArray();

        return $list;
    }

    /**
     * 获取商家的所有团长订单交易金额/佣金/数量
     *
     * @param $id
     * @param $date [单天的日期]
     * @param $type [1：商家，2：团长]
     * @return array
     */
    public function getDateShopWriteTrading($date, $id, $type = 1)
    {
        $number = 0;        // 订单数量
        $amount = 0;        // 核销金额
        $commission = 0;    // 佣金金额

        $leader_ids = $type == 1 ? $this->getShopPassLeaderIds($id) : [$id];

        // 核销订单
        $orders = $this->getLeaderPayOrder($leader_ids, function ($q) use ($date) {
            $q->where('platform_type', 2);
            $q->where('write_off_code_status', 2);
            if (is_array($date)) {
                $date = " ' " . join("','", array_values($date) ) . " ' ";
                $q->whereRaw("date(`complete_date`) in ($date)");
            } else {
                $q->whereDate('complete_date', $date);
            }
        });
        if (count($orders)) {
            $order_nos = $orders->pluck('order_no')->toArray();

            // 查询退款
            $_kzRefund = KzRefundLogModel::query()->whereIn('order_no', $order_nos)->get([
                'order_no',
                'refund_total_money',
                'refund_commission_money',
            ]);
            $kzRefund = [];
            foreach ($_kzRefund as $v) {
                $kzRefund[$v['order_no']][] = $v->toArray();
            }
            $_wxRefund = WxRefundLogModel::query()->whereIn('order_no', $order_nos)->get([
                'order_no',
                'refund_wx_money',
                'refund_commission_money',
            ]);
            $wxRefund = [];
            foreach ($_wxRefund as $v) {
                $wxRefund[$v['order_no']][] = $v->toArray();
            }

            $_amount = $orders->pluck('total_price')->toArray();
            $_commission = $orders->pluck('order_commission')->toArray();

            foreach ($order_nos as $index => $order_no) {

                $orderKzRefund = $kzRefund[$order_no] ?? [];
                $orderWxRefund = $wxRefund[$order_no] ?? [];

                // 计算退款金额
                $kz_money = array_sum(array_column($orderKzRefund, 'refund_total_money'));
                $kz_commission = array_sum(array_column($orderKzRefund, 'refund_commission_money'));
                $wx_money = array_sum(array_column($orderWxRefund, 'refund_wx_money'));
                $wx_commission = array_sum(array_column($orderWxRefund, 'refund_commission_money'));

                // 退款金额
                $refund_money = $kz_money + $wx_money;
                // 退款佣金
                $refund_commission = $kz_commission + $wx_commission;

                // 计算订单数量
                if ($_amount[$index] > $refund_money) {
                    $number++;
                }

                // 最终金额
                $amount += $_amount[$index] - $refund_money;
                $commission += $_commission[$index] - $refund_commission;

                // 打印日志
                $log = [
                    'order_no'             => $order_no,
                    'order_money'          => $_amount[$index],
                    'order_commission'     => $_commission[$index],
                    'wx_refund_money'      => $wx_money,
                    'wx_refund_commission' => $wx_commission,
                    'kz_refund_money'      => $kz_money,
                    'kz_refund_commission' => $kz_commission,
                    'kz_refund_list'       => $orderKzRefund,
                    'wx_refund_list'       => $orderWxRefund,
                ];

//                var_dump(var_export($log, true));
            }
        }

        return [
            'number'     => $number,
            'amount'     => round($amount, 2),
            'commission' => round($commission, 2),
        ];
    }

    /**
     * 获取指定日期的退佣金金额
     *
     * @param array $leader_ids
     * @param array $date
     * @return array
     */
    public function getBackCommissionValue(array $leader_ids, array $date)
    {
        $logs = TeamLeaderCommissionRecord::query()
            ->selectRaw('DATE(created_at) as date,SUM(expend) as expend')
            ->whereIn(Db::raw('date(created_at)'), $date)
            ->whereIn('leader_id',$leader_ids)
            ->where('type', 3)
            ->groupBy(['date'])
            ->get();
        $result = [];
        foreach ($logs as $log) {
            !isset($result[$log['date']]) && $result[$log['date']] = 0;
            $result[$log['date']] += intval($log['expend']);
        }
        return $result;
    }

    /**
     * 获取待核销数量
     *
     * @param $tid
     * @return int[]
     */
    public function getDataToBeWrittenOff($tid)
    {
        $_today = date('Y-m-d');
        $_yesterday = date('Y-m-d', strtotime('-1 day'));
        $where = function ($q) use ($_today, $_yesterday) {
            $q->whereDate('pay_at', $_today)->orWhereDate('pay_at', $_yesterday);
        };
        $orders = OrderModel::query()
            ->selectRaw('DATE(pay_at) as date,COUNT(1) as order_number')
            ->where($where)
            ->where('leader_id', $tid)
            ->whereIn('status', [2, 3])
            ->groupBy(['date'])
            ->get();
        if ($orders) {
            $data = collect($orders)->keyBy('date')->toArray();
        }
        return [
            'today'     => $data[$_today]['order_number'] ?? 0,
            'yesterday' => $data[$_yesterday]['order_number'] ?? 0,
        ];
    }

    /**
     * 获取团长数量
     *
     * @param $id
     * @param $up_to [截止到某個日期]
     * @return int
     */
    public function getLeaderCount($id, $up_to = null)
    {
        return TeamLeaderModel::query()->has('member')->whereIn('status', [
            Stakeholder::LEADER_STATUS_IN_BUSINESS,
            Stakeholder::LEADER_STATUS_OUT_OF_BUSINESS,
        ])->when($up_to, function ($q) use ($up_to) {
            $q->where('created_at', '<', $up_to . " 23:59:59");
        })->where('shop_id', $id)->count();
    }

    /**
     * 获取次日达订单商品分组销售额
     *
     * @param $order_nos
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection|\Hyperf\Database\Query\Builder[]|\Hyperf\Utils\Collection
     */
    function cateSales($order_nos)
    {
        return OrderGoodsModel::query()
            ->selectRaw('
                crd_cate_id,
                GROUP_CONCAT(distinct order_no) as order_nos,
                SUM(IF(status = 0,number,IF(number - refund_number - shop_refund_number > 0,number - refund_number - shop_refund_number,0)) * goods_dis_price * 100) as sales'
            )
            ->whereIn('order_no', $order_nos)
            ->groupBy(['crd_cate_id'])
            ->orderBy('sales', 'desc')
            ->get();
    }

    /**
     * 固定的五个百分比
     *
     * @param $id
     * @return array
     */
    public function percentage($id)
    {
        $result = $_result = [
            'all_total_amount'    => 0,
            'leader_total_amount' => 0,
            'store_total_amount'  => 0,
            'all'                 => [],
            'leader'              => [],
            'store'               => [],
        ];
        // 门店下面所有的团长
        $leader_ids = TeamLeaderModel::query()->where('shop_id', $id)->whereIn('status', [
            Stakeholder::LEADER_STATUS_IN_BUSINESS,
            Stakeholder::LEADER_STATUS_OUT_OF_BUSINESS,
        ])->pluck('id');
        // 团长所有订单
        $nos1 = OrderModel::query()
            ->whereDate('pay_at', date('Y-m-d'))
            ->whereIn('leader_id', $leader_ids)
            ->where('is_pay', 1)
            ->pluck('order_no');
        $nos1 = count($nos1) ? $nos1->toArray() : [];
        // 商店的所有订单
        $nos2 = OrderModel::query()
            ->whereDate('pay_at', date('Y-m-d'))
            ->where('shop_id', $id)
            ->where('leader_id', 0)
            ->where('is_pay', 1)
            ->pluck('order_no');
        $nos2 = count($nos2) ? $nos2->toArray() : [];
        // 分组
        $group = [
            'all'    => $this->cateSales(array_merge($nos1, $nos2)),
            'leader' => $this->cateSales($nos1),
            'store'  => $this->cateSales($nos2),
        ];
        foreach ($result as $key => $item) {
            if (!is_array($item)) {
                continue;
            }
            foreach ($group[$key] as $v) {
                $title = $v instanceof OrderGoodsModel && $v->crd_cate ? $v->crd_cate->title : $v->title;
                if (!$title) {
                    continue;
                }
                $_sales = $v->sales;
                $_result[$key][] = ['title' => $title, 'sales' => $_sales];
            }
            $sales = array_sum(array_column($_result[$key], 'sales'));
            if ($sales > 0) {
                $result[$key] = $this->cutToOther($_result[$key], 5);
            }
            $result[$key] = $this->percent($result[$key]);
            $result[$key . '_total_amount'] = $sales / 100;
        }
        return $result;
    }

    /**
     * 计算百分比
     *
     * @param $array
     * @return mixed
     */
    public function percent($array)
    {
        $total = array_sum(array_column($array, 'sales'));
        foreach ($array as &$v) {
            $v['percent'] = round($v['sales'] / $total * 100, 2);
        }
        return $array;
    }

    /**
     * 切入到其他分类
     *
     * @param $array
     * @param $cutNumber
     * @return array
     */
    public function cutToOther($array, $cutNumber)
    {
        $items = [];
        $list = array_chunk($array, $cutNumber, true);
        if (isset($list[0])) {
            foreach ($list[0] as $v) {
                $items[] = ['title' => $v['title'], 'sales' => $v['sales'] / 100];
            }
        }
        $sales = 0;
        for ($i = 1; $i < count($list) + 1; $i++) {
            if (isset($list[$i])) {
                $sales += array_sum(array_column($list[$i], 'sales'));
            }
        }
        if (count($array) > $cutNumber) {
            $items[] = ['title' => '其他', 'sales' => $sales / 100,];
        }
        return $items;
    }

    /**
     * 获取两个时间之间的日期
     * @param $startDate
     * @param $endDate
     * @return array
     */
    function getDatesBetweenTwoDays($startDate, $endDate)
    {
        $dates = [];
        if (strtotime($startDate) > strtotime($endDate)) {
            // 如果开始日期大于结束日期，直接return 防止下面的循环出现死循环
            return $dates;
        } elseif ($startDate == $endDate) {
            // 开始日期与结束日期是同一天时
            array_push($dates, $startDate);
            return $dates;
        } else {
            array_push($dates, $startDate);
            $currentDate = $startDate;
            do {
                $nextDate = date('Y-m-d', strtotime($currentDate . ' +1 days'));
                array_push($dates, $nextDate);
                $currentDate = $nextDate;
            } while ($endDate != $currentDate);

            return $dates;
        }
    }

    /**
     * 销售额 top20
     *
     * @param $id
     * @return array
     */
    public function top20($id = null)
    {
        return OrderGoodsModel::query()
            ->selectRaw('
                goods_id,
                goods_title,
                SUM(number) as number,
                SUM(dis_price) as sales
            ')
            ->whereHas('order', function ($q) use ($id) {
                $id && $q->whereIn('leader_id', $this->getShopPassLeaderIds($id));
                $q->where('is_pay', 1);
                $q->whereDate('pay_at', date('Y-m-d'));
                $q->where('platform_type', 2);
            })
            ->having('number', '>', 0)
            ->groupBy(['goods_id'])
            ->orderBy('number', 'desc')
            ->orderBy('sales', 'desc')
            ->limit(20)
            ->get();
    }

    /**
     * 团长列表
     *
     * @param $id
     * @param $row
     * @param $where
     * @return array
     */
    public function teamLeaderList($id, $row = 15, $where = [])
    {
        $teamLeaders = TeamLeaderModel::query()
            ->where($where)
            ->where('shop_id', $id)
            ->whereIn('status', [
                Stakeholder::LEADER_STATUS_IN_BUSINESS,
                Stakeholder::LEADER_STATUS_OUT_OF_BUSINESS,
            ])
            ->paginate($row, ['id', 'name', 'mid', 'phone']);
        if (!$teamLeaders) {
            return [];
        }
        $orders = OrderModel::query()
            ->selectRaw('leader_id,GROUP_CONCAT(DISTINCT order_no) as order_nos,SUM(total_price * 100) as total_price,SUM(if(status=4,order_commission * 100,0)) as total_commission')
            ->whereIn('leader_id', array_column(collect($teamLeaders->items())->toArray(), 'id'))
            ->where('is_pay', 1)
            ->whereDate('pay_at', date('Y-m-d'))
            ->groupBy(['leader_id'])
            ->get();
        if ($orders) {
            $orders = $orders->toArray();
            $order_nos = implode(',', array_column($orders, 'order_nos'));
            $order_nos = array_filter(array_unique(explode(',', $order_nos)));
            $kz_refund = KzRefundLogModel::query()->select([
                'leader_id',
                Db::raw('SUM(refund_balance_money * 100) as refund_money')
            ])->whereIn('order_no', $order_nos)->groupBy(['leader_id'])->get();
            $kz_refund = collect($kz_refund)->keyBy('leader_id')->toArray();
            $wx_refund = WxRefundLogModel::query()->select([
                'leader_id',
                Db::raw('SUM(refund_wx_money * 100) as refund_money')
            ])->whereIn('order_no', $order_nos)->groupBy(['leader_id'])->get();
            $wx_refund = collect($wx_refund)->keyBy('leader_id')->toArray();
            $priceArray = array_column($orders, 'total_price', 'leader_id');
        }
        $data = [];
        foreach ($teamLeaders as $leader) {
            if (!$leader->member) {
                continue;
            }
            $trading = $this->getDateShopWriteTrading(Carbon::today()->toDateTimeString(), $leader['id'], 2);
            $refund = ($kz_refund[$leader['id']]['refund_money'] ?? 0) + ($wx_refund[$leader['id']]['refund_money'] ?? 0);
            $totalMoney = ($priceArray[$leader['id']] ?? 0);
            if ($totalMoney > 0) {
                $totalMoney -= $refund;
            }
            $data[] = [
                'id'               => $leader['id'],
                'name'             => $leader['name'],
                'avatar'           => $leader->member->face_url,
                'phone'            => $leader['phone'],
                'total_commission' => $trading['commission'],
                'total_price'      => $totalMoney > 0 ? round($totalMoney / 100, 2) : 0,
            ];
        }
        $sort = array_column($data, 'total_price');
        array_multisort($sort, SORT_DESC, $data);
        return $data;
    }

    /**
     * 小程序首页店铺搜索
     * @param array $where
     * @param array|string[] $field
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public function queryShopList(array $where = [], array $field = ['*']){
        return ShopModel::query()
            ->select($field)
            ->whereRaw('INSTR(address, ?) > 0', [$where['address']])
            ->where('is_deleted', 0)
            ->get();
    }


    /**
     * 获取店铺配送信息
     * @param int $shopId
     * @return array
     * @author lulongfei
     */
    public function getDeliver(int $shopId)
    {
        $deliver = ShopDeliverModel::query()->where(['shop_id' => $shopId])->select(['deliver_money', 'shop_freight', 'free_deliver_money'])->first();
        return $deliver?$deliver->toArray():[];
    }

    /**
     * 获取门店会员列表
     * @param int $perPage
     * @param array $field
     * @return \Hyperf\Contract\LengthAwarePaginatorInterface
     * @author lulongfei
     */
    public function getShopMemberList(int $perPage, array $field)
    {
        return $this->memberService->getShopMemberList($perPage, $field);
    }

    /**
     * 今日营收明细
     *
     * type为order 查询订单  为citic查询返款信息表
     * @param array $where
     * @param string $field
     * @param int|string $type
     * @return array
     * @author lulongfei
     */
    public function storesIncomeDetail(array $where,string $field = '*', string $type=Stakeholder::FINANCE_ORDER)
    {
        return $this->orderBusinessService->storesIncomeDetail($where,$field,$type);
    }

    /**
     * 门店运营---财务对账--线上明细--已打款列表
     *  type为order 查询订单  为citic查询返款信息表
     * @param array $where
     * @param string $starTime
     * @param $endTime
     * @param int $page
     * @param int $perPage
     * @param int|string $type
     * @return array
     * @author lulongfei
     */
    public function getOrderSerialNumByShopId(array $where, string $starTime,string $endTime, int $page, int $perPage, string $type = Stakeholder::FINANCE_ORDER)
    {
        return $this->orderBusinessService->getOrderSerialNumByShopId($where,$starTime,$endTime,$page,$perPage,$type);

    }
    /**
     * 门店运营---销售对账--日预计计收入接口
     * @param int $shopId
     * @param string $times
     * @return array
     * @author lulongfei
     */
    public  function shopRevenue(int $shopId,string  $times)
    {
        return $this->orderBusinessService->shopRevenue($shopId,$times);
    }
    /**
     * 门店运营---销售对账--历史账单列表信息
     * @param $where
     * @param $page
     * @param $perPage
     * @return array
     * @author lulongfei
     */
    public  function storestoGroupTimeIncome(array $where,int $page, int $perPage)
    {
        return $this->orderBusinessService::storestoGroupTimeIncome($where,$page,$perPage);
    }
    /**
     * 门店运营---销售对账--历史账单---当日订单明细
     * @param array $where
     * @param string $times
     * @param int $perPage
     * @param string $field
     * @return array
     * @author lulongfei
     */
    public  function storestIncomeByTime(array $where,string $times,int $perPage,string $field = '*')
    {
        return $this->orderBusinessService::storestIncomeByTime($where,$times,$perPage,$field);
    }

    /**
     *
     * (次日达)根据分类获取商品列表 全部|售卖中
     * @param array $where
     * @param int $shopId
     * @param int $allGoods
     * @return array
     */
    public static function getTomorrowGoodsList(array $where,int $shopId,int $allGoods)
    {
        $goodsModel =  GoodsModel::where($where);
        if($allGoods==2){
            $goodsModel->whereRaw('!FIND_IN_SET(?,crd_shop_ids)', [$shopId]);
        }
        $_goodsList =$goodsModel->select(['id','status','crd_shop_ids'])->get();
        $goodsList = $_goodsList? $_goodsList->toArray():[];
        $goodsById = array_column($goodsList,null,'id');
        $goodsListArr = GoodsService::getCrdGoodsInfoData(array_keys($goodsById));
        $data = [];
        foreach ($goodsListArr as $v){
            $data[] = [
                'id'=>$v['id'],
                'title'=>$v['title'],
                'crd_cate_id'=>$v['crd_cate_id'],
                'logo'=>$v['logo'],
                'price_selling'=>$v['price_selling'],
                'goods_spec'=>$v['goods_spec'],
                'status'=>in_array($shopId,explode(',',$goodsById[$v['goods_id']]['crd_shop_ids']??''))?2:1,
                'number_sales'=>$v['number_sales'],
            ];
        }
        return $data;
    }

    /**
     *  下架商品$flag =1店铺下架 =2 平台下架
     * @param array $where
     * @param int $shopId
     * @param int $flag
     * @param int $perPage
     * @param string $sort
     * @return array
     */
    public static function getTomorrowSellGoodsList(array $where,int $shopId,int $flag,int $perPage, $orderBy = ['create_at', 'desc'])
    {
        if($flag==1){
            //门店下架
            $goodsIds = GoodsModel::where($where)->whereRaw('FIND_IN_SET(?,crd_shop_ids)', [$shopId])->orderBy($orderBy[0], $orderBy[1])->select(['id'])->paginate($perPage);
        }else{
            //平台下架
            $goodsIds = GoodsModel::where($where)->orderBy($orderBy[0], $orderBy[1])->select(['id'])->paginate($perPage);
        }
        $goodsIdsArr =$goodsIds? array_column($goodsIds->toArray()['data'],'id'):[];
        $goodsList = GoodsService::getCrdGoodsInfoData($goodsIdsArr);
        $data = [];
        foreach ($goodsList as &$v){
            $data[] = [
                'id'=>$v['id'],
                'title'=>$v['title'],
                'crd_cate_id'=>$v['crd_cate_id'],
                'logo'=>$v['logo'],
                'price_selling'=>$v['price_selling'],
                'goods_spec'=>$v['goods_spec'],
                'status'=>$flag==1?1:0,//店铺是否下架（0：未下架；1：已下架）
                'number_sales'=>$v['number_sales'],
                'is_deleted'=> $flag==1?0:1//平台是否下架（0：未下架；1：已下架）
            ];
        }
        return ['code' => ErrorCode::SUCCESS, 'data' => ['goodsList' => $data, 'total' => $goodsIds->total()]];
    }

    /**
     *
     * 商家APP 商品搜索
     * @param array $where
     * @param string $goodsName
     * @param int $shopId
     * @param string[] $orderBy
     * @return array
     */
    public static function tomorrowGoodsSearchName(array $where,string $goodsName,int $shopId, $orderBy = ['create_at', 'desc'])
    {
        $goodsList = GoodsModel::where($where)->whereRaw('INSTR(title, ?) > 0', [$goodsName])->orderBy($orderBy[0], $orderBy[1])->select(['id','crd_shop_ids'])->get();
        if($goodsList){
            $goodsById = array_column($goodsList->toArray(),'crd_shop_ids','id');
        }else{
            return [];
        }
        $goodsList = GoodsService::getCrdGoodsInfoData(array_keys($goodsById));
        $data = [];
        foreach ($goodsList as &$v){
            $data[] = [
                'id'=>$v['id'],
                'title'=>$v['title'],
                'crd_cate_id'=>$v['crd_cate_id'],
                'logo'=>$v['logo'],
                'price_selling'=>$v['price_selling'],
                'goods_spec'=>$v['goods_spec'],
                'status'=>$goodsById[$v['goods_id']]?(in_array($shopId,explode(',',$goodsById[$v['goods_id']]))?1:0):0,//店铺是否下架（0：未下架；1：已下架）
                'number_sales'=>$v['number_sales'],
            ];
        }
        return $data;
    }

    /**
     *
     * APP商品上下架
     * @param int $goodsId
     * @param int $shopId
     * @return int
     */
    public static function setTomorrowGoodsInfoById(int $goodsId,int $shopId)
    {
        $shopIds = GoodsModel::where(['id'=>$goodsId])->value('crd_shop_ids');
        $shopIdsArr =explode(',',$shopIds);
        if(empty($shopIds)){
            $shopIds = $shopId;
        }elseif(!in_array($shopId, $shopIdsArr)){
            $shopIds = $shopIds . ',' . $shopId;
        }else{
            foreach ($shopIdsArr as $k => $v) {
                if ($v == $shopId) {
                    unset($shopIdsArr[$k]);
                }
            }
            $shopIds = implode(',', $shopIdsArr);
        }
        return GoodsModel::where(['id'=>$goodsId])->update(['crd_shop_ids'=>$shopIds]);
    }
}
