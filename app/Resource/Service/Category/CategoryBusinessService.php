<?php
declare(strict_types=1);

namespace App\Resource\Service\Category;


class CategoryBusinessService extends CategoryBaseService implements CategoryServiceInterface
{
    /**
     * 业务场景（商家端及时达获取分类）
     * @return array
     * @author ran
     * @date 2021-03-05 15:06
     * mailbox 466180170@qq.com
     */
    public  function getResourceTimelyArriveCategoryList(): array
    {
        $needExceptKey= ['logo','desc','status','is_deleted','is_home','create_at','sort'];
        $where =['is_deleted'=>self::CATEGORY_IS_DELETED_0,'status'=>self::CATEGORY_STATUS_1];
        $isExist = $this->resourceRedis->exists(self::CTAEGORY_TIMELY_REDIS_KEY);
        if($isExist){
            $resourceCategoryArray = json_decode($this->resourceRedis->hGet(self::CTAEGORY_TIMELY_REDIS_KEY,'resource'),true);
        }else{
            $resourceCategoryArray=$this->getResourceCommonTimelyArriveCategoryListNoPage($needExceptKey,$where);
            $this->resourceRedis->hSet(self::CTAEGORY_TIMELY_REDIS_KEY, 'resource', json_encode($resourceCategoryArray));
        }
        return $resourceCategoryArray;
    }

    /**
     * 业务场景（商家端次日达获取分类）
     * @return array
     * @author ran
     * @date 2021-03-05 15:07
     * mailbox 466180170@qq.com
     */
    public  function getResourceOverNightArriveCategoryList(): array
    {
        $needExceptKey= ['logo','desc','status','is_deleted','is_home','create_at','sort'];
        $where =['is_deleted'=>self::CATEGORY_IS_DELETED_0,'status'=>self::CATEGORY_STATUS_1];
        $isExist = $this->resourceRedis->exists(self::CTAEGORY_OVERNIGHT_REDIS_KEY);
        if($isExist){
            $resourceCategoryArray = json_decode($this->resourceRedis->hGet(self::CTAEGORY_OVERNIGHT_REDIS_KEY,'resource'),true);
        }else{
            $resourceCategoryArray=$this->getResourceCommonOverNightArriveCategoryListNoPage($needExceptKey,$where);
            $this->resourceRedis->hSet(self::CTAEGORY_OVERNIGHT_REDIS_KEY, 'resource', json_encode($resourceCategoryArray));
        }
        return $resourceCategoryArray;
    }


}