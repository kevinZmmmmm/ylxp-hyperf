<?php
declare(strict_types=1);

namespace App\Resource\Service\Category;


use App\Common\Service\BaseService;
use App\Resource\Repository\RepositoryFactory;

class CategoryBaseService extends BaseService
{

    /**
     * Repository
     */
    protected $category;
    /**
     * GoodsMiniService constructor.
     * @param RepositoryFactory $repoFactory
     */
    public function __construct(RepositoryFactory $repoFactory)
    {
        parent::__construct();
        $this->category = $repoFactory->getRepository("category");
    }
    /**
     * 公共字段集合
     */
    const TIMELY_CATEGORY_FIELD=['id','logo','title','desc','status','sort','logo','is_deleted','is_home','create_at'];
    const OVER_NIGHT_CATEGORY_FIELD=['id','logo','title','desc','status','sort','logo','is_deleted','is_home','create_at'];

    const CTAEGORY_TIMELY_REDIS_KEY = 'category:timely';
    const CTAEGORY_OVERNIGHT_REDIS_KEY = 'category:overnight';
    const TIMELY_SOURCE = 1;
    const OVER_NIGHT_SOURCE = 2;

    /**
     * 状态 0禁用，1启用
     */
    const CATEGORY_STATUS_0 = 0;
    const CATEGORY_STATUS_1 = 1;
    /**
     * 删除状态 0未删除，1已删除
     */
    const CATEGORY_IS_DELETED_0 = 0;
    const CATEGORY_IS_DELETED_1 = 1;

    /**
     * 平台分类公共服务（功能适用平台多端）业务场景（及时达分类）
     * @param array $needExceptKey
     * @param array $where
     * @param array $orderBy
     * @return mixed
     * @author ran
     * @date 2021-03-05 15:05
     * mailbox 466180170@qq.com
     */
    protected function getResourceCommonTimelyArriveCategoryListNoPage(array $needExceptKey=[],array $where,array $orderBy=[])
    {
        $categoryOverNightArrivesArray=$this->category->selectListNoPage(self::TIMELY_CATEGORY_FIELD,$where,$orderBy,self::TIMELY_SOURCE);
        $categoryOverNightArrivesArray['list']=$this->arr->exceptMoreArrKey($categoryOverNightArrivesArray['list'],$needExceptKey);
        return $categoryOverNightArrivesArray;
    }

    /**
     * 平台分类公共服务（功能适用平台多端）业务场景（次日达分类）
     * @param int $id 主表主键
     * @param array|string[] $field  主表字段集合
     * @param array|string[] $sonField 子表字段集合
     * @param array $needExceptKey 需要剔除的键
     * @return array|false|string
     * @author zhangzhiyuan
     */
    protected function getResourceCommonOverNightArriveCategoryListNoPage(array $needExceptKey=[],array $where,array $orderBy=[])
    {
        $categoryOverNightArrivesArray=$this->category->selectListNoPage(self::OVER_NIGHT_CATEGORY_FIELD,$where,$orderBy,self::OVER_NIGHT_SOURCE);
        $categoryOverNightArrivesArray['list']=$this->arr->exceptMoreArrKey($categoryOverNightArrivesArray['list'],$needExceptKey);
        return $categoryOverNightArrivesArray;
    }

}