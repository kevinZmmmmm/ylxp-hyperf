<?php

declare(strict_types=1);

namespace App\Resource\Service;

use App\Common\Constants\ErrorCode;
use App\Common\Service\BaseService;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;
use App\Resource\Model\StoreShopPrinterModel;

class StoreShopPrinterService extends BaseService
{
    /**
     * 根据店铺id查找打印机
     * @param $where
     * @param string $field
     * @return array|int|string
     */
    public static function getPrinterInfoByPrinterId($where,$field = '*')
    {
        return StoreShopPrinterModel::query()->where(['shop_id' => $where])->select($field)->first();
    }

    /**
     * 插入或修改打印机设置
     * @param $data
     * @return bool
     */
    public static function printerSettings($data)
    {
        $shop_id = $data['shop_id'];
        $arr = self::getPrinterInfoByPrinterId($shop_id);
        if(empty($arr)){
            $data['create_time'] = date('Y-m-d H:i:s');
            $res = StoreShopPrinterModel::query()->insert($data);
        }else{
            $data['update_time'] = date('Y-m-d H:i:s');
            $res = StoreShopPrinterModel::query()->where(['shop_id' => $shop_id])->update($data);
        }
        return $res;
    }
}
