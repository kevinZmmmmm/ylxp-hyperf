<?php
declare(strict_types=1);

namespace App\Resource\Service\Goods;


use App\Resource\Model\CartModel;
use App\Activity\Service\ActivityService;

class GoodsMiniService extends GoodsBaseService implements GoodsServiceInterface
{
    /**
     * 及时达共用条件
     * @return array
     * @author ran
     * @date 2021-03-10 9:11
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyCommonCondition(int $shop_id){
        $whereRaw ='!FIND_IN_SET(' . $shop_id . ',store_goods.shop_ids)';
        $activeIds = ActivityService::getActivityGoodsByShopId($shop_id);
        $whereNotIn =['store_goods.id',$activeIds];
        return ['whereRaw'=>$whereRaw,'whereNotIn'=>$whereNotIn];
    }

    /**
     * 次日达共用条件
     * @return array
     * @author ran
     * @date 2021-03-10 9:11
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightCommonCondition(int $shop_id){
        $whereRaw ='!FIND_IN_SET(' . $shop_id . ',store_goods.crd_shop_ids)';
        $activeIds = ActivityService::getActivityGoodsByShopId($shop_id);
        $whereNotIn =['store_goods.id',$activeIds];
        return ['whereRaw'=>$whereRaw,'whereNotIn'=>$whereNotIn];
    }
    /**
     * 业务场景（及时达商城）
     * 及时达商城首页精选产品
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return mixed
     * @author ran
     * @date 2021-03-02 14:58
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveFineChoiceGoodsList(int $shop_id,int $page,int $limit): array
    {
        $needExceptKey= ['goods_pick_time','read_pos_stock','safety_stock','image','content','goods_crm_code','read_pos_price','scant_id','ratio','limite_num_per_day'];
        $needExceptRedisKey= ['goods_crm_code','spell_num','cate_id','scant_id','ratio','read_pos_stock','read_pos_price','safety_stock','shop_ids','crd_shop_ids','commission','cost_price','goods_pick_time','lsy_scheme_id','kz_goods_id','sort'];
        $where =['store_goods.group_id'=>self::GOODS_GROUP_0,'store_goods.is_home'=>self::GOODS_IS_HOME_1,'store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition=$this->getResourceTimelyCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        $resourceGoodsArray = $this->getResourceCommonGoodsListForShopByRedis(self::TIMELY_ARRIVE_SHOP_TYPE, $shop_id, $limit, $page);
        $resourceGoodsArray['list']=$this->getResourceCommonTimelyArriveExceptGoodsListKeyByRedis($resourceGoodsArray['list'],$needExceptRedisKey);
        if ($resourceGoodsArray['count'] > 0) return $resourceGoodsArray;
        return $this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,array_merge($extraCondition,$commonCondition));
    }

    /**
     * 业务场景（及时达商城热门搜索）
     * 及时达商城热门搜索
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @param string $searchGoodsName
     * @return array
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveHotSearchGoodsList(int $shop_id,int $page,int $limit): array
    {
        $needExceptKey= ['logo','group_id','share_image','introduction','mini_app_code','kz_goods_id','goods_spec','price_selling','price_market','number_stock','number_virtual','number_sales','goods_pick_time','read_pos_stock','safety_stock','image','content','goods_crm_code','read_pos_price','scant_id','ratio','limite_num_per_day'];
        $where =['store_goods.hot_id'=>self::GOODS_IS_HOT_1,'store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition=$this->getResourceTimelyCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        return $this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,array_merge($extraCondition,$commonCondition));
    }

    /**
     * 业务场景（小程序及时达商城类别ID获取商品列表）
     * @param int $category_id
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveCategoryGoodsList(int $category_id,int $shop_id,int $page,int $limit): array
    {
        $mid=$this->getCurrentInfo()??0;
        $needExceptKey= ['safety_stock','read_pos_stock','sort','goods_pick_time','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','kz_goods_id'];
        $where =['store_goods.cate_id'=>$category_id,'store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition=$this->getResourceTimelyCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        $userCartArray = CartModel::query()->where(['mid' => $mid,'shop_id'=>$shop_id, 'status' => 0, 'is_selected' => 1,'source'=>1,'group_id'=>0])->get(['goods_id', 'mid', 'goods_num'])->toArray();
        $userCartNumBindGoodsList = array_column($userCartArray,'goods_num', 'goods_id') ?: [];
        $goodsArray=$this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,array_merge($extraCondition,$commonCondition));
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['cart_num'] = $userCartNumBindGoodsList[$goods['id']] ?? 0;
            $goodsArray['list'][$k]['activity_goods_id'] = 0;
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
        }
        return $goodsArray;
    }

    /**
     * 业务场景（及时达搜索功能）
     * 及时达搜索功能
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @param string $searchGoodsName
     * @return array
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveSearchGoodsList(int $shop_id,int $page,int $limit,string $searchGoodsName): array
    {
        $needExceptKey= ['goods_pick_time','read_pos_stock','safety_stock','image','content','goods_crm_code','read_pos_price','scant_id','ratio','limite_num_per_day'];
        $whereOp =[['store_goods.title', 'like', "%{$searchGoodsName}%"]];
        $where =['store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition=$this->getResourceTimelyCommonCondition($shop_id);
        $extraCondition=['where'=>$where,'whereOp'=>$whereOp];
        return $this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,array_merge($extraCondition,$commonCondition));
    }

    /**
     * 业务场景（及时达商城会员商品更多）
     * 及时达商城会员商品更多
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @param string $searchGoodsName
     * @return array
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveMemberMoreGoodsList(int $shop_id,int $page,int $limit): array
    {
        $needExceptKey= ['goods_pick_time','read_pos_stock','safety_stock','image','content','goods_crm_code','read_pos_price','scant_id','ratio','limite_num_per_day'];
        $where =['store_goods.group_id'=>self::GOODS_GROUP_2,'store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition=$this->getResourceTimelyCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        return $this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,array_merge($extraCondition,$commonCondition));
    }

    /**
     * 业务场景（及时达商城）
     * 根据商品ID获取商品档案信息服务
     * @param int $id
     * @param int $shop_id
     * @return mixed
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveGoodsMsg(int $id, int $shop_id): array
    {
        $needExceptKey= ['goods_pick_time','goods_crm_code','read_pos_price','scant_id','ratio'];
        $isExist = $this->resourceRedis->exists(self::GOODS_ARCHIVES_REDIS_KEY . $id);
        if($isExist){
            $resourceGoodsArray = unserialize($this->resourceRedis->get(self::GOODS_ARCHIVES_REDIS_KEY . $id));
        }else{
            $resourceGoodsArray=$this->getResourceCommonTimelyArriveGoodsMsg($id,$shop_id,$needExceptKey);
            $resourceGoodsArray['image']=explode('|',$resourceGoodsArray['image']);
            $resourceGoodsArray['number_sales']=$resourceGoodsArray['number_sales']+$resourceGoodsArray['number_virtual'];
            $this->resourceRedis->set(self::GOODS_ARCHIVES_REDIS_KEY . $id, serialize($resourceGoodsArray), self::GOODS_EXPIRE);
        }
        return $resourceGoodsArray;
    }

    /**
     * 业务场景（次日达商城小跑精选）
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @param string $searchGoodsName
     * @return array
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveFineChoiceGoodsList(int $shop_id,int $page,int $limit): array
    {
        $mid=$this->getCurrentInfo()??0;
        $where =['store_goods.status' =>self::GOODS_STATUS_1,'store_goods.is_home'=>self::GOODS_IS_HOME_1];
        $overNightTakeDate=$this->getOverNightTakeDate();
        $needExceptKey= ['goods_crm_code','scant_id','ratio','image','read_pos_price','content'];
        $commonCondition =$this->getResourceOverNightCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        $userCartArray = CartModel::query()->where(['mid' => $mid,'shop_id'=>$shop_id, 'status' => 0, 'is_selected' => 1,'source'=>2,'group_id'=>0])->get(['goods_id', 'mid', 'goods_num'])->toArray();
        $userCartNumBindGoodsList = array_column($userCartArray,'goods_num', 'goods_id') ?: [];
        $goodsArray=$this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,array_merge($extraCondition,$commonCondition));
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['cart_num'] = $userCartNumBindGoodsList[$goods['id']] ?? 0;
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['goods_pick_time'] = $overNightTakeDate . ' ' . substr($goods['goods_pick_time'], 0, 5);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（次日达商城热门搜索）
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return mixed
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveHotSearchGoodsList(int $shop_id,int $page,int $limit): array
    {
        $needExceptKey= ['goods_crm_code','share_image','logo','group_id','introduction','mini_app_code','goods_spec','price_selling','price_market','number_virtual','number_sales','goods_pick_time','scant_id','ratio','image','number_stock','read_pos_price','content'];
        $where =['store_goods.hot_id'=>self::GOODS_IS_HOT_1,'store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition =$this->getResourceOverNightCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        return $this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,array_merge($extraCondition,$commonCondition));
    }

    /**
     * 业务场景（小程序次日达商品档案搜索服务）
     * @author ran
     * @date 2021-03-12 11:14
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveSearchGoodsList(int $shop_id,int $page,int $limit,string $searchGoodsName){
        $overNightTakeDate=$this->getOverNightTakeDate();
        $whereOp =[['store_goods.title', 'like', "%{$searchGoodsName}%"]];
        $where =['store_goods.status' =>self::GOODS_STATUS_1];
        $needExceptKey= ['goods_crm_code','scant_id','ratio','image','read_pos_price','content'];
        $commonCondition =$this->getResourceOverNightCommonCondition($shop_id);
        $extraCondition=['where'=>$where,'whereOp'=>$whereOp];
        $goodsArray=$this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,array_merge($extraCondition,$commonCondition));
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['goods_pick_time'] = $overNightTakeDate . ' ' . substr($goods['goods_pick_time'], 0, 5);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（次日达商城分类筛选商品列表）
     * @param int $category_id
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveCategoryGoodsList(int $category_id,int $shop_id,int $page,int $limit): array
    {
        $mid=$this->getCurrentInfo()??0;
        $overNightTakeDate=$this->getOverNightTakeDate();
        $needExceptKey= ['kz_goods_id','safety_stock','read_pos_stock','goods_crm_code','scant_id','ratio','image','read_pos_price','content'];
        $where =['store_goods.crd_cate_id'=>$category_id,'store_goods.status' =>self::GOODS_STATUS_1];
        $commonCondition =$this->getResourceOverNightCommonCondition($shop_id);
        $extraCondition=['where'=>$where];
        $userCartArray = CartModel::query()->where(['mid' => $mid,'shop_id'=>$shop_id, 'status' => 0, 'is_selected' => 1,'source'=>2,'group_id'=>0])->get(['goods_id', 'mid', 'goods_num'])->toArray();
        $userCartNumBindGoodsList = array_column($userCartArray,'goods_num', 'goods_id') ?: [];
        $goodsArray=$this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,array_merge($extraCondition,$commonCondition));
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['cart_num'] = $userCartNumBindGoodsList[$goods['id']] ?? 0;
            $goodsArray['list'][$k]['activity_goods_id'] = 0;
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['goods_pick_time'] = $overNightTakeDate . ' ' . substr($goods['goods_pick_time'], 0, 5);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（根据商品ID获取商品档案信息）
     * 根据商品ID获取商品档案信息
     * @param int $id
     * @return mixed
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveGoodsMsg(int $id): array
    {
        $needExceptKey= ['goods_crm_code','read_pos_price','scant_id','ratio'];
        $overNightTakeDate=$this->getOverNightTakeDate();
        $isExist = $this->resourceRedis->exists(self::GOODS_OVER_NIGHT_ARCHIVES_REDIS_KEY . $id);
        if($isExist){
            $goodsArray = unserialize($this->resourceRedis->get(self::GOODS_OVER_NIGHT_ARCHIVES_REDIS_KEY . $id));
        }else{
            $goodsArray=$this->getResourceCommonOverNightArriveGoodsMsg($id, $needExceptKey);
            $goodsArray['image']=explode('|',$goodsArray['image']);
            $goodsArray['number_sales']=$goodsArray['number_sales']+$goodsArray['number_virtual'];
            $goodsArray['goods_pick_time'] =$overNightTakeDate.' '.substr($goodsArray['goods_pick_time'], 0, 5);
            $this->resourceRedis->set(self::GOODS_OVER_NIGHT_ARCHIVES_REDIS_KEY . $id, serialize($goodsArray), self::GOODS_EXPIRE);
        }
        return $goodsArray;
    }



}