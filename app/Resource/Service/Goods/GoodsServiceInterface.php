<?php

declare(strict_types=1);

namespace App\Resource\Service\Goods;


interface GoodsServiceInterface
{
    public function getResourceTimelyArriveGoodsMsg(int $id, int $shop_id);
    public function getResourceOverNightArriveGoodsMsg(int $id);
}