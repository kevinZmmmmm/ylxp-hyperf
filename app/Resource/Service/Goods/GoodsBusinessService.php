<?php


namespace App\Resource\Service\Goods;
use App\Resource\Service\Cart\CartBusinessService;
use Hyperf\Di\Annotation\Inject;

class GoodsBusinessService extends GoodsBaseService implements GoodsServiceInterface
{

    /**
     * @Inject()
     * @var CartBusinessService
     */
    protected CartBusinessService $cartBusinessService;

    /**
     * 业务场景（小程序及时达商城类别ID获取商品列表）
     * type =1 全部 type=2
     * @param int $category_id
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveCategoryGoodsList(int $category_id,int $shop_id,int $page,int $limit,int $type): array
    {
        $needExceptKey= ['safety_stock','read_pos_stock','introduction','crd_shop_ids','status','mini_app_code','group_id','share_image','sort','goods_pick_time','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','kz_goods_id'];
        $where =['store_goods.cate_id'=>$category_id,'store_goods.status'=>self::GOODS_STATUS_1];
        $whereRaw='';
        switch ($type){
            case 1:
                break;
            case 2:
                $whereRaw ='!FIND_IN_SET(' . $shop_id . ',store_goods.shop_ids)';
                break;
        }
        $extraCondition=['where'=>$where,'whereRaw'=>$whereRaw];
        $goodsArray=$this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,$extraCondition);
        $goodsBindShopArray = array_column($goodsArray['list'],'shop_ids','id');
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['valet_cart_number'] = $this->cartBusinessService->selectedGoodsNumber($goods['id'],self::GOODS_VALET_TIMELY_CART);
            $goodsArray['list'][$k]['shop_shelves_status'] = $goodsBindShopArray[$goods['id']]?(in_array($shop_id,explode(',',$goodsBindShopArray[$goods['id']]))?1:0):0;
            unset($goodsArray['list'][$k]['shop_ids']);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（商家端及时达商品档案信息列表）
     *  1 店铺下架 2平台下架
     * @param int $category_id
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveGoodsList(int $shop_id,int $page,int $limit,int $type): array
    {
        $needExceptKey= ['safety_stock','read_pos_stock','introduction','crd_shop_ids','mini_app_code','group_id','share_image','sort','goods_pick_time','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','kz_goods_id'];
        $where=[];
        $whereRaw='';
        switch ($type){
            case 1:
                $where =['store_goods.status'=>self::GOODS_STATUS_1];
                $whereRaw ='FIND_IN_SET(' . $shop_id . ',store_goods.shop_ids)';
                $shelves_status=1;
                break;
            case 2:
                $where =['store_goods.status'=>self::GOODS_STATUS_0];
                $shelves_status=2;
                break;
        }
        $extraCondition=['where'=>$where,'whereRaw'=>$whereRaw];
        $goodsArray=$this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,$extraCondition);
        $goodsBindShopArray = array_column($goodsArray['list'],'shop_ids','id');
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['shelves_status'] = $shelves_status;
            $goodsArray['list'][$k]['shop_shelves_status'] = $goodsBindShopArray[$goods['id']]?(in_array($shop_id,explode(',',$goodsBindShopArray[$goods['id']]))?1:0):0;
            unset($goodsArray['list'][$k]['shop_ids']);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（商家端及时达商品档案搜索服务）
     * @author ran
     * @date 2021-03-12 11:14
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveSearchGoodsList(int $shop_id,int $page,int $limit,string $searchGoodsName){
        $whereOp =[['store_goods.title', 'like', "%{$searchGoodsName}%"]];
        $where =['store_goods.status' =>self::GOODS_STATUS_1];
        $needExceptKey= ['safety_stock','read_pos_stock','introduction','mini_app_code','group_id','share_image','sort','goods_pick_time','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','crd_shop_ids','kz_goods_id'];
        $extraCondition=['where'=>$where,'whereOp'=>$whereOp];
        $goodsArray=$this->getResourceCommonTimelyArriveGoodsList($needExceptKey,$shop_id,$page,$limit,$extraCondition);
        $goodsBindShopArray = array_column($goodsArray['list'],'shop_ids','id');
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['valet_cart_number'] = $this->cartBusinessService->selectedGoodsNumber($goods['id'],self::GOODS_VALET_TIMELY_CART);
            $goodsArray['list'][$k]['shop_shelves_status'] = $goodsBindShopArray[$goods['id']]?(in_array($shop_id,explode(',',$goodsBindShopArray[$goods['id']]))?1:0):0;
            unset($goodsArray['list'][$k]['shop_ids']);
        }
        return $goodsArray;
    }
    /**
     * 业务场景（商家端次日达商城类别ID获取商品列表）
     * type =1 全部 type=2
     * @param int $category_id
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveCategoryGoodsList(int $category_id,int $shop_id,int $page,int $limit,int $type): array
    {
        $overNightTakeDate=$this->getOverNightTakeDate();
        $needExceptKey= ['safety_stock','read_pos_stock','introduction','mini_app_code','group_id','share_image','sort','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','kz_goods_id'];
        $whereRaw='';
        $where =['store_goods.crd_cate_id'=>$category_id,'store_goods.status'=>self::GOODS_STATUS_1];
        switch ($type){
            case 1:
                break;
            case 2:
                $whereRaw ='!FIND_IN_SET(' . $shop_id . ',store_goods.crd_shop_ids)';
                break;
        }
        $extraCondition=['where'=>$where,'whereRaw'=>$whereRaw];
        $goodsArray=$this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,$extraCondition);
        $goodsBindShopArray = array_column($goodsArray['list'],'crd_shop_ids','id');
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['goods_pick_time'] = $overNightTakeDate . ' ' . substr($goods['goods_pick_time'], 0, 5);
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['valet_cart_number'] = $this->cartBusinessService->selectedGoodsNumber($goods['id'],self::GOODS_VALET_OVERNIGHT_CART);
            $goodsArray['list'][$k]['shop_shelves_status'] = $goodsBindShopArray[$goods['id']]?(in_array($shop_id,explode(',',$goodsBindShopArray[$goods['id']]))?1:0):0;
            unset($goodsArray['list'][$k]['crd_shop_ids']);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（商家端次日达商品档案信息列表）
     *  1 店铺下架 2平台下架
     * @param int $category_id
     * @param int $shop_id
     * @param int $page
     * @param int $limit
     * @return array
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveGoodsList(int $shop_id,int $page,int $limit,int $type): array
    {
        $needExceptKey= ['safety_stock','read_pos_stock','introduction','mini_app_code','group_id','share_image','sort','goods_pick_time','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','kz_goods_id'];
        $where=[];
        $whereRaw='';
        switch ($type){
            case 1:
                $where =['store_goods.status'=>self::GOODS_STATUS_1];
                $whereRaw ='FIND_IN_SET(' . $shop_id . ',store_goods.crd_shop_ids)';
                $shelves_status=1;
                break;
            case 2:
                $where =['store_goods.status'=>self::GOODS_STATUS_0];
                $shelves_status=2;
                break;
        }
        $extraCondition=['where'=>$where,'whereRaw'=>$whereRaw];
        $goodsArray=$this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,$extraCondition);
        $goodsBindShopArray = array_column($goodsArray['list'],'crd_shop_ids','id');
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['shelves_status'] = $shelves_status;
            $goodsArray['list'][$k]['shop_shelves_status'] = $goodsBindShopArray[$goods['id']]?(in_array($shop_id,explode(',',$goodsBindShopArray[$goods['id']]))?1:0):0;
        }
        return $goodsArray;
    }

    /**
     * 业务场景（商家次日达商品档案搜索服务）
     * @author ran
     * @date 2021-03-12 11:14
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveSearchGoodsList(int $shop_id,int $page,int $limit,string $searchGoodsName){
        $overNightTakeDate=$this->getOverNightTakeDate();
        $whereOp =[['store_goods.title', 'like', "%{$searchGoodsName}%"]];
        $where =['store_goods.status' =>self::GOODS_STATUS_1];
        $needExceptKey= ['safety_stock','read_pos_stock','introduction','mini_app_code','group_id','share_image','sort','goods_crm_code','scant_id','ratio','image','read_pos_price','content','limite_num_per_day','shop_ids','kz_goods_id'];
        $extraCondition=['where'=>$where,'whereOp'=>$whereOp];
        $goodsArray=$this->getResourceCommonOverNightArriveGoodsList($needExceptKey,$page,$limit,$extraCondition);
        $goodsBindShopArray = array_column($goodsArray['list'],'crd_shop_ids','id');
        foreach ($goodsArray['list'] as $k=>$goods){
            $goodsArray['list'][$k]['goods_pick_time'] = $overNightTakeDate . ' ' . substr($goods['goods_pick_time'], 0, 5);
            $goodsArray['list'][$k]['number_sales'] = $goods['number_sales'] + $goods['number_virtual'];
            $goodsArray['list'][$k]['valet_cart_number'] = $this->cartBusinessService->selectedGoodsNumber($goods['id'],self::GOODS_VALET_OVERNIGHT_CART);
            $goodsArray['list'][$k]['shop_shelves_status'] = $goodsBindShopArray[$goods['id']]?(in_array($shop_id,explode(',',$goodsBindShopArray[$goods['id']]))?1:0):0;
            unset($goodsArray['list'][$k]['crd_shop_ids']);
        }
        return $goodsArray;
    }

    /**
     * 业务场景（及时达商城）
     * 根据商品ID获取商品档案信息服务
     * @param int $id
     * @param int $shop_id
     * @return mixed
     * @author ran
     * @date 2021-03-02 14:59
     * mailbox 466180170@qq.com
     */
    public function getResourceTimelyArriveGoodsMsg(int $id, int $shop_id): array
    {
    }
    /**
     * 业务场景（根据商品ID获取商品档案信息）
     * 根据商品ID获取商品档案信息
     * @param int $id
     * @return mixed
     * @author ran
     * @date 2021-03-02 15:00
     * mailbox 466180170@qq.com
     */
    public function getResourceOverNightArriveGoodsMsg(int $id): array
    {
    }


}