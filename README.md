# Introduction

This is a skeleton application using the Hyperf framework. This application is meant to be used as a starting place for those looking to get their feet wet with Hyperf Framework.

# Requirements

Hyperf has some requirements for the system environment, it can only run under Linux and Mac environment, but due to the development of Docker virtualization technology, Docker for Windows can also be used as the running environment under Windows.

The various versions of Dockerfile have been prepared for you in the [hyperf\hyperf-docker](https://github.com/hyperf/hyperf-docker) project, or directly based on the already built [hyperf\hyperf](https://hub.docker.com/r/hyperf/hyperf) Image to run.

When you don't want to use Docker as the basis for your running environment, you need to make sure that your operating environment meets the following requirements:  

 - PHP >= 7.2
 - Swoole PHP extension >= 4.4，and Disabled `Short Name`
 - OpenSSL PHP extension
 - JSON PHP extension
 - PDO PHP extension （If you need to use MySQL Client）
 - Redis PHP extension （If you need to use Redis Client）
 - Protobuf PHP extension （If you need to use gRPC Server of Client）

# Installation using Composer

The easiest way to create a new Hyperf project is to use Composer. If you don't have it already installed, then please install as per the documentation.

To create your new Hyperf project:

$ composer create-project hyperf/hyperf-skeleton path/to/install

Once installed, you can run the server immediately using the command below.

$ cd path/to/install
$ php bin/hyperf.php start

This will start the cli-server on port `9501`, and bind it to all network interfaces. You can then visit the site at `http://localhost:9501/`

which will bring up Hyperf default home page.

# Windows10 wsl本地开发环境

## WSL安装步骤： https://www.codehui.net/info/81.html  *（本例使用的系统是是Ubuntu20.04 LTS）*

## Ubuntu切换源：https://www.cnblogs.com/zqifa/p/12910989.html
    
## Swoole安装步骤：

 - sudo apt-get install php7.4 php7.4-curl php7.4-gd php7.4-gmp php7.4-json php7.4-mysql php7.4-opcache php7.4-readline php7.4-sqlite3 php7.4-tidy php7.4-xml  php7.4-bcmath php7.4-bz2 php7.4-intl php7.4-mbstring  php7.4-soap php7.4-xsl  php7.4-zip
 - sudo apt-get install php7.4-dev 
 - sudo apt-get install zip
 - sudo pecl install swoole
 - echo 'extension=swoole.so' >> /etc/php/7.4/mods-available/swoole.ini
 - cd /etc/php/7.4/cli/conf.d/ && ln -s ../../mods-available/swoole.ini 20-swoole.ini
 - cd /etc/php/7.4/fpm/conf.d/ && ln -s ../../mods-available/swoole.ini 20-swoole.ini *（没有安装fpm不需要执行）*
 - sudo apt-get install php7.4-redis
 - echo 'extension=redis.so' >> /etc/php/7.4/mods-available/redis.ini
 - cd /etc/php/7.4/cli/conf.d/ && ln -s ../../mods-available/redis.ini 20-redis.ini
 - /etc/init.d/apache2 restart
 - php.ini中关闭 swoole.use_shortname = 'off'

## Composer切换阿里云源：
- composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/


## Php 相关查看命令：
 - php --ri redis
 - php --ri swoole
 - php --ini

## WSL代码目录
 - cd /mnt;

# Hyperf架构

## Hyperf热更新：
 - cd hyperf项目目录
 - 第一种：
    wget -O watch https://gitee.com/hanicc/hyperf-watch/raw/master/watch
    php watch（替代php bin/hyperf.php start）
 - 第二种：
    php bin/hyperf.php server:watch（建议使用）

## 目录结构

 - User 用户模块（会员管理，商户管理）
 - Order 订单模块 （订单管理） 
 - Activity 活动模块（活动管理）
 - Resource 资源模块（商品管理，店铺管理，平台信息管理）
 - Thrid 第三方模块 （三方商品管理）
 - Common 公共模块  （错误处理，数据格式处理）

## 编写规则

 - 每个模块都独立，原则：模块可在不依赖其他模块的情况下单独运行
 - 接口采用restful接口规范，定义接口时，尽量统一：例如user：/v1/user,/v1/user/login；order：/v1/order
 - 模块间接口权限验证可使用jwt
 - 模块间通信可使用rabbitMQ（Hyperf amqp），rpc（Hyperf json rpc）
 - 所有耗时操作均可放到task任务中（task已实现异步协程化）

## 迭代阶段
 - 第一阶段：以实现功能为主，首先应实现模块化独立，模块之间可以使用依赖注入
 - 第二阶段：以代码优化为主, 实现模块之间代码独立，取消模块间依赖注入，使用rpc，或者消息队列通信
 - 第三阶段：以数据库优化为主，实现数据库独立，redis集群化部署，使用docker，k8s进行微服务部署

## 接口规范
 - GET /zoos：列出所有动物园
 - POST /zoos：新建一个动物园
 - GET /zoos/ID：获取某个指定动物园的信息
 - PUT /zoos/ID：更新某个指定动物园的信息（提供该动物园的全部信息）
 - PATCH /zoos/ID：更新某个指定动物园的信息（提供该动物园的部分信息）
 - DELETE /zoos/ID：删除某个动物园
 - GET /zoos/ID/animals：列出某个指定动物园的所有动物
 - DELETE /zoos/ID/animals/ID：删除某个指定动物园的指定动物
